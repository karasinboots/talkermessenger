package org.matrix.androidsdk.rest.model;

public class TranslationMessage extends Message {

    public boolean fromImGroup;
    public String textOriginal;
    public String textTranslated;
    public String codeOriginal;
    public String codeTranslated;

    public TranslationMessage() {
        msgtype = MSGTYPE_TRANSLATION;
    }

    /**
     * Make a deep copy
     *
     * @return the copy
     */
    public TranslationMessage deepCopy() {
        TranslationMessage copy = new TranslationMessage();
        copy.fromImGroup = fromImGroup;
        copy.msgtype = msgtype;
        copy.textOriginal = textOriginal;
        copy.codeOriginal = codeOriginal;
        copy.textTranslated = textTranslated;
        copy.codeTranslated = codeTranslated;
        return copy;
    }
}
