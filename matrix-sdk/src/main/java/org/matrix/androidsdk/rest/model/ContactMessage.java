/*
 * Copyright 2014 OpenMarket Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.matrix.androidsdk.rest.model;

public class ContactMessage extends Message {
    public String displayName;
    public String phoneNumber;
    public String email;
    public String matrixId;

    public ContactMessage() {
        msgtype = MSGTYPE_CONTACT;
    }

    /**
     * Make a deep copy
     *
     * @return the copy
     */
    public ContactMessage deepCopy() {
        ContactMessage copy = new ContactMessage();
        copy.msgtype = msgtype;
        copy.body = body;
        copy.displayName = displayName;
        copy.phoneNumber = phoneNumber;
        copy.email = email;
        copy.matrixId = matrixId;
        return copy;
    }
}