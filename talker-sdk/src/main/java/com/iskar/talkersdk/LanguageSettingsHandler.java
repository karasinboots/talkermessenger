package com.iskar.talkersdk;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.iskar.talkersdk.model.Lang;
import com.iskar.talkersdk.model.Langs;
import com.iskar.talkersdk.model.UserLanguages;
import com.iskar.talkersdk.rest.client.LanguageClient;
import com.iskar.talkersdk.ui.DividerItemDecoration;
import com.iskar.talkersdk.ui.LanguageSelectorDialog;
import com.iskar.talkersdk.ui.languageRecycler.LanguageSelectorAdapter;
import com.iskar.talkersdk.ui.languageRecycler.Languages;
import com.iskar.talkersdk.ui.languageRecycler.RecyclerOnClick;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by karasinboots on 03.01.2018.
 */

public class LanguageSettingsHandler implements RecyclerOnClick {
    private Activity activity;
    private String userToken;
    private String userId;
    private String myUid;
    private RecyclerView languagesRecycler;
    private ArrayList<Languages> lList = new ArrayList<>();
    private Langs talkerLanguageList;
    private RecyclerOnClick recyclerOnClick = this;
    private LanguageSelectorAdapter languageSelectorAdapter;
    private ArrayList<Languages> languagesList = new ArrayList<>();
    private int textColor;
    private int backgroundColor;
    private boolean inMyProfie;
    private RelativeLayout pendingView;

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    public void setUserToken(String userToken) {
        this.userToken = userToken;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setTextColor(int textColor) {
        this.textColor = textColor;
    }

    public void setBackgroundColor(int backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    public Langs getTalkerLanguageList() {
        return talkerLanguageList;
    }

    public void init() {
        Realm.init(activity);
        configureRealm();
        languagesRecycler = activity.findViewById(R.id.languagesList);
        pendingView = activity.findViewById(R.id.pendingView);
        LinearLayoutManager llm = new LinearLayoutManager(activity);
        languagesRecycler.setLayoutManager(llm);
        languagesRecycler.addItemDecoration(new DividerItemDecoration(activity, R.drawable.divider, 0));
        lList = getLanguages(activity, Locale.getDefault().getLanguage());
        inMyProfie = userId != null && myUid != null && userId.equals(myUid);
        getUserLanguages();
    }


    private void showLanguageSelectionDialog(ArrayList<Languages> languages) {
        LanguageSelectorDialog languageSelectorDialog = new LanguageSelectorDialog(activity, userToken, userId, textColor, backgroundColor, languages, true);
        languageSelectorDialog.setTalkerLanguageList(talkerLanguageList);
        languageSelectorDialog.show();
        languageSelectorDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                getUserLanguages();
            }
        });
    }

    private void setLanguages() {
        List<String> list = new ArrayList<>();
        Realm realm = Realm.getDefaultInstance();
        if (realm.where(UserLanguages.class).equalTo("userId", userId).findFirst() != null
                && realm.where(UserLanguages.class).equalTo("userId", userId).findFirst().getLangs().size() > 0) {
            for (String l : realm.where(UserLanguages.class).equalTo("userId", userId).findFirst().getLangs()) {
                if (l != null)
                    for (Languages language : lList) {
                        if (l.equals(language.getLanguageCode())) {
                            languagesList.add(new Languages(language.getLanguageName(), language.getLanguageCode(), language.getFlagCode()));
                            list.add(language.getLanguageCode());
                        }
                    }
            }
            talkerLanguageList = new Langs(list);
        }
        if (talkerLanguageList == null
                || talkerLanguageList.getLangs() == null
                || talkerLanguageList.getLangs().size() == 0)
            activity.findViewById(R.id.languages_view).setVisibility(View.GONE);
        else activity.findViewById(R.id.languages_view).setVisibility(View.VISIBLE);
    }

    private void getUserLanguages() {
        languagesList.clear();
        setLanguages();
        Realm realm = Realm.getDefaultInstance();
        languageSelectorAdapter = new LanguageSelectorAdapter(languagesList, recyclerOnClick, textColor, inMyProfie, false, false);
        languagesRecycler.setAdapter(languageSelectorAdapter);
        LanguageClient.getLanguageApi().
                getUserLanguages(userId, getCredentials()).enqueue(new Callback<UserLanguages>() {
            @Override
            public void onResponse(Call call, Response response) {
                if (response.code() == 200 && response.body() != null) {
                    realm.beginTransaction();
                    realm.copyToRealmOrUpdate(new UserLanguages(userId, ((UserLanguages) response.body()).getLangs()));
                    realm.commitTransaction();
                    languagesList.clear();
                    setLanguages();
                    languageSelectorAdapter = new LanguageSelectorAdapter(languagesList, recyclerOnClick, textColor, inMyProfie, false, false);
                    languagesRecycler.setAdapter(languageSelectorAdapter);
                    hidePendingView();
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                t.printStackTrace();
                hidePendingView();
            }
        });
    }

    private Map<String, String> getCredentials() {
        Map<String, String> params = new HashMap<String, String>();
        params.put("token", userToken);
        return params;
    }

    public void updateLanguageList() {
        getUserLanguages();
    }

    private ArrayList<Languages> getLanguages(Context context, String locale) {
        ArrayList<Languages> lList = new ArrayList<>();
        String filename = locale + ".xml";
        try {
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setValidating(false);
            XmlPullParser myxml = factory.newPullParser();

            InputStream raw = context.getAssets().open(filename);
            myxml.setInput(raw, null);

            int eventType = myxml.getEventType();
            while (eventType != XmlPullParser.END_DOCUMENT) {

                String name = "";
                String code = "";

                if (eventType == XmlPullParser.START_TAG
                        && myxml.getName().equals("key"))
                //        && myxml.getText().equals("name")
                {
                    eventType = myxml.next();
                    // Log.d("---", " 1 ="+myxml.getName());
                    eventType = myxml.next();
                    // Log.d("---", " 2 ="+myxml.getName());
                    eventType = myxml.next();
                    // Log.d("---", " 3 ="+myxml.getName());
                    eventType = myxml.next();
                    // Log.d("---", " 4 ="+myxml.getName());
                    eventType = myxml.next();
                    // Log.d("---", " 5 ="+myxml.getName());
                    name = myxml.getText();
                    // Log.d("---", "name ="+name);
                    eventType = myxml.next();
                    //Log.d("---", " 6 ="+myxml.getName());
                    eventType = myxml.next();
                    // Log.d("---", " 7 ="+myxml.getName());
                    eventType = myxml.next();
                    // Log.d("---", " 8 ="+myxml.getName());
                    eventType = myxml.next();
                    //  Log.d("---", " 9 ="+myxml.getName());
                    eventType = myxml.next();
                    //  Log.d("---", " 10 ="+myxml.getName());
                    eventType = myxml.next();
                    // Log.d("---", " 11 ="+myxml.getName());
                    eventType = myxml.next();
                    // Log.d("---", " 12 ="+myxml.getName());
                    eventType = myxml.next();
                    //  Log.d("---", " 13 ="+myxml.getName());
                    code = myxml.getText();
                    // Log.d("---", " code= "+code);
                    lList.add(new Languages(name, code));


                }
                eventType = myxml.next();
            }
        } catch (Exception e) {
            Log.d("---", "Excepetion : " + e.getMessage());
        }
        Collections.sort(lList, new Comparator<Languages>() {
            @Override
            public int compare(Languages o1, Languages o2) {
                return o1.getLanguageName().compareTo(o2.getLanguageName());
            }
        });

        return lList;
    }

    private void showPendingView() {
        pendingView.setVisibility(View.VISIBLE);
    }

    private void hidePendingView() {
        pendingView.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onClick(Languages language, int position) {
        Log.e("onlanguage", "onclick");
        if (position != 0)
            changeUserMainLanguage(talkerLanguageList, language.getLanguageCode());
    }

    @Override
    public void onFavoriteClick(String code, boolean isFavorite) {
        Log.e("onlanguage", "onfavoriteclick");
    }

    @Override
    public void onDeleteClick(Languages languages, int position) {
        Log.e("onlanguage", "ondeleteclick");
        if (userId.equals(myUid) && talkerLanguageList != null && talkerLanguageList.getLangs().size() > 1) {
            deleteUserLanguage(new Lang(languages.getLanguageCode()));
        } else Toast.makeText(activity, R.string.cant_delete_language, Toast.LENGTH_SHORT).show();
    }

    private void deleteUserLanguage(Lang code) {
        new AlertDialog.Builder(activity).setMessage(R.string.delete_language).setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                showPendingView();
                LanguageClient.getLanguageApi().deleteUserLanguage(userId, getCredentials(), code).enqueue(new Callback<Void>() {
                    @Override
                    public void onResponse(Call<Void> call, Response<Void> response) {
                        getUserLanguages();
                        hidePendingView();
                    }

                    @Override
                    public void onFailure(Call<Void> call, Throwable t) {
                        getUserLanguages();
                        hidePendingView();
                    }
                });
            }
        }).setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).create().show();

    }

    private void changeUserMainLanguage(Langs languages, String newMainCode) {
        showPendingView();
        List<Integer> indexesForDeleting = new ArrayList<>();
        for (int i = 0; i < languages.getLangs().size(); i++) {
            if (languages.getLangs().get(i).equals(newMainCode)) {
                indexesForDeleting.add(i);
            }
        }
        for (int i : indexesForDeleting) {
            languages.getLangs().remove(i);
        }
        languages.getLangs().add(0, newMainCode);
        LanguageClient.getLanguageApi().editLanguages(userId, getCredentials(), languages).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                getUserLanguages();
                hidePendingView();
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                getUserLanguages();
                hidePendingView();
            }
        });
    }

    private void configureRealm() {
        Realm.init(activity);
        RealmConfiguration config = new RealmConfiguration
                .Builder().deleteRealmIfMigrationNeeded().name("libraryRealm")
                .modules(new com.iskar.talkersdk.realm.RealmModule())
                .build();
        Realm.setDefaultConfiguration(config);
    }

    public String getMyUid() {
        return myUid;
    }

    public void setMyUid(String myUid) {
        this.myUid = myUid;
    }
}
