package com.iskar.talkersdk.rest.api;

import com.iskar.talkersdk.model.BlockUserId;
import com.iskar.talkersdk.model.Blocked;
import com.iskar.talkersdk.model.Status;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;

/**
 * Created by karasinboots on 15.03.2018.
 */

public interface UserBlockApi {

    @POST("/user/{userId}/block")
    Call<Void> blockUser(@Path("userId") String userId, @QueryMap Map<String, String> params, @Body BlockUserId blockUserId);

    @POST("/user/{userId}/unblock")
    Call<Void> unblockUser(@Path("userId") String userId, @QueryMap Map<String, String> params, @Body BlockUserId blockUserId);

    @GET("/user/{userId}/blocked")
    Call<List<Blocked>> getBlockedUsers(@Path("userId") String userId, @QueryMap Map<String, String> params);

    /**GET Part (Params):
    * token: string
    * uid: string
    *build: string*/
    @Headers("Content-Type: application/json")
    @GET("/user/{userId}/blocked/{build}")
    Call<Status> checkIfBlocked(@Path("userId") String userId, @Path("build") String build, @QueryMap Map<String, String> params);
}
