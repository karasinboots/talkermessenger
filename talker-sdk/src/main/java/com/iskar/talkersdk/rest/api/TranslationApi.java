package com.iskar.talkersdk.rest.api;

import com.iskar.talkersdk.model.Statistics;
import com.iskar.talkersdk.model.StatisticsPost;
import com.iskar.talkersdk.model.Translation;
import com.iskar.talkersdk.model.TranslationParameters;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.QueryMap;
import retrofit2.http.Url;

/**
 * Created by karasinboots on 14.03.2018.
 */

public interface TranslationApi {
    @POST
    Call<Translation> getTranslation(@Url String url, @QueryMap Map<String, String> params, @Body TranslationParameters parameters);

    @POST
    Call<Statistics> getStatistics(@Url String url, @QueryMap Map<String, String> params, @Body StatisticsPost statisticsPost);
}
