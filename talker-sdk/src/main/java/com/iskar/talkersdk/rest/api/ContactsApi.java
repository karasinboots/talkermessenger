package com.iskar.talkersdk.rest.api;

import com.iskar.talkersdk.model.Contact;
import com.iskar.talkersdk.model.ContactInfo;
import com.iskar.talkersdk.model.Phones;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;

/**
 * Created by karasinboots on 14.03.2018.
 */

public interface ContactsApi {
    @POST("/user/{userId}/contacts")
    Call<List<ContactInfo>> postContactList(@Path("userId") String userId, @QueryMap Map<String, String> params, @Body Phones phones);

    @GET("/user/{userId}/contacts")
    Call<List<Contact>> getContacts(@Path("userId") String userId, @QueryMap Map<String, String> params);
}
