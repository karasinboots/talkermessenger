package com.iskar.talkersdk.rest.api;

import com.iskar.talkersdk.model.Fields;
import com.iskar.talkersdk.model.UserData;
import com.iskar.talkersdk.model.UserDataFields;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;

/**
 * Created by karasinboots on 14.03.2018.
 */

public interface PrivateDataApi {
    @GET("/user/{userId}/private")
    Call<UserData> getPrivateData(@Path("userId") String userId);

    @POST("/user/{userId}/private")
    Call<Void> setPrivateFields(@Path("userId") String userId, @QueryMap Map<String, String> params, @Body UserDataFields fields);

    @HTTP(method = "DELETE", path = "/user/{userId}/private", hasBody = true)
    Call<Void> deletePrivateFields(@Path("userId") String userId, @QueryMap Map<String, String> params, @Body Fields fields);
}
