package com.iskar.talkersdk.callback;

import com.iskar.talkersdk.model.Translation;

/**
 * Created by karasinboots on 16.03.2018.
 */

public interface TranslationInterface {
    void onTranslate(Translation translationResponse);

    void onFailure(String msg);
}
