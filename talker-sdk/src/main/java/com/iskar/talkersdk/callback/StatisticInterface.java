package com.iskar.talkersdk.callback;

import com.iskar.talkersdk.model.Statistics;

/**
 * Created by karasinboots on 18.03.2018.
 */

public interface StatisticInterface {
    void onStatisticReceived(Statistics statistics);

    void onFailure(String msg);
}
