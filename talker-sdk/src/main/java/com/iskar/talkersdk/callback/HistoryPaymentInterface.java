package com.iskar.talkersdk.callback;

import com.iskar.talkersdk.model.PaymentHistory;

import java.util.List;

public interface HistoryPaymentInterface {
    void onSuccess(List<PaymentHistory> history);

    void onFailure(String message);
}
