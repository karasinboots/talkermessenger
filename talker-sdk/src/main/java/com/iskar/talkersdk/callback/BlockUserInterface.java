package com.iskar.talkersdk.callback;

import com.iskar.talkersdk.model.Blocked;
import com.iskar.talkersdk.model.Status;

import java.util.List;

public interface BlockUserInterface {
    interface GetBlockUserInterface {
        void blockedUsersReceived(List<Blocked> blockedList);

        void onFailure(String message);
    }

    interface BlockingUser {
        void onSuccess();

        void onFailure(String message);
    }

    interface CheckBlockingUser {
        void statusReceived(Status status);

        void onFailure(String message);
    }
}
