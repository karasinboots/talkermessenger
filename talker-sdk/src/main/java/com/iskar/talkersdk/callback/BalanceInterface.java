package com.iskar.talkersdk.callback;

/**
 * Created by karasinboots on 25.03.2018.
 */

public interface BalanceInterface {
    void onBalanceRecieved(Long balance);

    void onFailure(String message);
}
