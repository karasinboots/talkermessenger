package com.iskar.talkersdk.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

@RealmClass
public class TranslationHistoryItem extends RealmObject {
    @PrimaryKey
    long id;
    String originalText;
    String originalLanguageCode;
    String translatedLanguageCode;
    String translatedText;
    String originalLangugeName;
    String translatedLanguageName;

    public TranslationHistoryItem() {
    }

    public TranslationHistoryItem(String originalText, String originalLanguageCode, String translatedText, String translatedLanguageCode, String originalLangugeName, String translatedLanguageName) {
        this.id = System.currentTimeMillis();
        this.originalText = originalText;
        this.originalLanguageCode = originalLanguageCode;
        this.translatedText = translatedText;
        this.translatedLanguageCode = translatedLanguageCode;
        this.originalLangugeName = originalLangugeName;
        this.translatedLanguageName = translatedLanguageName;
    }

    public String getOriginalLangugeName() {
        return originalLangugeName;
    }

    public void setOriginalLangugeName(String originalLangugeName) {
        this.originalLangugeName = originalLangugeName;
    }

    public String getTranslatedLanguageName() {
        return translatedLanguageName;
    }

    public void setTranslatedLanguageName(String translatedLanguageName) {
        this.translatedLanguageName = translatedLanguageName;
    }

    public String getOriginalText() {
        return originalText;
    }

    public void setOriginalText(String originalText) {
        this.originalText = originalText;
    }

    public String getOriginalLanguageCode() {
        return originalLanguageCode;
    }

    public void setOriginalLanguageCode(String originalLanguageCode) {
        this.originalLanguageCode = originalLanguageCode;
    }

    public String getTranslatedText() {
        return translatedText;
    }

    public void setTranslatedText(String translatedText) {
        this.translatedText = translatedText;
    }

    public String getTranslatedLanguageCode() {
        return translatedLanguageCode;
    }

    public void setTranslatedLanguageCode(String translatedLanguageCode) {
        this.translatedLanguageCode = translatedLanguageCode;
    }
}
