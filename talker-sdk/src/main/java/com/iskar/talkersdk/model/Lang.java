package com.iskar.talkersdk.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by karasinboots on 31.01.2018.
 */

public class Lang implements Serializable {

    @SerializedName("lang")
    @Expose
    private String lang;

    public Lang() {
    }

    public Lang(String lang) {
        this.lang = lang;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

}