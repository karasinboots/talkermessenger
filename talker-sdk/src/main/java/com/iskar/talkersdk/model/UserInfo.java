package com.iskar.talkersdk.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

@RealmClass
public class UserInfo extends RealmObject implements Serializable {

    @SerializedName("user_info")
    @Expose
    private String userInfo;

    @PrimaryKey
    @SerializedName("user_id")
    @Expose
    private String userId;

    public UserInfo() {
    }

    public UserInfo(String userInfo, String userId) {
        this.userInfo = userInfo;
        this.userId = userId;
    }

    public String getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(String userInfo) {
        this.userInfo = userInfo;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

}