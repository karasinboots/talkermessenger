package com.iskar.talkersdk.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Stat implements Serializable {

    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("num_req")
    @Expose
    private Long numReq;
    @SerializedName("num_char")
    @Expose
    private Long numChar;
    @SerializedName("lang")
    @Expose
    private String lang;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Long getNumReq() {
        return numReq;
    }

    public void setNumReq(Long numReq) {
        this.numReq = numReq;
    }

    public Long getNumChar() {
        return numChar;
    }

    public void setNumChar(Long numChar) {
        this.numChar = numChar;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

}