package com.iskar.talkersdk.model;

/**
 * Created by karasinboots on 03.01.2018.
 */

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Membership implements Parcelable
{

    @SerializedName("chunk")
    @Expose
    private List<Chunk> chunk = null;
    public final static Parcelable.Creator<Membership> CREATOR = new Creator<Membership>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Membership createFromParcel(Parcel in) {
            return new Membership(in);
        }

        public Membership[] newArray(int size) {
            return (new Membership[size]);
        }

    }
            ;

    protected Membership(Parcel in) {
        in.readList(this.chunk, (Chunk.class.getClassLoader()));
    }

    /**
     * No args constructor for use in serialization
     *
     */
    public Membership() {
    }

    /**
     *
     * @param chunk
     */
    public Membership(List<Chunk> chunk) {
        super();
        this.chunk = chunk;
    }

    public List<Chunk> getChunk() {
        return chunk;
    }

    public void setChunk(List<Chunk> chunk) {
        this.chunk = chunk;
    }

    public Membership withChunk(List<Chunk> chunk) {
        this.chunk = chunk;
        return this;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(chunk);
    }

    public int describeContents() {
        return 0;
    }

}