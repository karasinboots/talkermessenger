package com.iskar.talkersdk.model;

import java.io.Serializable;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserDataFields implements Serializable {

    @SerializedName("fields")
    @Expose
    private List<Field> fields = null;

    public List<Field> getFields() {
        return fields;
    }

    public void setFields(List<Field> fields) {
        this.fields = fields;
    }

}