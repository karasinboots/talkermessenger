package com.iskar.talkersdk.model;

/**
 * Created by karasinboots on 14.03.2018.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Balance implements Serializable {

    @SerializedName("balance")
    @Expose
    private Long balance;
    @SerializedName("error")
    @Expose
    private Long error;
    @SerializedName("raw_error")
    @Expose
    private String rawError;

    public Long getError() {
        return error;
    }

    public void setError(Long error) {
        this.error = error;
    }

    public String getRawError() {
        return rawError;
    }

    public void setRawError(String rawError) {
        this.rawError = rawError;
    }

    public Long getBalance() {
        return balance;
    }

    public void setBalance(Long balance) {
        this.balance = balance;
    }

}