package com.iskar.talkersdk.model;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.iskar.talkersdk.ui.languageRecycler.Languages;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

@RealmClass
public class Info extends RealmObject implements Serializable
{

    @SerializedName("is_blocked")
    @Expose
    private Boolean isBlocked;
    @SerializedName("languages")
    @Expose
    private UserLanguages languages;
    @SerializedName("matrix_id")
    @Expose
    private String matrixId;
    @PrimaryKey
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("public_fields")
    @Expose
    private RealmList<UserData> publicFields = null;

    public Boolean getIsBlocked() {
        return isBlocked;
    }

    public void setIsBlocked(Boolean isBlocked) {
        this.isBlocked = isBlocked;
    }

    public UserLanguages getLanguages() {
        return languages;
    }

    public void setLanguages(UserLanguages languages) {
        this.languages = languages;
    }

    public String getMatrixId() {
        return matrixId;
    }

    public void setMatrixId(String matrixId) {
        this.matrixId = matrixId;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public List<UserData> getPublicFields() {
        return publicFields;
    }

    public void setPublicFields(RealmList<UserData> publicFields) {
        this.publicFields = publicFields;
    }

}