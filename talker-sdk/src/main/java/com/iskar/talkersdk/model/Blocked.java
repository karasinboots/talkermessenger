package com.iskar.talkersdk.model;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Blocked implements Serializable
{

    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("blocked_at")
    @Expose
    private String blockedAt;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getBlockedAt() {
        return blockedAt;
    }

    public void setBlockedAt(String blockedAt) {
        this.blockedAt = blockedAt;
    }

}