package com.iskar.talkersdk.model;

/**
 * Created by karasinboots on 03.01.2018.
 */

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Content implements Parcelable
{

    @SerializedName("avatar_url")
    @Expose
    private String avatarUrl;
    @SerializedName("membership")
    @Expose
    private String membership;
    @SerializedName("displayname")
    @Expose
    private String displayname;
    public final static Parcelable.Creator<Content> CREATOR = new Creator<Content>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Content createFromParcel(Parcel in) {
            return new Content(in);
        }

        public Content[] newArray(int size) {
            return (new Content[size]);
        }

    }
            ;

    protected Content(Parcel in) {
        this.avatarUrl = ((String) in.readValue((String.class.getClassLoader())));
        this.membership = ((String) in.readValue((String.class.getClassLoader())));
        this.displayname = ((String) in.readValue((String.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     *
     */
    public Content() {
    }

    /**
     *
     * @param membership
     * @param avatarUrl
     * @param displayname
     */
    public Content(String avatarUrl, String membership, String displayname) {
        super();
        this.avatarUrl = avatarUrl;
        this.membership = membership;
        this.displayname = displayname;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public Content withAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
        return this;
    }

    public String getMembership() {
        return membership;
    }

    public void setMembership(String membership) {
        this.membership = membership;
    }

    public Content withMembership(String membership) {
        this.membership = membership;
        return this;
    }

    public String getDisplayname() {
        return displayname;
    }

    public void setDisplayname(String displayname) {
        this.displayname = displayname;
    }

    public Content withDisplayname(String displayname) {
        this.displayname = displayname;
        return this;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(avatarUrl);
        dest.writeValue(membership);
        dest.writeValue(displayname);
    }

    public int describeContents() {
        return 0;
    }

}