package com.iskar.talkersdk.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

@RealmClass
public class UserLanguages extends RealmObject {

    @PrimaryKey
    private String userId;

    @SerializedName("langs")
    @Expose
    private RealmList<String> langs = null;

    /**
     * No args constructor for use in serialization
     */
    public UserLanguages() {
    }

    /**
     * @param langs
     */
    public UserLanguages(String userId, RealmList<String> langs) {
        super();
        this.userId = userId;
        this.langs = langs;
    }


    public RealmList<String> getLangs() {
        return langs;
    }

    public void setLangs(RealmList<String> langs) {
        this.langs = langs;
    }

    public UserLanguages withLangs(RealmList<String> langs) {
        this.langs = langs;
        return this;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}