package com.iskar.talkersdk.model;

/**
 * Created by karasinboots on 03.01.2018.
 */

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Chunk implements Parcelable {

    @SerializedName("age")
    @Expose
    private Integer age;
    @SerializedName("content")
    @Expose
    private Content content;
    @SerializedName("event_id")
    @Expose
    private String eventId;
    @SerializedName("membership")
    @Expose
    private String membership;
    @SerializedName("origin_server_ts")
    @Expose
    private Integer originServerTs;
    @SerializedName("room_id")
    @Expose
    private String roomId;
    @SerializedName("sender")
    @Expose
    private String sender;
    @SerializedName("state_key")
    @Expose
    private String stateKey;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("replaces_state")
    @Expose
    private String replacesState;
    public final static Parcelable.Creator<Chunk> CREATOR = new Creator<Chunk>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Chunk createFromParcel(Parcel in) {
            return new Chunk(in);
        }

        public Chunk[] newArray(int size) {
            return (new Chunk[size]);
        }

    };

    protected Chunk(Parcel in) {
        this.age = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.content = ((Content) in.readValue((Content.class.getClassLoader())));
        this.eventId = ((String) in.readValue((String.class.getClassLoader())));
        this.membership = ((String) in.readValue((String.class.getClassLoader())));
        this.originServerTs = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.roomId = ((String) in.readValue((String.class.getClassLoader())));
        this.sender = ((String) in.readValue((String.class.getClassLoader())));
        this.stateKey = ((String) in.readValue((String.class.getClassLoader())));
        this.type = ((String) in.readValue((String.class.getClassLoader())));
        this.replacesState = ((String) in.readValue((String.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     */
    public Chunk() {
    }

    /**
     * @param sender
     * @param content
     * @param eventId
     * @param membership
     * @param stateKey
     * @param age
     * @param roomId
     * @param replacesState
     * @param type
     * @param originServerTs
     */
    public Chunk(Integer age, Content content, String eventId, String membership, Integer originServerTs, String roomId, String sender, String stateKey, String type, String replacesState) {
        super();
        this.age = age;
        this.content = content;
        this.eventId = eventId;
        this.membership = membership;
        this.originServerTs = originServerTs;
        this.roomId = roomId;
        this.sender = sender;
        this.stateKey = stateKey;
        this.type = type;
        this.replacesState = replacesState;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Chunk withAge(Integer age) {
        this.age = age;
        return this;
    }

    public Content getContent() {
        return content;
    }

    public void setContent(Content content) {
        this.content = content;
    }

    public Chunk withContent(Content content) {
        this.content = content;
        return this;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public Chunk withEventId(String eventId) {
        this.eventId = eventId;
        return this;
    }

    public String getMembership() {
        return membership;
    }

    public void setMembership(String membership) {
        this.membership = membership;
    }

    public Chunk withMembership(String membership) {
        this.membership = membership;
        return this;
    }

    public Integer getOriginServerTs() {
        return originServerTs;
    }

    public void setOriginServerTs(Integer originServerTs) {
        this.originServerTs = originServerTs;
    }

    public Chunk withOriginServerTs(Integer originServerTs) {
        this.originServerTs = originServerTs;
        return this;
    }

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public Chunk withRoomId(String roomId) {
        this.roomId = roomId;
        return this;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public Chunk withSender(String sender) {
        this.sender = sender;
        return this;
    }

    public String getStateKey() {
        return stateKey;
    }

    public void setStateKey(String stateKey) {
        this.stateKey = stateKey;
    }

    public Chunk withStateKey(String stateKey) {
        this.stateKey = stateKey;
        return this;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Chunk withType(String type) {
        this.type = type;
        return this;
    }

    public String getReplacesState() {
        return replacesState;
    }

    public void setReplacesState(String replacesState) {
        this.replacesState = replacesState;
    }

    public Chunk withReplacesState(String replacesState) {
        this.replacesState = replacesState;
        return this;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(age);
        dest.writeValue(content);
        dest.writeValue(eventId);
        dest.writeValue(membership);
        dest.writeValue(originServerTs);
        dest.writeValue(roomId);
        dest.writeValue(sender);
        dest.writeValue(stateKey);
        dest.writeValue(type);
        dest.writeValue(replacesState);
    }

    public int describeContents() {
        return 0;
    }

}