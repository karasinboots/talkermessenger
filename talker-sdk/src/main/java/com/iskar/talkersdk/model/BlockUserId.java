package com.iskar.talkersdk.model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class BlockUserId implements Serializable {

    @SerializedName("user_id")
    @Expose
    private String userId;

    public BlockUserId(String userId) {
        this.userId = userId;
    }

    public BlockUserId() {
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

}