package com.iskar.talkersdk.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Members implements Parcelable
{

    @SerializedName("application/json")
    @Expose
    private com.iskar.talkersdk.model.Membership Membership;
    public final static Parcelable.Creator<Members> CREATOR = new Creator<Members>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Members createFromParcel(Parcel in) {
            return new Members(in);
        }

        public Members[] newArray(int size) {
            return (new Members[size]);
        }

    }
            ;

    protected Members(Parcel in) {
        this.Membership = ((Membership) in.readValue((Membership.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     *
     */
    public Members() {
    }

    /**
     *
     * @param Membership
     */
    public Members(Membership Membership) {
        super();
        this.Membership = Membership;
    }

    public Membership getMembership() {
        return Membership;
    }

    public void setMembership(Membership Membership) {
        this.Membership = Membership;
    }

    public Members withMembership(Membership Membership) {
        this.Membership = Membership;
        return this;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(Membership);
    }

    public int describeContents() {
        return 0;
    }

}