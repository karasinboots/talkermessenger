package com.iskar.talkersdk.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Phones implements Serializable {

    @SerializedName("phones")
    @Expose
    private List<String> phones = null;

    public Phones() {
    }

    public Phones(List<String> phones) {

        this.phones = phones;
    }

    public List<String> getPhones() {
        return phones;
    }

    public void setPhones(List<String> phones) {
        this.phones = phones;
    }

}