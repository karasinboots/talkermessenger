package com.iskar.talkersdk;

import android.content.Context;

import com.iskar.talkersdk.callback.PublicFieldInterface;
import com.iskar.talkersdk.model.Phone;
import com.iskar.talkersdk.model.UserPhone;
import com.iskar.talkersdk.rest.client.PublicDataClient;

import java.util.HashMap;
import java.util.Map;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserPublicFieldsHandler {
    private String token;
    private Context context;

    public UserPublicFieldsHandler(String token, Context context) {
        this.token = token;
        this.context = context;
    }

    public String getUserNumber(String userId, PublicFieldInterface publicFieldInterface) {
        configureRealm(context);
        Realm realm = Realm.getDefaultInstance();
        if (realm.where(UserPhone.class).equalTo("userId", userId).findFirst() != null) {
            return realm.where(UserPhone.class).equalTo("userId", userId).findFirst().getPhone();
        } else
            PublicDataClient.getPublicDataApi().getPhone(userId, getCredentials()).enqueue(new Callback<Phone>() {
                @Override
                public void onResponse(Call<Phone> call, Response<Phone> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        if (response.body().getPhone() != null) {
                            UserPhone userPhone = new UserPhone(userId, response.body().getPhone());
                            Realm realm = Realm.getDefaultInstance();
                            realm.beginTransaction();
                            realm.copyToRealmOrUpdate(userPhone);
                            realm.commitTransaction();
                            publicFieldInterface.onNumberReceived(response.body().getPhone());
                        }
                    }
                }

                @Override
                public void onFailure(Call<Phone> call, Throwable t) {
                }
            });
        return null;
    }

    private void configureRealm(Context context) {
        Realm.init(context);
        RealmConfiguration config = new RealmConfiguration
                .Builder().deleteRealmIfMigrationNeeded().name("libraryRealm")
                .modules(new com.iskar.talkersdk.realm.RealmModule())
                .build();
        Realm.setDefaultConfiguration(config);
    }

    private Map<String, String> getCredentials() {
        Map<String, String> params = new HashMap<String, String>();
        params.put("token", token);
        return params;
    }
}
