package com.iskar.talkersdk.constants;

/**
 * Created by karasinboots on 14.03.2018.
 */

public class ApiConsts {
    public static String CONTENT_TYPE = "application/json";
    public static String HEADER_FIELD = "Content-Type";
    public static String TRANSLATION_URL = "http://translate.talkermessenger.com:8000/translate/";
    public static String STATISTICS_URL = "http://translate.talkermessenger.com:8000/stat/";
}
