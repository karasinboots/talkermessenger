package com.iskar.talkersdk.realm.wrappers;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import com.iskar.talkersdk.model.TargetLanguage;

import java.io.IOException;

import io.realm.RealmList;

/**
 * Created by karasinboots on 09.01.2018.
 */

public class RealmLanguagesListTypeAdapter extends TypeAdapter<RealmList<TargetLanguage>> {
    public static final TypeAdapter<RealmList<TargetLanguage>> INSTANCE =
            new RealmLanguagesListTypeAdapter().nullSafe();

    private RealmLanguagesListTypeAdapter() {
    }

    @Override
    public void write(JsonWriter out, RealmList<TargetLanguage> src) throws IOException {
        out.beginArray();
        for (TargetLanguage realmString : src) {
            out.value(realmString.getLanguage());
        }
        out.endArray();
    }

    @Override
    public RealmList<TargetLanguage> read(JsonReader in) throws IOException {
        RealmList<TargetLanguage> realmStrings = new RealmList<>();
        in.beginArray();
        while (in.hasNext()) {
            if (in.peek() == JsonToken.NULL) {
                in.nextNull();
            } else {
                String realmString = in.nextString();
                realmStrings.add(new TargetLanguage(realmString));
            }
        }
        in.endArray();
        return realmStrings;
    }

}