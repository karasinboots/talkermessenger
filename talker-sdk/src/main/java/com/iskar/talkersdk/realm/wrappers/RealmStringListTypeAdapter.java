package com.iskar.talkersdk.realm.wrappers;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;

import io.realm.RealmList;

/**
 * Created by karasinboots on 27.12.2017.
 */

public class RealmStringListTypeAdapter extends TypeAdapter<RealmList<String>> {
    public static final TypeAdapter<RealmList<String>> INSTANCE =
            new RealmStringListTypeAdapter().nullSafe();

    private RealmStringListTypeAdapter() {
    }

    @Override
    public void write(JsonWriter out, RealmList<String> src) throws IOException {
        out.beginArray();
        for (String realmString : src) {
            out.value(realmString);
        }
        out.endArray();
    }

    @Override
    public RealmList<String> read(JsonReader in) throws IOException {
        RealmList<String> realmStrings = new RealmList<>();
        in.beginArray();
        while (in.hasNext()) {
            if (in.peek() == JsonToken.NULL) {
                in.nextNull();
            } else {
                String realmString = in.nextString();
                realmStrings.add(realmString);
            }
        }
        in.endArray();
        return realmStrings;
    }

}