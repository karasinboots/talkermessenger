package com.iskar.talkersdk.realm.wrappers;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import com.iskar.talkersdk.model.UserData;

import java.io.IOException;

import io.realm.RealmList;

/**
 * Created by karasinboots on 09.01.2018.
 */

public class RealmUserDataListTypeAdapter extends TypeAdapter<RealmList<UserData>> {
    public static final TypeAdapter<RealmList<UserData>> INSTANCE =
            new RealmUserDataListTypeAdapter().nullSafe();

    private RealmUserDataListTypeAdapter() {
    }

    @Override
    public void write(JsonWriter out, RealmList<UserData> src) throws IOException {
        out.beginArray();
        for (UserData userData : src) {
            out.value(userData.getValue());
            out.value(userData.getId());
            out.value(userData.getKey());
            out.value(userData.getMatrixId());
        }
        out.endArray();
    }

    @Override
    public RealmList<UserData> read(JsonReader in) throws IOException {
        RealmList<UserData> realmData = new RealmList<>();
        in.beginArray();
        Long id = 0L;
        String key = "";
        String matrixId = "";
        String value = "";
        while (in.hasNext()) {
            if (in.peek() == JsonToken.NULL) {
                in.nextNull();
            } else {
                String name = in.nextName();
                switch (name) {
                    case "id":
                        id = in.nextLong();
                        break;
                    case "key":
                        key = in.nextString();
                        break;
                    case "matrix_id":
                        matrixId = in.nextString();
                        break;
                    case "value":
                        value = in.nextString();
                        break;
                    default:
                        in.skipValue();
                        break;
                }
            }
            realmData.add(new UserData(id, key, matrixId, value));
        }
        in.endArray();
        return realmData;
    }

}