package com.iskar.talkersdk;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import com.iskar.talkersdk.callback.TranslationInterface;
import com.iskar.talkersdk.model.Translation;
import com.iskar.talkersdk.model.TranslationParameters;
import com.iskar.talkersdk.model.UserInfo;
import com.iskar.talkersdk.rest.client.TranslationClient;

import java.util.HashMap;
import java.util.Map;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.iskar.talkersdk.constants.ApiConsts.TRANSLATION_URL;

public class FullscreenTranslationHandler {
    private Context context;
    private String sourceLanguageCode;
    private String targetLanguageCode;
    private String sourceText;
    private String userId;
    private String token;
    private TranslationInterface translationInterface = null;
    private String userInfo;
    private Boolean doubleTranslation;

    public FullscreenTranslationHandler(Context context, String userId, String userInfo, String token, boolean doubleTranslation) {
        this.context = context;
        this.userId = userId;
        this.userInfo = userInfo;
        this.token = token;
        this.doubleTranslation = doubleTranslation;
        configureRealm();
    }

    public void configureRealm() {
        Realm.init(context);
        RealmConfiguration config = new RealmConfiguration
                .Builder().deleteRealmIfMigrationNeeded().name("libraryRealm")
                .modules(new com.iskar.talkersdk.realm.RealmModule())
                .build();
        Realm.setDefaultConfiguration(config);
    }

    public void makeTranslation(String sourceLanguageCode, String targetLanguageCode, String sourceText, TranslationInterface translationInterface) {
        Realm realm = Realm.getDefaultInstance();
        RealmList<String> mainLanguages = new RealmList<>();
        if (targetLanguageCode != null)
            mainLanguages.add(targetLanguageCode);
        if (realm.where(Translation.class).equalTo("sourceText", sourceText)
                .equalTo("targetLangs.language", targetLanguageCode).findFirst() != null) {
            translationInterface.onTranslate(realm.where(Translation.class).equalTo("sourceText", sourceText)
                    .equalTo("targetLangs.language", targetLanguageCode).findFirst());
        } else
            TranslationClient.getTranslationApi().getTranslation(TRANSLATION_URL + userId, getCredentials(),
                    new TranslationParameters(new UserInfo(userInfo, userId),
                            doubleTranslation, 0, sourceLanguageCode, sourceText, mainLanguages, true)).enqueue(new Callback<Translation>() {
                @Override
                public void onResponse(@NonNull Call<Translation> call, @NonNull Response<Translation> response) {
                    Log.e("onresponse", response.toString());
                    if (response.isSuccessful() && response.body() != null) {
                        Translation translationBody = response.body();
                        Realm realm = Realm.getDefaultInstance();
                        realm.beginTransaction();
                        realm.copyToRealmOrUpdate(translationBody);
                        realm.commitTransaction();
                        translationInterface.onTranslate(translationBody);
                    } else if (response.code() == 402)
                        translationInterface.onFailure("payment");
                }

                @Override
                public void onFailure(Call<Translation> call, Throwable t) {
                    t.printStackTrace();
                    translationInterface.onFailure(t.getMessage());

                }
            });
    }

    private Map<String, String> getCredentials() {
        Map<String, String> params = new HashMap<String, String>();
        params.put("token", token);
        return params;
    }
}
