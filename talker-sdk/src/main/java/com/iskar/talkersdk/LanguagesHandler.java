package com.iskar.talkersdk;

import android.content.Context;

import com.iskar.talkersdk.callback.LanguagesInterface;
import com.iskar.talkersdk.model.UserLanguages;
import com.iskar.talkersdk.rest.client.LanguageClient;

import java.util.HashMap;
import java.util.Map;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LanguagesHandler {

    private Context context;
    private String userId;
    private String userToken;
    private LanguagesInterface languagesInterface = null;

    public LanguagesHandler(Context context, String userId, String userToken, LanguagesInterface languagesInterface) {
        this.context = context;
        this.userId = userId;
        this.userToken = userToken;
        this.languagesInterface = languagesInterface;
    }

    public LanguagesHandler(Context context) {
        this.context = context;
    }

    public LanguagesHandler(Context context, String userId, String userToken) {
        this.context = context;
        this.userId = userId;
        this.userToken = userToken;
    }

    public LanguagesHandler(String userToken) {
        this.userToken = userToken;
    }

    public UserLanguages getMyLanguages(String myId) {
        configureRealm();
        Realm realm = Realm.getDefaultInstance();
        if (realm.where(UserLanguages.class).equalTo("userId", myId).findFirst() != null) {
            return realm.copyFromRealm(realm.where(UserLanguages.class).equalTo("userId", myId).findFirst());
        }
        return null;
    }

    public String getMainUserLanguage() {
        configureRealm();
        Realm realm = Realm.getDefaultInstance();
        LanguageClient.getLanguageApi().
                getUserLanguages(userId, getCredentials()).enqueue(new Callback<UserLanguages>() {
            @Override
            public void onResponse(Call call, Response response) {
                if (response.isSuccessful() && response.body() != null) {
                    realm.beginTransaction();
                    realm.copyToRealmOrUpdate(new UserLanguages(userId, ((UserLanguages) response.body()).getLangs()));
                    realm.commitTransaction();
                    if (realm.where(UserLanguages.class).equalTo("userId", userId).findFirst() != null
                            && realm.where(UserLanguages.class)
                            .equalTo("userId", userId).findFirst().getLangs().size() > 0) {
                        languagesInterface.onMainLanguageReceived(realm.where(UserLanguages.class)
                                .equalTo("userId", userId).findFirst().getLangs().get(0));
                    }
                    languagesInterface.onLanguagesReceived((UserLanguages) response.body());
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                languagesInterface.onFailure(t.getMessage());
            }
        });
        if (realm.where(UserLanguages.class).equalTo("userId", userId).findFirst() != null
                && realm.where(UserLanguages.class)
                .equalTo("userId", userId).findFirst().getLangs().size() > 0) {
            return realm.where(UserLanguages.class)
                    .equalTo("userId", userId).findFirst().getLangs().get(0);
        }
        return "";
    }

    private void configureRealm() {
        Realm.init(context);
        RealmConfiguration config = new RealmConfiguration
                .Builder().deleteRealmIfMigrationNeeded().name("libraryRealm")
                .modules(new com.iskar.talkersdk.realm.RealmModule())
                .build();
        Realm.setDefaultConfiguration(config);
    }

    private Map<String, String> getCredentials() {
        Map<String, String> params = new HashMap<String, String>();
        params.put("token", userToken);
        return params;
    }

}
