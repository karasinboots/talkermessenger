package com.iskar.talkersdk.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;

import com.iskar.talkersdk.R;

public class LanguageSettingsView extends LinearLayout {

    public LanguageSettingsView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initView();
    }

    public LanguageSettingsView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    private void initView() {
        View view = inflate(getContext(), R.layout.language_settings_layout, null);
        addView(view);
    }
}

