package com.iskar.talkersdk.ui.languageRecycler;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by karasinboots on 03.01.2018.
 */

public class Languages {
    private List<Languages> list = new ArrayList<>();
    private String languageName;
    private String languageCode;
    private String flagCode;

    public String getLanguageName() {
        return languageName;
    }

    public String getFlagCode() {
        return flagCode;
    }

    public void setFlagCode(String flagCode) {
        this.flagCode = flagCode;
    }

    public void setLanguageName(String languageName) {
        this.languageName = languageName;
    }

    public String getLanguageCode() {
        return languageCode;
    }

    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }

    public List<Languages> getList() {
        return list;
    }

    public Languages(String name, String code) {
        this.languageName = name;
        this.languageCode = code;

    }
    public Languages(String name, String code, String flagCode) {
        this.languageName = name;
        this.languageCode = code;
        this.flagCode = flagCode;
    }
}
