package com.iskar.talkersdk.ui.languageRecycler;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.iskar.talkersdk.R;

/**
 * Created by karasinboots on 03.01.2018.
 */

public class LanguageSelectorViewHolder extends RecyclerView.ViewHolder {

    TextView language;
    ImageView flag;
    RelativeLayout parent;
    ImageView favorite;

    public LanguageSelectorViewHolder(View itemView) {
        super(itemView);
        language = itemView.findViewById(R.id.language);
        flag = itemView.findViewById(R.id.flag);
        parent = itemView.findViewById(R.id.parentItemLayout);
        favorite = itemView.findViewById(R.id.favorite);
    }
}
