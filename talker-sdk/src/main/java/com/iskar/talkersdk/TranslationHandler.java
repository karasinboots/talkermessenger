package com.iskar.talkersdk;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.iskar.talkersdk.callback.TranslationInterface;
import com.iskar.talkersdk.model.Translation;
import com.iskar.talkersdk.model.TranslationParameters;
import com.iskar.talkersdk.model.UserInfo;
import com.iskar.talkersdk.model.UserLanguages;
import com.iskar.talkersdk.rest.client.LanguageClient;
import com.iskar.talkersdk.rest.client.TranslationClient;
import com.jakewharton.rxbinding2.widget.RxTextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.iskar.talkersdk.constants.ApiConsts.TRANSLATION_URL;

/**
 * Created by karasinboots on 27.12.2017.
 */
@SuppressWarnings("unchecked")
public class TranslationHandler {
    //external timeout
    private int timeout;

    //external context
    private Activity activity;
    private List<String> memberIds;

    //external views
    private EditText inputField;
    private boolean isFlexibleEnabled = true;
    //external UserInfo
    private String userInfo;
    private Integer userIp;
    private String myUserId;
    private String userToken;

    //external Translation
    private String sourceLang;

    //internal fields
    private CardView cardView;
    private Translation translationResponse;
    private Translation previousTranslationResponse;
    private TranslationInterface translationInterface = null;

    public TranslationHandler(int timeout, Activity activity, List<String> memberIds, EditText inputField, String userInfo, Integer userIp, String myUserId, String userToken) {
        this.timeout = timeout;
        this.activity = activity;
        this.memberIds = memberIds;
        this.inputField = inputField;
        this.userInfo = userInfo;
        this.userIp = userIp;
        this.myUserId = myUserId;
        this.userToken = userToken;
    }

    public TranslationHandler(int timeout, Activity activity, EditText inputField, String userInfo, Integer userIp, String myUserId, String userToken) {
        this.timeout = timeout;
        this.activity = activity;
        this.inputField = inputField;
        this.userInfo = userInfo;
        this.userIp = userIp;
        this.myUserId = myUserId;
        this.userToken = userToken;
    }

    public TranslationHandler(int timeout, Activity activity, String userInfo, Integer userIp, String myUserId, String userToken) {
        this.timeout = timeout;
        this.activity = activity;
        this.userInfo = userInfo;
        this.userIp = userIp;
        this.myUserId = myUserId;
        this.userToken = userToken;
    }

    public TranslationHandler(Activity activity) {
        this.activity = activity;
    }

    public Translation getTranslationResponse() {
        return translationResponse;
    }

    public void setTranslationResponse(Translation translationResponse) {
        this.translationResponse = translationResponse;
    }

    public void setFlexibleEnabled(boolean flexibleEnabled) {
        isFlexibleEnabled = flexibleEnabled;
    }

    private void configureRealm() {
        Realm.init(activity);
        RealmConfiguration config = new RealmConfiguration
                .Builder().deleteRealmIfMigrationNeeded().name("libraryRealm")
                .modules(new com.iskar.talkersdk.realm.RealmModule())
                .build();
        Realm.setDefaultConfiguration(config);
    }

    public void cleanTransaltionCache() {
        configureRealm();
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        realm.delete(Translation.class);
        realm.commitTransaction();
    }

    public void setTranslationListener(TranslationInterface translationInterface) {
        this.translationInterface = translationInterface;
        configureRealm();
        getUserLanguages();
    }

    public void setImGroupTranslationListener(String myLanguage, TranslationInterface translationInterface) {
        this.translationInterface = translationInterface;
        configureRealm();
        getUserLanguages(myLanguage);
    }

    public Translation checkIfAlreadyTranslated(String myLanguage, String text) {
        configureRealm();
        RealmList<String> targetLanguages = new RealmList<>();
        if (myLanguage != null)
            targetLanguages.add(myLanguage);
        Realm realm = Realm.getDefaultInstance();
        if (realm.where(Translation.class).equalTo("sourceText", text).equalTo("sourceLang", "eo")
                .equalTo("targetLangs.language", targetLanguages.get(0)).findFirst() != null) {
            Log.e("TRANSLATION_HANDLER", "translation exist " + realm.where(Translation.class).equalTo("sourceText", text).equalTo("sourceLang", "eo")
                    .equalTo("targetLangs.language", targetLanguages.get(0)).findFirst().getSourceText());
            return realm.where(Translation.class).equalTo("sourceText", text).equalTo("sourceLang", "eo")
                    .equalTo("targetLangs.language", targetLanguages.get(0)).findFirst();
        } else return null;
    }

    public void setImGroupIncomingTranslationListener(String myLanguage, String text, TranslationInterface translationInterface) {
        configureRealm();
        getUserLanguagesForImIncoming(myLanguage, text, translationInterface);
    }


    @SuppressLint("CheckResult")
    private void registerObservable(RealmList<String> realmTargetLangs, String myMain) {
        if (timeout <= 0) timeout = 1;
        cardView = activity.findViewById(R.id.translationCardView);
        RxTextView.textChanges(inputField).debounce(timeout, TimeUnit.SECONDS, AndroidSchedulers.mainThread())
                .subscribe(text -> {
                    if (text.length() == 0 || text.toString().replaceAll("\\s+", "").length() == 0) {
                        translationResponse = null;
                        cardView.setVisibility(GONE);
                        return;
                    }
                    if (isFlexibleEnabled) {
                        try {
                            text = text.toString().replaceAll("\".*?\"", "");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    Realm realm = Realm.getDefaultInstance();
                    if (realmTargetLangs.size() > 0 && myMain != null)
                        if (realm.where(Translation.class).equalTo("sourceText", text.toString()).equalTo("sourceLang", myMain)
                                .equalTo("targetLangs.language", realmTargetLangs.get(0)).findFirst() != null) {
                            translationResponse = realm.where(Translation.class).equalTo("sourceText", text.toString()).equalTo("sourceLang", myMain)
                                    .equalTo("targetLangs.language", realmTargetLangs.get(0)).findFirst();
                            updateViews();
                        } else {
                            if (realm.where(UserLanguages.class).equalTo("userId", myUserId).findFirst() != null)
                                sourceLang = realm.where(UserLanguages.class).equalTo("userId", myUserId).findFirst().getLangs().get(0);
                            TranslationClient.getTranslationApi().getTranslation(TRANSLATION_URL + myUserId, getCredentials(),
                                    new TranslationParameters(new UserInfo(userInfo, myUserId),
                                            true, 0, sourceLang, text.toString(), realmTargetLangs, true)).enqueue(new Callback<Translation>() {
                                @Override
                                public void onResponse(@NonNull Call<Translation> call, @NonNull Response<Translation> response) {
                                    if (response.isSuccessful() && response.body() != null) {
                                        Translation translationBody = response.body();
                                        translationResponse = response.body();
                                        Realm realm = Realm.getDefaultInstance();
                                        realm.beginTransaction();
                                        realm.copyToRealmOrUpdate(translationBody);
                                        realm.commitTransaction();
                                        updateViews();
                                    } else if (response.code() == 402)
                                        translationInterface.onFailure("payment");
                                }

                                @Override
                                public void onFailure(Call<Translation> call, Throwable t) {
                                    t.printStackTrace();
                                }
                            });
                        }
                });
    }

    private void updateViews() {
        TextView translationTextWindow = activity.findViewById(R.id.translationTextWindow);
        ImageView leftArrow = activity.findViewById(R.id.arrow_left);
        ImageView rightArrow = activity.findViewById(R.id.arrow_right);
        TextView languageCode = activity.findViewById(R.id.languageCode);
        translationTextWindow.setText(translationResponse.getDualTranslation().get(0));
        if (isDualTranslationCorrect()) {
            rightArrow.setVisibility(View.GONE);
        } else rightArrow.setVisibility(View.VISIBLE);
        cardView.setVisibility(View.VISIBLE);
        setOnclickListeners();
    }

    private boolean isDualTranslationCorrect() {
        return translationResponse.getDualTranslation().get(0) != null &&
                translationResponse.getDualTranslation().get(0).toLowerCase().replaceAll("\\s+", "")
                        .equals(inputField.getText().toString().toLowerCase().replaceAll("\\s+", ""));
    }

    private boolean translatedText = false;

    private void setOnclickListeners() {
        TextView translationTextWindow = activity.findViewById(R.id.translationTextWindow);
        ImageView leftArrow = activity.findViewById(R.id.arrow_left);
        ImageView rightArrow = activity.findViewById(R.id.arrow_right);
        TextView languageCode = activity.findViewById(R.id.languageCode);
        leftArrow.setOnClickListener(v -> {
            if (previousTranslationResponse != null)
                translationResponse = previousTranslationResponse;
            translationTextWindow.setText(translationResponse.getDualTranslation().get(0));
            inputField.setText(translationResponse.getSourceText());
            leftArrow.setVisibility(GONE);
            previousTranslationResponse = null;
        });
        rightArrow.setOnClickListener(v -> {
            previousTranslationResponse = translationResponse;
            leftArrow.setVisibility(View.VISIBLE);
            rightArrow.setVisibility(View.INVISIBLE);
            inputField.setText(translationResponse.getDualTranslation().get(0));
        });
        languageCode.setOnClickListener(v -> {
            if (!translatedText) {
                translationTextWindow.setText(translationResponse.getTranslation().get(0));
                leftArrow.setVisibility(GONE);
                rightArrow.setVisibility(GONE);
                languageCode.setText(translationResponse.getTargetLangs().get(0).getLanguage());
                translatedText = true;
            } else {
                translationTextWindow.setText(translationResponse.getDualTranslation().get(0));
                languageCode.setText(translationResponse.getSourceLang());
                if (previousTranslationResponse != null)
                    leftArrow.setVisibility(VISIBLE);
                if (!isDualTranslationCorrect()) {
                    rightArrow.setVisibility(View.VISIBLE);
                }
                translatedText = false;
            }
        });
    }

    private Map<String, String> getCredentials() {
        Map<String, String> params = new HashMap<String, String>();
        params.put("token", userToken);
        return params;
    }

    private void getUserLanguages() {
        LanguageClient.getLanguageApi().getUserLanguages(myUserId, getCredentials()).enqueue(new Callback<UserLanguages>() {
            @Override
            public void onResponse(Call call, Response response) {
                if (response.isSuccessful() && response.body() != null) {
                    Realm realm = Realm.getDefaultInstance();
                    realm.beginTransaction();
                    UserLanguages userLanguages = ((UserLanguages) response.body());
                    userLanguages.setUserId(myUserId);
                    realm.copyToRealmOrUpdate(userLanguages);
                    realm.commitTransaction();
                }
                getMembersUserLanguage(memberIds);
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                // Log.e("error",t.toString()+ t.getMessage());
                t.printStackTrace();
            }
        });
    }

    private void getUserLanguages(String myLanguage) {
        RealmList<String> targetLanguages = new RealmList<>();
        if (myLanguage != null) {
            targetLanguages.add("eo");
            registerObservable(targetLanguages, myLanguage);
        }
    }

    private void getUserLanguagesForImIncoming(String myLanguage, String text, TranslationInterface translationInterface) {
        RealmList<String> targetLanguages = new RealmList<>();
        if (myLanguage != null) {
            targetLanguages.add(myLanguage);
            makeImIncomingTranslation(targetLanguages, text, translationInterface);
        }
    }

    private void makeImIncomingTranslation(RealmList<String> realmTargetLangs, String text, TranslationInterface translationInterface) {
        Realm realm = Realm.getDefaultInstance();
        if (realmTargetLangs.size() > 0)
            if (realm.where(Translation.class).equalTo("sourceText", text).equalTo("sourceLang", "eo")
                    .equalTo("targetLangs.language", realmTargetLangs.get(0)).findFirst() != null) {
                translationInterface.onTranslate(realm.where(Translation.class).equalTo("sourceText", text).equalTo("sourceLang", "eo")
                        .equalTo("targetLangs.language", realmTargetLangs.get(0)).findFirst());
            } else {
                if (realm.where(UserLanguages.class).equalTo("userId", myUserId).findFirst() != null)
                    TranslationClient.getTranslationApi().getTranslation(TRANSLATION_URL + myUserId, getCredentials(),
                            new TranslationParameters(new UserInfo(userInfo, myUserId),
                                    true, 0, "eo", text, realmTargetLangs, true)).enqueue(new Callback<Translation>() {
                        @Override
                        public void onResponse(@NonNull Call<Translation> call, @NonNull Response<Translation> response) {
                            if (response.isSuccessful() && response.body() != null) {
                                Translation translationBody = response.body();
                                Realm realm = Realm.getDefaultInstance();
                                realm.beginTransaction();
                                realm.copyToRealmOrUpdate(translationBody);
                                realm.commitTransaction();
                                translationInterface.onTranslate(translationBody);
                            } else if (response.code() == 402)
                                translationInterface.onFailure("payment");
                        }

                        @Override
                        public void onFailure(Call<Translation> call, Throwable t) {
                            t.printStackTrace();
                        }
                    });
            }
    }

    private void getMembersUserLanguage(List<String> memberIds) {
        Realm realm = Realm.getDefaultInstance();
        if (memberIds.size() == 1)
            LanguageClient.getLanguageApi().getUserLanguages(memberIds.get(0), getCredentials()).enqueue(new Callback<UserLanguages>() {
                @Override
                public void onResponse(Call call, Response response) {
                    if (response.isSuccessful() && response.body() != null) {
                        realm.beginTransaction();
                        UserLanguages userLanguages = ((UserLanguages) response.body());
                        userLanguages.setUserId(memberIds.get(0));
                        realm.copyToRealmOrUpdate(userLanguages);
                        realm.commitTransaction();
                        if (userLanguages.getLangs() != null && userLanguages.getLangs().size() > 0) {
                            List<String> myLangs = new ArrayList<>(realm.where(UserLanguages.class).equalTo("userId", myUserId).findFirst().getLangs());
                            int sizeBefore = myLangs.size();
                            List<String> userLangs = new ArrayList<>(realm.where(UserLanguages.class).equalTo("userId", memberIds.get(0)).findFirst().getLangs());
                            myLangs.retainAll(userLangs);
                            if (myLangs.size() != 0 && myLangs.size() != sizeBefore) {
                                return;
                            }
                        }
                        if (realm.where(UserLanguages.class).equalTo("userId", myUserId) == null)
                            return;
                        if (realm.where(UserLanguages.class).equalTo("userId", myUserId).findFirst() != null &&
                                realm.where(UserLanguages.class).equalTo("userId", myUserId).findFirst().getLangs().size() > 0) {
                            String myLang = realm.where(UserLanguages.class).equalTo("userId", myUserId).findFirst().getLangs().get(0);
                            if (myLang == null)
                                myLang = realm.where(UserLanguages.class).equalTo("userId", myUserId).findFirst().getLangs().get(0);
                            RealmList<String> targetLanguages = new RealmList<>();
                            if (userLanguages.getLangs().size() > 0)
                                if (userLanguages.getLangs().get(0) != null) {
                                    targetLanguages.add(userLanguages.getLangs().get(0));
                                    registerObservable(targetLanguages, myLang);
                                }
                        }
                    }
                }

                @Override
                public void onFailure(Call call, Throwable t) {
                    Log.e("response error", "" + t.getMessage());
                }
            });
    }

}
