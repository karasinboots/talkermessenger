package com.talkermessenger.purchase;

/**
 * Created by djnig on 3/2/2018.
 */

public class Purchase {

    public static final String APP_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAqVZzSR1burz6fl" +
            "v1AkXxJrWzAm3AweHy0MzD/IvBA6Wp0lBjloePjtuEqOtZGf0qw3mW/Uz1DJq2m5aOR+bS0G9UBg/qJzsf8yrW4HYp" +
            "7pLUj89vI8h0lg6sG/BpR9EmTD3tKakIkvuv/l6VIy8NCCsa5H3MUBcE/Nu8zxO4HhUZLZAgEjcOAOivis/NQdpkjVRoi" +
            "kvqAG4b8u5Rv4+R/QmRCmn8nRAYyGSblIP+MiaB3q8dVJiAo+l+fW/XYvTLs5HU0wlPS+GpZDWq9lC2Gyeyb+nM9i9dwVvW" +
            "CDw5KIueI81d0SM7OTrYWk9P9mN+EkdSV1Jdcr6JjTIi6aVHUQIDAQAB";

    public static final String APP_KEY_TEST = null;

    public static final String TEST_SKU = "android.test.purchased";
    public static final String SYMBOLS_20_000_SKU = "world.talker.characters.buy1";
    public static final String SYMBOLS_40_000_SKU = "world.talker.characters.buy2";
    public static final String SYMBOLS_100_000_SKU = "world.talker.characters.buy5";
    public static final String SYMBOLS_250_000_SKU = "world.talker.characters.buy10";
    public static final String SYMBOLS_500_000_SKU = "world.talker.characters.buy20";
}
