package com.talkermessenger.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.talkermessenger.R;
import com.talkermessenger.activity.MXCActionBarActivity;
import com.talkermessenger.activity.settings.NewSettingActivity;
import com.talkermessenger.adapters.CallLogAdapter;
import com.talkermessenger.util.PreferencesManager;
import com.talkermessenger.util.ThemeUtils;
import com.talkermessenger.view.EmptyViewItemDecoration;
import com.talkermessenger.view.SimpleDividerItemDecoration;

import org.matrix.androidsdk.MXDataHandler;
import org.matrix.androidsdk.MXSession;
import org.matrix.androidsdk.adapters.MessageRow;
import org.matrix.androidsdk.data.Room;
import org.matrix.androidsdk.data.RoomSummary;
import org.matrix.androidsdk.data.RoomTag;
import org.matrix.androidsdk.data.store.IMXStore;
import org.matrix.androidsdk.rest.model.Event;
import org.matrix.androidsdk.rest.model.RoomMember;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

/**
 * Created by karasinboots on 22.01.2018.
 */

public class CallLogFragment extends Fragment {
    MXSession mSession;
    CallLogAdapter callLogAdapter;

    public MXSession getmSession() {
        return mSession;
    }

    public void setmSession(MXSession mSession) {
        this.mSession = mSession;
    }

    public CallLogAdapter getCallLogAdapter() {
        return callLogAdapter;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_call_log, null);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setHasOptionsMenu(true);
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recyclerview);
        if (mSession != null)
            callLogAdapter = new CallLogAdapter(getContext(), fillCallList(), mSession);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        int margin = (int) getResources().getDimension(R.dimen.call_item_decoration_left_margin);
        recyclerView.addItemDecoration(new SimpleDividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL, margin));
        recyclerView.addItemDecoration(new EmptyViewItemDecoration(getActivity(), DividerItemDecoration.VERTICAL, 40, 16, 14));
        recyclerView.setAdapter(callLogAdapter);
    }

    public void refreshCalls() {
        List<MessageRow> calls = fillCallList();
        if (calls != null) {
            callLogAdapter.setCalls(calls);
            callLogAdapter.notifyDataSetChanged();
        }
    }

    public List<MessageRow> fillCallList() {
        ArrayList<String> roomIds = getRoomList();
        List<MessageRow> messages = new ArrayList<>();
        for (String roomId : roomIds) {
            MessageRow messageRow = null;
            Room room = mSession.getDataHandler().getRoom(roomId);
            ArrayList<Event> events;
            if (room.getAllRoomMessages() != null && room.getAllRoomMessages().size() > 0)
                events = new ArrayList<>(room.getAllRoomMessages());
            else events = null;
            if (events != null && events.size() > 0)
                for (int i = 0, eventsSize = events.size(); i < eventsSize; i++) {
                    Event event = events.get(i);
                    if (event.originServerTs > PreferencesManager.getCallLogClearTime(getContext()))
                        if (event.isCallEvent() && !event.isHidden()) {
                            if (event.getSender().equals(mSession.getMyUserId())) {
                                for (RoomMember roomMember : mSession.getDataHandler().getRoom(event.roomId).getMembers()) {
                                    String userId = roomMember.getUserId();
                                    if (!userId.equals(mSession.getMyUserId())) {
                                        event.setCustomSender(userId);
                                        event.setReceiver(userId);
                                    }
                                }
                                event.setIncoming(false);
                            } else {
                                event.setReceiver("");
                                event.setIncoming(true);
                            }
                            try {
                                if ((event.getType().equals(Event.EVENT_TYPE_CALL_HANGUP) && event.isIncoming())
                                        && !events.get(i - 1).getType().equals(Event.EVENT_TYPE_CALL_ANSWER)) {
                                    event.setMissed(true);
                                } else event.setMissed(false);
                            } catch (Exception e) {
                                event.setMissed(false);
                            }
                            if (mSession.getDataHandler().getRoom(event.roomId).getMembers().size() > 2)
                                event.setReceiver(Event.EVENT_RECEIVER_CONFERENCE);
                            messageRow = new MessageRow(event, room.getState());
                            messageRow.setRoomId(roomId);
                        }
                }
            if (messageRow != null)
                messages.add(messageRow);
        }

        try {
            Collections.sort(messages, (m1, m2) -> {
                long diff = m1.getEvent().getOriginServerTs() - m2.getEvent().getOriginServerTs();
                return (diff < 0) ? +1 : (diff > 0 ? -1 : 0);
            });
        } catch (Exception e) {
            //   Log.e(LOG_TAG, "## notifyDataSetChanged () : failed to sort events " + e.getMessage());
        }
        return messages;
    }

    private ArrayList<String> getRoomList() {
        ArrayList<String> roomList = new ArrayList<>();
        final MXDataHandler dataHandler = mSession.getDataHandler();
        final IMXStore store = dataHandler.getStore();
        List<RoomSummary> roomSummaries = new ArrayList<>(store.getSummaries());
        HashSet<String> lowPriorityRoomIds = new HashSet<>(mSession.roomIdsWithTag(RoomTag.ROOM_TAG_LOW_PRIORITY));
        HashSet<String> archived = new HashSet<>(mSession.roomIdsWithTag(RoomTag.ROOM_TAG_ARCHIVED));
        HashSet<String> deleted = new HashSet<>(mSession.roomIdsWithTag(RoomTag.ROOM_TAG_DELETED));
        for (RoomSummary summary : roomSummaries) {
            if (!summary.isInvited()) {
                Room room = store.getRoom(summary.getRoomId());
                if ((null != room) && // if the room still exists
                        !room.isConferenceUserRoom() && // not a VOIP conference room
                        !lowPriorityRoomIds.contains(room.getRoomId()) &&
                        !archived.contains(room.getRoomId()) &&
                        !deleted.contains(room.getRoomId())) {
                    roomList.add(room.getRoomId());
                }
            }
        }
        return roomList;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_call_log, menu);
        MenuItem mSearchMenuItem = menu.findItem(R.id.action_search);
        if (ThemeUtils.getApplicationTheme(getContext()).equals(ThemeUtils.THEME_LIGHT_LIGHT_BLUE_HEADER_VALUE)) {
            mSearchMenuItem.setIcon(getResources().getDrawable(R.drawable.ic_material_search_white));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.ic_action_clear:
                clearLog();
                break;
            case R.id.ic_action_settings:
                final Intent settingsIntent = new Intent(getContext(), NewSettingActivity.class);
                settingsIntent.putExtra(MXCActionBarActivity.EXTRA_MATRIX_ID, mSession.getCredentials().userId);
                getContext().startActivity(settingsIntent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void clearLog() {
        PreferencesManager.setCallLogClearTime(getContext(), System.currentTimeMillis());
        refreshCalls();
    }
}
