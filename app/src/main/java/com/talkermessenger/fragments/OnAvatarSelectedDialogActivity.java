package com.talkermessenger.fragments;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.talkermessenger.Matrix;
import com.talkermessenger.R;
import com.talkermessenger.activity.CommonActivityUtils;
import com.talkermessenger.activity.VectorCallViewActivity;
import com.talkermessenger.activity.VectorMemberDetailsActivity;
import com.talkermessenger.activity.VectorRoomActivity;
import com.talkermessenger.util.RoomUtils;
import com.talkermessenger.util.VectorUtils;

import org.matrix.androidsdk.MXSession;
import org.matrix.androidsdk.call.IMXCall;
import org.matrix.androidsdk.data.Room;
import org.matrix.androidsdk.data.RoomSummary;
import org.matrix.androidsdk.rest.callback.ApiCallback;
import org.matrix.androidsdk.rest.model.MatrixError;
import org.matrix.androidsdk.rest.model.User;
import org.matrix.androidsdk.util.Log;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.talkermessenger.activity.VectorRoomActivity.EXTRA_ROOM_ID;

public class OnAvatarSelectedDialogActivity extends AppCompatActivity {

    @BindView(R.id.avatar)
    ImageView avatar;

    @BindView(R.id.send_message)
    ImageView sendMessage;

    @BindView(R.id.call)
    ImageView call;

    @BindView(R.id.info)
    ImageView info;

    @BindView(R.id.name)
    TextView name;

    @BindView(R.id.function_panel)
    ConstraintLayout functionPanel;

    @BindView(R.id.for_back)
    ConstraintLayout forBack;

    @BindView(R.id.root_layout)
    ConstraintLayout rootLayout;

    private MXSession mSession;
    private static final String LOG_TAG = VectorMemberDetailsActivity.class.getSimpleName();
    private boolean isCall;
    private String roomId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_on_avatar_selected);
        ButterKnife.bind(this);

        getWindow().setBackgroundDrawable(null);
        String id = getIntent().getStringExtra("id");
        mSession = Matrix.getInstance(this).getDefaultSession();
        User user = mSession.getDataHandler().getUser(id);
        VectorUtils.loadProfileAvatar(this, mSession, avatar, user.avatar_url, user.user_id);
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        rootLayout.setOnClickListener(v -> onBackPressed());
        forBack.setOnClickListener(v -> {
        });
        name.setText(VectorUtils.getUserDisplayName(id, mSession, this));
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        info.setOnClickListener(v -> {
            Intent memberDetailsIntent = new Intent(OnAvatarSelectedDialogActivity.this, VectorMemberDetailsActivity.class);
            memberDetailsIntent.putExtra(VectorMemberDetailsActivity.EXTRA_MEMBER_ID, id);
            memberDetailsIntent.putExtra(VectorMemberDetailsActivity.EXTRA_MATRIX_ID, mSession.getCredentials().userId);
            startActivity(memberDetailsIntent);
        });

        sendMessage.setOnClickListener(v -> startRoom(false, id));

        call.setOnClickListener(v -> startRoom(true, id));
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        name.setText("");
        functionPanel.setVisibility(View.GONE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            finishAfterTransition();
        } else {
            finish();
        }
    }

    private void startRoom(boolean isCall, String userId) {
        this.isCall = isCall;
        String existingRoomId;
        if (null != (existingRoomId = RoomUtils.isDirectChatRoomAlreadyExist(userId, mSession))) {
            openRoom(mSession.getDataHandler().getRoom(existingRoomId), isCall);
        } else {
            mSession.createDirectMessageRoom(userId, mCreateDirectMessageCallBack);
        }
    }

    void openRoom(final Room room, boolean isCall) {
        if ((null == mSession.getDataHandler()) || (null == mSession.getDataHandler().getStore())) {
            return;
        }

        final String roomId;
        // cannot join a leaving room
        if (room == null || room.isLeaving()) {
            roomId = null;
        } else {
            roomId = room.getRoomId();
        }

        if (roomId != null) {
            openRoomById(roomId, isCall);
        }
    }

    private void openRoomById(String roomId, boolean isCall) {
        Room room = mSession.getDataHandler().getRoom(roomId);
        this.roomId = roomId;
        final RoomSummary roomSummary = mSession.getDataHandler().getStore().getSummary(roomId);
        if (null != roomSummary) {
            room.sendReadReceipt();
        }
        CommonActivityUtils.specificUpdateBadgeUnreadCount(mSession, this);
        if (isCall) {
            startCheckCallPermissions(false, roomId);
        } else {
            Intent intent = new Intent(this, VectorRoomActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            intent.putExtra(EXTRA_ROOM_ID, roomId);
            startActivity(intent);
        }
        finish();
    }

    /**
     * Check the permissions to establish an audio/video call.
     * If permissions are already granted, the call is established, otherwise
     * the permissions are checked against the system. Final result is provided in
     * {@link #onRequestPermissionsResult(int, String[], int[])}.
     *
     * @param aIsVideoCall true if video call, false if audio call
     */
    private void startCheckCallPermissions(boolean aIsVideoCall, String roomId) {
        int requestCode = CommonActivityUtils.REQUEST_CODE_PERMISSION_AUDIO_IP_CALL;

        if (aIsVideoCall) {
            requestCode = CommonActivityUtils.REQUEST_CODE_PERMISSION_VIDEO_IP_CALL;
        }

        if (CommonActivityUtils.checkPermissions(requestCode, this)) {
            startCall(aIsVideoCall, roomId);
        }
    }

    /**
     * Start a call in a dedicated room
     *
     * @param isVideo true if the call is a video call
     */
    private void startCall(final boolean isVideo, String roomId) {
        if (!mSession.isAlive()) {
            Log.e(LOG_TAG, "startCall : the session is not anymore valid");
            return;
        }

        // create the call object
        mSession.mCallsManager.createCallInRoom(roomId, isVideo, new ApiCallback<IMXCall>() {
            @Override
            public void onSuccess(final IMXCall call) {
                Log.d(LOG_TAG, "## startIpCall(): onSuccess");
                OnAvatarSelectedDialogActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        final Intent intent = new Intent(OnAvatarSelectedDialogActivity.this, VectorCallViewActivity.class);

                        intent.putExtra(VectorCallViewActivity.EXTRA_MATRIX_ID, mSession.getCredentials().userId);
                        intent.putExtra(VectorCallViewActivity.EXTRA_CALL_ID, call.getCallId());

                        OnAvatarSelectedDialogActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                OnAvatarSelectedDialogActivity.this.startActivity(intent);
                            }
                        });
                    }
                });
            }

            private void onError(final String errorMessage) {
                OnAvatarSelectedDialogActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Activity activity = OnAvatarSelectedDialogActivity.this;
                        CommonActivityUtils.displayToastOnUiThread(activity, activity.getString(R.string.cannot_start_call) + " (user offline)");
                    }
                });
            }

            @Override
            public void onNetworkError(Exception e) {
                Log.e(LOG_TAG, "## startIpCall(): onNetworkError Msg=" + e.getMessage());
                onError(e.getLocalizedMessage());
            }

            @Override
            public void onMatrixError(MatrixError e) {
                Log.e(LOG_TAG, "## startIpCall(): onMatrixError Msg=" + e.getLocalizedMessage());
                onError(e.getLocalizedMessage());
            }

            @Override
            public void onUnexpectedError(Exception e) {
                Log.e(LOG_TAG, "## startIpCall(): onUnexpectedError Msg=" + e.getLocalizedMessage());
                onError(e.getLocalizedMessage());
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int aRequestCode, @NonNull String[] aPermissions, @NonNull int[] aGrantResults) {
        if (0 == aPermissions.length) {
            Log.e(LOG_TAG, "## onRequestPermissionsResult(): cancelled " + aRequestCode);
        } else if (aRequestCode == CommonActivityUtils.REQUEST_CODE_PERMISSION_AUDIO_IP_CALL) {
            if (CommonActivityUtils.onPermissionResultAudioIpCall(this, aPermissions, aGrantResults)) {
                startCall(false, roomId);
            }
        }
    }

    private final ApiCallback<String> mCreateDirectMessageCallBack = new ApiCallback<String>() {
        @Override
        public void onSuccess(String roomId) {
            Log.d(LOG_TAG, "## mCreateDirectMessageCallBack: onSuccess - start goToRoomPage " + roomId);
            openRoomById(roomId, isCall);
        }

        @Override
        public void onMatrixError(MatrixError e) {
            Log.d(LOG_TAG, "## mCreateDirectMessageCallBack: onMatrixError Msg=" + e.getLocalizedMessage());
            Toast.makeText(OnAvatarSelectedDialogActivity.this, e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
        }

        @Override
        public void onNetworkError(Exception e) {
            Log.d(LOG_TAG, "## mCreateDirectMessageCallBack: onNetworkError Msg=" + e.getLocalizedMessage());
            Toast.makeText(OnAvatarSelectedDialogActivity.this, e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
        }

        @Override
        public void onUnexpectedError(Exception e) {
            Log.d(LOG_TAG, "## mCreateDirectMessageCallBack: onUnexpectedError Msg=" + e.getLocalizedMessage());
            Toast.makeText(OnAvatarSelectedDialogActivity.this, e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
        }
    };

}
