package com.talkermessenger.util;

import android.content.Context;

import com.talkermessenger.R;

/**
 * Created by Serhii on 01.02.2018.
 */

public enum PrivacyMethod {
    EVERYONE,
    NOBODY,
    MY_CONTACTS,
    FAVOURITES,
    EXCEPT,
    ONLY;

    public String getString(Context context) {
        if (this == EVERYONE)
            return context.getString(R.string.privacy_everyone);
        if (this == NOBODY)
            return context.getString(R.string.privacy_nobody);
        if (this == MY_CONTACTS)
            return context.getString(R.string.privacy_my_contacts);
        if (this == FAVOURITES)
            return context.getString(R.string.privacy_favourites);
        if (this == EXCEPT)
            return context.getString(R.string.privacy_except);
        if (this == ONLY)
            return context.getString(R.string.privacy_only);
        return context.getString(R.string.privacy_undefined);
    }

    public static PrivacyMethod getPrivacyMethod(String text, Context context) {
        if (text.equals(context.getString(R.string.privacy_everyone)))
            return EVERYONE;
        if (text.equals(context.getString(R.string.privacy_nobody)))
                return NOBODY;
        if (text.equals(context.getString(R.string.privacy_my_contacts)))
                return MY_CONTACTS;
        if (text.equals(context.getString(R.string.privacy_favourites)))
                return FAVOURITES;
        if (text.equals(context.getString(R.string.privacy_except)))
                return EXCEPT;
        if (text.equals(context.getString(R.string.privacy_only)))
                return ONLY;
        return EVERYONE;
    }

}
