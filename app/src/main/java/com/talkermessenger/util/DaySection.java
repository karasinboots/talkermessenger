package com.talkermessenger.util;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.talkermessenger.R;

import org.matrix.androidsdk.rest.model.Event;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import io.github.luizgrp.sectionedrecyclerviewadapter.SectionParameters;
import io.github.luizgrp.sectionedrecyclerviewadapter.StatelessSection;


public class DaySection extends StatelessSection {

    String mTitle;
    List<Event> mData;
    private Activity mActivity;

    public DaySection(String title, List<Event> list, Activity activity) {
        super(SectionParameters.builder()
                .itemResourceId(R.layout.item_call_history)
                .headerResourceId(R.layout.item_header_call_history)
                .build());

        this.mTitle = title;
        this.mData = list;
        this.mActivity = activity;
    }

    @Override
    public int getContentItemsTotal() {
        return mData.size();
    }

    @Override
    public RecyclerView.ViewHolder getItemViewHolder(View view) {
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindItemViewHolder(RecyclerView.ViewHolder holder, int position) {
        final ItemViewHolder itemViewHolder = (ItemViewHolder) holder;

        Event event = mData.get(position);
        boolean hangup = false;
        if (event.getContentAsJsonObject().get("reason") != null
                && event.getContentAsJsonObject().get("reason").getAsString().equals("user hangup"))
            hangup = true;
        if (event.isIncoming()) {
            if (event.isMissed() && hangup && event.userId == null) {
                itemViewHolder.mType.setText(R.string.call_history_missed);
                itemViewHolder.mIcon.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.missed));
            } else {
                itemViewHolder.mType.setText(R.string.call_history_incoming);
                itemViewHolder.mIcon.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.incoming));
            }
        } else {
            itemViewHolder.mType.setText(R.string.call_history_outgoing);
            itemViewHolder.mIcon.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.outgoing));
        }

        Calendar time = Calendar.getInstance();
        time.setTimeInMillis(event.getOriginServerTs());
        itemViewHolder.mTime.setText(new SimpleDateFormat("HH:mm", Locale.getDefault()).format(event.getOriginServerTs()));
    }


    @Override
    public RecyclerView.ViewHolder getHeaderViewHolder(View view) {
        return new HeaderViewHolder(view);
    }

    @Override
    public void onBindHeaderViewHolder(RecyclerView.ViewHolder holder) {
        HeaderViewHolder headerHolder = (HeaderViewHolder) holder;
        headerHolder.mTitle.setText(mTitle);
    }


    private class HeaderViewHolder extends RecyclerView.ViewHolder {

        private TextView mTitle;

        HeaderViewHolder(View view) {
            super(view);
            mTitle = (TextView) view.findViewById(R.id.call_history_day);
        }
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        ImageView mIcon;
        TextView mType;
        TextView mTime;

        ItemViewHolder(View itemView) {
            super(itemView);
            mIcon = (ImageView) itemView.findViewById(R.id.call_history_icon);
            mType = (TextView) itemView.findViewById(R.id.call_history_type);
            mTime = (TextView) itemView.findViewById(R.id.call_history_time);
        }
    }


    public void onItemClick(ItemViewHolder itemViewHolder, Event event) {

    }

}
