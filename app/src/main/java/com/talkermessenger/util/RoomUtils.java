/*
 * Copyright 2017 Vector Creations Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.talkermessenger.util;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.PopupMenu;
import android.widget.Toast;

import com.talkermessenger.Matrix;
import com.talkermessenger.R;
import com.talkermessenger.activity.CommonActivityUtils;
import com.talkermessenger.activity.GroupInfoActivity;
import com.talkermessenger.activity.VectorMemberDetailsActivity;
import com.talkermessenger.activity.VectorRoomActivity;
import com.talkermessenger.adapters.AdapterUtils;
import com.talkermessenger.adapters.ParticipantAdapterItem;
import com.talkermessenger.adapters.VectorMessagesAdapterHelper;

import org.matrix.androidsdk.MXDataHandler;
import org.matrix.androidsdk.MXSession;
import org.matrix.androidsdk.adapters.MessageRow;
import org.matrix.androidsdk.data.Room;
import org.matrix.androidsdk.data.RoomAccountData;
import org.matrix.androidsdk.data.RoomSummary;
import org.matrix.androidsdk.data.RoomTag;
import org.matrix.androidsdk.data.store.IMXStore;
import org.matrix.androidsdk.rest.callback.ApiCallback;
import org.matrix.androidsdk.rest.callback.SimpleApiCallback;
import org.matrix.androidsdk.rest.model.CreateRoomParams;
import org.matrix.androidsdk.rest.model.Event;
import org.matrix.androidsdk.rest.model.MatrixError;
import org.matrix.androidsdk.rest.model.Message;
import org.matrix.androidsdk.rest.model.RoomMember;
import org.matrix.androidsdk.rest.model.User;
import org.matrix.androidsdk.util.BingRulesManager;
import org.matrix.androidsdk.util.EventDisplay;
import org.matrix.androidsdk.util.JsonUtils;
import org.matrix.androidsdk.util.Log;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import static com.talkermessenger.activity.GroupInfoActivity.EXTRA_MATRIX_ID;
import static com.talkermessenger.activity.VectorRoomActivity.EXTRA_ROOM_ID;

public class RoomUtils {

    private static final String LOG_TAG = RoomUtils.class.getSimpleName();

    public interface MoreActionListener {
        void onToggleRoomNotifications(MXSession session, String roomId);

        void onToggleDirectChat(MXSession session, String roomId);

        void moveToFavorites(MXSession session, String roomId);

        void moveToArchived(MXSession session, String roomId);

        void moveToConversations(MXSession session, String roomId);

        void moveToPin(MXSession session, String roomId);

        void moveToLowPriority(MXSession session, String roomId);

        void onLeaveRoom(MXSession session, String roomId);

        void onDeleteRoom(MXSession session, String roomId);
    }

    public interface HistoricalRoomActionListener {
        void onForgotRoom(Room room);
    }

    /**
     * Return comparator to sort rooms by date
     *
     * @param session
     * @param reverseOrder
     * @return comparator
     */
    public static Comparator<Room> getRoomsDateComparator(final MXSession session, final boolean reverseOrder) {
        return new Comparator<Room>() {
            private Comparator<RoomSummary> mRoomSummaryComparator;
            private final HashMap<String, RoomSummary> mSummaryByRoomIdMap = new HashMap<>();

            /**
             * Retrieve the room summary comparator
             * @return comparator
             */
            private Comparator<RoomSummary> getSummaryComparator() {
                if (null == mRoomSummaryComparator) {
                    mRoomSummaryComparator = getRoomSummaryComparator(reverseOrder);
                }
                return mRoomSummaryComparator;
            }

            /**
             * Retrieve a summary from its room id
             * @param roomId
             * @return the summary
             */
            private RoomSummary getSummary(String roomId) {
                if (TextUtils.isEmpty(roomId)) {
                    return null;
                }

                RoomSummary summary = mSummaryByRoomIdMap.get(roomId);

                if (null == summary) {
                    summary = session.getDataHandler().getStore().getSummary(roomId);

                    if (null != summary) {
                        mSummaryByRoomIdMap.put(roomId, summary);
                    }
                }

                return summary;
            }

            public int compare(Room aLeftObj, Room aRightObj) {

                final Set<String> aLeftTags = aLeftObj.getAccountData().getKeys();
                final boolean isLeftArchived = aLeftTags != null && aLeftTags.contains(RoomTag.ROOM_TAG_ARCHIVED);
                final boolean isLeftPin = aLeftTags != null && aLeftTags.contains(RoomTag.ROOM_TAG_PIN);
                final boolean isLeftDeleted = aLeftTags != null && aLeftTags.contains(RoomTag.ROOM_TAG_DELETED);

                final Set<String> aRightTags = aRightObj.getAccountData().getKeys();
                final boolean isRightArchived = aRightTags != null && aRightTags.contains(RoomTag.ROOM_TAG_ARCHIVED);
                final boolean isRightPin = aRightTags != null && aRightTags.contains(RoomTag.ROOM_TAG_PIN);
                final boolean isRightDeleted = aRightTags != null && aRightTags.contains(RoomTag.ROOM_TAG_DELETED);
                if (!isLeftDeleted && !isLeftArchived && isLeftPin) {
                    return -1;
                } else if (!isRightDeleted && !isRightArchived && isRightPin) {
                    return 1;
                }

                final RoomSummary leftRoomSummary = getSummary(aLeftObj.getRoomId());
                final RoomSummary rightRoomSummary = getSummary(aRightObj.getRoomId());

                return getSummaryComparator().compare(leftRoomSummary, rightRoomSummary);
            }
        };
    }

    /**
     * Return comparator to sort rooms by:
     * 1- the highlighted rooms (sub sorted by date) if pinMissedNotifications is true
     * 2- the notified rooms (sub sorted by date) if pinMissedNotifications is true
     * 3- the unread rooms if pinUnreadMessages is true
     * 4- latest event timestamp
     *
     * @param session                the session
     * @param pinMissedNotifications whether missed notifications should be pinned
     * @param pinUnreadMessages      whether unread messages should be pinned
     * @return comparator
     */
    public static Comparator<Room> getNotifCountRoomsComparator(final MXSession session, final boolean pinMissedNotifications, final boolean pinUnreadMessages) {
        return new Comparator<Room>() {
            private Comparator<RoomSummary> mRoomSummaryComparator;
            private final HashMap<String, RoomSummary> mSummaryByRoomIdMap = new HashMap<>();

            /**
             * Retrieve the room summary comparator
             * @return comparator
             */
            private Comparator<RoomSummary> getSummaryComparator() {
                if (null == mRoomSummaryComparator) {
                    mRoomSummaryComparator = getNotifCountRoomSummaryComparator(session.getDataHandler().getBingRulesManager(), pinMissedNotifications, pinUnreadMessages);
                }
                return mRoomSummaryComparator;
            }

            /**
             * Retrieve a summary from its room id
             * @param roomId
             * @return the summary
             */
            private RoomSummary getSummary(String roomId) {
                if (TextUtils.isEmpty(roomId)) {
                    return null;
                }

                RoomSummary summary = mSummaryByRoomIdMap.get(roomId);

                if (null == summary) {
                    summary = session.getDataHandler().getStore().getSummary(roomId);

                    if (null != summary) {
                        mSummaryByRoomIdMap.put(roomId, summary);
                    }
                }

                return summary;
            }


            public int compare(Room aLeftObj, Room aRightObj) {
                final RoomSummary leftRoomSummary = getSummary(aLeftObj.getRoomId());
                final RoomSummary rightRoomSummary = getSummary(aRightObj.getRoomId());

                return getSummaryComparator().compare(leftRoomSummary, rightRoomSummary);
            }
        };
    }

    /**
     * Return comparator to sort historical rooms by date
     *
     * @param session
     * @param reverseOrder
     * @return comparator
     */
    public static Comparator<Room> getHistoricalRoomsComparator(final MXSession session, final boolean reverseOrder) {
        return new Comparator<Room>() {
            public int compare(Room aLeftObj, Room aRightObj) {
                final RoomSummary leftRoomSummary = session.getDataHandler().getStore(aLeftObj.getRoomId()).getSummary(aLeftObj.getRoomId());
                final RoomSummary rightRoomSummary = session.getDataHandler().getStore(aRightObj.getRoomId()).getSummary(aRightObj.getRoomId());

                return getRoomSummaryComparator(reverseOrder).compare(leftRoomSummary, rightRoomSummary);
            }
        };
    }

    /**
     * Return a comparator to sort summaries by latest event
     *
     * @param reverseOrder
     * @return ordered list
     */
    private static Comparator<RoomSummary> getRoomSummaryComparator(final boolean reverseOrder) {
        return new Comparator<RoomSummary>() {
            public int compare(RoomSummary leftRoomSummary, RoomSummary rightRoomSummary) {
                int retValue;
                long deltaTimestamp;

                if ((null == leftRoomSummary) || (null == leftRoomSummary.getLatestReceivedEvent())) {
                    retValue = 1;
                } else if ((null == rightRoomSummary) || (null == rightRoomSummary.getLatestReceivedEvent())) {
                    retValue = -1;
                } else if ((deltaTimestamp = rightRoomSummary.getLatestReceivedEvent().getOriginServerTs()
                        - leftRoomSummary.getLatestReceivedEvent().getOriginServerTs()) > 0) {
                    retValue = 1;
                } else if (deltaTimestamp < 0) {
                    retValue = -1;
                } else {
                    retValue = 0;
                }

                return reverseOrder ? -retValue : retValue;
            }
        };
    }

    /**
     * Return comparator to sort room summaries  by
     * 1- the highlighted rooms (sub sorted by date) if pinMissedNotifications is true
     * 2- the notified rooms (sub sorted by date) if pinMissedNotifications is true
     * 3- the unread rooms if pinUnreadMessages is true
     * 4- latest event timestamp
     *
     * @param bingRulesManager       the bing rules manager
     * @param pinMissedNotifications whether missed notifications should be pinned
     * @param pinUnreadMessages      whether unread messages should be pinned
     * @return comparator
     */
    private static Comparator<RoomSummary> getNotifCountRoomSummaryComparator(final BingRulesManager bingRulesManager,
                                                                              final boolean pinMissedNotifications,
                                                                              final boolean pinUnreadMessages) {
        return new Comparator<RoomSummary>() {
            public int compare(RoomSummary leftRoomSummary, RoomSummary rightRoomSummary) {
                int retValue;
                long deltaTimestamp;
                int leftHighlightCount = 0, rightHighlightCount = 0;
                int leftNotificationCount = 0, rightNotificationCount = 0;
                int leftUnreadCount = 0, rightUnreadCount = 0;

                if (null != leftRoomSummary) {
                    leftHighlightCount = leftRoomSummary.getHighlightCount();
                    leftNotificationCount = leftRoomSummary.getNotificationCount();
                    leftUnreadCount = leftRoomSummary.getUnreadEventsCount();

                    if (bingRulesManager.isRoomMentionOnly(leftRoomSummary.getRoomId())) {
                        leftNotificationCount = leftHighlightCount;
                    }
                }

                if (null != rightRoomSummary) {
                    rightHighlightCount = rightRoomSummary.getHighlightCount();
                    rightNotificationCount = rightRoomSummary.getNotificationCount();
                    rightUnreadCount = rightRoomSummary.getUnreadEventsCount();

                    if (bingRulesManager.isRoomMentionOnly(rightRoomSummary.getRoomId())) {
                        rightNotificationCount = rightHighlightCount;
                    }
                }

                if ((null == leftRoomSummary) || (null == leftRoomSummary.getLatestReceivedEvent())) {
                    retValue = 1;
                } else if ((null == rightRoomSummary) || (null == rightRoomSummary.getLatestReceivedEvent())) {
                    retValue = -1;
                } else if (pinMissedNotifications && (rightHighlightCount > 0) && (leftHighlightCount == 0)) {
                    retValue = 1;
                } else if (pinMissedNotifications && (rightHighlightCount == 0) && (leftHighlightCount > 0)) {
                    retValue = -1;
                } else if (pinMissedNotifications && (rightNotificationCount > 0) && (leftNotificationCount == 0)) {
                    retValue = 1;
                } else if (pinMissedNotifications && (rightNotificationCount == 0) && (leftNotificationCount > 0)) {
                    retValue = -1;
                } else if (pinUnreadMessages && (rightUnreadCount > 0) && (leftUnreadCount == 0)) {
                    retValue = 1;
                } else if (pinUnreadMessages && (rightUnreadCount == 0) && (leftUnreadCount > 0)) {
                    retValue = -1;
                } else if ((deltaTimestamp = rightRoomSummary.getLatestReceivedEvent().getOriginServerTs()
                        - leftRoomSummary.getLatestReceivedEvent().getOriginServerTs()) > 0) {
                    retValue = 1;
                } else if (deltaTimestamp < 0) {
                    retValue = -1;
                } else {
                    retValue = 0;
                }

                return retValue;
            }
        };
    }

    /**
     * Provides the formatted timestamp for the room
     *
     * @param context
     * @param latestEvent the latest event.
     * @return the formatted timestamp to display.
     */
    public static String getRoomTimestamp(final Context context, final Event latestEvent) {
        String text = AdapterUtils.tsToString(context, latestEvent.getOriginServerTs(), false);

        // don't display the today before the time
        String today = context.getString(R.string.today) + " ";
        if (text.startsWith(today)) {
            text = text.substring(today.length());
        }

        return text;
    }

    /**
     * Retrieve the text to display for a RoomSummary.
     *
     * @param context
     * @param session
     * @param roomSummary the roomSummary.
     * @return the text to display.
     */
    public static CharSequence getRoomMessageToDisplay(final Context context, final MXSession session,
                                                       final RoomSummary roomSummary) {
        CharSequence messageToDisplay = null;
        EventDisplay eventDisplay;

        if (null != roomSummary) {
            if (roomSummary.getLatestReceivedEvent() != null) {
                eventDisplay = new EventDisplay(context, roomSummary.getLatestReceivedEvent(), roomSummary.getLatestRoomState());
                if (roomSummary.getLatestReceivedEvent().getOriginServerTs() > roomSummary.getDeletionTime()) {
                    boolean incoming = !roomSummary.getLatestReceivedEvent().getSender().equals(session.getMyUserId());
                    Log.e("incoming", incoming + "");
                    if (!CommonActivityUtils.isGroupChat(session.getDataHandler().getRoom(roomSummary.getRoomId()))) {
                        eventDisplay.setPrependMessagesWithAuthor(false);
                        messageToDisplay = eventDisplay.getTextualDisplay(ThemeUtils.getColor(context, R.attr.room_notification_text_color), incoming);
                    } else {
                        eventDisplay.setPrependMessagesWithAuthor(true);
                        messageToDisplay = VectorMessagesAdapterHelper.getUserDisplayName(roomSummary.getLatestReceivedEvent().getSender(), session, context) + eventDisplay.getTextualDisplay(ThemeUtils.getColor(context, R.attr.room_notification_text_color), incoming);
                    }

                } else messageToDisplay = "";
            }

            // check if this is an invite
    /*        if (roomSummary.isInvited() && (null != roomSummary.getInviterUserId())) {
                RoomState latestRoomState = roomSummary.getLatestRoomState();
                String inviterUserId = VectorMessagesAdapterHelper.getUserDisplayName(roomSummary.getInviterUserId(), session);
                String myName = roomSummary.getMatrixId();

                if (null != latestRoomState) {
                    inviterUserId = latestRoomState.getMemberName(inviterUserId);
                    myName = latestRoomState.getMemberName(myName);
                } else {
                    inviterUserId = getMemberDisplayNameFromUserId(context, roomSummary.getMatrixId(), inviterUserId);
                    myName = getMemberDisplayNameFromUserId(context, roomSummary.getMatrixId(), myName);
                }

                if (TextUtils.equals(session.getMyUserId(), roomSummary.getMatrixId())) {
                    messageToDisplay = context.getString(org.matrix.androidsdk.R.string.notice_room_invite_you, inviterUserId);
                } else {
                    messageToDisplay = context.getString(org.matrix.androidsdk.R.string.notice_room_invite, inviterUserId, myName);
                }
            }*/
        }

        return messageToDisplay;
    }

    /**
     * Get the displayable name of the user whose ID is passed in aUserId.
     *
     * @param context
     * @param matrixId matrix ID
     * @param userId   user ID
     * @return the user display name
     */
    private static String getMemberDisplayNameFromUserId(final Context context, final String matrixId,
                                                         final String userId) {
        String displayNameRetValue;
        MXSession session;

        if (null == matrixId || null == userId) {
            displayNameRetValue = null;
        } else if ((null == (session = Matrix.getMXSession(context, matrixId))) || (!session.isAlive())) {
            displayNameRetValue = null;
        } else {
            User user = session.getDataHandler().getStore().getUser(userId);

            if ((null != user) && !TextUtils.isEmpty(user.displayname)) {
                displayNameRetValue = user.displayname;
            } else {
                displayNameRetValue = userId;
            }
        }

        return displayNameRetValue;
    }

    /**
     * See {@link #displayPopupMenu(Context, MXSession, Room, View, boolean, boolean, boolean, boolean, MoreActionListener, HistoricalRoomActionListener)}
     */
    public static void displayPopupMenu(final Context context, final MXSession session, final Room room,
                                        final View actionView, final boolean isFavorite, final boolean isLowPrior,
                                        final boolean isArchived, final boolean isPin,
                                        @NonNull final MoreActionListener listener) {
        if (listener != null) {
            displayPopupMenu(context, session, room, actionView, isFavorite, isLowPrior, isArchived, isPin, listener, null);
        }
    }


    /**
     * See {@link #displayPopupMenu(Context, MXSession, Room, View, boolean, boolean, boolean, boolean, MoreActionListener, HistoricalRoomActionListener)}
     */
    public static void displayHistoricalRoomMenu(final Context context, final MXSession session, final Room room,
                                                 final View actionView, @NonNull final HistoricalRoomActionListener listener) {
        if (listener != null) {
            displayPopupMenu(context, session, room, actionView, false, false, false, false, null, listener);
        }
    }

    /**
     * Display the room action popup.
     *
     * @param context
     * @param session
     * @param room               the room in which the actions should be triggered in.
     * @param actionView         the anchor view.
     * @param isFavorite         true if it is a favorite room
     * @param isLowPrior         true it it is a low priority room
     * @param isArchived         true it it is a archived room
     * @param moreActionListener
     */
    @SuppressLint("NewApi")
    private static void displayPopupMenu(final Context context, final MXSession session, final Room room,
                                         final View actionView, final boolean isFavorite, final boolean isLowPrior,
                                         final boolean isArchived, final boolean isPin,
                                         final MoreActionListener moreActionListener, final HistoricalRoomActionListener historicalRoomActionListener) {
        // sanity check
        if (null == room) {
            return;
        }

        final PopupMenu popup;

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            popup = new PopupMenu(context, actionView, Gravity.END);
        } else {
            popup = new PopupMenu(context, actionView);
        }
        popup.getMenuInflater().inflate(R.menu.vector_home_room_settings, popup.getMenu());
        CommonActivityUtils.tintMenuIcons(popup.getMenu(), ThemeUtils.getColor(context, R.attr.settings_icon_tint_color));

        if (room.isLeft()) {
            popup.getMenu().setGroupVisible(R.id.active_room_actions, false);
            popup.getMenu().setGroupVisible(R.id.historical_room_actions, true);

            if (historicalRoomActionListener != null) {
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(final MenuItem item) {
                        if (item.getItemId() == R.id.action_forget_room) {
                            historicalRoomActionListener.onForgotRoom(room);
                        }
                        return true;
                    }
                });
            }
        } else {
            popup.getMenu().setGroupVisible(R.id.active_room_actions, true);
            popup.getMenu().setGroupVisible(R.id.historical_room_actions, false);

            MenuItem item;

            final BingRulesManager bingRulesManager = session.getDataHandler().getBingRulesManager();

            if (!isArchived) {
                item = popup.getMenu().getItem(0);
                item.setIcon(null);
            }

            if (!isPin) {
                item = popup.getMenu().getItem(1);
                item.setIcon(null);
            }

            if (!bingRulesManager.isRoomNotificationsDisabled(room.getRoomId())) {
                item = popup.getMenu().findItem(R.id.ic_action_select_notifications);
                item.setTitle(R.string.rooms_popup_menu_mute);
                item.setIcon(null);
            } else {
                item = popup.getMenu().findItem(R.id.ic_action_select_notifications);
                item.setTitle(R.string.rooms_popup_menu_unmute);
                item.setIcon(null);
            }

            if (isPin) {
                item = popup.getMenu().findItem(R.id.ic_action_select_pin);
                item.setTitle(R.string.unpin);
            }

//            if (!isFavorite) {
//                item = popup.getMenu().getItem(3);
//                item.setIcon(null);
//            }

//            if (!isLowPrior) {
//                item = popup.getMenu().getItem(4);
//                item.setIcon(null);
//            }

//            if (session.getDirectChatRoomIdsList().indexOf(room.getRoomId()) < 0) {
//                item = popup.getMenu().getItem(5);
//                item.setIcon(null);
//            }


            if (moreActionListener != null) {
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(final MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.ic_action_select_notifications: {
                                moreActionListener.onToggleRoomNotifications(session, room.getRoomId());
                                break;
                            }
//                            case R.id.ic_action_select_fav: {
//                                if (isFavorite) {
//                                    moreActionListener.moveToConversations(session, room.getRoomId());
//                                } else {
//                                    moreActionListener.moveToFavorites(session, room.getRoomId());
//                                }
//                                break;
//                            }
                            case R.id.ic_action_show_info: {
                                if (CommonActivityUtils.isGroupChat(room)) {
                                    Log.e("ROOM_NAME", room.getName(session.getMyUserId()));
                                    Intent intent = new Intent(context, GroupInfoActivity.class);
                                    intent.putExtra(EXTRA_MATRIX_ID, session.getMyUserId());
                                    intent.putExtra(EXTRA_ROOM_ID, room.getRoomId());
                                    context.startActivity(intent);
                                } else {
                                    Log.e("chat_NAME", room.getName(session.getMyUserId()));
                                    for (RoomMember member : room.getMembers()) {
                                        if (!member.getUserId().equals(session.getMyUserId())) {
                                            ParticipantAdapterItem userItem = new ParticipantAdapterItem(member);
                                            Intent startRoomInfoIntent = new Intent(context, VectorMemberDetailsActivity.class);
                                            startRoomInfoIntent.putExtra(VectorMemberDetailsActivity.EXTRA_MEMBER_ID, userItem.mUserId);

                                            if (!TextUtils.isEmpty(userItem.mAvatarUrl)) {
                                                startRoomInfoIntent.putExtra(VectorMemberDetailsActivity.EXTRA_MEMBER_AVATAR_URL, userItem.mAvatarUrl);
                                            }

                                            if (!TextUtils.isEmpty(userItem.mDisplayName)) {
                                                startRoomInfoIntent.putExtra(VectorMemberDetailsActivity.EXTRA_MEMBER_DISPLAY_NAME, userItem.mDisplayName);
                                            }

                                            if (!TextUtils.isEmpty(userItem.mAvatarUrl)) {
                                                startRoomInfoIntent.putExtra(VectorMemberDetailsActivity.EXTRA_MEMBER_AVATAR_URL, userItem.mAvatarUrl);
                                            }

                                            startRoomInfoIntent.putExtra(VectorMemberDetailsActivity.EXTRA_MATRIX_ID, session.getCredentials().userId);
                                            if (userItem.mContact != null) {

                                                if (userItem.mContact.getLookupUri() != null) {
                                                    startRoomInfoIntent.putExtra(VectorMemberDetailsActivity.EXTRA_LOOKUP_URI, userItem.mContact.getLookupUri());
                                                }

                                                if (userItem.mContact.getContactId() != null) {
                                                    startRoomInfoIntent.putExtra(VectorMemberDetailsActivity.EXTRA_CONTACT_ID, userItem.mContact.getContactId());
                                                }
                                            }
                                            context.startActivity(startRoomInfoIntent);
                                        }
                                    }
                                }
                                break;
                            }
                            case R.id.ic_action_select_archive: {
                                if (isArchived) {
                                    moreActionListener.moveToConversations(session, room.getRoomId());
                                } else {
                                    moreActionListener.moveToArchived(session, room.getRoomId());
                                }
                                break;
                            }
                            case R.id.ic_action_select_pin: {
                                if (isPin) {
                                    moreActionListener.moveToConversations(session, room.getRoomId());
                                } else {
                                    moreActionListener.moveToPin(session, room.getRoomId());
                                }
                                break;
                            }
//                            case R.id.ic_action_select_deprioritize: {
//                                if (isLowPrior) {
//                                    moreActionListener.moveToConversations(session, room.getRoomId());
//                                } else {
//                                    moreActionListener.moveToLowPriority(session, room.getRoomId());
//                                }
//                                break;
//                            }
                            case R.id.ic_action_select_remove: {
                                moreActionListener.onDeleteRoom(session, room.getRoomId());
                                break;
                            }
//                            case R.id.ic_action_select_direct_chat: {
//                                moreActionListener.onToggleDirectChat(session, room.getRoomId());
//                                break;
//                            }
                        }
                        return false;
                    }
                });
            }
        }

        // force to display the icon
//        try {
//            Field[] fields = popup.getClass().getDeclaredFields();
//            for (Field field : fields) {
//                if ("mPopup".equals(field.getName())) {
//                    field.setAccessible(true);
//                    Object menuPopupHelper = field.get(popup);
//                    Class<?> classPopupHelper = Class.forName(menuPopupHelper.getClass().getName());
//                    Method setForceIcons = classPopupHelper.getMethod("setForceShowIcon", boolean.class);
//                    setForceIcons.invoke(menuPopupHelper, true);
//                    break;
//                }
//            }
//        } catch (Exception e) {
//            Log.e(LOG_TAG, "## displayPopupMenu() : failed " + e.getMessage());
//        }

        popup.show();
    }

    /**
     * Display a confirmation dialog when user wants to leave a room
     *
     * @param context
     * @param onClickListener
     */
    public static void showLeaveRoomDialog(final Context context, final DialogInterface.OnClickListener onClickListener) {
        new AlertDialog.Builder(context)
                .setTitle(R.string.room_participants_leave_prompt_title)
                .setMessage(R.string.room_participants_leave_prompt_msg)
                .setPositiveButton(R.string.remove, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        onClickListener.onClick(dialog, which);
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .create()
                .show();
    }

    /**
     * Display a confirmation dialog when user wants to leave a room
     *
     * @param context
     * @param onClickListener
     */
    public static void showDeleteRoomDialog(final Context context, final DialogInterface.OnClickListener onClickListener) {
        new AlertDialog.Builder(context)
                .setTitle(R.string.room_participants_leave_prompt_title)
                .setMessage(R.string.room_participants_leave_prompt_msg)
                .setPositiveButton(R.string.remove, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        onClickListener.onClick(dialog, which);
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .create()
                .show();
    }

    /**
     * Update a room Tag
     *
     * @param session     the session
     * @param roomId      the room Id
     * @param aTagOrder   the new tag order
     * @param newTag      the new Tag
     * @param apiCallback the asynchronous callback
     */
    public static void updateRoomTag(final MXSession session, final String roomId, final Double aTagOrder, final String newTag, final ApiCallback<Void> apiCallback) {
        Room room = session.getDataHandler().getRoom(roomId);

        if (null != room) {
            String oldTag = null;

            // retrieve the tag from the room info
            RoomAccountData accountData = room.getAccountData();

            if ((null != accountData) && accountData.hasTags()) {
                oldTag = accountData.getKeys().iterator().next();
            }

            Double tagOrder = aTagOrder;

            // if the tag order is not provided, compute it
            if (null == tagOrder) {
                tagOrder = 0.0;

                if (null != newTag) {
                    tagOrder = session.tagOrderToBeAtIndex(0, Integer.MAX_VALUE, newTag);
                }
            }
            // and work
            if (oldTag != null && oldTag.equals(RoomTag.ROOM_TAG_IM))
                room.replaceTag(null, newTag, tagOrder, apiCallback);
            else room.replaceTag(oldTag, newTag, tagOrder, apiCallback);
        }
    }

    /**
     * Add or remove the given room from direct chats
     *
     * @param session
     * @param roomId
     * @param apiCallback
     */
    public static void toggleDirectChat(final MXSession session, String roomId, final ApiCallback<Void> apiCallback) {
        Room room = session.getDataHandler().getRoom(roomId);
        if (null != room) {
            session.toggleDirectChatRoom(roomId, null, apiCallback);
        }
    }

    /**
     * Enable or disable notifications for the given room
     *
     * @param session
     * @param roomId
     * @param listener
     */
    public static void toggleNotifications(final MXSession session, final String roomId, final BingRulesManager.onBingRuleUpdateListener listener) {
        BingRulesManager bingRulesManager = session.getDataHandler().getBingRulesManager();
        bingRulesManager.muteRoomNotifications(roomId, !bingRulesManager.isRoomNotificationsDisabled(roomId), listener);
    }

    /**
     * Get whether the room of the given is a direct chat
     *
     * @param roomId
     * @return true if direct chat
     */
    public static boolean isDirectChat(final MXSession session, final String roomId) {
        final IMXStore store = session.getDataHandler().getStore();
        final Map<String, List<String>> directChatRoomsDict;

        if (store.getDirectChatRoomsDict() != null) {
            directChatRoomsDict = new HashMap<>(store.getDirectChatRoomsDict());

            if (directChatRoomsDict.containsKey(session.getMyUserId())) {
                List<String> roomIdsList = new ArrayList<>(directChatRoomsDict.get(session.getMyUserId()));
                return roomIdsList.contains(roomId);
            }
        }

        return false;
    }

    /**
     * Create a list of rooms by filtering the given list with the given pattern
     *
     * @param roomsToFilter
     * @param constraint
     * @return filtered rooms
     */
    public static List<Room> getFilteredRooms(final Context context, final MXSession session,
                                              final List<Room> roomsToFilter, final CharSequence constraint) {
        final String filterPattern = constraint != null ? constraint.toString().trim() : null;
        if (!TextUtils.isEmpty(filterPattern)) {
            List<Room> filteredRoom = new ArrayList<>();
            Pattern pattern = Pattern.compile(Pattern.quote(filterPattern), Pattern.CASE_INSENSITIVE);
            for (final Room room : roomsToFilter) {
                final String roomName = VectorUtils.getRoomDisplayName(context, session, room);
                if (pattern.matcher(roomName).find()) {
                    filteredRoom.add(room);
                }
            }
            return filteredRoom;
        } else {
            return roomsToFilter;
        }
    }

    /**
     * Format the unread messages counter.
     *
     * @param count the count
     * @return the formatted value
     */
    public static String formatUnreadMessagesCounter(int count) {
        if (count > 0) {
            if (count > 999) {
                return (count / 1000) + "." + ((count % 1000) / 100) + "K";
            } else {
                return String.valueOf(count);
            }
        } else {
            return null;
        }
    }

    /**
     * Fill the direct chats mAdapter with data
     */
    public static List<Room> getAllRooms(MXSession mSession, Context context) {
        if ((null == mSession) || (null == mSession.getDataHandler())) {
            Log.e(LOG_TAG, "## getAllRooms() : null session");
        }

        final List<Room> mDirectChats = new ArrayList<>();
        final List<String> directChatIds = mSession.getDirectChatRoomIdsList();
        final MXDataHandler dataHandler = mSession.getDataHandler();
        final IMXStore store = dataHandler.getStore();

        if (directChatIds != null && !directChatIds.isEmpty()) {
            for (String roomId : directChatIds) {
                Room room = store.getRoom(roomId);

                if ((null != room) && !room.isConferenceUserRoom()) {
                    // it seems that the server syncs some left rooms
                    if (null == room.getMember(mSession.getMyUserId())) {
                        Log.e(LOG_TAG, "## initDirectChatsData(): invalid room " + room.getRoomId() + ", the user is not anymore member of it");
                    } else {
                        final Set<String> tags = room.getAccountData().getKeys();
                        if ((null == tags) || !tags.contains(RoomTag.ROOM_TAG_LOW_PRIORITY)) {
                            final boolean isArchived = tags != null && tags.contains(RoomTag.ROOM_TAG_ARCHIVED);
                            final boolean isDeleted = tags != null && tags.contains(RoomTag.ROOM_TAG_DELETED);
                            final boolean isFavourite = roomId.equals(PreferencesManager.getFavouriteMsgCommonRoom(context));
                            final boolean isNameEmptyChat = VectorUtils.getRoomDisplayName(context, mSession, room).equals(context.getString(R.string.room_displayname_no_title));
                            IMXStore imxStore = mSession.getDataHandler().getStore(roomId);
                            Collection<Event> allEvents = imxStore.getRoomMessages(roomId);
                            ArrayList<Event> roomMessages = new ArrayList<Event>();
                            if (allEvents != null)
                                for (Event event : allEvents) {
                                    if (event.getType().equals(Event.EVENT_TYPE_MESSAGE))
                                        roomMessages.add(event);
                                }
                            Event latestEvent = imxStore.getLatestEvent(roomId);
                            long originServerTs = 0;
                            if (latestEvent != null)
                                originServerTs = latestEvent.getOriginServerTs();
                            boolean isEmpty = roomMessages.size() == 0 && System.currentTimeMillis() - originServerTs > 12 * 60 * 60 * 1000;
                            if (!isArchived && !isDeleted && !isFavourite && !isNameEmptyChat && !isEmpty) {
                                mDirectChats.add(dataHandler.getRoom(roomId));
                            }
                        }
                    }
                }
            }
        }

        List<RoomSummary> roomSummaries = new ArrayList<>(store.getSummaries());
        HashSet<String> directChatRoomIds = new HashSet<>(mSession.getDirectChatRoomIdsList());
        HashSet<String> lowPriorityRoomIds = new HashSet<>(mSession.roomIdsWithTag(RoomTag.ROOM_TAG_LOW_PRIORITY));
        HashSet<String> deletedRoomIds = new HashSet<>(mSession.roomIdsWithTag(RoomTag.ROOM_TAG_DELETED));
        for (RoomSummary summary : roomSummaries) {
            // don't display the invitations
            if (!summary.isInvited()) {
                Room room = store.getRoom(summary.getRoomId());
                // test
                if ((null != room) && // if the room still exists
                        !room.isConferenceUserRoom() && // not a VOIP conference room
                        !directChatRoomIds.contains(room.getRoomId()) &&
                        !lowPriorityRoomIds.contains(room.getRoomId()) &&
                        !deletedRoomIds.contains(room.getRoomId())) {
                    final Set<String> tags = room.getAccountData().getKeys();
                    final boolean isArchived = tags != null && tags.contains(RoomTag.ROOM_TAG_ARCHIVED);
                    final boolean isDeleted = tags != null && tags.contains(RoomTag.ROOM_TAG_DELETED);
                    final boolean isFavourite = room.getRoomId().equals(PreferencesManager.getFavouriteMsgCommonRoom(context));
                    final boolean isNameEmptyChat = VectorUtils.getRoomDisplayName(context, mSession, room).equals(context.getString(R.string.room_displayname_no_title));
                    IMXStore imxStore = mSession.getDataHandler().getStore(summary.getRoomId());
                    Collection<Event> allEvents = imxStore.getRoomMessages(summary.getRoomId());
                    ArrayList<Event> roomMessages = new ArrayList<Event>();
                    for (Event event : allEvents) {
                        if (event.getType().equals(Event.EVENT_TYPE_MESSAGE))
                            roomMessages.add(event);
                    }
                    Event latestEvent = imxStore.getLatestEvent(summary.getRoomId());
                    long originServerTs = latestEvent.getOriginServerTs();
                    boolean isEmpty = roomMessages.size() == 0 && System.currentTimeMillis() - originServerTs > 12 * 60 * 60 * 1000;
                    if (!isArchived && !isDeleted && !isFavourite && !isNameEmptyChat && !isEmpty)
                        mDirectChats.add(room);
                }
            }
        }
        return mDirectChats;
    }

    /**
     * Find favourite msgs room if it was created in past.
     *
     * @param mSession
     * @return favourite msgs room
     */
    public static Room findFavouriteMsgsRoom(MXSession mSession) {
        if ((null == mSession) || (null == mSession.getDataHandler())) {
            Log.e(LOG_TAG, "## getAllRooms() : null session");
        }

        final List<String> directChatIds = mSession.getDirectChatRoomIdsList();
        final MXDataHandler dataHandler = mSession.getDataHandler();
        final IMXStore store = dataHandler.getStore();

        if (directChatIds != null && !directChatIds.isEmpty()) {
            for (String roomId : directChatIds) {
                Room room = store.getRoom(roomId);

                if ((null != room) && !room.isConferenceUserRoom()) {
                    // it seems that the server syncs some left rooms
                    if (null == room.getMember(mSession.getMyUserId())) {
                        Log.e(LOG_TAG, "## initDirectChatsData(): invalid room " + room.getRoomId() + ", the user is not anymore member of it");
                    } else {
                        final Set<String> tags = room.getAccountData().getKeys();
                        if ((null == tags) || !tags.contains(RoomTag.ROOM_TAG_LOW_PRIORITY)) {
                            final boolean isArchived = tags != null && tags.contains(RoomTag.ROOM_TAG_ARCHIVED);
                            final boolean isDeleted = tags != null && tags.contains(RoomTag.ROOM_TAG_DELETED);
                            boolean isFavourite = tags != null && tags.contains(RoomTag.ROOM_TAG_FAVOURITE);
                            if (!isArchived && !isDeleted && isFavourite) {
                                return dataHandler.getRoom(roomId);
                            }
                        }
                    }
                }
            }
        }

        List<RoomSummary> roomSummaries = new ArrayList<>(store.getSummaries());
        HashSet<String> directChatRoomIds = new HashSet<>(mSession.getDirectChatRoomIdsList());
        HashSet<String> lowPriorityRoomIds = new HashSet<>(mSession.roomIdsWithTag(RoomTag.ROOM_TAG_LOW_PRIORITY));
        HashSet<String> deletedRoomIds = new HashSet<>(mSession.roomIdsWithTag(RoomTag.ROOM_TAG_DELETED));
        for (RoomSummary summary : roomSummaries) {
            // don't display the invitations
            if (!summary.isInvited()) {
                Room room = store.getRoom(summary.getRoomId());
                // test
                if ((null != room) && // if the room still exists
                        !room.isConferenceUserRoom() && // not a VOIP conference room
                        !directChatRoomIds.contains(room.getRoomId()) &&
                        !lowPriorityRoomIds.contains(room.getRoomId()) &&
                        !deletedRoomIds.contains(room.getRoomId())) {
                    final Set<String> tags = room.getAccountData().getKeys();
                    final boolean isArchived = tags != null && tags.contains(RoomTag.ROOM_TAG_ARCHIVED);
                    final boolean isDeleted = tags != null && tags.contains(RoomTag.ROOM_TAG_DELETED);
                    boolean isFavourite = tags != null && tags.contains(RoomTag.ROOM_TAG_FAVOURITE);
                    if (!isArchived && !isDeleted && isFavourite) {
                        return room;
                    }
                }
            }
        }
        return null;
    }


    /**
     * Clear all messages from room
     */

    public static void clearChat(Room room) {
        List<Event> events = room.getAllRoomMessages();
        for (Event event : events) {
            if (!event.isHidden()) {
                event.hide(true);
            }
            try {
                String eventType = event.getType();
                if (Event.EVENT_TYPE_MESSAGE.equals(eventType)) {
                    Message message = JsonUtils.toMessage(event.getContent());
                    if (message.msgtype.equals(Message.MSGTYPE_TEXT) ||
                            message.msgtype.equals(Message.MSGTYPE_AUDIO) ||
                            message.msgtype.equals(Message.MSGTYPE_IMAGE) ||
                            message.msgtype.equals(Message.MSGTYPE_FILE) ||
                            message.msgtype.equals(Message.MSGTYPE_VIDEO)) {
                        room.redact(event.eventId, new ApiCallback<Event>() {
                            @Override
                            public void onSuccess(final Event redactedEvent) {
                            }

                            private void onError(String message) {
                            }

                            @Override
                            public void onNetworkError(Exception e) {
                                onError(e.getMessage());
                            }

                            @Override
                            public void onMatrixError(MatrixError e) {
                                onError(e.getMessage());
                            }

                            @Override
                            public void onUnexpectedError(Exception e) {
                                onError(e.getMessage());
                            }
                        });
                    }
                }
            } catch (NullPointerException ignore) {
            }
        }
    }

    /**
     * Clear all messages from room
     */

    public static void clearCalls(Room room) {
        List<Event> events = room.getAllRoomMessages();
        if (events != null && events.size() > 0)
            for (Event event : events) {
                try {
                    if (event.isCallEvent()) {
                        if (!event.isHidden()) {
                            event.hide(true);
                        }
                        room.redact(event.eventId, new ApiCallback<Event>() {
                            @Override
                            public void onSuccess(final Event redactedEvent) {
                            }

                            private void onError(String message) {
                            }

                            @Override
                            public void onNetworkError(Exception e) {
                                onError(e.getMessage());
                            }

                            @Override
                            public void onMatrixError(MatrixError e) {
                                onError(e.getMessage());
                            }

                            @Override
                            public void onUnexpectedError(Exception e) {
                                onError(e.getMessage());
                            }
                        });
                    }
                } catch (NullPointerException ignore) {
                }
            }
    }

    public static void createFavouriteMsgRoom(MXSession session, Activity activity, String favouriteRoomId, String userId) {
        CreateRoomParams params = new CreateRoomParams();
        List<String> ids = new ArrayList<>();
        ids.add(session.getMyUserId());
        params.addParticipantIds(session.getHomeServerConfig(), ids);
        session.createRoom(params, new SimpleApiCallback<String>(activity) {
            @Override
            public void onSuccess(final String roomId) {
                PreferencesManager.addFavouriteMsgCommonRoom(activity, roomId);
                RoomUtils.setFavouritesTag(session, roomId);
                openFavouriteMsgRoomById(roomId, session, activity, favouriteRoomId, userId);
            }

            private void onError(final String message) {
                if (null != message) {
                    Toast.makeText(activity, message, Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onNetworkError(Exception e) {
                onError(e.getLocalizedMessage());
            }

            @Override
            public void onMatrixError(final MatrixError e) {
                onError(e.getLocalizedMessage());
            }

            @Override
            public void onUnexpectedError(final Exception e) {
                onError(e.getLocalizedMessage());
            }
        });
    }

    public static void createFavouriteMsgRoomWithoutOpening(MXSession session, Activity
            activity) {

        CreateRoomParams params = new CreateRoomParams();
        List<String> ids = new ArrayList<>();
        ids.add(session.getMyUserId());
        params.addParticipantIds(session.getHomeServerConfig(), ids);
        session.createRoom(params, new SimpleApiCallback<String>(activity) {
            @Override
            public void onSuccess(final String roomId) {
                PreferencesManager.addFavouriteMsgCommonRoom(activity, roomId);
                setFavouritesTag(session, roomId);
            }

            private void onError(final String message) {

                if (null != message) {
                    Toast.makeText(activity, message, Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onNetworkError(Exception e) {
                onError(e.getLocalizedMessage());
            }

            @Override
            public void onMatrixError(final MatrixError e) {
                onError(e.getLocalizedMessage());
            }

            @Override
            public void onUnexpectedError(final Exception e) {
                onError(e.getLocalizedMessage());
            }
        });

    }

    private static void setFavouritesTag(MXSession session, String roomId) {
        session.getDataHandler().getRoom(roomId).replaceTag(null, RoomTag.ROOM_TAG_FAVOURITE, 0d, new ApiCallback<Void>() {
            @Override
            public void onSuccess(Void info) {

            }

            @Override
            public void onNetworkError(Exception e) {

            }

            @Override
            public void onMatrixError(MatrixError e) {

            }

            @Override
            public void onUnexpectedError(Exception e) {

            }
        });
    }


    public static void openFavouriteMsgRoomById(String roomId, MXSession session, Activity
            activity, String favouriteRoomId, String userId) {
        Room room = session.getDataHandler().getRoom(roomId);
        final RoomSummary roomSummary = session.getDataHandler().getStore().getSummary(roomId);

        if (null != roomSummary) {
            room.sendReadReceipt();
        }
        Intent intent = new Intent(activity, VectorRoomActivity.class);
//        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.putExtra(EXTRA_ROOM_ID, roomId);
        if (userId != null) intent.putExtra("userId", userId);
        if (favouriteRoomId != null) intent.putExtra("roomId", favouriteRoomId);
        intent.putExtra("isFavouriteMode", true);
        activity.startActivity(intent);
    }

    public static void openFavoutiteMsgs(Activity activity, MXSession mSession, String favouriteRoomId, String userId) {
        String roomId = PreferencesManager.getFavouriteMsgCommonRoom(activity);
        if (roomId != null)
            RoomUtils.openFavouriteMsgRoomById(roomId, mSession, activity, favouriteRoomId, userId);
        else {
            Room favouriteMsgsRoom = RoomUtils.findFavouriteMsgsRoom(mSession);
            if (favouriteMsgsRoom != null)
                RoomUtils.openFavouriteMsgRoomById(favouriteMsgsRoom.getRoomId(), mSession, activity, favouriteRoomId, userId);
            else
                RoomUtils.createFavouriteMsgRoom(mSession, activity, favouriteRoomId, userId);
        }
    }

    /**
     * Return the first direct chat room for a given user ID.
     *
     * @param userId  user ID to search for
     * @param session current MXSession
     * @return a room ID if search succeed, null otherwise.
     */
    public static String isDirectChatRoomAlreadyExist(String userId, MXSession session) {
        if (null != session) {
            IMXStore store = session.getDataHandler().getStore();

            HashMap<String, List<String>> directChatRoomsDict;

            if (null != store.getDirectChatRoomsDict()) {
                directChatRoomsDict = new HashMap<>(store.getDirectChatRoomsDict());

                if (directChatRoomsDict.containsKey(userId)) {
                    ArrayList<String> roomIdsList = new ArrayList<>(directChatRoomsDict.get(userId));

                    if (0 != roomIdsList.size()) {
                        for (String roomId : roomIdsList) {
                            Room room = session.getDataHandler().getRoom(roomId, false);

                            // check if the room is already initialized
                            if ((null != room) && room.isReady() && !room.isInvited() && !room.isLeaving() && !CommonActivityUtils.isGroupChat(room)) {
                                // test if the member did not leave the room
                                Collection<RoomMember> members = room.getActiveMembers();

                                for (RoomMember member : members) {
                                    if (TextUtils.equals(member.getUserId(), userId)) {
                                        Log.d(LOG_TAG, "## isDirectChatRoomAlreadyExist(): for user=" + userId + " roomFound=" + roomId);
                                        return roomId;
                                    }
                                }

                            }
                        }
                    }
                }
            }
        }
        Log.d(LOG_TAG, "## isDirectChatRoomAlreadyExist(): for user=" + userId + " no found room");
        return null;
    }

    public static void exportChat(String roomId, MXSession session, Context context, String userId) throws IOException {
        Room room = session.getDataHandler().getRoom(roomId);
        File dstDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        if (dstDir != null) {
            dstDir.mkdirs();
        }
        boolean isGroupChat = CommonActivityUtils.isGroupChat(room);
        String senderName = "";
        String fileName;
        if (isGroupChat)
            fileName = room.getLiveState().name;
        else {
            senderName = VectorUtils.getUserDisplayName(userId, session, context);
            fileName = senderName;
        }
        File file = new File(dstDir, fileName + ".txt");
        FileOutputStream stream = new FileOutputStream(file);
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(stream));

        ArrayList<Event> events;
        if (room.getAllRoomMessages() != null && room.getAllRoomMessages().size() > 0)
            events = new ArrayList<>(room.getAllRoomMessages());
        else events = null;
        if (events != null && events.size() > 0)
            for (int i = 0, eventsSize = events.size(); i < eventsSize; i++) {
                Event event = events.get(i);
                String date;
                String time;
                String name;
                if (Event.EVENT_TYPE_MESSAGE.equals(event.getType())) {
                    Message message = JsonUtils.toMessage(event.getContent());
                    if (Message.MSGTYPE_TEXT.equals(message.msgtype)) {
                        if (event.getSender().equals(session.getMyUserId())) name = "I :";
                        else {
                            if (isGroupChat) name = VectorUtils.getUserDisplayName(event.getSender(), session, context);
                            else name = senderName;
                        }
                        date = AdapterUtils.tsToStringForExportChat(context, event.getOriginServerTs(), false);
                        time = AdapterUtils.tsToStringForExportChat(context, event.getOriginServerTs(), true);
                        writer.write(date + " " + time + " " + name + " " + message.body);
                        writer.newLine();
                    }

                }
            }
        writer.close();
        Toast.makeText(context, R.string.file_was_saved_in_downloads, Toast.LENGTH_LONG).show();
    }
}
