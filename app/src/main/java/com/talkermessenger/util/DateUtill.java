package com.talkermessenger.util;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Nikita on 19.03.2018.
 */

public class DateUtill {

    public static String currentDateForStats(){
        return new SimpleDateFormat("yyyy-MM-dd").format(new Date(System.currentTimeMillis()));
    }
}
