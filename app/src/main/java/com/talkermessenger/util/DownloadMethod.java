package com.talkermessenger.util;

import com.talkermessenger.R;

/**
 * Created by Serhii on 31.01.2018.
 */

public enum DownloadMethod {
    WIFI_AND_CELLULAR,
    WIFI,
    OFF;

    public int getString() {
        if (this == WIFI_AND_CELLULAR)
            return R.string.wifi_and_cellular;
        if (this == WIFI)
            return R.string.wifi_only;
        if (this == OFF)
            return R.string.not_allowed;
        return -1;
    }
}
