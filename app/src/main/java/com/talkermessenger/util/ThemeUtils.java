/*
 * Copyright 2017 OpenMarket Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.talkermessenger.util;


import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.annotation.AttrRes;
import android.support.annotation.ColorInt;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.TypedValue;
import android.widget.ImageView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.talkermessenger.GlideApp;
import com.talkermessenger.R;
import com.talkermessenger.VectorApp;
import com.talkermessenger.activity.CountryPickerActivity;
import com.talkermessenger.activity.HistoricalRoomsActivity;
import com.talkermessenger.activity.LanguagePickerActivity;
import com.talkermessenger.activity.LoggingOutActivity;
import com.talkermessenger.activity.PhoneNumberAdditionActivity;
import com.talkermessenger.activity.PhoneNumberVerificationActivity;
import com.talkermessenger.activity.RegisterActivity;
import com.talkermessenger.activity.RoomDirectoryPickerActivity;
import com.talkermessenger.activity.SplashActivity;
import com.talkermessenger.activity.VectorBaseSearchActivity;
import com.talkermessenger.activity.VectorCallViewActivity;
import com.talkermessenger.activity.VectorHomeActivity;
import com.talkermessenger.activity.VectorMediasPickerActivity;
import com.talkermessenger.activity.VectorMediasViewerActivity;
import com.talkermessenger.activity.VectorMemberDetailsActivity;
import com.talkermessenger.activity.VectorPublicRoomsActivity;
import com.talkermessenger.activity.VectorRoomActivity;
import com.talkermessenger.activity.VectorRoomCreationActivity;
import com.talkermessenger.activity.VectorUniversalLinkActivity;
import com.talkermessenger.activity.settings.AboutCooperateActivity;
import com.talkermessenger.activity.settings.ArchivedChatsActivity;
import com.talkermessenger.activity.settings.FavouriteChatsActivity;
import com.talkermessenger.backup.SmartSettingsWorker;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import androidx.work.Constraints;
import androidx.work.NetworkType;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;

/**
 * Util class for managing themes.
 */
public class ThemeUtils {
    // preference key
    public static final String APPLICATION_THEME_KEY = "APPLICATION_THEME_KEY";

    // the theme description
    public static final String THEME_DARK_VALUE = "dark";
    public static final String THEME_LIGHT_VALUE = "light";
    public static final String THEME_LIGHT_BLACK_STATUSBAR_VALUE = "light_black";
    public static final String THEME_LIGHT_LIGHT_BLUE_HEADER_VALUE = "light_light_blue_header";
    public static final String THEME_BLACK_VALUE = "black";

    private static final Map<Integer, Integer> mColorByAttr = new HashMap<>();

    /**
     * Provides the selected application theme
     *
     * @param context the context
     * @return the selected application theme
     */
    public static String getApplicationTheme(Context context) {
        String appTheme = THEME_LIGHT_LIGHT_BLUE_HEADER_VALUE;
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);

        // defines a default value if not defined
        if (!sp.contains(APPLICATION_THEME_KEY)) {
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString(APPLICATION_THEME_KEY, THEME_LIGHT_LIGHT_BLUE_HEADER_VALUE);
            editor.commit();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Constraints constraints = new Constraints.Builder()
                        .setRequiredNetworkType(NetworkType.CONNECTED)
                        .build();

                OneTimeWorkRequest compressionWork =
                        new OneTimeWorkRequest.Builder(SmartSettingsWorker.class)
                                .setConstraints(constraints)
                                .build();
                WorkManager.getInstance().enqueue(compressionWork);
            }
        } else {
            appTheme = sp.getString(APPLICATION_THEME_KEY, THEME_LIGHT_LIGHT_BLUE_HEADER_VALUE);
        }

        return appTheme;
    }

    /**
     * Update the application theme
     *
     * @param aTheme the new theme
     */
    public static void setApplicationTheme(Context context, String aTheme) {
        if (null != aTheme) {
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString(APPLICATION_THEME_KEY, aTheme);
            editor.commit();
        }

        if (TextUtils.equals(aTheme, THEME_LIGHT_VALUE)) {
            VectorApp.getInstance().setTheme(R.style.AppTheme);
        } else if (TextUtils.equals(aTheme, THEME_DARK_VALUE)) {
            VectorApp.getInstance().setTheme(R.style.AppTheme_Dark);
        } else if (TextUtils.equals(aTheme, THEME_BLACK_VALUE)) {
            VectorApp.getInstance().setTheme(R.style.AppTheme_Black);
        } else if (TextUtils.equals(aTheme, THEME_LIGHT_LIGHT_BLUE_HEADER_VALUE)) {
            VectorApp.getInstance().setTheme(R.style.AppTheme_Blue);
        } else if (TextUtils.equals(aTheme, THEME_LIGHT_BLACK_STATUSBAR_VALUE)) {
            VectorApp.getInstance().setTheme(R.style.AppThemeBlackStatusBar);
        }
        mColorByAttr.clear();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Constraints constraints = new Constraints.Builder()
                    .setRequiredNetworkType(NetworkType.CONNECTED)
                    .build();

            OneTimeWorkRequest compressionWork =
                    new OneTimeWorkRequest.Builder(SmartSettingsWorker.class)
                            .setConstraints(constraints)
                            .build();
            WorkManager.getInstance().enqueue(compressionWork);
        }
    }

    /**
     * Set the activity theme according to the selected one.
     *
     * @param activity the activity
     */
    public static void setActivityTheme(Activity activity) {
        if (TextUtils.equals(getApplicationTheme(activity), THEME_DARK_VALUE)) {
            if (activity instanceof FavouriteChatsActivity) {
                activity.setTheme(R.style.AppTheme_Dark);
            } else if (activity instanceof RegisterActivity) {
                activity.setTheme(R.style.AppTheme_Dark);
            } else if (activity instanceof CountryPickerActivity) {
                activity.setTheme(R.style.CountryPickerTheme_Dark);
            } else if (activity instanceof HistoricalRoomsActivity) {
                activity.setTheme(R.style.HomeActivityTheme_Dark);
            } else if (activity instanceof LanguagePickerActivity) {
                activity.setTheme(R.style.CountryPickerTheme_Dark);
            } else if (activity instanceof PhoneNumberAdditionActivity) {
                activity.setTheme(R.style.AppTheme_NoActionBar_Dark);
            } else if (activity instanceof PhoneNumberVerificationActivity) {
                activity.setTheme(R.style.AppTheme_NoActionBar_Dark);
            } else if (activity instanceof RoomDirectoryPickerActivity) {
                activity.setTheme(R.style.DirectoryPickerTheme_Dark);
            } else if (activity instanceof SplashActivity) {
                activity.setTheme(R.style.AppTheme_NoActionBar_Dark);
            } else if (activity instanceof LoggingOutActivity) {
                activity.setTheme(R.style.AppTheme_NoActionBar_Dark);
            } else if (activity instanceof VectorBaseSearchActivity) {
                activity.setTheme(R.style.AppTheme_Dark);
            } else if (activity instanceof VectorCallViewActivity) {
                activity.setTheme(R.style.CallActivityTheme_Dark);
            } else if (activity instanceof VectorHomeActivity) {
                activity.setTheme(R.style.AppTheme_NoActionBar_Dark);
            } else if (activity instanceof VectorMediasPickerActivity) {
                activity.setTheme(R.style.AppTheme_NoActionBar_FullScreen_Dark);
            } else if (activity instanceof VectorMediasViewerActivity) {
                activity.setTheme(R.style.AppTheme_Dark);
            } else if (activity instanceof VectorMemberDetailsActivity) {
                activity.setTheme(R.style.AppTheme_NoActionBar_Dark);
            } else if (activity instanceof VectorPublicRoomsActivity) {
                activity.setTheme(R.style.AppTheme_Dark);
            } else if (activity instanceof VectorRoomActivity) {
                activity.setTheme(R.style.AppTheme_NoActionBar_Dark);
            } else if (activity instanceof VectorRoomCreationActivity) {
                activity.setTheme(R.style.AppTheme_Dark);
            } else if (activity instanceof VectorUniversalLinkActivity) {
                activity.setTheme(R.style.AppTheme_Dark);
            } else if (activity instanceof ArchivedChatsActivity) {
                activity.setTheme(R.style.AppTheme_Dark);
            } else if (activity instanceof AboutCooperateActivity) {
                activity.setTheme(R.style.AppTheme_Dark);
            } else activity.setTheme(R.style.AppTheme_NoActionBar_Dark);
        }

        if (TextUtils.equals(getApplicationTheme(activity), THEME_BLACK_VALUE)) {
            if (activity instanceof FavouriteChatsActivity) {
                activity.setTheme(R.style.AppTheme_Black);
            } else if (activity instanceof RegisterActivity) {
                activity.setTheme(R.style.AppTheme_Black);
            } else if (activity instanceof CountryPickerActivity) {
                activity.setTheme(R.style.CountryPickerTheme_Black);
            } else if (activity instanceof HistoricalRoomsActivity) {
                activity.setTheme(R.style.AppTheme_NoActionBar_Black);
            } else if (activity instanceof LanguagePickerActivity) {
                activity.setTheme(R.style.CountryPickerTheme_Black);
            } else if (activity instanceof PhoneNumberAdditionActivity) {
                activity.setTheme(R.style.AppTheme_NoActionBar_Black);
            } else if (activity instanceof PhoneNumberVerificationActivity) {
                activity.setTheme(R.style.AppTheme_NoActionBar_Black);
            } else if (activity instanceof RoomDirectoryPickerActivity) {
                activity.setTheme(R.style.DirectoryPickerTheme_Black);
            } else if (activity instanceof SplashActivity) {
                activity.setTheme(R.style.AppTheme_NoActionBar_Black);
            } else if (activity instanceof LoggingOutActivity) {
                activity.setTheme(R.style.AppTheme_NoActionBar_Black);
            } else if (activity instanceof VectorBaseSearchActivity) {
                activity.setTheme(R.style.AppTheme_Black);
            } else if (activity instanceof VectorCallViewActivity) {
                activity.setTheme(R.style.CallActivityTheme_Black);
            } else if (activity instanceof VectorHomeActivity) {
                activity.setTheme(R.style.AppTheme_NoActionBar_Black);
            } else if (activity instanceof VectorMediasPickerActivity) {
                activity.setTheme(R.style.AppTheme_NoActionBar_FullScreen_Black);
            } else if (activity instanceof VectorMediasViewerActivity) {
                activity.setTheme(R.style.AppTheme_Black);
            } else if (activity instanceof VectorMemberDetailsActivity) {
                activity.setTheme(R.style.AppTheme_NoActionBar_Black);
            } else if (activity instanceof VectorPublicRoomsActivity) {
                activity.setTheme(R.style.AppTheme_Black);
            } else if (activity instanceof VectorRoomActivity) {
                activity.setTheme(R.style.AppTheme_NoActionBar_Black);
            } else if (activity instanceof VectorRoomCreationActivity) {
                activity.setTheme(R.style.AppTheme_Black);
            } else if (activity instanceof VectorUniversalLinkActivity) {
                activity.setTheme(R.style.AppTheme_Black);
            } else if (activity instanceof ArchivedChatsActivity) {
                activity.setTheme(R.style.AppTheme_Black);
            } else if (activity instanceof AboutCooperateActivity) {
                activity.setTheme(R.style.AppTheme_Black);
            } else activity.setTheme(R.style.AppTheme_NoActionBar_Black);
        }
        if (TextUtils.equals(getApplicationTheme(activity), THEME_LIGHT_VALUE)) {
            // Specific quirk for quick reply screen
            if (activity instanceof FavouriteChatsActivity) {
                activity.setTheme(R.style.AppTheme);
            } else if (activity instanceof VectorMediasViewerActivity) {
                activity.setTheme(R.style.AppTheme);
            } else if (activity instanceof AboutCooperateActivity) {
                activity.setTheme(R.style.AppTheme);
            } else if (activity instanceof VectorBaseSearchActivity) {
                activity.setTheme(R.style.AppTheme);
            } else if (activity instanceof ArchivedChatsActivity) {
                activity.setTheme(R.style.AppTheme);
            } else if (activity instanceof VectorRoomCreationActivity) {
                activity.setTheme(R.style.AppTheme);
            } else activity.setTheme(R.style.AppTheme_NoActionBar);
        }
        if (TextUtils.equals(getApplicationTheme(activity), THEME_LIGHT_LIGHT_BLUE_HEADER_VALUE)) {
            // Specific quirk for quick reply screen
            if (activity instanceof FavouriteChatsActivity) {
                activity.setTheme(R.style.AppTheme_Blue);
            } else if (activity instanceof AboutCooperateActivity) {
                activity.setTheme(R.style.AppTheme_Blue);
            } else if (activity instanceof VectorMediasViewerActivity) {
                activity.setTheme(R.style.AppTheme_Blue);
            } else if (activity instanceof VectorBaseSearchActivity) {
                activity.setTheme(R.style.AppTheme_Blue);
            } else if (activity instanceof ArchivedChatsActivity) {
                activity.setTheme(R.style.AppTheme_Blue);
            } else if (activity instanceof VectorRoomCreationActivity) {
                activity.setTheme(R.style.AppTheme_Blue);
            } else activity.setTheme(R.style.AppTheme_Blue_noActionBar);
        }
        mColorByAttr.clear();
    }

    /**
     * Translates color attributes to colors
     *
     * @param c              Context
     * @param colorAttribute Color Attribute
     * @return Requested Color
     */
    public static
    @ColorInt
    int getColor(Context c, @AttrRes final int colorAttribute) {
        if (mColorByAttr.containsKey(colorAttribute)) {
            return mColorByAttr.get(colorAttribute);
        }

        int matchedColor;

        try {
            TypedValue color = new TypedValue();
            c.getTheme().resolveAttribute(colorAttribute, color, true);
            matchedColor = color.data;
        } catch (Exception e) {
            matchedColor = ContextCompat.getColor(c, android.R.color.holo_red_dark);
        }

        mColorByAttr.put(colorAttribute, matchedColor);

        return matchedColor;
    }

    /**
     * Get the resource Id applied to the current theme
     *
     * @param c          the context
     * @param resourceId the resource id
     * @return the resource Id for the current theme
     */
    public static int getResourceId(Context c, int resourceId) {
        if (TextUtils.equals(getApplicationTheme(c), THEME_DARK_VALUE)) {

            if (resourceId == R.drawable.line_divider_light) {
                return R.drawable.line_divider_dark;
            }
        }
        return resourceId;
    }

    public static void setTransitionImage(Context context, String imageName, ImageView transition){
        File baseFolderFile = new File(context.getApplicationContext().getFilesDir(), "SmartSettings");
        if (!baseFolderFile.exists())
            baseFolderFile.mkdirs();
        File file = new File(baseFolderFile.getPath() + "/" + imageName+".png");
        if (file.exists())
            GlideApp.with(context).load(file).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(transition);
    }
}
