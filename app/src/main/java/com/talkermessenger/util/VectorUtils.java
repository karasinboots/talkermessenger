/*
 * Copyright 2016 OpenMarket Ltd
 * Copyright 2017 Vector Creations Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.talkermessenger.util;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.v4.util.LruCache;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewParent;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.Toast;

import com.iskar.talkersdk.ContactHandler;
import com.iskar.talkersdk.model.TalkerContact;
import com.iskar.talkersdk.ui.languageRecycler.Languages;
import com.talkermessenger.GlideApp;
import com.talkermessenger.R;
import com.talkermessenger.VectorApp;
import com.talkermessenger.activity.CommonActivityUtils;
import com.talkermessenger.adapters.ParticipantAdapterItem;
import com.talkermessenger.adapters.VectorMessagesAdapterHelper;

import org.matrix.androidsdk.MXSession;
import org.matrix.androidsdk.call.MXCallsManager;
import org.matrix.androidsdk.data.Room;
import org.matrix.androidsdk.data.RoomState;
import org.matrix.androidsdk.data.RoomTag;
import org.matrix.androidsdk.db.MXMediasCache;
import org.matrix.androidsdk.rest.callback.ApiCallback;
import org.matrix.androidsdk.rest.callback.SimpleApiCallback;
import org.matrix.androidsdk.rest.model.MatrixError;
import org.matrix.androidsdk.rest.model.PublicRoom;
import org.matrix.androidsdk.rest.model.RoomMember;
import org.matrix.androidsdk.rest.model.User;
import org.matrix.androidsdk.util.ImageUtils;
import org.matrix.androidsdk.util.Log;
import org.matrix.androidsdk.util.ResourceUtils;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import de.hdodenhof.circleimageview.CircleImageView;

public class VectorUtils {

    private static final String LOG_TAG = VectorUtils.class.getSimpleName();

    //public static final int REQUEST_FILES = 0;
    public static final int TAKE_IMAGE = 1;

    //==============================================================================================================
    // permalink methods
    //==============================================================================================================

    /**
     * Provides a permalink for a room id and an eventId.
     * The eventId is optional.
     *
     * @param roomIdOrAlias the room id or alias.
     * @param eventId       the event id (optional)
     * @return the permalink
     */
    public static String getPermalink(String roomIdOrAlias, String eventId) {
        if (TextUtils.isEmpty(roomIdOrAlias)) {
            return null;
        }

        String link = "https://matrix.to/#/" + roomIdOrAlias;

        if (!TextUtils.isEmpty(eventId)) {
            link += "/" + eventId;
        }

        // the $ character is not as a part of an url so escape it.
        return link.replace("$", "%24");
    }

    //==============================================================================================================
    // Clipboard helper
    //==============================================================================================================

    /**
     * Copy a text to the clipboard.
     *
     * @param context the context
     * @param text    the text to copy
     */
    public static void copyToClipboard(Context context, CharSequence text) {
        ClipboardManager clipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        clipboard.setPrimaryClip(ClipData.newPlainText("", text));
        Toast.makeText(context, context.getString(R.string.copied_to_clipboard), Toast.LENGTH_SHORT).show();
    }

    //==============================================================================================================
    // Rooms methods
    //==============================================================================================================

    /**
     * Returns the public room display name..
     *
     * @param publicRoom the public room.
     * @return the room display name.
     */
    public static String getPublicRoomDisplayName(PublicRoom publicRoom) {
        String displayName = publicRoom.name;

        if (TextUtils.isEmpty(displayName)) {
            if (publicRoom.getAliases().size() > 0) {
                displayName = publicRoom.getAliases().get(0);
            } else {
                displayName = publicRoom.roomId;
            }
        } else if (!displayName.startsWith("#") && (0 < publicRoom.getAliases().size())) {
            displayName = displayName + " (" + publicRoom.getAliases().get(0) + ")";
        }

        return displayName;
    }

    /**
     * Provide a display name for a calling room
     *
     * @param context the application context.
     * @param session the room session.
     * @param room    the room.
     * @return the calling room display name.
     */
    public static String getCallingRoomDisplayName(Context context, MXSession session, Room room) {
        if ((null == context) || (null == session) || (null == room)) {
            return null;
        }

        Collection<RoomMember> roomMembers = room.getJoinedMembers();

        if (2 == roomMembers.size()) {
            ArrayList<RoomMember> roomMembersList = new ArrayList<>(roomMembers);

            if (TextUtils.equals(roomMembersList.get(0).getUserId(), session.getMyUserId())) {
                return room.getLiveState().getMemberName(roomMembersList.get(1).getUserId());
            } else {
                return room.getLiveState().getMemberName(roomMembersList.get(0).getUserId());
            }
        } else {
            return getRoomDisplayName(context, session, room);
        }
    }

    /**
     * Vector client formats the room display with a different manner than the SDK one.
     *
     * @param context the application context.
     * @param session the room session.
     * @param room    the room.
     * @return the room display name.
     */
    public static String getRoomDisplayName(Context context, MXSession session, Room room) {
        // sanity checks
        if (null == room) {
            return null;
        }

        try {

            // this algorithm is the one defined in
            // https://github.com/matrix-org/matrix-js-sdk/blob/develop/lib/models/room.js#L617
            // calculateRoomName(room, userId)

            RoomState roomState = room.getLiveState();

            if (!TextUtils.isEmpty(roomState.name)) {
                return roomState.name;
            }

            String alias = roomState.alias;

            if (TextUtils.isEmpty(alias) && (roomState.getAliases().size() > 0)) {
                alias = roomState.getAliases().get(0);
            }

            if (!TextUtils.isEmpty(alias)) {
                return alias;
            }

            String myUserId = session.getMyUserId();

            Collection<RoomMember> members = roomState.getDisplayableMembers();
            ArrayList<RoomMember> othersActiveMembers = new ArrayList<>();
            ArrayList<RoomMember> activeMembers = new ArrayList<>();

            for (RoomMember member : members) {
                if (!TextUtils.equals(member.membership, RoomMember.MEMBERSHIP_LEAVE)) {
                    if (!TextUtils.equals(member.getUserId(), myUserId)) {
                        othersActiveMembers.add(member);
                    }
                    activeMembers.add(member);
                }
            }

            Collections.sort(othersActiveMembers, new Comparator<RoomMember>() {
                @Override
                public int compare(RoomMember m1, RoomMember m2) {
                    long diff = m1.getOriginServerTs() - m2.getOriginServerTs();

                    return (diff == 0) ? 0 : ((diff < 0) ? -1 : +1);
                }
            });

            String displayName = "";

            if (othersActiveMembers.size() == 0) {
                if (activeMembers.size() == 1) {
                    RoomMember member = activeMembers.get(0);

                    if (TextUtils.equals(member.membership, RoomMember.MEMBERSHIP_INVITE)) {

                        if (!TextUtils.isEmpty(member.getUserId())) {
                            // extract who invited us to the room
                            displayName = VectorMessagesAdapterHelper.getUserDisplayName(member.getUserId(), session, context);
                        } else {
                            displayName = context.getString(R.string.room_displayname_room_invite);
                        }
                    } else {
                        displayName = context.getString(R.string.room_displayname_no_title);
                    }
                } else {
                    displayName = context.getString(R.string.room_displayname_no_title);
                }
            } else if (othersActiveMembers.size() == 1) {
                RoomMember member = othersActiveMembers.get(0);
                displayName = VectorMessagesAdapterHelper.getUserDisplayName(member.getUserId(), session, context);
            } else if (othersActiveMembers.size() == 2) {
                RoomMember member1 = othersActiveMembers.get(0);
                RoomMember member2 = othersActiveMembers.get(1);

                displayName = context.getString(R.string.room_displayname_two_members, roomState.getMemberName(member1.getUserId()), roomState.getMemberName(member2.getUserId()));
            } else {
                RoomMember member = othersActiveMembers.get(0);
                displayName = context.getString(R.string.room_displayname_more_than_two_members, roomState.getMemberName(member.getUserId()), othersActiveMembers.size() - 1);
            }

            return displayName;
        } catch (Exception e) {
            Log.e(LOG_TAG, "## getRoomDisplayName() failed " + e.getMessage());
        }

        return room.getRoomId();
    }
    //==============================================================================================================
    // Avatars generation
    //==============================================================================================================

    // avatars cache
    static final private LruCache<String, Bitmap> mAvatarImageByKeyDict = new LruCache<>(20 * 1024 * 1024);
    // the avatars background color
    static final private ArrayList<Integer> mColorList = new ArrayList<>(Arrays.asList(0xff76cfa6, 0xff50e2c2, 0xfff4c371));

    /**
     * Provides the avatar background color from a text.
     *
     * @param text the text.
     * @return the color.
     */
    public static int getAvatarColor(String text) {
        long colorIndex = 0;

        if (!TextUtils.isEmpty(text)) {
            long sum = 0;

            for (int i = 0; i < text.length(); i++) {
                sum += text.charAt(i);
            }

            colorIndex = sum % mColorList.size();
        }

        return mColorList.get((int) colorIndex);
    }

    /**
     * Create a thumbnail avatar.
     *
     * @param context         the context
     * @param backgroundColor the background color
     * @param text            the text to display.
     * @return the generated bitmap
     */
    private static Bitmap createAvatarThumbnail(Context context, int backgroundColor, String text) {
        float densityScale = context.getResources().getDisplayMetrics().density;
        // the avatar size is 42dp, convert it in pixels.
        return createAvatar(backgroundColor, text, (int) (42 * densityScale));
    }

    /**
     * Create an avatar bitmap from a text.
     *
     * @param backgroundColor the background color.
     * @param text            the text to display.
     * @param pixelsSide      the avatar side in pixels
     * @return the generated bitmap
     */
    private static Bitmap createAvatar(int backgroundColor, String text, int pixelsSide) {
        android.graphics.Bitmap.Config bitmapConfig = android.graphics.Bitmap.Config.ARGB_8888;

        Bitmap bitmap = Bitmap.createBitmap(pixelsSide, pixelsSide, bitmapConfig);
        Canvas canvas = new Canvas(bitmap);

        canvas.drawColor(backgroundColor);

        // prepare the text drawing
        Paint textPaint = new Paint();
        textPaint.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));
        textPaint.setColor(Color.WHITE);
        // the text size is proportional to the avatar size.
        // by default, the avatar size is 42dp, the text size is 28 dp (not sp because it has to be fixed).
        textPaint.setTextSize(pixelsSide * 2 / 3);

        // get its size
        Rect textBounds = new Rect();
        textPaint.getTextBounds(text, 0, text.length(), textBounds);

        // draw the text in center
        canvas.drawText(text, (canvas.getWidth() - textBounds.width() - textBounds.left) / 2, (canvas.getHeight() + textBounds.height() - textBounds.bottom) / 2, textPaint);

        // Return the avatar
        return bitmap;
    }

    /**
     * Return the char to display for a name
     *
     * @param name the name
     * @return teh first char
     */
    private static String getInitialLetter(String name) {
        String firstChar = " ";

        if (!TextUtils.isEmpty(name)) {
            int idx = 0;
            char initial = name.charAt(idx);

            if ((initial == '@' || initial == '#') && (name.length() > 1)) {
                idx++;
            }

            // string.codePointAt(0) would do this, but that isn't supported by
            // some browsers (notably PhantomJS).
            int chars = 1;
            char first = name.charAt(idx);

            // LEFT-TO-RIGHT MARK
            if ((name.length() >= 2) && (0x200e == first)) {
                idx++;
                first = name.charAt(idx);
            }

            // check if it’s the start of a surrogate pair
            if (first >= 0xD800 && first <= 0xDBFF && (name.length() > (idx + 1))) {
                char second = name.charAt(idx + 1);
                if (second >= 0xDC00 && second <= 0xDFFF) {
                    chars++;
                }
            }

            firstChar = name.substring(idx, idx + chars);
        }

        return firstChar.toUpperCase();
    }

    /**
     * Returns an avatar from a text.
     *
     * @param context the context.
     * @param aText   the text.
     * @param create  create the avatar if it does not exist
     * @return the avatar.
     */
    public static Bitmap getAvatar(Context context, int backgroundColor, String aText, boolean create) {
        String firstChar = getInitialLetter(aText);
        String key = firstChar + "_" + backgroundColor;

        // check if the avatar is already defined
        Bitmap thumbnail = mAvatarImageByKeyDict.get(key);

        if ((null == thumbnail) && create) {
            thumbnail = VectorUtils.createAvatarThumbnail(context, backgroundColor, firstChar);
            mAvatarImageByKeyDict.put(key, thumbnail);
        }

        return thumbnail;
    }

    /**
     * Set the default vector avatar for a member.
     *
     * @param imageView   the imageView to set.
     * @param userId      the member userId.
     * @param displayName the member display name.
     */
    private static void setDefaultMemberAvatar(final ImageView imageView, final String userId, final String displayName) {
        // sanity checks
        if (null != imageView && !TextUtils.isEmpty(userId)) {
            final Bitmap bitmap = VectorUtils.getAvatar(imageView.getContext(), VectorUtils.getAvatarColor(userId), TextUtils.isEmpty(displayName) ? userId : displayName, true);

            if (Looper.getMainLooper().getThread() == Thread.currentThread()) {
                imageView.setImageBitmap(bitmap);
            } else {
                final String tag = userId + " - " + displayName;
                imageView.setTag(tag);

                mUIHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (TextUtils.equals(tag, (String) imageView.getTag())) {
                            imageView.setImageBitmap(bitmap);
                        }
                    }
                });
            }
        }
    }

    /**
     * Set the room avatar in an imageView.
     *
     * @param context   the context
     * @param session   the session
     * @param imageView the image view
     * @param room      the room
     */
    public static void loadRoomAvatar(Context context, MXSession session, ImageView imageView, Room room) {
        if (null != room) {
            String memberId = null;
            String avatarUrl = null;
            if (!CommonActivityUtils.isGroupChat(room)) {
                for (RoomMember member : room.getMembers()) {
                    if (!member.getUserId().equals(session.getMyUserId())) {
                        memberId = member.getUserId();
                        avatarUrl = member.getAvatarUrl();
                        VectorUtils.loadUserAvatar(context, session, imageView, avatarUrl, memberId);
                    }
                }
            } else {
                avatarUrl = room.getAvatarUrl();
                String topic = room.getStore().getSummary(room.getRoomId()).getRoomTopic();
                if (avatarUrl != null && !avatarUrl.equals("")) {
                    GlideApp.with(context).load(session.getMediasCache().getDownloadableUrl(avatarUrl)).into(imageView);
                } else {
                    if (topic != null && topic.equals(RoomTag.ROOM_TAG_IM)) {
                        GlideApp.with(context).load(context.getResources().getDrawable(R.drawable.planet)).into(imageView);
                    } else
                        GlideApp.with(context).load(context.getResources().getDrawable(R.drawable.men)).into(imageView);
                }
            }
        }
    }

    /**
     * Set the room avatar in an imageView.
     *
     * @param context   the context
     * @param session   the session
     * @param imageView the image view
     * @param room      the room
     */
    public static void loadListRoomAvatar(Context context, MXSession session, ImageView imageView, Room room) {
        if (null != room) {
            String memberId = null;
            String avatarUrl = null;
            if (!CommonActivityUtils.isGroupChat(room)) {
                for (RoomMember member : room.getMembers()) {
                    if (!member.getUserId().equals(session.getMyUserId())) {
                        memberId = member.getUserId();
                        avatarUrl = member.getAvatarUrl();
                        VectorUtils.loadUserAvatar(context, session, imageView, avatarUrl, memberId);
                    }
                }
            } else {
                avatarUrl = room.getAvatarUrl();
                String topic = room.getStore().getSummary(room.getRoomId()).getRoomTopic();
                if (avatarUrl != null && !avatarUrl.equals("")) {
                    GlideApp.with(context).load(session.getMediasCache().getDownloadableUrl(avatarUrl)).into(imageView);
                } else {
                    if (topic != null && topic.equals(RoomTag.ROOM_TAG_IM)) {
                        GlideApp.with(context).load(context.getResources().getDrawable(R.drawable.planet_room)).into(imageView);
                    } else
                        GlideApp.with(context).load(context.getResources().getDrawable(R.drawable.men_room)).into(imageView);
                }
            }
        }
    }

    public static void loadRoomAvatarForHeader(Context context, MXSession session, ImageView imageView, Room room) {
        if (null != room) {
            String memberId = null;
            String avatarUrl = null;
            //  boolean dummy=true;
            if (!CommonActivityUtils.isGroupChat(room)) {
                for (RoomMember member : room.getMembers()) {
                    if (!member.getUserId().equals(session.getMyUserId())) {
                        memberId = member.getUserId();
                        avatarUrl = member.getAvatarUrl();
                        VectorUtils.loadUserAvatarForHeader(context, session, imageView, avatarUrl, memberId);
                    }
                }
            } else {
                avatarUrl = room.getAvatarUrl();
                String topic = room.getStore().getSummary(room.getRoomId()).getRoomTopic();
                if (avatarUrl != null && !avatarUrl.equals("")) {
                    GlideApp.with(context).load(session.getMediasCache().getDownloadableUrl(avatarUrl)).into(imageView);
                } else {
                    if (topic != null && topic.equals(RoomTag.ROOM_TAG_IM)) {
                        GlideApp.with(context).load(context.getResources().getDrawable(R.drawable.planet)).into(imageView);
                    } else
                        GlideApp.with(context).load(context.getResources().getDrawable(R.drawable.men)).into(imageView);
                    imageView.setColorFilter(ThemeUtils.getColor(context, R.attr.actionbar_text_color));
                }
            }
        }
    }

    public static void loadUserAvatarForHeader(final Context context, final MXSession session, final ImageView imageView, final String avatarUrl, final String userId) {
        // sanity check
        if ((null == session) || (null == imageView) || !session.isAlive() || userId == null) {
            return;
        }
        GlideApp.with(context).load(context.getResources().getDrawable(R.drawable.man2)).into(imageView);
        if (avatarUrl != null && !avatarUrl.equals("")) {
            GlideApp.with(context).load(session.getMediasCache().getDownloadableUrl(avatarUrl)).into(imageView);
            return;
        } else {
            TalkerContact contact = new ContactHandler().getContactFromTalkerContactSnapshot(userId, context);
            if (contact != null && contact.getThumbnailUri() != null) {
                GlideApp.with(context).load(contact.getThumbnailUri()).into(imageView);
                return;
            }
        }
        imageView.setColorFilter(ThemeUtils.getColor(context, R.attr.actionbar_text_color));
    }

    /**
     * Set the call avatar in an imageView.
     *
     * @param context   the context
     * @param session   the session
     * @param imageView the image view
     * @param room      the room
     */
    public static void loadCallAvatar(Context context, MXSession session, ImageView imageView, Room room) {
        // sanity check
        if ((null != room) && (null != session) && (null != imageView) && session.isAlive()) {
            // reset the imageView tag
            imageView.setTag(null);

            String callAvatarUrl = room.getCallAvatarUrl();
            String roomId = room.getRoomId();
            String displayName = VectorUtils.getRoomDisplayName(context, session, room);
            int pixelsSide = imageView.getLayoutParams().width;

            // when size < 0, it means that the render graph must compute it
            // so, we search the valid parent view with valid size
            if (pixelsSide < 0) {
                ViewParent parent = imageView.getParent();

                while ((pixelsSide < 0) && (null != parent)) {
                    if (parent instanceof View) {
                        View parentAsView = (View) parent;
                        pixelsSide = parentAsView.getLayoutParams().width;
                    }
                    parent = parent.getParent();
                }
            }

            // if the avatar is already cached, use it
            if (session.getMediasCache().isAvatarThumbnailCached(callAvatarUrl, context.getResources().getDimensionPixelSize(R.dimen.profile_avatar_size))) {
                session.getMediasCache().loadAvatarThumbnail(session.getHomeServerConfig(), imageView, callAvatarUrl, context.getResources().getDimensionPixelSize(R.dimen.profile_avatar_size));
            } else {
                Bitmap bitmap = null;

                if (pixelsSide > 0) {
                    // get the avatar bitmap.
                    bitmap = VectorUtils.createAvatar(VectorUtils.getAvatarColor(roomId), getInitialLetter(displayName), pixelsSide);
                }

                // until the dedicated avatar is loaded.
                session.getMediasCache().loadAvatarThumbnail(session.getHomeServerConfig(), imageView, callAvatarUrl, context.getResources().getDimensionPixelSize(R.dimen.profile_avatar_size), bitmap);
            }
        }
    }

    /**
     * Set the room member avatar in an imageView.
     *
     * @param context    the context
     * @param session    the session
     * @param imageView  the image view
     * @param roomMember the room member
     */
    public static void loadRoomMemberAvatar(Context context, MXSession session, CircleImageView imageView, RoomMember roomMember) {
        if (null != roomMember) {
            VectorUtils.loadUserAvatar(context, session, imageView, roomMember.getAvatarUrl(), roomMember.getUserId());
        }
    }

    /**
     * Set the user avatar in an imageView.
     *
     * @param context   the context
     * @param session   the session
     * @param imageView the image view
     * @param user      the user
     */
    public static void loadUserAvatar(Context context, MXSession session, ImageView imageView, User user) {
        if (null != user) {
            VectorUtils.loadUserAvatar(context, session, imageView, user.getAvatarUrl(), user.user_id);
        } else
            GlideApp.with(context).load(context.getResources().getDrawable(R.drawable.man2)).into(imageView);
    }

    /**
     * Set the user avatar in an imageView.
     *
     * @param context   the context
     * @param session   the session
     * @param imageView the image view
     * @param user      the user
     */
    public static void loadContactAvatar(Context context, MXSession session, ImageView imageView, User user) {
        if (null != user) {
            VectorUtils.loadUserAvatar(context, session, imageView, user.getAvatarUrl(), user.user_id);
        }
    }

    // the background thread
    private static HandlerThread mImagesThread = null;
    private static android.os.Handler mImagesThreadHandler = null;
    private static Handler mUIHandler = null;

    /**
     * Set the user avatar in an imageView.
     *
     * @param context   the context
     * @param session   the session
     * @param imageView the image view
     * @param avatarUrl the avatar url
     * @param userId    the user id
     */
    public static void loadUserAvatar(final Context context, final MXSession session, final ImageView imageView, final String avatarUrl, final String userId) {
        // sanity check
        if ((null == session) || (null == imageView) || !session.isAlive() || userId == null) {
            return;
        }
        GlideApp.with(context).load(context.getResources().getDrawable(R.drawable.man2)).into(imageView);
        if (avatarUrl != null && !avatarUrl.equals("")) {
            GlideApp.with(context).load(session.getMediasCache().getDownloadableUrl(avatarUrl)).into(imageView);
        } else {
            TalkerContact contact = new ContactHandler().getContactFromTalkerContactSnapshot(userId, context);
            if (contact != null && contact.getThumbnailUri() != null) {
                GlideApp.with(context).load(contact.getThumbnailUri()).into(imageView);
            }
        }
    }

    /**
     * Set the user avatar in an imageView.
     *
     * @param context   the context
     * @param session   the session
     * @param imageView the image view
     * @param avatarUrl the avatar url
     * @param userId    the user id
     */
    public static void loadProfileAvatar(final Context context, final MXSession session, final ImageView imageView, final String avatarUrl, final String userId) {
        // sanity check
        if ((null == session) || (null == imageView) || !session.isAlive()) {
            return;
        }
        GlideApp.with(context).load(context.getResources().getDrawable(R.drawable.man_profile)).into(imageView);
        if (avatarUrl != null && !avatarUrl.equals("")) {
            GlideApp.with(context).load(session.getMediasCache().getDownloadableUrl(avatarUrl)).into(imageView);
        } else {
            TalkerContact contact = new ContactHandler().getContactFromTalkerContactSnapshot(userId, context);
            if (contact != null && contact.getPhotoUri() != null) {
                GlideApp.with(context).load(contact.getPhotoUri()).into(imageView);
            }
        }
    }


//==============================================================================================================
// About / terms and conditions
//==============================================================================================================

    private static AlertDialog mMainAboutDialog = null;

    /**
     * Provide the application version
     *
     * @param context the application context
     * @return the version. an empty string is not found.
     */
    public static String getApplicationVersion(final Context context) {
        return com.talkermessenger.Matrix.getInstance(context).getVersion(false, true);
    }

    /**
     * Display the licenses text.
     */
    public static void displayThirdPartyLicenses() {
        final Activity activity = VectorApp.getCurrentActivity();

        if (null != activity) {
            if (null != mMainAboutDialog) {
                if (mMainAboutDialog.isShowing() && (null != mMainAboutDialog.getOwnerActivity())) {
                    try {
                        mMainAboutDialog.dismiss();
                    } catch (Exception e) {
                        Log.e(LOG_TAG, "## displayThirdPartyLicenses() : " + e.getMessage());
                    }
                }
                mMainAboutDialog = null;
            }

            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    WebView view = (WebView) LayoutInflater.from(activity).inflate(R.layout.dialog_licenses, null);
                    view.loadUrl("file:///android_asset/open_source_licenses.html");

                    View titleView = LayoutInflater.from(activity).inflate(R.layout.dialog_licenses_header, null);

                    view.setScrollbarFadingEnabled(false);
                    mMainAboutDialog = new AlertDialog.Builder(activity)
                            .setCustomTitle(titleView)
                            .setView(view)
                            .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    mMainAboutDialog = null;
                                }
                            })
                            .setOnCancelListener(new DialogInterface.OnCancelListener() {
                                @Override
                                public void onCancel(DialogInterface dialog) {
                                    mMainAboutDialog = null;
                                }
                            })
                            .create();

                    mMainAboutDialog.show();
                }
            });
        }
    }

    /**
     * Open a webview above the current activity.
     *
     * @param context the application context
     * @param url     the url to open
     */
    private static void displayInWebview(final Context context, String url) {
        AlertDialog.Builder alert = new AlertDialog.Builder(context);

        WebView wv = new WebView(context);
        wv.loadUrl(url);
        wv.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);

                return true;
            }
        });

        alert.setView(wv);
        alert.setPositiveButton(android.R.string.ok, null);
        alert.show();
    }

    /**
     * Display the term and conditions.
     */
    public static void displayAppTac() {
        if (null != VectorApp.getCurrentActivity()) {
            displayInWebview(VectorApp.getCurrentActivity(), "https://riot.im/tac");
        }
    }

    /**
     * Display the copyright.
     */
    public static void displayAppCopyright() {
        if (null != VectorApp.getCurrentActivity()) {
            displayInWebview(VectorApp.getCurrentActivity(), "https://riot.im/copyright");
        }
    }

    /**
     * Display the privacy policy.
     */
    public static void displayAppPrivacyPolicy() {
        if (null != VectorApp.getCurrentActivity()) {
            displayInWebview(VectorApp.getCurrentActivity(), "https://riot.im/privacy");
        }
    }

//==============================================================================================================
// List uris from intent
//==============================================================================================================

    /**
     * Return a selected bitmap from an intent.
     *
     * @param intent the intent
     * @return the bitmap uri
     */
    @SuppressLint("NewApi")
    public static Uri getThumbnailUriFromIntent(Context context,
                                                final Intent intent, MXMediasCache mediasCache) {
        // sanity check
        if ((null != intent) && (null != context) && (null != mediasCache)) {
            Uri thumbnailUri = null;
            ClipData clipData = null;

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
                clipData = intent.getClipData();
            }

            // multiple data
            if (null != clipData) {
                if (clipData.getItemCount() > 0) {
                    thumbnailUri = clipData.getItemAt(0).getUri();
                }
            } else if (null != intent.getData()) {
                thumbnailUri = intent.getData();
            }

            if (null != thumbnailUri) {
                try {
                    ResourceUtils.Resource resource = ResourceUtils.openResource(context, thumbnailUri, null);

                    // sanity check
                    if ((null != resource) && resource.isJpegResource()) {
                        InputStream stream = resource.mContentStream;
                        int rotationAngle = ImageUtils.getRotationAngleForBitmap(context, thumbnailUri);

                        Log.d(LOG_TAG, "## getThumbnailUriFromIntent() :  " + thumbnailUri + " rotationAngle " + rotationAngle);

                        String mediaUrl = ImageUtils.scaleAndRotateImage(context, stream, resource.mMimeType, 1024, rotationAngle, mediasCache);
                        thumbnailUri = Uri.parse(mediaUrl);
                    } else if (null != resource) {
                        Log.d(LOG_TAG, "## getThumbnailUriFromIntent() : cannot manage " + thumbnailUri + " mMimeType " + resource.mMimeType);
                    } else {
                        Log.d(LOG_TAG, "## getThumbnailUriFromIntent() : cannot manage " + thumbnailUri + " --> cannot open the dedicated file");
                    }

                    return thumbnailUri;
                } catch (Exception e) {
                    Log.e(LOG_TAG, "## getThumbnailUriFromIntent failed " + e.getMessage());
                }
            }
        }

        return null;
    }

//==============================================================================================================
// User presence
//==============================================================================================================

    /**
     * Format a time interval in seconds to a string
     *
     * @param context         the context.
     * @param secondsInterval the time interval.
     * @return the formatted string
     */
    private static String formatSecondsIntervalFloored(Context context,
                                                       long secondsInterval) {
        String formattedString;

        if (secondsInterval < 0) {
//            formattedString = "0" + context.getResources().getString(R.string.format_time_s);
            formattedString = "";
        } else {
            if (secondsInterval < 60) {
//                formattedString = secondsInterval + " " + context.getResources().getString(R.string.format_time_s);
                formattedString = context.getResources().getQuantityString(R.plurals.format_time_s, (int) secondsInterval, (int) secondsInterval);
            } else if (secondsInterval < 3600) {
                int count = (int) (secondsInterval / 60);
//                formattedString = count + " " + context.getResources().getString(R.string.format_time_m);
                formattedString = context.getResources().getQuantityString(R.plurals.format_time_m, count, count);
            } else if (secondsInterval < 86400) {
                int hours = (int) (secondsInterval / 3600);
//                formattedString = hours + " " + context.getResources().getString(R.string.format_time_h);
                formattedString = context.getResources().getQuantityString(R.plurals.format_time_h, hours, hours);

            } else {
                int days = (int) (secondsInterval / 86400);
//                formattedString = days + " " + context.getResources().getString(R.string.format_time_d);
                formattedString = context.getResources().getQuantityString(R.plurals.format_time_d, days, days);
            }
        }

        return formattedString;
    }

    /**
     * Provide the user online status from his user Id.
     * if refreshCallback is set, try to refresh the user presence if it is not known
     *
     * @param context         the context.
     * @param session         the session.
     * @param userId          the userId.
     * @param refreshCallback the presence callback.
     * @return the online status description.
     */
    public static String getUserOnlineStatus(final Context context,
                                             final MXSession session, final String userId,
                                             final SimpleApiCallback<Void> refreshCallback) {
        // sanity checks
        if ((null == session) || (null == userId)) {
            return null;
        }

        final User user = session.getDataHandler().getStore().getUser(userId);

        // refresh the presence with this conditions
        boolean triggerRefresh = (null == user) || user.isPresenceObsolete();

        if ((null != refreshCallback) && triggerRefresh) {
            Log.d(LOG_TAG, "Get the user presence : " + userId);

            final String fPresence = (null != user) ? user.presence : null;

            session.refreshUserPresence(userId, new ApiCallback<Void>() {
                @Override
                public void onSuccess(Void info) {
                    boolean isUpdated = false;
                    User updatedUser = session.getDataHandler().getStore().getUser(userId);

                    // don't find any info for the user
                    if ((null == user) && (null == updatedUser)) {
                        Log.d(LOG_TAG, "Don't find any presence info of " + userId);
                    } else if ((null == user) && (null != updatedUser)) {
                        Log.d(LOG_TAG, "Got the user presence : " + userId);
                        isUpdated = true;
                    } else if (!TextUtils.equals(fPresence, updatedUser.presence)) {
                        isUpdated = true;
                        Log.d(LOG_TAG, "Got some new user presence info : " + userId);
                        Log.d(LOG_TAG, "currently_active : " + updatedUser.currently_active);
                        Log.d(LOG_TAG, "lastActiveAgo : " + updatedUser.lastActiveAgo);
                    }

                    if (isUpdated && (null != refreshCallback)) {
                        try {
                            refreshCallback.onSuccess(null);
                        } catch (Exception e) {
                            Log.e(LOG_TAG, "getUserOnlineStatus refreshCallback failed");
                        }
                    }
                }

                @Override
                public void onNetworkError(Exception e) {
                    Log.e(LOG_TAG, "getUserOnlineStatus onNetworkError " + e.getLocalizedMessage());
                }

                @Override
                public void onMatrixError(MatrixError e) {
                    Log.e(LOG_TAG, "getUserOnlineStatus onMatrixError " + e.getLocalizedMessage());
                }

                @Override
                public void onUnexpectedError(Exception e) {
                    Log.e(LOG_TAG, "getUserOnlineStatus onUnexpectedError " + e.getLocalizedMessage());
                }
            });
        }

        // unknown user
        if (null == user) {
            return null;
        }

        String presenceText = null;
        if (TextUtils.equals(user.presence, User.PRESENCE_ONLINE)) {
            presenceText = context.getResources().getString(R.string.room_participants_online);
        } else if (TextUtils.equals(user.presence, User.PRESENCE_UNAVAILABLE)) {
            presenceText = context.getResources().getString(R.string.room_participants_idle);
        } else if (TextUtils.equals(user.presence, User.PRESENCE_OFFLINE) || (null == user.presence)) {
            presenceText = context.getResources().getString(R.string.room_participants_last_seen);
        }

        if (presenceText != null) {
            Calendar today = Calendar.getInstance();
            today = clearTimes(today);
            Calendar yesterday = Calendar.getInstance();
            yesterday.add(Calendar.DAY_OF_YEAR, -1);
            yesterday = clearTimes(yesterday);
            SimpleDateFormat hourAndMinuteFormatter = new SimpleDateFormat("HH:mm");
            SimpleDateFormat weekDayFormatter = new SimpleDateFormat("EEEE");
            SimpleDateFormat dateFormatter = new SimpleDateFormat("dd.MM.yyyy");


            if ((null != user.currently_active) && user.currently_active) {
//                presenceText += " " + context.getResources().getString(R.string.room_participants_now);
            } else {
                long absoluteLastActiveAgo = user.getAbsoluteLastActiveAgo();
                Long lastActiveAgo = System.currentTimeMillis() - absoluteLastActiveAgo;
                long yesterdayTimeInMillis = yesterday.getTimeInMillis();
                long todayInMillis = today.getTimeInMillis();
                long differenceToday = System.currentTimeMillis() - todayInMillis;
                long differenceYesterday = System.currentTimeMillis() - yesterdayTimeInMillis;
                String hourAndMinuteFormat = hourAndMinuteFormatter.format(lastActiveAgo);
                String weekDayFormat = weekDayFormatter.format(lastActiveAgo).toLowerCase();
                String dateFormat = dateFormatter.format(lastActiveAgo);
                if ((null != lastActiveAgo) && (lastActiveAgo > 0)) {
                    if (absoluteLastActiveAgo < 12 * 60 * 60 * 1000)
                        presenceText += " " + formatSecondsIntervalFloored(context, absoluteLastActiveAgo / 1000L) + " " + context.getResources().getString(R.string.room_participants_ago);
                    else if (absoluteLastActiveAgo < differenceToday)
                        presenceText += " " + context.getString(R.string.online_status_today_at) + " " + hourAndMinuteFormat;
                    else if (absoluteLastActiveAgo < differenceYesterday)
                        presenceText += " " + context.getString(R.string.online_status_yesterday_at) + " " + hourAndMinuteFormat;
                    else if (absoluteLastActiveAgo < 5 * 24 * 60 * 60 * 1000)
                        presenceText += " " + weekDayFormat;
                    else if (absoluteLastActiveAgo >= 5 * 24 * 60 * 60 * 1000)
                        presenceText += " " + dateFormat;
                }
            }
            if (presenceText.equals("last seen 0 seconds ago")) {
                presenceText = "";
            }
        }

        return presenceText;
    }

    /**
     * Provide the group online status from members Id.
     * if refreshCallback is set, try to refresh the user presence if it is not known
     *
     * @param context         the context.
     * @param session         the session.
     * @param members         the members.
     * @param refreshCallback the presence callback.
     * @return the online status description.
     */
    public static String getGroupOnlineStatus(final Context context,
                                              final MXSession session, final Collection<RoomMember> members,
                                              final SimpleApiCallback<Void> refreshCallback) {
        // sanity checks
        if ((null == session) || (null == members)) {
            return null;
        }
        User userForPresence = null;
        long minAbsoluteLastActiveAgo = 0L;
        String presenceText = null;
        for (RoomMember member : members) {
            String userId = member.getUserId();
            if (!session.getMyUserId().equals(userId)) {
                final User user = session.getDataHandler().getStore().getUser(userId);

                // refresh the presence with this conditions
                boolean triggerRefresh = (null == user) || user.isPresenceObsolete();

                if ((null != refreshCallback) && triggerRefresh) {
                    Log.d(LOG_TAG, "Get the user presence : " + userId);

                    final String fPresence = (null != user) ? user.presence : null;

                    session.refreshUserPresence(userId, new ApiCallback<Void>() {
                        @Override
                        public void onSuccess(Void info) {
                            boolean isUpdated = false;
                            User updatedUser = session.getDataHandler().getStore().getUser(userId);

                            // don't find any info for the user
                            if ((null == user) && (null == updatedUser)) {
                                Log.d(LOG_TAG, "Don't find any presence info of " + userId);
                            } else if ((null == user) && (null != updatedUser)) {
                                Log.d(LOG_TAG, "Got the user presence : " + userId);
                                isUpdated = true;
                            } else if (!TextUtils.equals(fPresence, updatedUser.presence)) {
                                isUpdated = true;
                                Log.d(LOG_TAG, "Got some new user presence info : " + userId);
                                Log.d(LOG_TAG, "currently_active : " + updatedUser.currently_active);
                                Log.d(LOG_TAG, "lastActiveAgo : " + updatedUser.lastActiveAgo);
                            }

                            if (isUpdated && (null != refreshCallback)) {
                                try {
                                    refreshCallback.onSuccess(null);
                                } catch (Exception e) {
                                    Log.e(LOG_TAG, "getUserOnlineStatus refreshCallback failed");
                                }
                            }
                        }

                        @Override
                        public void onNetworkError(Exception e) {
                            Log.e(LOG_TAG, "getUserOnlineStatus onNetworkError " + e.getLocalizedMessage());
                        }

                        @Override
                        public void onMatrixError(MatrixError e) {
                            Log.e(LOG_TAG, "getUserOnlineStatus onMatrixError " + e.getLocalizedMessage());
                        }

                        @Override
                        public void onUnexpectedError(Exception e) {
                            Log.e(LOG_TAG, "getUserOnlineStatus onUnexpectedError " + e.getLocalizedMessage());
                        }
                    });
                }

                // unknown user
                if (null == user) {
                    return null;
                }

                if (TextUtils.equals(user.presence, User.PRESENCE_ONLINE)) {
                    presenceText = context.getResources().getString(R.string.room_participants_online);
                    userForPresence = user;
                    break;
                } else if (TextUtils.equals(user.presence, User.PRESENCE_UNAVAILABLE)) {
                    if (minAbsoluteLastActiveAgo == 0L) {
                        presenceText = context.getResources().getString(R.string.room_participants_idle);
                        userForPresence = user;
                    }
                } else if (TextUtils.equals(user.presence, User.PRESENCE_OFFLINE) || (null == user.presence)) {
                    if (minAbsoluteLastActiveAgo > user.getAbsoluteLastActiveAgo() || minAbsoluteLastActiveAgo == 0L) {
                        presenceText = context.getResources().getString(R.string.room_participants_last_seen);
                        minAbsoluteLastActiveAgo = user.getAbsoluteLastActiveAgo();
                        userForPresence = user;
                    }
                }
            }
        }

        if (presenceText != null) {
            Calendar today = Calendar.getInstance();
            today = clearTimes(today);
            Calendar yesterday = Calendar.getInstance();
            yesterday.add(Calendar.DAY_OF_YEAR, -1);
            yesterday = clearTimes(yesterday);
            SimpleDateFormat hourAndMinuteFormatter = new SimpleDateFormat("HH:mm");
            SimpleDateFormat weekDayFormatter = new SimpleDateFormat("EEEE");
            SimpleDateFormat dateFormatter = new SimpleDateFormat("dd.MM.yyyy");


            if ((null != userForPresence.currently_active) && userForPresence.currently_active) {
//                presenceText += " " + context.getResources().getString(R.string.room_participants_now);
            } else {
                long absoluteLastActiveAgo = userForPresence.getAbsoluteLastActiveAgo();
                Long lastActiveAgo = System.currentTimeMillis() - absoluteLastActiveAgo;
                long yesterdayTimeInMillis = yesterday.getTimeInMillis();
                long todayInMillis = today.getTimeInMillis();
                long differenceToday = System.currentTimeMillis() - todayInMillis;
                long differenceYesterday = System.currentTimeMillis() - yesterdayTimeInMillis;
                String hourAndMinuteFormat = hourAndMinuteFormatter.format(lastActiveAgo);
                String weekDayFormat = weekDayFormatter.format(lastActiveAgo).toLowerCase();
                String dateFormat = dateFormatter.format(lastActiveAgo);
                if ((null != lastActiveAgo) && (lastActiveAgo > 0)) {
                    if (absoluteLastActiveAgo < 12 * 60 * 60 * 1000)
                        presenceText += " " + formatSecondsIntervalFloored(context, absoluteLastActiveAgo / 1000L) + " " + context.getResources().getString(R.string.room_participants_ago);
                    else if (absoluteLastActiveAgo < differenceToday)
                        presenceText += " " + context.getString(R.string.online_status_today_at) + " " + hourAndMinuteFormat;
                    else if (absoluteLastActiveAgo < differenceYesterday)
                        presenceText += " " + context.getString(R.string.online_status_yesterday_at) + " " + hourAndMinuteFormat;
                    else if (absoluteLastActiveAgo < 5 * 24 * 60 * 60 * 1000)
                        presenceText += " " + weekDayFormat;
                    else if (absoluteLastActiveAgo >= 5 * 24 * 60 * 60 * 1000)
                        presenceText += " " + dateFormat;
                }
            }
        }

        return presenceText;
    }


    private static Calendar clearTimes(Calendar c) {
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        return c;
    }

    /**
     * Provide the user online status from his user Id.
     * if refreshCallback is set, try to refresh the user presence if it is not known
     *
     * @param context         the context.
     * @param session         the session.
     * @param userId          the userId.
     * @param refreshCallback the presence callback.
     * @return the online status description.
     */
    public static String getUserPresence(final Context context,
                                         final MXSession session, final String userId,
                                         final SimpleApiCallback<Void> refreshCallback) {
        // sanity checks
        if ((null == session) || (null == userId)) {
            return null;
        }

        final User user = session.getDataHandler().getStore().getUser(userId);

        // refresh the presence with this conditions
        boolean triggerRefresh = (null == user) || user.isPresenceObsolete();

        if ((null != refreshCallback) && triggerRefresh) {
            Log.d(LOG_TAG, "Get the user presence : " + userId);

            final String fPresence = (null != user) ? user.presence : null;

            session.refreshUserPresence(userId, new ApiCallback<Void>() {
                @Override
                public void onSuccess(Void info) {
                    boolean isUpdated = false;
                    User updatedUser = session.getDataHandler().getStore().getUser(userId);

                    // don't find any info for the user
                    if ((null == user) && (null == updatedUser)) {
                        Log.d(LOG_TAG, "Don't find any presence info of " + userId);
                    } else if ((null == user) && (null != updatedUser)) {
                        Log.d(LOG_TAG, "Got the user presence : " + userId);
                        isUpdated = true;
                    } else if (!TextUtils.equals(fPresence, updatedUser.presence)) {
                        isUpdated = true;
                        Log.d(LOG_TAG, "Got some new user presence info : " + userId);
                        Log.d(LOG_TAG, "currently_active : " + updatedUser.currently_active);
                        Log.d(LOG_TAG, "lastActiveAgo : " + updatedUser.lastActiveAgo);
                    }

                    if (isUpdated && (null != refreshCallback)) {
                        try {
                            refreshCallback.onSuccess(null);
                        } catch (Exception e) {
                            Log.e(LOG_TAG, "getUserOnlineStatus refreshCallback failed");
                        }
                    }
                }

                @Override
                public void onNetworkError(Exception e) {
                    Log.e(LOG_TAG, "getUserOnlineStatus onNetworkError " + e.getLocalizedMessage());
                }

                @Override
                public void onMatrixError(MatrixError e) {
                    Log.e(LOG_TAG, "getUserOnlineStatus onMatrixError " + e.getLocalizedMessage());
                }

                @Override
                public void onUnexpectedError(Exception e) {
                    Log.e(LOG_TAG, "getUserOnlineStatus onUnexpectedError " + e.getLocalizedMessage());
                }
            });
        }

        // unknown user
        if (null == user) {
            return null;
        }
        return user.presence;
    }
//==============================================================================================================
// Users list
//==============================================================================================================

    /**
     * List the active users i.e the active rooms users (invited or joined) and the contacts with matrix id emails.
     * This function could require a long time to process so it should be called in background.
     *
     * @param session the session.
     * @return a map indexed by the matrix id.
     */
    public static Map<String, ParticipantAdapterItem> listKnownParticipants
    (MXSession session) {
        // check known users
        Collection<User> users = session.getDataHandler().getStore().getUsers();

        // a hash map is a lot faster than a list search
        Map<String, ParticipantAdapterItem> map = new HashMap<>(users.size());

        // we don't need to populate the room members or each room
        // because an user is created for each joined / invited room member event
        for (User user : users) {
            if (!MXCallsManager.isConferenceUserId(user.user_id)) {
                map.put(user.user_id, new ParticipantAdapterItem(user));
            }
        }

        return map;
    }

//==============================================================================================================
// URL parser
//==============================================================================================================

    private static final Pattern mUrlPattern = Pattern.compile(
            "(?:^|[\\W])((ht|f)tp(s?):\\/\\/|www\\.)"
                    + "(([\\w\\-]+\\.){1,}?([\\w\\-.~]+\\/?)*"
                    + "[\\p{Alnum}.,%_=?&#\\-+()\\[\\]\\*$~@!:/{};']*)",
            Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);

//==============================================================================================================
// ExpandableListView tools
//==============================================================================================================

    /**
     * Provides the visible child views.
     * The map key is the group position.
     * The map values are the visible child views.
     *
     * @param expandableListView the listview
     * @param adapter            the linked adapter
     * @return visible views map
     */
    public static HashMap<Integer, List<Integer>> getVisibleChildViews
    (ExpandableListView expandableListView, BaseExpandableListAdapter adapter) {
        HashMap<Integer, List<Integer>> map = new HashMap<>();

        long firstPackedPosition = expandableListView.getExpandableListPosition(expandableListView.getFirstVisiblePosition());

        int firstGroupPosition = ExpandableListView.getPackedPositionGroup(firstPackedPosition);
        int firstChildPosition = ExpandableListView.getPackedPositionChild(firstPackedPosition);

        long lastPackedPosition = expandableListView.getExpandableListPosition(expandableListView.getLastVisiblePosition());

        int lastGroupPosition = ExpandableListView.getPackedPositionGroup(lastPackedPosition);
        int lastChildPosition = ExpandableListView.getPackedPositionChild(lastPackedPosition);

        for (int groupPos = firstGroupPosition; groupPos <= lastGroupPosition; groupPos++) {
            ArrayList<Integer> list = new ArrayList<>();

            int startChildPos = (groupPos == firstGroupPosition) ? firstChildPosition : 0;
            int endChildPos = (groupPos == lastGroupPosition) ? lastChildPosition : adapter.getChildrenCount(groupPos) - 1;

            for (int index = startChildPos; index <= endChildPos; index++) {
                list.add(index);
            }

            map.put(groupPos, list);
        }

        return map;
    }

    /**
     * Sets the background picture for msgs list fragment.
     *
     * @param v       View for setting background
     * @param context The context.
     */

    @SuppressLint("StaticFieldLeak")
    public static void setBackgroundImage(View v, Context context) {
        ImageView roomBackground = v.findViewById(R.id.background_image);
        DisplayMetrics metrics = new DisplayMetrics();
        WindowManager windowManager = (WindowManager) v.getContext()
                .getSystemService(Context.WINDOW_SERVICE);
        windowManager.getDefaultDisplay().getMetrics(metrics);
        int width = metrics.widthPixels / 2;
        int height = metrics.heightPixels / 2;
        Bitmap bitmap = null;
        try {
            Uri backgroundUri = PreferencesManager.getBackgroundChatPicture(context);
            if (backgroundUri != null) {
                bitmap = BitmapFactory.decodeFile(getPath(context, backgroundUri));
            } else {
                bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.default_background);
            }
            Drawable newDrw = new BitmapDrawable(context.getResources(), Bitmap.createScaledBitmap(bitmap, width, height, true));
            roomBackground.setImageDrawable(newDrw);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Get a file path from a Uri. This will get the the path for Storage Access
     * Framework Documents, as well as the _data field for the MediaStore and
     * other file-based ContentProviders.
     *
     * @param context The context.
     * @param uri     The Uri to query.
     * @author paulburke
     */
    @SuppressLint("NewApi")
    public static String getPath(final Context context, final Uri uri) {
        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;
        if (uri.toString().contains("/im_wallpapers/"))
            return uri.toString();
            // DocumentProvider
        else if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {
            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }
        return null;
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    /**
     * Get the value of the data column for this Uri. This is useful for
     * MediaStore Uris, and other file-based ContentProviders.
     *
     * @param context       The context.
     * @param uri           The Uri to query.
     * @param selection     (Optional) Filter used in the query.
     * @param selectionArgs (Optional) Selection arguments used in the query.
     * @return The value of the _data column, which is typically a file path.
     */
    public static String getDataColumn(Context context, Uri uri, String
            selection,
                                       String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int column_index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(column_index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    public static ArrayList<Languages> getLanguages(Context context, String locale) {
        ArrayList<Languages> lList = new ArrayList<>();
        String filename = locale + ".xml";
        try {
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setValidating(false);
            XmlPullParser myxml = factory.newPullParser();

            InputStream raw = context.getAssets().open(filename);
            myxml.setInput(raw, null);

            int eventType = myxml.getEventType();
            while (eventType != XmlPullParser.END_DOCUMENT) {

                String name = "";
                String code = "";

                if (eventType == XmlPullParser.START_TAG
                        && myxml.getName().equals("key"))
                //        && myxml.getText().equals("name")
                {
                    eventType = myxml.next();
                    // Log.d("---", " 1 ="+myxml.getName());
                    eventType = myxml.next();
                    // Log.d("---", " 2 ="+myxml.getName());
                    eventType = myxml.next();
                    // Log.d("---", " 3 ="+myxml.getName());
                    eventType = myxml.next();
                    // Log.d("---", " 4 ="+myxml.getName());
                    eventType = myxml.next();
                    // Log.d("---", " 5 ="+myxml.getName());
                    name = myxml.getText();
                     Log.d("---", "name ="+name);
                    eventType = myxml.next();
                    //Log.d("---", " 6 ="+myxml.getName());
                    eventType = myxml.next();
                    // Log.d("---", " 7 ="+myxml.getName());
                    eventType = myxml.next();
                    // Log.d("---", " 8 ="+myxml.getName());
                    eventType = myxml.next();
                    //  Log.d("---", " 9 ="+myxml.getName());
                    eventType = myxml.next();
                    //  Log.d("---", " 10 ="+myxml.getName());
                    eventType = myxml.next();
                    // Log.d("---", " 11 ="+myxml.getName());
                    eventType = myxml.next();
                    // Log.d("---", " 12 ="+myxml.getName());
                    eventType = myxml.next();
                    //  Log.d("---", " 13 ="+myxml.getName());
                    code = myxml.getText();
                     Log.d("---", " code= "+code);
                    lList.add(new Languages(name, code));


                }
                eventType = myxml.next();
            }
        } catch (Exception e) {
            Log.d("---", "Excepetion : " + e.getMessage());
        }
        Collections.sort(lList, new Comparator<Languages>() {
            @Override
            public int compare(Languages o1, Languages o2) {
                return o1.getLanguageName().compareTo(o2.getLanguageName());
            }
        });

        return lList;
    }

    public static int getTextColor(Context context) {
        TypedValue typedValue = new TypedValue();
        Resources.Theme theme = context.getTheme();
        theme.resolveAttribute(R.attr.riot_primary_text_color, typedValue, true);
        return typedValue.data;
    }

    public static int getBackgroundColor(Context context) {
        TypedValue typedValue = new TypedValue();
        Resources.Theme theme = context.getTheme();
        theme.resolveAttribute(R.attr.riot_primary_background_color, typedValue, true);
        return typedValue.data;
    }

    /**
     * Returns an user display name for an user Id.
     *
     * @param userId the user id.
     * @return teh user display name.
     */
    public static String getUserDisplayName(String userId, MXSession
            mxSession, Context context) {
        //  Log.e(LOG_TAG, userId + "");
        for (TalkerContact contact : new ContactHandler().getTalkerContactSnapshot(context)) {
            if (contact.getMatrixId() != null && contact.getMatrixId().equals(userId)) {
                return contact.getDisplayName();
            }
        }
        if (mxSession.getDataHandler().getUser(userId).displayname != null) {
            return mxSession.getDataHandler().getUser(userId).displayname;
        } else
            return userId;
    }
}
