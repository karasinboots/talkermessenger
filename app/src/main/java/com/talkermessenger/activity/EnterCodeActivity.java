package com.talkermessenger.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.iskar.talkersdk.LoginHandler;
import com.iskar.talkersdk.callback.MatrixLoginInterface;
import com.iskar.talkersdk.model.MatrixCredentials;
import com.iskar.talkersdk.model.PhoneBadRequest;
import com.stfalcon.smsverifycatcher.SmsVerifyCatcher;
import com.talkermessenger.Matrix;
import com.talkermessenger.R;
import com.talkermessenger.RegistrationManager;
import com.talkermessenger.receiver.VectorUniversalLinkReceiver;
import com.talkermessenger.util.PreImEditText;
import com.talkermessenger.util.PreferencesManager;

import org.matrix.androidsdk.HomeServerConnectionConfig;
import org.matrix.androidsdk.MXSession;
import org.matrix.androidsdk.rest.model.login.Credentials;
import org.matrix.androidsdk.ssl.Fingerprint;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EnterCodeActivity extends AppCompatActivity implements View.OnKeyListener,
        MatrixLoginInterface,
        TextView.OnEditorActionListener,
        View.OnClickListener {


    @BindView(R.id.et1)
    PreImEditText et1;
    @BindView(R.id.et2)
    PreImEditText et2;
    @BindView(R.id.et3)
    PreImEditText et3;
    @BindView(R.id.et4)
    PreImEditText et4;
    @BindView(R.id.et5)
    PreImEditText et5;
    @BindView(R.id.et6)
    PreImEditText et6;

    @BindView(R.id.call_progress)
    ProgressBar progressBar;

    @BindView(R.id.tvChangeNumber)
    TextView tvChangeNumber;
    @BindView(R.id.tvNumber)
    TextView tvNumber;
    @BindView(R.id.tvConfirm)
    TextView tvConfirm;

    @BindView(R.id.tvResendCode)
    TextView tvResendCode;

    @BindView(R.id.llProgressBar)
    LinearLayout llProgressBar;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    private SmsVerifyCatcher smsVerifyCatcher;
    LoginHandler loginHandler;
    private boolean isSmsSent = false;
    private boolean isReSend = true;
    String res = "";
    String number = "";
    private Parcelable mUniversalLinkUri;
    private final String LOG_TAG = "---";

    private CountDownTimer timer = new CountDownTimer(60000, 1000) {

        @Override
        public void onTick(long millisUntilFinished) {
            updateTime(millisUntilFinished);
        }


        @Override
        public void onFinish() {
            tvResendCode.setTextColor(getResources().getColor(R.color.riot_primary_background_color_black));
            tvResendCode.setClickable(true);
            tvResendCode.setText(getResources().getString(R.string.resend_code));

            // Toast.makeText(EnterCodeActivity.this,"onFinish isResend = "+isReSend ,Toast.LENGTH_SHORT).show();
            isReSend = true;

            if (llProgressBar.getVisibility() == View.VISIBLE)
                llProgressBar.setVisibility(View.GONE);
            //  timeLeft.setText("");
        }

    };

    private void updateTime(long time) {
        // Log.d("---","Tick : "+time);
        // tvResendCode.setText(getResources().getString(R.string.resend_in)+" " + TimeUnit.MILLISECONDS.toSeconds(time)+" " + getResources().getString(R.string.seconds_resend));
        tvResendCode.setText(getResources().getString(R.string.resend_in) + " " + TimeUnit.MILLISECONDS.toSeconds(time) + " "
                + getResources().getQuantityString(R.plurals.seconds_resend, (int) TimeUnit.MILLISECONDS.toSeconds(time)));
    }


    private String parseSms(String message) {
        Pattern p = Pattern.compile("\\b\\d{6}\\b");
        Matcher m = p.matcher(message);
        String code = "";
        while (m.find()) {
            code = m.group(0);
        }
        return code;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvConfirm:
                if (isReSend)
                {

                    sendCode();
                }
                break;
            case R.id.tvResendCode:
                // Toast.makeText(this,"Click",Toast.LENGTH_SHORT).show();
                if (isReSend)
                {
                    Log.d("---","Click Resend");
                    Toast.makeText(this,getResources().getString(R.string.resend_code),Toast.LENGTH_SHORT).show();
                    sendNumber();
                }
                else{
                   // Toast.makeText(this,"Resend FALSE\n"+getResources().getString(R.string.resend_code),Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.tvChangeNumber:
                Intent intent = new Intent(EnterCodeActivity.this, EnterNumberActivity.class);
                startActivity(intent);
                break;
        }
    }

    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {

        //    Toast.makeText(EnterCodeActivity.this, "onKey "+KeyEvent.keyCodeToString(keyCode)+event.getAction(), Toast.LENGTH_SHORT).show();
        //    Log.d("---","onKey =   "+event.getAction()+" Code "+KeyEvent.keyCodeToString(keyCode));

//        if (event.getAction() != KeyEvent.ACTION_UP) {
//            Toast.makeText(EnterCodeActivity.this, "RETURN "+KeyEvent.keyCodeToString(keyCode)+event.getAction(), Toast.LENGTH_SHORT).show();
//            Log.d("---","RETURN Action  "+event.getAction());
//            return false;
//        }

        if (keyCode == KeyEvent.KEYCODE_DEL &&
                event.getAction() == KeyEvent.ACTION_DOWN) {

            switch (v.getId()) {
                case R.id.et1:
                    // et1.setText(""+s);
                    // Toast.makeText(MainActivity.this, "Click et1", Toast.LENGTH_SHORT).show();
                    //event.getCharacters();
                    //    Log.d("---","onKey et1 =    et1 clear "+KeyEvent.keyCodeToString(keyCode));
                    et1.setText("");
                    et1.requestFocus();
                    break;
                case R.id.et2:

                    // Toast.makeText(MainActivity.this, "Click et2", Toast.LENGTH_SHORT).show();
                    if (et2.getText().toString().equals("")) {
                        //Log.d("---","onKey et2 =    et1 clear "+KeyEvent.keyCodeToString(keyCode));
                        et1.setText("");
                        et1.requestFocus();
                    }
                    break;
                case R.id.et3:
                    if (et3.getText().toString().equals("")) {
                        //  Log.d("---","onKey et3 =    et2 clear "+KeyEvent.keyCodeToString(keyCode));
                        et2.setText("");
                        //Toast.makeText(MainActivity.this, "Click et3", Toast.LENGTH_SHORT).show();
                        et2.requestFocus();
                    }
                    break;
                case R.id.et4:
                    if (et4.getText().toString().equals("")) {
                        // Log.d("---","onKey et4 =    et3 clear "+KeyEvent.keyCodeToString(keyCode));
                        et3.setText("");
                        // Toast.makeText(MainActivity.this, "Click et4", Toast.LENGTH_SHORT).show();
                        et3.requestFocus();
                    }
                    break;
                case R.id.et5:
                    if (et5.getText().toString().equals("")) {
                        //  Log.d("---","onKey et5 =    et4 clear "+KeyEvent.keyCodeToString(keyCode));
                        et4.setText("");
                        // Toast.makeText(MainActivity.this, "Click et5", Toast.LENGTH_SHORT).show();
                        et4.requestFocus();
                    }
                    break;
                case R.id.et6:
                    if (et6.getText().toString().equals("")) {
                        //  Log.d("---","onKey et6 =    et5 clear "+KeyEvent.keyCodeToString(keyCode));
                        et5.setText("");
                        //  Toast.makeText(EnterCodeActivity.this, "Click et6", Toast.LENGTH_SHORT).show();
                        et5.requestFocus();
                    }
                    break;


            }
            // ((PreImEditText) v).setText("");
        } else {
            if (keyCode == KeyEvent.KEYCODE_ENTER) {
                sendCode();
                return true;
            }

//            switch (keyCode) {
//                case KeyEvent.KEYCODE_0:
//                    res = "0";
//                    break;
//                case KeyEvent.KEYCODE_1:
//                    res = "1";
//                    break;
//                case KeyEvent.KEYCODE_2:
//                    res = "2";
//                    break;
//                case KeyEvent.KEYCODE_3:
//                    res = "3";
//                    break;
//                case KeyEvent.KEYCODE_4:
//                    res = "4";
//                    break;
//                case KeyEvent.KEYCODE_5:
//                    res = "5";
//                    break;
//                case KeyEvent.KEYCODE_6:
//                    res = "6";
//                    break;
//                case KeyEvent.KEYCODE_7:
//                    res = "7";
//                    break;
//                case KeyEvent.KEYCODE_8:
//                    res = "8";
//                    break;
//                case KeyEvent.KEYCODE_9:
//                    res = "9";
//                    break;
//            }

//            switch (v.getId()) {
//                case R.id.et1:
//                    // et1.setText(""+s);
//                    // Toast.makeText(MainActivity.this,"Click et1" ,Toast.LENGTH_SHORT).show();
//                    //event.getCharacters();
//
//                    et2.requestFocus();
//                    break;
//                case R.id.et2:
//
//                    // Toast.makeText(MainActivity.this,"Click et2" ,Toast.LENGTH_SHORT).show();
//                    et3.requestFocus();
//                    break;
//                case R.id.et3:
//
//                    //  Toast.makeText(MainActivity.this,"Click et3" ,Toast.LENGTH_SHORT).show();
//                    et4.requestFocus();
//                    break;
//                case R.id.et4:
//                    // Toast.makeText(MainActivity.this,"Click et4" ,Toast.LENGTH_SHORT).show();
//                    et5.requestFocus();
//                    break;
//                case R.id.et5:
//                    // Toast.makeText(MainActivity.this,"Click et5" ,Toast.LENGTH_SHORT).show();
//                    et6.requestFocus();
//                    break;
//                case R.id.et6:
//                    //  Toast.makeText(MainActivity.this,"Click et6" ,Toast.LENGTH_SHORT).show();
//                    tvChangeNumber.requestFocus();
//                    break;
//            }
            // ((PreImEditText) v).setText(res);
        }

        return false;
    }


    @SuppressLint("CheckResult")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_enter_code);

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getResources().getString(R.string.enter_code));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(EnterCodeActivity.this, EnterNumberActivity.class);
                startActivity(intent);
            }
        });

        tvResendCode.setClickable(true);
        tvResendCode.setOnClickListener(this);
        tvConfirm.setOnClickListener(this);
        tvChangeNumber.setOnClickListener(this);

        this.number = PreferencesManager.getUserNumber(EnterCodeActivity.this);


        //Log.d("---", "get " + number);
        //Log.d("---","get "+getIntent().getStringExtra("number"));
        tvNumber.setText(number);

        PreImEditText.OnKeyPreImeListener onKeyPreImeListener = new PreImEditText.OnKeyPreImeListener() {
            @Override
            public void onBackPressed() {
                EnterCodeActivity.super.onBackPressed();
            }
        };


        //isReSend=false;
        timer.start();

        et1.requestFocus();
        showKeyboard();
        et1.setOnKeyListener(this);
        et2.setOnKeyListener(this);
        et3.setOnKeyListener(this);
        et4.setOnKeyListener(this);
        et5.setOnKeyListener(this);
        et6.setOnKeyListener(this);
        et6.setOnEditorActionListener(this);
        et1.setOnKeyPreImeListener(onKeyPreImeListener);
        et2.setOnKeyPreImeListener(onKeyPreImeListener);
        et3.setOnKeyPreImeListener(onKeyPreImeListener);
        et4.setOnKeyPreImeListener(onKeyPreImeListener);
        et5.setOnKeyPreImeListener(onKeyPreImeListener);
        et6.setOnKeyPreImeListener(onKeyPreImeListener);


        et1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (!et1.getText().toString().equals(""))
                    et2.requestFocus();
            }
        });


        et2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (!et2.getText().toString().equals(""))
                    et3.requestFocus();
            }
        });


        et3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (!et3.getText().toString().equals(""))
                    et4.requestFocus();
            }
        });

        et4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (!et4.getText().toString().equals(""))
                    et5.requestFocus();
            }
        });

        et5.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (!et5.getText().toString().equals(""))
                    et6.requestFocus();
            }
        });

        et6.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                // Toast.makeText(EnterCodeActivity.this,"Change et6 ",Toast.LENGTH_SHORT).show();
                if (!et6.getText().toString().equals("")) {
                   // tvConfirm.requestFocus();
                    if (isReSend) {
                         sendCode();
                    }
                }
                //sendCode();
            }
        });

//        RxTextView.textChanges(et6).skipInitialValue()
//                .debounce(100, TimeUnit.MILLISECONDS, AndroidSchedulers.mainThread())
//                .subscribe(text -> {
//                    if (validateCode()) {
//                    }
//                    // sendCode();
//                });
        loginHandler = new LoginHandler(EnterCodeActivity.this);
        //timer.start();
        SharedPreferences preferences = getSharedPreferences("this", MODE_PRIVATE);
        if (preferences.getBoolean("SMS_PERM", false)) {
            smsVerifyCatcher = new SmsVerifyCatcher(this, message -> {
                String code = parseSms(message);
                // this.code.setText(code);
                if (code.length() == 6) {
                    et1.setText(code.substring(0, 1));
                    et2.setText(code.substring(1, 2));
                    et3.setText(code.substring(2, 3));
                    et4.setText(code.substring(3, 4));
                    et5.setText(code.substring(4, 5));
                    et6.setText(code.substring(5, 6));
                    //  sendCode();
                } else
                    Toast.makeText(EnterCodeActivity.this, "Incorrect code : " + code, Toast.LENGTH_SHORT).show();
            });
          //  smsVerifyCatcher.setPhoneNumberFilter("TALKER");
            smsVerifyCatcher.setFilter("(.*)talkermessenger(.*)");
            smsVerifyCatcher.onStart();
        } else {
            Log.d("---", "CodeActivity SMS perm DENIED");
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        showKeyboard();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        timer.cancel();
        if (smsVerifyCatcher != null)
            smsVerifyCatcher.onStop();
    }

    private boolean validateCode(){
        if (et1.getText().length() + et2.getText().length() +
                et3.getText().length() + et4.getText().length() +
                et5.getText().length() + et6.getText().length() == 6) {
            return true;
        }
        return false;
    }
    private void sendNumber() {
        //  String code = "" + et1.getText() + et2.getText() + et3.getText() + et4.getText() + et5.getText() + et6.getText();
        Log.d("---","sendNumber");
        // llProgressBar.setVisibility(View.VISIBLE);
        tvResendCode.setClickable(false);

        loginHandler = new LoginHandler(EnterCodeActivity.this);
        loginHandler.requestCode(number);

       // smsVerifyCatcher.setPhoneNumberFilter("TALKER");

        smsVerifyCatcher.onStart();
    }


    private void sendCode() {
        llProgressBar.setVisibility(View.VISIBLE);
        String code = "" + et1.getText() + et2.getText() + et3.getText() + et4.getText() + et5.getText() + et6.getText();
        if (code.length() == 6) {
            //   Toast.makeText(EnterCodeActivity.this, "Code OK : " + code + " Send to ", Toast.LENGTH_SHORT).show();
//            if (tvResendCode.isClickable())
//                tvResendCode.setClickable(false);
            isReSend = false;
          //  timer.start();
            loginHandler.login(number, code);
        } else
            llProgressBar.setVisibility(View.GONE);
    }


    @Override
    public void getCodeSuccess() {
        isSmsSent = true;
        // login.setTextColor(context.getResources().getColor(R.color.vector_dark_grey_color));
        tvResendCode.setClickable(false);
        timer.start();

        llProgressBar.setVisibility(View.GONE);
        if (smsVerifyCatcher != null)
            smsVerifyCatcher.onStop();
        //  Toast.makeText(EnterCodeActivity.this, "CODE OK", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void tooManyAttempts(PhoneBadRequest phoneBadRequest) {

        llProgressBar.setVisibility(View.GONE);
        int seconds = 0;
        if (phoneBadRequest != null) {
            seconds = phoneBadRequest.getTimeRemaining();
            // Log.d("---", "SECOND " + seconds);
            if (seconds != 0) {
                int min = ((int) seconds / 60);
                Toast.makeText(EnterCodeActivity.this,
                        getResources().getString(R.string.try_after)+" "
                                + min +" "+ this.getResources().getQuantityString(R.plurals.minutes, min), Toast.LENGTH_SHORT).show();
                isReSend=true;
                tvResendCode.setClickable(true);
            }
        }


    }

    @Override
    public void changePhoneSuccess(String newPhone) {

    }

    @Override
    public void onFailure(String msg) {
        timer.onFinish();
        llProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void getCredentialsSuccess(MatrixCredentials matrixCredentials) {
        String homeServer = getResources().getString(R.string.default_hs_server_url);
        String userId = matrixCredentials.getUserId();
        String accessToken = matrixCredentials.getAccessToken();
        llProgressBar.setVisibility(View.GONE);
        // build a credential with the provided items
        Credentials credentials = new Credentials();
        credentials.userId = userId;
        credentials.homeServer = homeServer;
        credentials.accessToken = accessToken;
        HomeServerConnectionConfig hsConfig = getHsConfig();

        try {
            hsConfig.setCredentials(credentials);
        } catch (Exception e) {
            org.matrix.androidsdk.util.Log.d(LOG_TAG, "hsConfig setCredentials failed " + e.getLocalizedMessage());
        }

        org.matrix.androidsdk.util.Log.d(LOG_TAG, "Account creation succeeds");

        // let's go...
        MXSession session = Matrix.getInstance(getApplicationContext()).createSession(hsConfig);
        Matrix.getInstance(getApplicationContext()).addSession(session);
        goToSplash();
        finish();
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        showKeyboard();
    }

    private void goToSplash() {
        org.matrix.androidsdk.util.Log.d(LOG_TAG, "## gotoSplash(): Go to splash.");
        Intent intent = new Intent(this, SplashActivity.class);
        if (null != mUniversalLinkUri) {
            intent.putExtra(VectorUniversalLinkReceiver.EXTRA_UNIVERSAL_LINK_URI, mUniversalLinkUri);
        }
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    // save the config because trust a certificate is asynchronous.
    private HomeServerConnectionConfig mHomeserverConnectionConfig;

    /**
     * @return the homeserver config. null if the url is not valid
     */
    private HomeServerConnectionConfig getHsConfig() {
        if (null == mHomeserverConnectionConfig) {
            String hsUrlString = getResources().getString(R.string.default_hs_server_url);

            if (TextUtils.isEmpty(hsUrlString) || !hsUrlString.startsWith("http") || TextUtils.equals(hsUrlString, "http://") || TextUtils.equals(hsUrlString, "https://")) {
                Toast.makeText(this, getString(R.string.login_error_must_start_http), Toast.LENGTH_SHORT).show();
                return null;
            }

            if (!hsUrlString.startsWith("http://") && !hsUrlString.startsWith("https://")) {
                hsUrlString = "https://" + hsUrlString;
            }

            String identityServerUrlString = getResources().getString(R.string.default_identity_server_url);

            if (TextUtils.isEmpty(identityServerUrlString) || !identityServerUrlString.startsWith("http") || TextUtils.equals(identityServerUrlString, "http://") || TextUtils.equals(identityServerUrlString, "https://")) {
                Toast.makeText(this, getString(R.string.login_error_must_start_http), Toast.LENGTH_SHORT).show();
                return null;
            }

            if (!identityServerUrlString.startsWith("http://") && !identityServerUrlString.startsWith("https://")) {
                identityServerUrlString = "https://" + identityServerUrlString;
            }

            try {
                mHomeserverConnectionConfig = null;
                mHomeserverConnectionConfig = new HomeServerConnectionConfig(Uri.parse(hsUrlString), Uri.parse(identityServerUrlString), null, new ArrayList<Fingerprint>(), false);
            } catch (Exception e) {
                org.matrix.androidsdk.util.Log.e(LOG_TAG, "getHsConfig fails " + e.getLocalizedMessage());
            }
        }

        RegistrationManager.getInstance().setHsConfig(mHomeserverConnectionConfig);
        return mHomeserverConnectionConfig;
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        sendCode();
        return true;
    }

    private void showKeyboard() {
        et1.requestFocus();
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(view, 0);
        }
    }
}
