package com.talkermessenger.activity.settings;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SeekBar;

import com.flask.colorpicker.ColorPickerView;
import com.flask.colorpicker.OnColorSelectedListener;
import com.talkermessenger.R;
import com.talkermessenger.util.PreferencesManager;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Serhii on 30.01.2018.
 */

public class ColorMixerSettingsActivity extends AppCompatActivity implements SeekBar.OnSeekBarChangeListener {

    public static final String INT_EXTRA = "INT_EXTRA";
    @BindView(R.id.account_toolbar)
    Toolbar mToolbar;
    @BindView(R.id.fSeekBar)
    SeekBar fSeekBar;
    @BindView(R.id.faSeekBar)
    SeekBar faSeekBar;
    @BindView(R.id.color_r)
    EditText rColor;
    @BindView(R.id.color_g)
    EditText gColor;
    @BindView(R.id.color_b)
    EditText bColor;
    @BindView(R.id.result_color)
    ImageView resultColor;
    @BindView(R.id.color_picker_view)
    ColorPickerView colorPickerView;
    int selectedColor;
    private int Red;
    private int Green;
    private int Blue;

    public int[] calcColor(int progress) {
        if (progress == 0) {
            Red = 255;
            Green = 0;
            Blue = 0;
        }
        if (progress > 0 & progress <= 42) {
            Red = 255;
            Green = progress * 6 - 1;
            Blue = 0;
        } else if (progress > 42 & progress <= 84) {
            Red = 256 - (progress - 42) * 6 - 1;
            Green = 255;
            Blue = 0;
        } else if (progress > 84 & progress <= 126) {
            Red = 0;
            Green = 255;
            Blue = (progress - 84) * 6 - 1;
        } else if (progress > 126 & progress <= 168) {
            Red = 0;
            Green = 256 - (progress - 126) * 6 - 1;
            Blue = 255;
        } else if (progress > 168 & progress <= 210) {
            Red = (progress - 168) * 6 - 1;
            Green = 0;
            Blue = 255;
        } else if (progress > 210 & progress <= 252) {
            Red = 255;
            Green = 0;
            Blue = 256 - (progress - 210) * 6 - 1;
        }
        int[] color = {Red, Green, Blue};
        return color;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings_color_mixer);

        ButterKnife.bind(this);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        fSeekBar.setOnSeekBarChangeListener(this);
        faSeekBar.setOnSeekBarChangeListener(this);

        rColor.addTextChangedListener(new RGBEditTextListener(R.id.color_r));
        gColor.addTextChangedListener(new RGBEditTextListener(R.id.color_g));
        bColor.addTextChangedListener(new RGBEditTextListener(R.id.color_b));

        colorPickerView.addOnColorSelectedListener(new OnColorSelectedListener() {
            @Override
            public void onColorSelected(int i) {
                resultColor.setBackgroundColor(i);
                selectedColor = i;
                int r = (selectedColor >> 16) & 0xFF;
                int g = (selectedColor >> 8) & 0xFF;
                int b = (selectedColor >> 0) & 0xFF;
                rColor.setText(String.valueOf(r));
                gColor.setText(String.valueOf(g));
                bColor.setText(String.valueOf(b));
            }
        });
        if (getIntent().getAction() != null)
            if (getIntent().getAction().equals("incoming"))
                colorPickerView.setInitialColor(PreferencesManager.getIntIncomingChatColor(colorPickerView.getContext()), false);
            else
                colorPickerView.setInitialColor(PreferencesManager.getIntOutgoingChatColor(colorPickerView.getContext()), false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onProgressChanged(@NonNull SeekBar seekBar, int progress, boolean fromUser) {
        ImageView resultColor = findViewById(R.id.result_color);

        switch (seekBar.getId()) {
            case R.id.fSeekBar:
            case R.id.faSeekBar: {
                int color[] = calcColor(fSeekBar.getProgress());
                resultColor.setBackgroundColor(Color.argb(faSeekBar.getProgress(), color[0], color[1], color[2]));
                resultColor.invalidate();
                rColor.setText(String.valueOf(color[0]));
                gColor.setText(String.valueOf(color[1]));
                bColor.setText(String.valueOf(color[2]));
                break;
            }
        }

    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
    }

    public void saveButton(View view) {
        int alpha = faSeekBar.getProgress();
        int red = Integer.parseInt(rColor.getText().toString());
        int green = Integer.parseInt(gColor.getText().toString());
        int blue = Integer.parseInt(bColor.getText().toString());
        //String hex = String.format("#%02x%02x%02x%02x", alpha, red, green, blue);

        if (selectedColor != 0) {
            //  String hex = String.format("#%06X", 0xFFFFFF & selectedColor);
            Intent intent = new Intent();
            intent.putExtra(INT_EXTRA, selectedColor);
            setResult(RESULT_OK, intent);
        } else {
            Intent intent = new Intent();
            setResult(RESULT_CANCELED, intent);
            finish();//finishing activity
        }
        finish();//finishing activity
    }

    public void closeButton(View view) {
        Intent intent = new Intent();
        setResult(RESULT_CANCELED, intent);
        finish();//finishing activity
    }

    private class RGBEditTextListener implements TextWatcher {

        private int id;

        RGBEditTextListener(int id) {
            this.id = id;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (s.length() == 0) return;
            switch (id) {
                case R.id.color_r:
                    if (Integer.parseInt(s.toString()) > 255)
                        rColor.setText(String.valueOf(255));
                    else if (Integer.parseInt(s.toString()) < 0)
                        rColor.setText(String.valueOf(0));
                    break;
                case R.id.color_g:
                    if (Integer.parseInt(s.toString()) > 255)
                        gColor.setText(String.valueOf(255));
                    else if (Integer.parseInt(s.toString()) < 0)
                        gColor.setText(String.valueOf(0));
                    break;
                case R.id.color_b:
                    if (Integer.parseInt(s.toString()) > 255)
                        gColor.setText(String.valueOf(255));
                    else if (Integer.parseInt(s.toString()) < 0)
                        gColor.setText(String.valueOf(0));
                    break;
                default:
                    return;
            }

            if (rColor.getText().toString().length() == 0) rColor.setText(String.valueOf(0));
            if (gColor.getText().toString().length() == 0) gColor.setText(String.valueOf(0));
            if (bColor.getText().toString().length() == 0) bColor.setText(String.valueOf(0));

            int red = Integer.parseInt(rColor.getText().toString());
            int green = Integer.parseInt(gColor.getText().toString());
            int blue = Integer.parseInt(bColor.getText().toString());
            selectedColor = android.graphics.Color.rgb(red, green, blue);
            colorPickerView.setColor(selectedColor, false);
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    }
}
