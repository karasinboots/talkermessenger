package com.talkermessenger.activity.settings;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jakewharton.rxbinding2.widget.RxTextView;
import com.talkermessenger.R;
import com.talkermessenger.activity.SplashActivity;
import com.talkermessenger.util.PreImEditText;
import com.talkermessenger.util.PreferencesManager;

import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;

public class EnterPassCodeActivity extends AppCompatActivity implements View.OnKeyListener, TextView.OnEditorActionListener, View.OnClickListener {

   public static String EXTRA_IS_SET_MODE = "isSetMode";

/*
    @BindView(R.id.go_next)
    TextView login;
    @BindView(R.id.phone_number)
    EditText phone;
    @BindView(R.id.code)
    EditText code;
    @BindView(R.id.call_progress)
    ProgressBar progressBar;
    @BindView(R.id.time_left)
    TextView timeLeft;*/

    @BindView(R.id.et1)
    PreImEditText et1;
    @BindView(R.id.et2)
    PreImEditText et2;
    @BindView(R.id.et3)
    PreImEditText et3;
    @BindView(R.id.et4)
    PreImEditText et4;
    @BindView(R.id.et5)
    PreImEditText et5;
    @BindView(R.id.et6)
    PreImEditText et6;

    @BindView(R.id.tvConfirm)
    TextView tvConfirm;

    @BindView(R.id.tvWrongPass)
    TextView tvWrongPass;

    @BindView(R.id.llProgressBar)
    LinearLayout llProgressBar;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    String res = "";
    boolean isSetMode;



    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvConfirm:
                sendCode();
                break;
        }
    }

    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {

        if (event.getAction() != KeyEvent.ACTION_UP) return false;

        if (keyCode == KeyEvent.KEYCODE_DEL) {

            switch (v.getId()) {
                case R.id.et1:
                    et1.setText("");
                    et1.requestFocus();
                    break;
                case R.id.et2:

                    et1.setText("");
                    et1.requestFocus();
                    break;
                case R.id.et3:
                    et2.setText("");
                    et2.requestFocus();
                    break;
                case R.id.et4:
                    et3.setText("");
                    et3.requestFocus();
                    break;
                case R.id.et5:
                    et4.setText("");
                    et4.requestFocus();
                    break;
                case R.id.et6:
                    et5.setText("");
                    et5.requestFocus();
                    break;


            }
            ((PreImEditText) v).setText("");
        } else {
            if (keyCode == KeyEvent.KEYCODE_ENTER) {
                sendCode();
                return true;
            }

            switch (keyCode) {
                case KeyEvent.KEYCODE_0:
                    res = "0";
                    break;
                case KeyEvent.KEYCODE_1:
                    res = "1";
                    break;
                case KeyEvent.KEYCODE_2:
                    res = "2";
                    break;
                case KeyEvent.KEYCODE_3:
                    res = "3";
                    break;
                case KeyEvent.KEYCODE_4:
                    res = "4";
                    break;
                case KeyEvent.KEYCODE_5:
                    res = "5";
                    break;
                case KeyEvent.KEYCODE_6:
                    res = "6";
                    break;
                case KeyEvent.KEYCODE_7:
                    res = "7";
                    break;
                case KeyEvent.KEYCODE_8:
                    res = "8";
                    break;
                case KeyEvent.KEYCODE_9:
                    res = "9";
                    break;
            }

            switch (v.getId()) {
                case R.id.et1:
                    et2.requestFocus();
                    break;
                case R.id.et2:
                    et3.requestFocus();
                    break;
                case R.id.et3:
                    et4.requestFocus();
                    break;
                case R.id.et4:
                    et5.requestFocus();
                    break;
                case R.id.et5:
                    et6.requestFocus();
                    break;
                case R.id.et6:
//                    tvChangeNumber.requestFocus();
                    break;
            }
            ((PreImEditText) v).setText(res);
        }

        return false;
    }

    @SuppressLint("CheckResult")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_enter_pass_code);

        ButterKnife.bind(this);


        this.isSetMode = getIntent().getBooleanExtra(EXTRA_IS_SET_MODE, false);
        getIntent().removeExtra(EXTRA_IS_SET_MODE);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getResources().getString(R.string.enter_code));
        if(isSetMode) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               onBackPressed();
            }
        });

        tvConfirm.setOnClickListener(this);

        PreImEditText.OnKeyPreImeListener onKeyPreImeListener = new PreImEditText.OnKeyPreImeListener() {
            @Override
            public void onBackPressed() {
                EnterPassCodeActivity.super.onBackPressed();
            }
        };
        et1.requestFocus();
        showKeyboard();
        et1.setOnKeyListener(this);
        et2.setOnKeyListener(this);
        et3.setOnKeyListener(this);
        et4.setOnKeyListener(this);
        et5.setOnKeyListener(this);
        et6.setOnKeyListener(this);
        et6.setOnEditorActionListener(this);
        et1.setOnKeyPreImeListener(onKeyPreImeListener);
        et2.setOnKeyPreImeListener(onKeyPreImeListener);
        et3.setOnKeyPreImeListener(onKeyPreImeListener);
        et4.setOnKeyPreImeListener(onKeyPreImeListener);
        et5.setOnKeyPreImeListener(onKeyPreImeListener);
        et6.setOnKeyPreImeListener(onKeyPreImeListener);
        RxTextView.textChanges(et6).skipInitialValue()
                .debounce(100, TimeUnit.MILLISECONDS, AndroidSchedulers.mainThread())
                .subscribe(text -> {
                    if (et1.getText().length() + et2.getText().length() +
                            et3.getText().length() + et4.getText().length() +
                            et5.getText().length() + et6.getText().length() == 6)
                        sendCode();
                });

    }

    @Override
    protected void onResume() {
        super.onResume();
        showKeyboard();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void sendCode() {
        String code = "" + et1.getText() + et2.getText() + et3.getText() + et4.getText() + et5.getText() + et6.getText();
        if (code.length() == 6) {
            if(isSetMode) {
                PreferencesManager.setPassword(this, code);
                onBackPressed();
                finish();
                //                startActivity(new Intent(this, SecurityActivity.class));
            } else {
                if(code.equals(PreferencesManager.getPassword(this))){
                    startActivity(new Intent(this, SplashActivity.class));
                    finish();
                }
                else
                    tvWrongPass.setVisibility(View.VISIBLE);

            }
        }
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        showKeyboard();
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        sendCode();
        return true;
    }

    private void showKeyboard() {
        et1.requestFocus();
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(view, 0);
        }
    }
}
