package com.talkermessenger.activity.settings;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.res.Resources;
import android.graphics.Paint;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.transition.Transition;
import android.util.Log;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.talkermessenger.GlideApp;
import com.talkermessenger.MyPresenceManager;
import com.talkermessenger.R;
import com.talkermessenger.util.PreferencesManager;
import com.talkermessenger.util.ThemeUtils;

import java.io.File;
import java.lang.reflect.Field;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Serhii on 13.02.2018.
 */

public class DoNotDisturbSettingsActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.account_toolbar)
    Toolbar mToolbar;

    @BindView(R.id.start_settings_click)
    RelativeLayout mStartDoNotDisturb;

    @BindView(R.id.finish_settings_click)
    RelativeLayout mFinishDoNotDisturb;

    @BindView(R.id.start_time)
    TimePicker startTime;

    @BindView(R.id.end_time)
    TimePicker endTime;

    @BindView(R.id.time_piker_layout)
    LinearLayout pikerLayout;

    @BindView(R.id.time)
    TextView time;
    @BindView(R.id.isactivte)
    ImageView isactive;
    @BindView(R.id.stop_dnd)
    TextView stopDnd;

    @Nullable
    @BindView(R.id.for_transition)
    ImageView transition;

    @OnClick(R.id.apply_dnd)
    void applyDNDMode() {
        long start = TimeUnit.HOURS.toMillis(startTime.getCurrentHour()) + TimeUnit.MINUTES.toMillis(startTime.getCurrentMinute());
        long end = TimeUnit.HOURS.toMillis(endTime.getCurrentHour()) + TimeUnit.MINUTES.toMillis(endTime.getCurrentMinute());
        PreferencesManager.saveDNDtime(start, end, this);
        Toast.makeText(this, getString(R.string.done), Toast.LENGTH_SHORT).show();
        pikerLayout.setVisibility(View.GONE);

        int startHour = (int) TimeUnit.MILLISECONDS.toHours(PreferencesManager.getStartDNDTime(this));
        int startMin = (int) TimeUnit.MILLISECONDS.toMinutes(PreferencesManager.getStartDNDTime(this) - TimeUnit.HOURS.toMillis(startHour));
        int endHour = (int) TimeUnit.MILLISECONDS.toHours(PreferencesManager.getEndDNDTime(this));
        int endMin = (int) TimeUnit.MILLISECONDS.toMinutes(PreferencesManager.getEndDNDTime(this) - TimeUnit.HOURS.toMillis(endHour));
        time.setText(String.format("%02d : %02d - %02d : %02d", startHour, startMin, endHour, endMin));
        stopDnd.setVisibility(View.VISIBLE);
        if (PreferencesManager.isInDNDMode(this))
            MyPresenceManager.advertiseAllUnavailable();
        else MyPresenceManager.advertiseAllOnline();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_settings_do_not_disturb);

        ButterKnife.bind(this);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        startTime.setIs24HourView(true); // set 24 hours mode for the time picker
        endTime.setIs24HourView(true);

        int startHour = (int) TimeUnit.MILLISECONDS.toHours(PreferencesManager.getStartDNDTime(this));
        int startMin = (int) TimeUnit.MILLISECONDS.toMinutes(PreferencesManager.getStartDNDTime(this) - TimeUnit.HOURS.toMillis(startHour));
        int endHour = (int) TimeUnit.MILLISECONDS.toHours(PreferencesManager.getEndDNDTime(this));
        int endMin = (int) TimeUnit.MILLISECONDS.toMinutes(PreferencesManager.getEndDNDTime(this) - TimeUnit.HOURS.toMillis(endHour));

        if (startHour != 0 && startMin != 0 && endHour != 0 && endMin != 0) {
            startTime.setCurrentHour(startHour);
            startTime.setCurrentMinute(startMin);
            endTime.setCurrentHour(endHour);
            endTime.setCurrentMinute(endMin);
            time.setText(String.format("%02d : %02d - %02d : %02d", startHour, startMin, endHour, endMin));
            isactive.setVisibility(View.VISIBLE);
            stopDnd.setVisibility(View.VISIBLE);
            stopDnd.setOnClickListener(v -> {
                PreferencesManager.stopDNDMode(DoNotDisturbSettingsActivity.this);
                Toast.makeText(DoNotDisturbSettingsActivity.this, R.string.dnd_stopped, Toast.LENGTH_SHORT).show();
                pikerLayout.setVisibility(View.GONE);
                time.setText(R.string.tap_to_start);
            });
        } else {
            isactive.setVisibility(View.GONE);
            time.setText(R.string.tap_to_start);
            stopDnd.setVisibility(View.GONE);
        }
        set_timepicker_text_colour(startTime);
        set_timepicker_text_colour(endTime);
//        startTime.setOnTimeChangedListener((view, hourOfDay, minute) ->
//                Toast.makeText(DoNotDisturbSettingsActivity.this,
//                        String.valueOf(hourOfDay) + " " + minute, Toast.LENGTH_SHORT).show());

//        setupUserData();
        setupOnClickListeners();


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            ThemeUtils.setTransitionImage(this, "dnd_settings", transition);

            if (!getIntent().getBooleanExtra(SmartSettingsActivity.IS_SHARED_ELEMENT_TRANSITION_ENABLED, false))
                findViewById(R.id.hide_transition).setVisibility(View.GONE);

            getWindow().getSharedElementEnterTransition().addListener(new Transition.TransitionListener() {
                @Override
                public void onTransitionStart(Transition transition) {

                }

                @Override
                public void onTransitionEnd(Transition transition) {
                    findViewById(R.id.hide_transition).setVisibility(View.INVISIBLE);
                }

                @Override
                public void onTransitionCancel(Transition transition) {

                }

                @Override
                public void onTransitionPause(Transition transition) {

                }

                @Override
                public void onTransitionResume(Transition transition) {

                }
            });
        }

    }

    @Override
    public void onBackPressed() {
        if (getIntent().getBooleanExtra(SmartSettingsActivity.IS_SHARED_ELEMENT_TRANSITION_ENABLED, false))
            findViewById(R.id.hide_transition).setVisibility(View.VISIBLE);
        supportFinishAfterTransition();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (getIntent().getBooleanExtra(SmartSettingsActivity.IS_SHARED_ELEMENT_TRANSITION_ENABLED, false))
                    findViewById(R.id.hide_transition).setVisibility(View.VISIBLE);
                supportFinishAfterTransition();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.start_settings_click:

                //TODO temporery commented
               /* if (pikerLayout.getVisibility() == View.GONE) {
                    pikerLayout.animate()
                            .alpha(1.0f)
                            .setDuration(500)
                            .setListener(new AnimatorListenerAdapter() {
                                @Override
                                public void onAnimationEnd(Animator animation) {
                                    super.onAnimationEnd(animation);
                                    pikerLayout.setVisibility(View.VISIBLE);
                                }
                            });
                } else {
                    pikerLayout.setVisibility(View.GONE);
                }*/
                break;
            case R.id.finish_settings_click:
                if (pikerLayout.getVisibility() == View.VISIBLE) {
                    pikerLayout.animate()
                            .alpha(0.0f)
                            .setDuration(500)
                            .setListener(new AnimatorListenerAdapter() {
                                @Override
                                public void onAnimationEnd(Animator animation) {
                                    super.onAnimationEnd(animation);
                                    pikerLayout.setVisibility(View.GONE);
                                }
                            });
                }
                Toast.makeText(this, R.string.stop, Toast.LENGTH_SHORT).show();
                break;
        }

    }

    private void setupOnClickListeners() {
        mStartDoNotDisturb.setOnClickListener(this);
        mFinishDoNotDisturb.setOnClickListener(this);
    }

    Resources system;

    private void set_timepicker_text_colour(TimePicker time_picker) {
        system = Resources.getSystem();
        int hour_numberpicker_id = system.getIdentifier("hour", "id", "android");
        int minute_numberpicker_id = system.getIdentifier("minute", "id", "android");
        int ampm_numberpicker_id = system.getIdentifier("amPm", "id", "android");

        NumberPicker hour_numberpicker = time_picker.findViewById(hour_numberpicker_id);
        NumberPicker minute_numberpicker = time_picker.findViewById(minute_numberpicker_id);
        NumberPicker ampm_numberpicker = time_picker.findViewById(ampm_numberpicker_id);

        set_numberpicker_text_colour(hour_numberpicker);
        set_numberpicker_text_colour(minute_numberpicker);
        set_numberpicker_text_colour(ampm_numberpicker);
    }

    private void set_numberpicker_text_colour(NumberPicker number_picker) {
        TypedValue vectorTabBarColor = new TypedValue();
        this.getTheme().resolveAttribute(R.attr.primary_text_color, vectorTabBarColor, true);
        final int count = number_picker.getChildCount();
        final int color = getResources().getColor(vectorTabBarColor.resourceId);

        for (int i = 0; i < count; i++) {
            View child = number_picker.getChildAt(i);

            try {
                Field wheelpaint_field = number_picker.getClass().getDeclaredField("mSelectorWheelPaint");
                wheelpaint_field.setAccessible(true);

                ((Paint) wheelpaint_field.get(number_picker)).setColor(color);
                ((EditText) child).setTextColor(color);
                number_picker.invalidate();
            } catch (NoSuchFieldException e) {
                Log.w("setNumberPickerTextColo", e);
            } catch (IllegalAccessException e) {
                Log.w("setNumberPickerTextColo", e);
            } catch (IllegalArgumentException e) {
                Log.w("setNumberPickerTextColo", e);
            }
        }
    }
}
