package com.talkermessenger.activity.settings;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.transition.Transition;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.talkermessenger.Matrix;
import com.talkermessenger.R;
import com.talkermessenger.util.PreferencesManager;
import com.talkermessenger.util.VectorUtils;

import org.matrix.androidsdk.MXSession;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class ExtraSettingsActivity extends AppCompatActivity {

    private MXSession session;

    @BindView(R.id.name_extra)
    TextView name;
    @BindView(R.id.name_icon)
    CircleImageView avatar;
    @BindView(R.id.phone_extra)
    TextView phone;
    @BindView(R.id.extra_toolbar)
    Toolbar toolbar;

    @OnClick(R.id.delete_account_extra)
    void deleteAccount() {
        startActivity(new Intent(this, DeleteNumberSettings.class));
        finish();
    }

    @OnClick(R.id.change_number_extra)
    void changeNumber() {
        startActivity(new Intent(this, ChangeNumberSettings.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_extra_settings);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        session = Matrix.getInstance(this).getDefaultSession();
        name.setText(session.getMyUser().displayname);
        try {
            PhoneNumberUtil pnu = PhoneNumberUtil.getInstance();
            Phonenumber.PhoneNumber pn = pnu.parse(PreferencesManager.getUserNumber(this), null);
            String phoneNumber = PhoneNumberUtil.getInstance().format(pn,
                    PhoneNumberUtil.PhoneNumberFormat.INTERNATIONAL);
            phone.setText(phoneNumber);
        } catch (Exception e) {
            phone.setText(PreferencesManager.getUserNumber(this));
            e.printStackTrace();
        }
        VectorUtils.loadUserAvatar(
                this,
                session,
                avatar,
                session
                        .getDataHandler()
                        .getUser(session.getCredentials().userId));


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (!getIntent().getBooleanExtra(SmartSettingsActivity.IS_SHARED_ELEMENT_TRANSITION_ENABLED, false))
                findViewById(R.id.hide_transition).setVisibility(View.GONE);

            getWindow().getSharedElementEnterTransition().addListener(new Transition.TransitionListener() {
                @Override
                public void onTransitionStart(Transition transition) {

                }

                @Override
                public void onTransitionEnd(Transition transition) {
                    findViewById(R.id.hide_transition).setVisibility(View.INVISIBLE);
                }

                @Override
                public void onTransitionCancel(Transition transition) {

                }

                @Override
                public void onTransitionPause(Transition transition) {

                }

                @Override
                public void onTransitionResume(Transition transition) {

                }
            });
        }

    }

    @Override
    public void onBackPressed() {
        if (getIntent().getBooleanExtra(SmartSettingsActivity.IS_SHARED_ELEMENT_TRANSITION_ENABLED, false))
            findViewById(R.id.hide_transition).setVisibility(View.VISIBLE);
        supportFinishAfterTransition();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (getIntent().getBooleanExtra(SmartSettingsActivity.IS_SHARED_ELEMENT_TRANSITION_ENABLED, false))
                    findViewById(R.id.hide_transition).setVisibility(View.VISIBLE);
                supportFinishAfterTransition();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
