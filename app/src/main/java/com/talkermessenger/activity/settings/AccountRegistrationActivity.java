package com.talkermessenger.activity.settings;

import android.Manifest;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.iskar.talkersdk.ContactHandler;
import com.iskar.talkersdk.LanguageSettingsHandler;
import com.iskar.talkersdk.model.TalkerContact;
import com.iskar.talkersdk.ui.LanguageSettingsView;
import com.talkermessenger.Matrix;
import com.talkermessenger.R;
import com.talkermessenger.activity.CommonActivityUtils;
import com.talkermessenger.activity.CountryPickerActivity;
import com.talkermessenger.activity.NewGroupCreationActivity;
import com.talkermessenger.activity.SplashActivity;
import com.talkermessenger.activity.VectorHomeActivity;
import com.talkermessenger.activity.VectorMediasPickerActivity;
import com.talkermessenger.activity.VectorMediasViewerActivity;
import com.talkermessenger.activity.VectorRoomActivity;
import com.talkermessenger.activity.VectorRoomInviteMembersActivity;
import com.talkermessenger.adapters.ParticipantAdapterItem;
import com.talkermessenger.receiver.VectorUniversalLinkReceiver;
import com.talkermessenger.util.PhoneNumberUtils;
import com.talkermessenger.util.PreferencesManager;
import com.talkermessenger.util.SlidableMediaInfo;
import com.talkermessenger.util.ThemeUtils;
import com.talkermessenger.util.VectorUtils;

import org.matrix.androidsdk.MXSession;
import org.matrix.androidsdk.listeners.MXMediaUploadListener;
import org.matrix.androidsdk.rest.callback.ApiCallback;
import org.matrix.androidsdk.rest.model.MatrixError;
import org.matrix.androidsdk.rest.model.Message;
import org.matrix.androidsdk.rest.model.User;
import org.matrix.androidsdk.util.Log;
import org.matrix.androidsdk.util.ResourceUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class AccountRegistrationActivity extends AppCompatActivity {

    @BindView(R.id.account_toolbar)
    Toolbar mToolbar;

    @BindView(R.id.name_section_text)
    EditText userName;

    @BindView(R.id.languages_holder)
    LanguageSettingsView languagesHolder;

    @BindView(R.id.add_language_button)
    RelativeLayout addLanguageButton;

    @BindView(R.id.circular_image_new_group)
    CircleImageView emptyImage;

    @BindView(R.id.name_section_icon_main_empty_image)
    RelativeLayout emptyImageFrame;

    @BindView(R.id.group_image)
    ImageView groupImage;

    @BindView(R.id.change_image)
    TextView changeImageText;

    private LanguageSettingsHandler languageSettingsHandler;
    private MXSession mSession;
    private User mUser;
    private String LOG_TAG = getClass().getName();
//    private String avatarUri;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_registration);

        ButterKnife.bind(this);
        setSupportActionBar(mToolbar);
        mSession = Matrix.getInstance(this).getDefaultSession();

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        userName.clearFocus();
        findViewById(R.id.focus_layout).requestFocus();

        setupUserLanguagesView();
        addLanguageButton.setOnClickListener(view -> {
            CommonActivityUtils.showLanguageSelectionDialog(mSession, this, languageSettingsHandler);
        });

//        setupUserData();
        userName.getBackground().setColorFilter(getResources().getColor(android.R.color.transparent), PorterDuff.Mode.SRC_IN);
        userName.setOnEditorActionListener((v, actionId, event) -> {
            if ((actionId == EditorInfo.IME_ACTION_DONE)) {
                onDoneNameEditing(v);
                return true;
            }
            return false;
        });

        emptyImage.setImageDrawable(getResources().getDrawable(R.drawable.ic_circle));
        emptyImage.setOnClickListener(v -> onUpdateAvatarClick());
        changeImageText.setOnClickListener(v -> onUpdateAvatarClick());
        groupImage.setOnClickListener(v -> onUpdateAvatarClick());
        emptyImage.setOnClickListener(v -> onUpdateAvatarClick());

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == VectorUtils.TAKE_IMAGE) {
            onActivityResultRoomAvatarUpdate(resultCode, data);
        }
    }


    private void onUpdateAvatarClick() {
        runOnUiThread(() -> {
            if (CommonActivityUtils.checkPermissions(CommonActivityUtils.REQUEST_CODE_PERMISSION_TAKE_PHOTO,
                    AccountRegistrationActivity.this)) {
                Intent intent = new Intent(AccountRegistrationActivity.this,
                        VectorMediasPickerActivity.class);
                intent.putExtra(VectorMediasPickerActivity.EXTRA_AVATAR_MODE, true);
                startActivityForResult(intent, VectorUtils.TAKE_IMAGE);
            }
        });
    }

    private void onActivityResultRoomAvatarUpdate(int aResultCode, final Intent aData) {
        // sanity check
        if (null == mSession) {
            return;
        }

        if (aResultCode == Activity.RESULT_OK) {
            Uri thumbnailUri = VectorUtils.getThumbnailUriFromIntent(this, aData, mSession.getMediasCache());

            if (null != thumbnailUri) {
//                avatarUri = thumbnailUri.toString();
                groupImage.setImageURI(thumbnailUri);
                emptyImageFrame.setVisibility(View.GONE);
                groupImage.setVisibility(View.VISIBLE);
                // save the bitmap URL on the server
                ResourceUtils.Resource resource = ResourceUtils.openResource(this, thumbnailUri, null);
                if (null != resource) {
                    mSession.getMediasCache().uploadContent(resource.mContentStream, null, resource.mMimeType, null, new MXMediaUploadListener() {

                        @Override
                        public void onUploadError(String uploadId, int serverResponseCode, String serverErrorMessage) {
                            AccountRegistrationActivity.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Log.e(LOG_TAG, "Fail to upload the avatar");
                                }
                            });
                        }

                        @Override
                        public void onUploadComplete(final String uploadId, final String contentUri) {
                            AccountRegistrationActivity.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Log.d(LOG_TAG, "The avatar has been uploaded, update the room avatar");
//                                    avatarUri = contentUri;
                                    mSession.getMyUser().updateAvatarUrl(contentUri, new ApiCallback<Void>() {
                                        @Override
                                        public void onSuccess(Void info) {
                                        }

                                        @Override
                                        public void onNetworkError(Exception e) {
                                        }

                                        @Override
                                        public void onMatrixError(MatrixError e) {
                                        }

                                        @Override
                                        public void onUnexpectedError(Exception e) {
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (CommonActivityUtils.shouldRestartApp(this) || (null == mSession)) {
            return false;
        }
        getMenuInflater().inflate(R.menu.account_registration, menu);
        CommonActivityUtils.tintMenuIcons(menu, ThemeUtils.getColor(this, R.attr.actionbar_text_color));
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_register:
                if (userName.getText().toString().equals("")) {
                    Toast.makeText(this, "Enter name", Toast.LENGTH_LONG).show();
                    return false;
                }
                if ((languageSettingsHandler.getTalkerLanguageList() == null)
                        || (languageSettingsHandler.getTalkerLanguageList()
                        != null && languageSettingsHandler.getTalkerLanguageList().getLangs().size() == 0)) {
                    Toast.makeText(this, "Add at least one language", Toast.LENGTH_LONG).show();
                    return false;
                }
                updateDisplayName(userName.getText().toString());
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupUserData() {

        if (mSession != null) mUser = mSession.getMyUser();
        if (null != mUser) {
            userName.setText(mUser.displayname);
        } else Toast.makeText(this, "user == null", Toast.LENGTH_LONG).show();

    }


    private void onDoneNameEditing(View view) {
        String youEditTextValue = userName.getText().toString();
        if (youEditTextValue.length() > 0) {
            userName.clearFocus();
            findViewById(R.id.focus_layout).requestFocus();
        }
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private void updateDisplayName(final String newName) {
        mSession.getMyUser().updateDisplayName(newName, new ApiCallback<Void>() {
            @Override
            public void onSuccess(Void info) {
                // refresh the settings value
                SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(AccountRegistrationActivity.this);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putString(PreferencesManager.SETTINGS_DISPLAY_NAME_PREFERENCE_KEY, newName);
                editor.commit();

                Intent intent = new Intent(AccountRegistrationActivity.this, VectorHomeActivity.class);

                Bundle receivedBundle = getIntent().getExtras();

                if (null != receivedBundle) {
                    intent.putExtras(receivedBundle);
                }

                // display a spinner while managing the universal link
                if (intent.hasExtra(VectorUniversalLinkReceiver.EXTRA_UNIVERSAL_LINK_URI)) {
                    intent.putExtra(VectorHomeActivity.EXTRA_WAITING_VIEW_STATUS, VectorHomeActivity.WAITING_VIEW_START);
                }

                // launch from a shared files menu
                if (getIntent().hasExtra(VectorHomeActivity.EXTRA_SHARED_INTENT_PARAMS)) {
                    intent.putExtra(VectorHomeActivity.EXTRA_SHARED_INTENT_PARAMS, (Parcelable) getIntent().getParcelableExtra(VectorHomeActivity.EXTRA_SHARED_INTENT_PARAMS));
                    getIntent().removeExtra(VectorHomeActivity.EXTRA_SHARED_INTENT_PARAMS);
                }

                if (getIntent().hasExtra(SplashActivity.EXTRA_ROOM_ID) && getIntent().hasExtra(SplashActivity.EXTRA_MATRIX_ID)) {
                    HashMap<String, Object> params = new HashMap<>();

                    params.put(VectorRoomActivity.EXTRA_MATRIX_ID, getIntent().getStringExtra(SplashActivity.EXTRA_MATRIX_ID));
                    params.put(VectorRoomActivity.EXTRA_ROOM_ID, getIntent().getStringExtra(SplashActivity.EXTRA_ROOM_ID));
                    intent.putExtra(VectorHomeActivity.EXTRA_JUMP_TO_ROOM_PARAMS, params);
                }
                startActivity(intent);
                finish();
            }

            @Override
            public void onNetworkError(Exception e) {
            }

            @Override
            public void onMatrixError(MatrixError e) {
            }

            @Override
            public void onUnexpectedError(Exception e) {
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        userName.clearFocus();
        findViewById(R.id.focus_layout).requestFocus();
    }

    @Override
    protected void onResume() {
        super.onResume();
        userName.clearFocus();
        findViewById(R.id.focus_layout).requestFocus();
    }

    private void setupUserLanguagesView() {
        languageSettingsHandler = new LanguageSettingsHandler();
        languageSettingsHandler.setActivity(this);
        languageSettingsHandler.setUserId(mSession.getMyUserId());
        languageSettingsHandler.setMyUid(mSession.getMyUserId());
        languageSettingsHandler.setUserToken(mSession.getCredentials().accessToken);
        languageSettingsHandler.setTextColor(VectorUtils.getTextColor(this));
        languageSettingsHandler.setBackgroundColor(VectorUtils.getBackgroundColor(this));
        languageSettingsHandler.init();
    }

}
