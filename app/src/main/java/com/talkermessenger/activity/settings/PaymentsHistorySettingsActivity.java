package com.talkermessenger.activity.settings;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.iskar.talkersdk.PaymentsHandler;
import com.iskar.talkersdk.callback.HistoryPaymentInterface;
import com.iskar.talkersdk.model.PaymentHistory;
import com.talkermessenger.Matrix;
import com.talkermessenger.R;
import com.talkermessenger.adapters.settings.PaymentsHistoryAdapter;
import com.talkermessenger.view.SimpleDividerItemDecoration;

import org.matrix.androidsdk.MXSession;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Serhii on 30.01.2018.
 */

public class PaymentsHistorySettingsActivity extends AppCompatActivity {

    @BindView(R.id.account_toolbar)
    Toolbar mToolbar;

    @BindView(R.id.payments_list)
    RecyclerView paymentsList;

    private MXSession mxSession;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings_payments);

        ButterKnife.bind(this);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mxSession = Matrix.getInstance(this).getDefaultSession();

        getPaymentsHistory();

    }

    private void getPaymentsHistory() {
        PaymentsHandler paymentsHandler = new PaymentsHandler(this);
        paymentsHandler.getPaymentHistory(mxSession.getMyUserId(), mxSession.getCredentials().accessToken, new HistoryPaymentInterface() {
            @Override
            public void onSuccess(List<PaymentHistory> history) {
                if (history != null && history.size() > 0)
                    setupRecycler(history);
            }

            @Override
            public void onFailure(String message) {

            }
        });
    }

    private void setupRecycler(List<PaymentHistory> history) {
        PaymentsHistoryAdapter paymentsHistoryAdapter = new PaymentsHistoryAdapter(this, history);
        paymentsList.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        paymentsList.addItemDecoration(new SimpleDividerItemDecoration(this, DividerItemDecoration.VERTICAL, 0));
        paymentsList.setAdapter(paymentsHistoryAdapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
