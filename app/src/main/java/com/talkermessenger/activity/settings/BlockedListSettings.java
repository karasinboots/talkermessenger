package com.talkermessenger.activity.settings;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.transition.Transition;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.Toast;

import com.iskar.talkersdk.BlockedUserHandler;
import com.iskar.talkersdk.callback.BlockUserInterface;
import com.iskar.talkersdk.model.Blocked;
import com.iskar.talkersdk.model.TalkerContact;
import com.talkermessenger.Matrix;
import com.talkermessenger.R;
import com.talkermessenger.activity.VectorRoomInviteGroupMembersActivity;
import com.talkermessenger.activity.VectorRoomInviteMembersActivity;
import com.talkermessenger.adapters.ParticipantAdapterItem;
import com.talkermessenger.adapters.settings.BlockedAdapter;

import org.jetbrains.annotations.NotNull;
import org.matrix.androidsdk.MXSession;

import java.util.List;

public class BlockedListSettings extends AppCompatActivity {

    private SearchView searchView;
    private RecyclerView recyclerView;
    private BlockedAdapter adapter;
    private BlockedUserHandler handler;
    private List<TalkerContact> blockedList;
    private Toolbar toolbar;
    private ProgressBar progressBar;
    private RelativeLayout blockContact;
    private static final int INVITE_USER_REQUEST_CODE = 456;
    MXSession mSession;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blocked_list_settings);
        mSession = Matrix.getInstance(this).getDefaultSession();
        setupViews();
        getData();


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (!getIntent().getBooleanExtra(SmartSettingsActivity.IS_SHARED_ELEMENT_TRANSITION_ENABLED, false))
                findViewById(R.id.hide_transition).setVisibility(View.GONE);

            getWindow().getSharedElementEnterTransition().addListener(new Transition.TransitionListener() {
                @Override
                public void onTransitionStart(Transition transition) {

                }

                @Override
                public void onTransitionEnd(Transition transition) {
                    findViewById(R.id.hide_transition).setVisibility(View.INVISIBLE);
                }

                @Override
                public void onTransitionCancel(Transition transition) {

                }

                @Override
                public void onTransitionPause(Transition transition) {

                }

                @Override
                public void onTransitionResume(Transition transition) {

                }
            });
        }

    }

    private void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    private void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onBackPressed() {
        if (getIntent().getBooleanExtra(SmartSettingsActivity.IS_SHARED_ELEMENT_TRANSITION_ENABLED, false))
            findViewById(R.id.hide_transition).setVisibility(View.VISIBLE);
        supportFinishAfterTransition();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (getIntent().getBooleanExtra(SmartSettingsActivity.IS_SHARED_ELEMENT_TRANSITION_ENABLED, false))
                    findViewById(R.id.hide_transition).setVisibility(View.VISIBLE);
                supportFinishAfterTransition();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupViews() {
        progressBar = findViewById(R.id.progress_bar);
        recyclerView = findViewById(R.id.blocked_list);
        toolbar = findViewById(R.id.blocked_toolbar);
        blockContact = findViewById(R.id.block_settings_click);
        blockContact.setOnClickListener(v -> {
            Intent intent = new Intent(this, VectorRoomInviteGroupMembersActivity.class);
            intent.putExtra(VectorRoomInviteGroupMembersActivity.EXTRA_MATRIX_ID, mSession.getMyUserId());
//            intent.putExtra(VectorRoomInviteGroupMembersActivity.EXTRA_HIDDEN_PARTICIPANT_ITEMS, mParticipants);
            this.startActivityForResult(intent, INVITE_USER_REQUEST_CODE);
        });
        searchView = new SearchView(this);
        setSupportActionBar(toolbar);
        try {
            getSupportActionBar().setCustomView(searchView);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

    }
    private void getData() {
        showProgress();
        handler = new BlockedUserHandler(
                mSession.getMyUserId(),
                mSession.getCredentials().accessToken);
        handler.getBlockedUsers(new BlockUserInterface.GetBlockUserInterface() {
            @Override
            public void blockedUsersReceived(List<Blocked> blockedList) {
                BlockedListSettings.this.blockedList = handler.getBlockedUsersData(BlockedListSettings.this, (List<Blocked>) blockedList);
                setupAdapter();
                hideProgress();
            }

            @Override
            public void onFailure(String message) {
                Toast.makeText(BlockedListSettings.this, getString(R.string.something_wrong), Toast.LENGTH_SHORT).show();
                hideProgress();
            }
        });
    }

    private void setupAdapter() {
        adapter = new BlockedAdapter(this, blockedList,
                contact -> {
                    showProgress();
                    handler.unblockUser(contact.getMatrixId(), new BlockUserInterface.BlockingUser() {
                        @Override
                        public void onSuccess() {
                            blockedList.remove(contact);
                            adapter.notifyDataSetChanged();
                            hideProgress();
                        }

                        @Override
                        public void onFailure(String message) {
                            Toast.makeText(BlockedListSettings.this, getString(R.string.something_wrong), Toast.LENGTH_SHORT).show();
                            hideProgress();
                        }
                    });
                });
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(adapter);
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        getData();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == INVITE_USER_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                List<ParticipantAdapterItem> items = (List<ParticipantAdapterItem>) data.getSerializableExtra(VectorRoomInviteMembersActivity.EXTRA_OUT_SELECTED_PARTICIPANT_ITEMS);
                for (ParticipantAdapterItem newItem : items) {
                    handler.blockUser(newItem.mUserId, new BlockUserInterface.BlockingUser() {
                        @Override
                        public void onSuccess() {
                            Toast.makeText(BlockedListSettings.this, "User was blocked", Toast.LENGTH_SHORT).show();
                            getData();
                        }

                        @Override
                        public void onFailure(@NotNull String message) {
                            Toast.makeText(BlockedListSettings.this, getString(R.string.something_wrong), Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        }
    }
}
