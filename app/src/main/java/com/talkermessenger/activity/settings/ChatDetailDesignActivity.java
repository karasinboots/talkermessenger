package com.talkermessenger.activity.settings;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Intent;
import android.content.res.AssetManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.transition.Transition;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.jaredrummler.android.colorpicker.ColorPickerDialog;
import com.jaredrummler.android.colorpicker.ColorPickerDialogListener;
import com.talkermessenger.GlideApp;
import com.talkermessenger.R;
import com.talkermessenger.VectorApp;
import com.talkermessenger.adapters.WallpaperChooserAdapter;
import com.talkermessenger.util.PreferencesManager;
import com.talkermessenger.util.ThemeUtils;

import org.matrix.androidsdk.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.talkermessenger.activity.settings.ChatsDesignPreview.IN_BG;
import static com.talkermessenger.activity.settings.ChatsDesignPreview.IN_TXT;
import static com.talkermessenger.activity.settings.ChatsDesignPreview.OUT_BG;
import static com.talkermessenger.activity.settings.ChatsDesignPreview.OUT_TXT;
import static com.talkermessenger.util.ThemeUtils.THEME_LIGHT_LIGHT_BLUE_HEADER_VALUE;

/**
 * Created by Nikita on 29.03.2018.
 */

public class ChatDetailDesignActivity extends AppCompatActivity implements WallpaperChooserAdapter.ItemClickListener, ColorPickerDialogListener {

    @BindView(R.id.photo_library_click)
    RelativeLayout photoLibraryClick;

    @BindView(R.id.toolbar2)
    Toolbar toolbar;

    @BindView(R.id.wallpaper_recycler)
    RecyclerView wallpaperRecycler;

    @BindView(R.id.wallpaper_progress)
    ProgressBar progressBar;

    @BindView(R.id.bg_in_color_layout)
    LinearLayout bgInColorLayout;
    @BindView(R.id.bg_in_color)
    ImageView bgInColor;
    @BindView(R.id.txt_in_color_layout)
    LinearLayout txtInColorLayout;
    @BindView(R.id.txt_in_color)
    ImageView txtInColor;

    @BindView(R.id.bg_out_color_layout)
    LinearLayout bgOutColorLayout;
    @BindView(R.id.bg_out_color)
    ImageView bgOutColor;
    @BindView(R.id.txt_out_color_layout)
    LinearLayout txtOutColorLayout;
    @BindView(R.id.txt_out_color)
    ImageView txtOutColor;
    @BindView(R.id.app_theme_settings_click)
    RelativeLayout mThemeSettings;
    @BindView(R.id.default_theme_settings_click)
    RelativeLayout restoreDefaultsClick;
    Handler handler = new Handler();
    WallpaperChooserAdapter mAdapter;

    private static final int PICK_IMAGE = 1111;

    @Nullable
    @BindView(R.id.for_transition)
    ImageView transition;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chat_detail_desighn);

        ButterKnife.bind(this);
        copyAssets();


        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        photoLibraryClick.setOnClickListener(v -> {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_PICK);
            startActivityForResult(Intent.createChooser(intent, getString(R.string.select_picture)), PICK_IMAGE);
        });
        mThemeSettings.setOnClickListener(v -> startActivity(new Intent(ChatDetailDesignActivity.this, AppThemeActivity.class)));
        restoreDefaultsClick.setOnClickListener(v -> {
            PreferencesManager.setIntIncomingBgColor(this, this.getResources().getColor(R.color.default_income));
            PreferencesManager.setIntOutgoingBgColor(this, this.getResources().getColor(R.color.default_outgoing));
            PreferencesManager.setIntIncomingChatColor(this, -16777216);
            PreferencesManager.setIntOutgoingChatColor(this, -16777216);
            PreferencesManager.setBackgroundChatPicture(this, null);
            applyTheme(THEME_LIGHT_LIGHT_BLUE_HEADER_VALUE);
            Toast.makeText(this, "Settings restored", Toast.LENGTH_LONG).show();
            colorLogic();
        });


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            ThemeUtils.setTransitionImage(this, "design_settings", transition);

            if (!getIntent().getBooleanExtra(SmartSettingsActivity.IS_SHARED_ELEMENT_TRANSITION_ENABLED, false))
                findViewById(R.id.hide_transition).setVisibility(View.GONE);

            getWindow().getSharedElementEnterTransition().addListener(new Transition.TransitionListener() {
                @Override
                public void onTransitionStart(Transition transition) {

                }

                @Override
                public void onTransitionEnd(Transition transition) {
                    findViewById(R.id.hide_transition).setVisibility(View.INVISIBLE);
                }

                @Override
                public void onTransitionCancel(Transition transition) {

                }

                @Override
                public void onTransitionPause(Transition transition) {

                }

                @Override
                public void onTransitionResume(Transition transition) {

                }
            });
        }

    }

    private void applyTheme(String theme) {

        if (theme.equals(ThemeUtils.getApplicationTheme(this)))
            return;

        // configureSelectedIcon(theme);

        handler.postDelayed(() -> {
            VectorApp.updateApplicationTheme(theme);
            startActivity(getIntent());
            finish();
        }, 500);

    }

    @Override
    public void onBackPressed() {
        if (getIntent().getBooleanExtra(SmartSettingsActivity.IS_SHARED_ELEMENT_TRANSITION_ENABLED, false))
            findViewById(R.id.hide_transition).setVisibility(View.VISIBLE);
        supportFinishAfterTransition();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (getIntent().getBooleanExtra(SmartSettingsActivity.IS_SHARED_ELEMENT_TRANSITION_ENABLED, false))
                    findViewById(R.id.hide_transition).setVisibility(View.VISIBLE);
                supportFinishAfterTransition();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void colorLogic() {
        bgInColorLayout.setOnClickListener(v -> startPreview(IN_BG));
        bgOutColorLayout.setOnClickListener(v -> startPreview(OUT_BG));
        txtInColorLayout.setOnClickListener(v -> startPreview(IN_TXT));
        txtOutColorLayout.setOnClickListener(v -> startPreview(OUT_TXT));

        bgInColor.setBackgroundColor(PreferencesManager.getIntIncomingBgColor(this));
        bgOutColor.setBackgroundColor(PreferencesManager.getIntOutgoingBgColor(this));
        txtInColor.setBackgroundColor(PreferencesManager.getIntIncomingChatColor(this));
        txtOutColor.setBackgroundColor(PreferencesManager.getIntOutgoingChatColor(this));
    }

    @Override
    public void onColorSelected(int dialogId, int color) {
        switch (dialogId) {
            case IN_BG:
                PreferencesManager.setIntIncomingBgColor(this, color);
                break;
            case OUT_BG:
                PreferencesManager.setIntOutgoingBgColor(this, color);
                break;
            case IN_TXT:
                PreferencesManager.setIntIncomingChatColor(this, color);
                break;
            case OUT_TXT:
                PreferencesManager.setIntOutgoingChatColor(this, color);
                break;
        }
        colorLogic();
    }

    @Override
    public void onDialogDismissed(int dialogId) {

    }

    @Override
    protected void onResume() {
        super.onResume();
        colorLogic();
    }

    private void startPreview(int tag) {
//        Intent intent = new Intent(this, ChatsDesignPreview.class);
//        intent.putExtra(ChatsDesignPreview.TAG, tag);
//        startActivity(intent);
        int color = 0;
        switch (tag) {
            case IN_BG:
                color = PreferencesManager.getIntIncomingBgColor(this);
                break;
            case OUT_BG:
                color = PreferencesManager.getIntOutgoingBgColor(this);
                break;
            case IN_TXT:
                color = PreferencesManager.getIntIncomingChatColor(this);
                break;
            case OUT_TXT:
                color = PreferencesManager.getIntOutgoingChatColor(this);
                break;
        }
        ColorPickerDialog.newBuilder()
                .setColor(color)
                .setDialogId(tag)
                .show(this);
    }

    private void setupRecycler(List<String> wallpaperList) {
        // set up the RecyclerView
        wallpaperRecycler.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        mAdapter = new WallpaperChooserAdapter(this, wallpaperList);
        mAdapter.setClickListener(this);
        wallpaperRecycler.setAdapter(mAdapter);
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onItemClick(View view, int position) {
        Log.e("wallpaper clicked", "You clicked number " + mAdapter.getItem(position) + ", which is at cell position " + position);
        Uri pictureUri = Uri.parse(mAdapter.getItem(position));
        if (null != pictureUri) {
            PreferencesManager.setBackgroundChatPicture(this, pictureUri.toString());
            Toast.makeText(this, pictureUri == null ? getString(R.string.something_wrong) : getString(R.string.success), Toast.LENGTH_SHORT).show();
        }
    }


    private List<String> getWallpaperList() {
        List<String> paths = new ArrayList<String>();
        File directory = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/im_wallpapers/");
        Log.e("directory path", Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/im_wallpapers");
        File[] files = directory.listFiles();
        if (files != null && files.length > 0)
            for (File file : files) {
                if (file.getAbsolutePath().substring(file.getAbsolutePath().lastIndexOf('/')).contains("wallpaper")) {
                    paths.add(file.getAbsolutePath());
                    Log.e("wallpaper_Added", file.getAbsolutePath());
                }
            }
        return paths;
    }

    @SuppressLint("StaticFieldLeak")
    private void copyAssets() {
        progressBar.setVisibility(View.VISIBLE);
        new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] objects) {
                AssetManager assetManager = getAssets();
                String[] files = null;
                try {

                    File directory = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/im_wallpapers/");
                    directory.mkdirs();
                    File noMedia = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/im_wallpapers/.nomedia");
                    try {
                        if (!noMedia.exists())
                            noMedia.createNewFile();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    files = assetManager.list("wallpapers");
                } catch (IOException e) {
                    Log.e("tag", "Failed to get asset file list.", e);
                }
                if (files != null) for (String filename : files) {
                    InputStream in = null;
                    OutputStream out = null;
                    try {
                        in = assetManager.open("wallpapers/" + filename);
                        File outFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/im_wallpapers/", filename);
                        out = new FileOutputStream(outFile);
                        copyFile(in, out);
                    } catch (IOException e) {
                        Log.e("tag", "Failed to copy asset file: " + filename, e);
                    } finally {
                        if (in != null) {
                            try {
                                in.close();
                            } catch (IOException e) {
                                // NOOP
                            }
                        }
                        if (out != null) {
                            try {
                                out.close();
                            } catch (IOException e) {
                                // NOOP
                            }
                        }
                    }
                }
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                setupRecycler(getWallpaperList());
            }
        }.execute();
    }

    private void copyFile(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[1024];
        int read;
        while ((read = in.read(buffer)) != -1) {
            out.write(buffer, 0, read);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case PICK_IMAGE:
                    Uri pictureUri = data.getData();
                    Toast.makeText(this, pictureUri == null ? getString(R.string.something_wrong) : getString(R.string.success), Toast.LENGTH_SHORT).show();
                    if (null != pictureUri) {
                        PreferencesManager.setBackgroundChatPicture(this, pictureUri.toString());
                    }
                    finish();
                    break;
            }
        }
    }


}
