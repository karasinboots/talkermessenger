package com.talkermessenger.activity.settings;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.talkermessenger.R;

import butterknife.ButterKnife;

/**
 * Created by Nikita on 30.03.2018.
 */

public class ChatsDesignPreview extends Activity {

    public static final int IN_BG = 0;
    public static final int IN_TXT = 1;
    public static final int OUT_BG = 2;
    public static final int OUT_TXT = 3;

    public static final String TAG = "intent_tag";

    private int currentTag;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_design_preview);
        ButterKnife.bind(this);
        currentTag = getIntent().getIntExtra(TAG, -1);

    }
}
