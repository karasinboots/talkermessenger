package com.talkermessenger.activity.settings;

import android.Manifest;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.transition.Transition;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.iskar.talkersdk.ContactHandler;
import com.iskar.talkersdk.LanguageSettingsHandler;
import com.iskar.talkersdk.model.TalkerContact;
import com.iskar.talkersdk.ui.LanguageSettingsView;
import com.iskar.talkersdk.ui.languageRecycler.Languages;
import com.talkermessenger.Matrix;
import com.talkermessenger.R;
import com.talkermessenger.activity.CommonActivityUtils;
import com.talkermessenger.activity.CountryPickerActivity;
import com.talkermessenger.activity.VectorMediasPickerActivity;
import com.talkermessenger.activity.VectorMediasViewerActivity;
import com.talkermessenger.activity.VectorMemberMediaActivity;
import com.talkermessenger.util.PhoneNumberUtils;
import com.talkermessenger.util.PreferencesManager;
import com.talkermessenger.util.RoomUtils;
import com.talkermessenger.util.SlidableMediaInfo;
import com.talkermessenger.util.ThemeUtils;
import com.talkermessenger.util.VectorUtils;

import org.matrix.androidsdk.MXSession;
import org.matrix.androidsdk.listeners.MXMediaUploadListener;
import org.matrix.androidsdk.rest.callback.ApiCallback;
import org.matrix.androidsdk.rest.model.MatrixError;
import org.matrix.androidsdk.rest.model.Message;
import org.matrix.androidsdk.rest.model.User;
import org.matrix.androidsdk.util.Log;
import org.matrix.androidsdk.util.ResourceUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by dev_serhii on 18.01.2018.
 */

public class AccountActivity extends AppCompatActivity {

    private static final int REQUEST_LOGIN_COUNTRY = 5678;
    private String LOG_TAG = getClass().getName();
//    private static final int REQUEST_LOCALE = 777;

    @BindView(R.id.account_toolbar)
    Toolbar mToolbar;

    @BindView(R.id.member_image)
    ImageView userAvatar;

    @BindView(R.id.name_section_text)
    EditText userName;

    @BindView(R.id.country_section_text)
    TextView userCountry;

    @BindView(R.id.contact_flag)
    ImageView countryFlag;

    @BindView(R.id.languages_holder)
    LanguageSettingsView languagesHolder;

    @BindView(R.id.favourite_section)
    RelativeLayout mFavouriteLayout;

    @BindView(R.id.show_all_media_section)
    RelativeLayout mShowAllMediaLayout;

//    @BindView(R.id.name_section_edit_name)
//    ImageView editName;

    @BindView(R.id.collapsing_toolbar)
    CollapsingToolbarLayout collapsingToolbarLayout;

    @BindView(R.id.statistics_section)
    RelativeLayout mStatisticsLayout;

    @Nullable
    @BindView(R.id.for_transition)
    ImageView transition;

    private LanguageSettingsHandler languageSettingsHandler;
    private MXSession mSession;
    private User mUser;
    private int mCameraPermissionAction;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_account);

        ButterKnife.bind(this);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        // userAvatar.requestFocus();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        userName.clearFocus();
//        userName.setCursorVisible(false);

        mSession = Matrix.getInstance(this).getDefaultSession();

        findViewById(R.id.country_section_button).setOnClickListener(view -> {
            final Intent intent = CountryPickerActivity.getIntent(AccountActivity.this, true);
            startActivityForResult(intent, REQUEST_LOGIN_COUNTRY);
        });

        findViewById(R.id.add_language_button).setOnClickListener(view -> {
            // startActivityForResult(LanguagePickerActivity.getIntent(AccountActivity.this), REQUEST_LOCALE);
            CommonActivityUtils.showLanguageSelectionDialog(mSession, this, languageSettingsHandler);
        });

        userName.getBackground().setColorFilter(getResources().getColor(android.R.color.transparent), PorterDuff.Mode.SRC_IN);

        userName.setOnEditorActionListener((v, actionId, event) -> {
            if ((actionId == EditorInfo.IME_ACTION_DONE)) {
                onDoneNameEditing(v);
                return true;
            }
            return false;
        });

//        userAvatar.setOnClickListener(v -> onUpdateAvatarClick());
        userAvatar.setOnClickListener(v -> getAvatarDialog().show());

        setupUserData();
        setupUserLanguagesView();

        mFavouriteLayout.setOnClickListener(view -> {
//            String roomId = PreferencesManager.getFavouriteMsgCommonRoom(this);
//            if (roomId != null)
//                RoomUtils.openFavouriteMsgRoomById(roomId, mSession, this);
//            else {
//                Room favouriteMsgsRoom = RoomUtils.findFavouriteMsgsRoom(mSession);
//                if (favouriteMsgsRoom != null)
//                    RoomUtils.openFavouriteMsgRoomById(favouriteMsgsRoom.getRoomId(), mSession, this);
//                else
//                    RoomUtils.createFavouriteMsgRoom(mSession, this);
//            }
            RoomUtils.openFavoutiteMsgs(this, mSession, null, null);
        });

        mShowAllMediaLayout.setOnClickListener(view -> {
            Intent mediaIntent = new Intent(this, VectorMemberMediaActivity.class);
            mediaIntent.putExtra("userId", mUser.user_id);
            startActivity(mediaIntent);
        });

        mStatisticsLayout.setOnClickListener(v -> {
            startActivity(new Intent(this, StatisticsSettingsActivity.class));
        });

        if (!getIntent().getBooleanExtra(SmartSettingsActivity.IS_SHARED_ELEMENT_TRANSITION_ENABLED, false)) {
            if (findViewById(R.id.hide_transition) != null)
                findViewById(R.id.hide_transition).setVisibility(View.GONE);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            ThemeUtils.setTransitionImage(this, "profile_settings", transition);

            getWindow().getSharedElementEnterTransition().addListener(new Transition.TransitionListener() {
                @Override
                public void onTransitionStart(Transition transition) {

                }

                @Override
                public void onTransitionEnd(Transition transition) {
                    findViewById(R.id.hide_transition).setVisibility(View.INVISIBLE);
                }

                @Override
                public void onTransitionCancel(Transition transition) {

                }

                @Override
                public void onTransitionPause(Transition transition) {

                }

                @Override
                public void onTransitionResume(Transition transition) {

                }
            });
        }

    }

    private AlertDialog getAvatarDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        CharSequence menuItems[];
        ArrayList<CharSequence> arrayList = new ArrayList<CharSequence>();
        arrayList.add("Camera");
        arrayList.add("Media");
        TalkerContact contact = new ContactHandler().getContactFromTalkerContactSnapshot(mUser.user_id, this);
        if (!(TextUtils.isEmpty(mUser.getAvatarUrl()) && !(contact != null && contact.getThumbnailUri() != null))) {
            menuItems = new CharSequence[]{getString(R.string.account_activity_camera), getString(R.string.account_activity_media), getString(R.string.account_activity_open), getString(R.string.account_activity_delete)};
//            arrayList.add("Open");
//            arrayList.add("Delete");
        } else {
            menuItems = new CharSequence[]{getString(R.string.account_activity_camera), getString(R.string.account_activity_media)};
        }
        builder.setItems(menuItems, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                if (CommonActivityUtils.checkPermissions(CommonActivityUtils.REQUEST_CODE_PERMISSION_TAKE_PHOTO, AccountActivity.this)) {
                                    launchCamera();
                                } else {
                                    mCameraPermissionAction = R.string.option_take_photo;
                                }
                                dialog.dismiss();
                                break;
                            case 1:
                                if (CommonActivityUtils.checkPermissions(CommonActivityUtils.REQUEST_CODE_PERMISSION_TAKE_PHOTO, AccountActivity.this)) {
                                    openPhotoExplorer();
                                } else {
                                    mCameraPermissionAction = R.string.option_take_photo_from_media;
                                }
                                dialog.dismiss();
                                break;
                            case 2:
                                openImage();
                                dialog.dismiss();
                                break;
                            case 3:
                                deleteAvatar();
                                mUser.setAvatarUrl("");
                                setupUserData();
                                dialog.dismiss();
                                break;
                        }
                    }
                }
        );
        return builder.create();
    }

    private void deleteAvatar() {

        // Uri man2Uri=Uri.parse((getResources().getDrawable(R.drawable.man2)));

        Uri man2Uri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE +
                "://" + getResources().getResourcePackageName(R.drawable.man2)
                + '/' + getResources().getResourceTypeName(R.drawable.man2) + '/' + getResources().getResourceEntryName(R.drawable.man2));
        //  Log.d("---","Uri man2:"+man2Uri );

        ResourceUtils.Resource resource = ResourceUtils.openResource(AccountActivity.this, man2Uri, null);

        mSession.getMediasCache().uploadContent(resource.mContentStream, null, resource.mMimeType, null, new MXMediaUploadListener() {

            @Override
            public void onUploadError(final String uploadId, final int serverResponseCode, final String serverErrorMessage) {
                //  Log.d("---","uploadError"+serverErrorMessage);
                                    /*AccountActivity.this.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            onCommonDone(serverResponseCode + " : " + serverErrorMessage);
                                        }
                                    });*/
            }

            @Override
            public void onUploadComplete(final String uploadId, final String contentUri) {
                AccountActivity.this.runOnUiThread(() ->
                        mSession.getMyUser().updateAvatarUrl(contentUri, new ApiCallback<Void>() {
                            @Override
                            public void onSuccess(Void info) {
                                setupUserData();
                            }

                            @Override
                            public void onNetworkError(Exception e) {
                            }

                            @Override
                            public void onMatrixError(MatrixError e) {
                            }

                            @Override
                            public void onUnexpectedError(Exception e) {
                            }
                        }));
            }
        });

    }

    private void openImage() {
        ArrayList<SlidableMediaInfo> mediaMessagesList = listSlidableMessages();
        if (mediaMessagesList != null) {
            Intent viewImageIntent = new Intent(this, VectorMediasViewerActivity.class);
            viewImageIntent.putExtra(VectorMediasViewerActivity.EXTRA_MATRIX_ID, mSession.getCredentials().userId);
            viewImageIntent.putExtra(VectorMediasViewerActivity.KEY_INFO_LIST, mediaMessagesList);
            viewImageIntent.putExtra(VectorMediasViewerActivity.KEY_INFO_LIST_INDEX, 0);
            startActivity(viewImageIntent);
        }

    }

    private ArrayList<SlidableMediaInfo> listSlidableMessages() {
        ArrayList<SlidableMediaInfo> res = new ArrayList<>();
        TalkerContact contact = new ContactHandler().getContactFromTalkerContactSnapshot(mUser.user_id, this);
        SlidableMediaInfo info = new SlidableMediaInfo();
        info.mMessageType = Message.MSGTYPE_IMAGE;
        if (contact != null)
            info.mFileName = contact.getDisplayName();
        else
            info.mFileName = mUser.displayname;
        if (mSession.getDataHandler().getUser(mUser.user_id).getAvatarUrl() != null)
            info.mMediaUrl = mSession.getDataHandler().getUser(mUser.user_id).getAvatarUrl();
        else if (contact.getPhotoUri() != null)
            info.mMediaUrl = contact.getPhotoUri();
        else
            return null;
        res.add(info);
        return res;
    }

    private void openPhotoExplorer() {
        try {
            Intent fileIntent = new Intent(Intent.ACTION_PICK);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
                fileIntent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, false);
            }
            fileIntent.setType(ResourceUtils.MIME_TYPE_IMAGE_ALL);
            startActivityForResult(fileIntent, VectorUtils.TAKE_IMAGE);
        } catch (Exception e) {
            Toast.makeText(this, e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Launch the camera
     */
    private void launchCamera() {
//        enableActionBarHeader(HIDE_ACTION_BAR_HEADER);
        Intent intent = new Intent(this, VectorMediasPickerActivity.class);
        intent.putExtra(VectorMediasPickerActivity.EXTRA_VIDEO_RECORDING_MODE, true);
        startActivityForResult(intent, VectorUtils.TAKE_IMAGE);
    }

    private void onDoneNameEditing(View view) {
        String youEditTextValue = userName.getText().toString();
        if (youEditTextValue.length() > 0) {
            updateDisplayName(youEditTextValue);
//            editName.setVisibility(View.GONE);
//            userName.setCursorVisible(false);
            userName.clearFocus();
        }
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

//    private void closeNameEditing(){
//        userName.clearFocus();
//        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
//        inputMethodManager.hideSoftInputFromWindow(userName.getWindowToken(), 0);
//    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case REQUEST_LOGIN_COUNTRY:
                    if (data != null && data.hasExtra(CountryPickerActivity.EXTRA_OUT_COUNTRY_CODE)) {
                        userCountry.setText(data.getStringExtra(CountryPickerActivity.EXTRA_OUT_COUNTRY_NAME));
                        final String countryCode = data.getStringExtra(CountryPickerActivity.EXTRA_OUT_COUNTRY_CODE);
                        if (!TextUtils.equals(countryCode, PhoneNumberUtils.getCountryCode(this))) {
                            PhoneNumberUtils.setCountryCode(this, countryCode);
                        }
                        setCountryFlag();
                    }
                    break;
//                case REQUEST_LOCALE:
//                    refreshDisplay();
//                    break;
                case VectorUtils.TAKE_IMAGE:
                    Uri thumbnailUri = VectorUtils.getThumbnailUriFromIntent(AccountActivity.this, data, mSession.getMediasCache());

                    if (null != thumbnailUri) {
                        ResourceUtils.Resource resource = ResourceUtils.openResource(AccountActivity.this, thumbnailUri, null);
                        if (null != resource) {
                            mSession.getMediasCache().uploadContent(resource.mContentStream, null, resource.mMimeType, null, new MXMediaUploadListener() {

                                @Override
                                public void onUploadError(final String uploadId, final int serverResponseCode, final String serverErrorMessage) {
                                    /*AccountActivity.this.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            onCommonDone(serverResponseCode + " : " + serverErrorMessage);
                                        }
                                    });*/
                                }

                                @Override
                                public void onUploadComplete(final String uploadId, final String contentUri) {
                                    AccountActivity.this.runOnUiThread(() ->
                                            mSession.getMyUser().updateAvatarUrl(contentUri, new ApiCallback<Void>() {
                                                @Override
                                                public void onSuccess(Void info) {
                                                    setupUserData();
                                                }

                                                @Override
                                                public void onNetworkError(Exception e) {
                                                }

                                                @Override
                                                public void onMatrixError(MatrixError e) {
                                                }

                                                @Override
                                                public void onUnexpectedError(Exception e) {
                                                }
                                            }));
                                }
                            });
                        }
                    }
                    break;
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (getIntent().getBooleanExtra(SmartSettingsActivity.IS_SHARED_ELEMENT_TRANSITION_ENABLED, false))
            findViewById(R.id.hide_transition).setVisibility(View.VISIBLE);
        supportFinishAfterTransition();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (getIntent().getBooleanExtra(SmartSettingsActivity.IS_SHARED_ELEMENT_TRANSITION_ENABLED, false))
                    findViewById(R.id.hide_transition).setVisibility(View.VISIBLE);
                supportFinishAfterTransition();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private ArrayList<Languages> getLanguages() {
        ArrayList<Languages> lList = new ArrayList<>();
        BufferedReader reader = null;
        try {
            lList.clear();
            reader = new BufferedReader(
                    new InputStreamReader(this.getAssets().open("lang.txt"), "UTF-8"));
            String mLine;
            while ((mLine = reader.readLine()) != null) {
                String[] separated = mLine.split(",");
                lList.add(new Languages(separated[0], separated[1], separated[2]));
            }
            return lList;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void setupUserData() {

        if (mSession != null) mUser = mSession.getMyUser();
        if (null != mUser) {
            VectorUtils.loadUserAvatar(this, mSession, userAvatar, mUser);
            userName.setText(mUser.displayname);
            userCountry.setText(PhoneNumberUtils.getHumanCountryCode(PhoneNumberUtils.getCountryCode(this)));
            setCountryFlag();
        } else showToast("user == null");

    }

    @Override
    protected void onStart() {
        super.onStart();
        setupUserData();
        userName.clearFocus();
        findViewById(R.id.focus_layout).requestFocus();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setupUserData();
        userName.clearFocus();
        findViewById(R.id.focus_layout).requestFocus();
    }

    private void setCountryFlag() {
        try {
            countryFlag.setImageDrawable(getResources().getDrawable(getResources().
                    getIdentifier(PhoneNumberUtils.getCountryCode(this).toLowerCase(),
                            "drawable", getPackageName())));
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void setupUserLanguagesView() {
        languageSettingsHandler = new LanguageSettingsHandler();
        languageSettingsHandler.setActivity(this);
        languageSettingsHandler.setUserId(mSession.getMyUserId());
        languageSettingsHandler.setMyUid(mSession.getMyUserId());
        languageSettingsHandler.setUserToken(mSession.getCredentials().accessToken);
        languageSettingsHandler.setTextColor(VectorUtils.getTextColor(this));
        languageSettingsHandler.setBackgroundColor(VectorUtils.getBackgroundColor(this));
        languageSettingsHandler.init();
    }

    /**
     * Update the avatar.
     */
    private void onUpdateAvatarClick() {
        runOnUiThread(() -> {
            if (CommonActivityUtils.checkPermissions(CommonActivityUtils.REQUEST_CODE_PERMISSION_TAKE_PHOTO,
                    AccountActivity.this)) {
                Intent intent = new Intent(AccountActivity.this,
                        VectorMediasPickerActivity.class);
                intent.putExtra(VectorMediasPickerActivity.EXTRA_AVATAR_MODE, true);
                startActivityForResult(intent, VectorUtils.TAKE_IMAGE);
            }
        });
    }


    private void showSetNameAlert() {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle(R.string.set_your_display_name);

        LinearLayout layout = new LinearLayout(this);
        layout.setOrientation(LinearLayout.VERTICAL);

        final EditText editText = new EditText(this);
        layout.addView(editText);

        alert.setView(layout);

        alert.setPositiveButton(getResources().getString(R.string.save), (dialog, whichButton) -> {
            String youEditTextValue = editText.getText().toString();
            if (youEditTextValue.length() > 0) {
                updateDisplayName(youEditTextValue);
            }
        });

        alert.setNegativeButton(getResources().getString(R.string.cancel), (dialog, whichButton) -> {
            // what ever you want to do with No option.
        });

        alert.show();
    }

    /**
     * Update the display name.
     *
     * @param newName
     */
    private void updateDisplayName(final String newName) {
        if (!TextUtils.equals(mSession.getMyUser().displayname, newName)) {
            mSession.getMyUser().updateDisplayName(newName, new ApiCallback<Void>() {
                @Override
                public void onSuccess(Void info) {
                    // refresh the settings value
                    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(AccountActivity.this);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putString(PreferencesManager.SETTINGS_DISPLAY_NAME_PREFERENCE_KEY, newName);
                    editor.commit();
                    setupUserData();
                }

                @Override
                public void onNetworkError(Exception e) {
                }

                @Override
                public void onMatrixError(MatrixError e) {
                }

                @Override
                public void onUnexpectedError(Exception e) {
                }
            });
        }
    }

    private void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRequestPermissionsResult(int aRequestCode, @NonNull String[] aPermissions, @NonNull int[] aGrantResults) {
        if (0 == aPermissions.length) {
            Log.e(LOG_TAG, "## onRequestPermissionsResult(): cancelled " + aRequestCode);
        } else if (aRequestCode == CommonActivityUtils.REQUEST_CODE_PERMISSION_TAKE_PHOTO) {
            boolean isCameraPermissionGranted = false;

            for (int i = 0; i < aPermissions.length; i++) {
                Log.d(LOG_TAG, "## onRequestPermissionsResult(): " + aPermissions[i] + "=" + aGrantResults[i]);

                if (Manifest.permission.CAMERA.equals(aPermissions[i])) {
                    if (PackageManager.PERMISSION_GRANTED == aGrantResults[i]) {
                        Log.d(LOG_TAG, "## onRequestPermissionsResult(): CAMERA permission granted");
                        isCameraPermissionGranted = true;
                    } else {
                        Log.d(LOG_TAG, "## onRequestPermissionsResult(): CAMERA permission not granted");
                    }
                }

                if (Manifest.permission.WRITE_EXTERNAL_STORAGE.equals(aPermissions[i])) {
                    if (PackageManager.PERMISSION_GRANTED == aGrantResults[i]) {
                        Log.d(LOG_TAG, "## onRequestPermissionsResult(): WRITE_EXTERNAL_STORAGE permission granted");
                    } else {
                        Log.d(LOG_TAG, "## onRequestPermissionsResult(): WRITE_EXTERNAL_STORAGE permission not granted");
                    }
                }
            }

            // Because external storage permission is not mandatory to launch the camera,
            // external storage permission is not tested.
            if (isCameraPermissionGranted) {
                if (mCameraPermissionAction == R.string.option_take_photo)
                    launchCamera();
                else if (mCameraPermissionAction == R.string.option_take_photo_from_media)
                    openPhotoExplorer();

            } else {
                CommonActivityUtils.displayToast(this, getString(R.string.missing_permissions_warning));
            }
        }
    }

}
