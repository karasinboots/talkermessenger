package com.talkermessenger.activity.settings;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.github.mikephil.charting.charts.BarChart;
import com.iskar.talkersdk.StatisticHandler;
import com.iskar.talkersdk.callback.StatisticInterface;
import com.iskar.talkersdk.model.Statistics;
import com.talkermessenger.Matrix;
import com.talkermessenger.R;
import com.talkermessenger.util.DateUtill;

import org.matrix.androidsdk.util.Log;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by dev_serhii on 25.01.2018.
 */

public class TranslationStatisticsSettingsActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.account_toolbar)
    Toolbar mToolbar;

    @BindView(R.id.daily_chart)
    Button mDailyChartButton;

    @BindView(R.id.weekly_chart)
    Button mWeeklyChartButton;

    @BindView(R.id.monthly_chart)
    Button mMonthlyChartButton;

    @BindView(R.id.all_time_chart)
    Button mAllTimeChartButton;

    @BindView(R.id.translated_char_count)
    TextView charCounter;

    @BindView(R.id.chart)
    BarChart chart;

    private StatisticHandler handler;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings_statistics_translation);

        ButterKnife.bind(this);
        handler = new StatisticHandler(
                Matrix.getInstance(this).getDefaultSession().getMyUserId(),
                Matrix.getInstance(this).getDefaultSession().getCredentials().accessToken,
                new StatisticInterface() {
                    @Override
                    public void onStatisticReceived(Statistics statistics) {
                        long counter = 0;
                       charCounter.setText(String.valueOf(statistics.getStat().size()));
                    }

                    @Override
                    public void onFailure(String msg) {
                        Log.d("Fail", msg);
                    }
                });
        handler.getStatistics("1970-01-01", DateUtill.currentDateForStats());
        mDailyChartButton.setBackground(getResources().getDrawable(R.drawable.chart_button_focused));
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        setupOnClickListeners();

        mDailyChartButton.setBackground(null);
        mWeeklyChartButton.setBackground(null);
        mMonthlyChartButton.setBackground(null);
        mAllTimeChartButton.setBackground(null);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupOnClickListeners() {
        mDailyChartButton.setOnClickListener(this);
        mWeeklyChartButton.setOnClickListener(this);
        mMonthlyChartButton.setOnClickListener(this);
        mAllTimeChartButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        mDailyChartButton.setBackground(null);
        mWeeklyChartButton.setBackground(null);
        mMonthlyChartButton.setBackground(null);
        mAllTimeChartButton.setBackground(null);

        switch (v.getId()) {
            case R.id.daily_chart:
                mDailyChartButton.setBackground(getResources().getDrawable(R.drawable.chart_button_focused));
                break;
            case R.id.weekly_chart:
                mWeeklyChartButton.setBackground(getResources().getDrawable(R.drawable.chart_button_focused));
                break;
            case R.id.monthly_chart:
                mMonthlyChartButton.setBackground(getResources().getDrawable(R.drawable.chart_button_focused));
                break;
            case R.id.all_time_chart:
                mAllTimeChartButton.setBackground(getResources().getDrawable(R.drawable.chart_button_focused));
                break;
        }
    }
}