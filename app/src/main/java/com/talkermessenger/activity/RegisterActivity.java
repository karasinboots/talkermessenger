package com.talkermessenger.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.iskar.talkersdk.LoginHandler;
import com.iskar.talkersdk.callback.MatrixLoginInterface;
import com.iskar.talkersdk.model.MatrixCredentials;
import com.iskar.talkersdk.model.PhoneBadRequest;
import com.stfalcon.smsverifycatcher.SmsVerifyCatcher;
import com.talkermessenger.Matrix;
import com.talkermessenger.R;
import com.talkermessenger.RegistrationManager;
import com.talkermessenger.receiver.VectorUniversalLinkReceiver;
import com.talkermessenger.util.PreferencesManager;

import org.matrix.androidsdk.HomeServerConnectionConfig;
import org.matrix.androidsdk.MXSession;
import org.matrix.androidsdk.rest.model.login.Credentials;
import org.matrix.androidsdk.ssl.Fingerprint;
import org.matrix.androidsdk.util.Log;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RegisterActivity extends AppCompatActivity implements MatrixLoginInterface, TextView.OnEditorActionListener {
    private static final String LOG_TAG = "RegisterActivity";
    @BindView(R.id.go_next)
    TextView login;
    @BindView(R.id.phone_number)
    EditText phone;
    @BindView(R.id.code)
    EditText code;
    @BindView(R.id.call_progress)
    ProgressBar progressBar;
    @BindView(R.id.time_left)
    TextView timeLeft;

    RegisterActivity registerActivity = this;
    private boolean isSmsSent = false;
    private Context context;
    private SharedPreferences preferences;
    private CountDownTimer timer = new CountDownTimer(60000, 1000) {
        @Override
        public void onTick(long millisUntilFinished) {
            updateTime(millisUntilFinished);
        }

        @Override
        public void onFinish() {
            login.setTextColor(context.getResources().getColor(R.color.riot_primary_background_color_black));
            login.setClickable(true);
            timeLeft.setText("");
        }
    };

    // save the config because trust a certificate is asynchronous.
    private HomeServerConnectionConfig mHomeserverConnectionConfig;
    // the pending universal link uri (if any)
    private Parcelable mUniversalLinkUri;
    private SmsVerifyCatcher smsVerifyCatcher;

    LoginHandler loginHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_new);
        ButterKnife.bind(this);
        preferences = getSharedPreferences("this", MODE_PRIVATE);
        context = this;
        // already registered
        if (hasCredentials()) {
            // detect if the application has already been started
            Log.d(LOG_TAG, "## onCreate(): goToSplash with credentials but there is no event stream service.");
            goToSplash();
            finish();
        }

        phone.setSelection(1);

        phone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (phone.length() == 0) {
                    phone.setText("+");
                    phone.setSelection(1);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        phone.setOnEditorActionListener(this);
        code.setOnEditorActionListener(this);

        code.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (code.length() == 6){
                    code.clearFocus();
                    progressBar.setVisibility(View.VISIBLE);
                    loginHandler.login(code.getText().toString());
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        if (preferences.getBoolean("SMS_PERM", false)){
            smsVerifyCatcher = new SmsVerifyCatcher(this, message -> {
                String code = parseSms(message);
                this.code.setText(code);
            });
            smsVerifyCatcher.setPhoneNumberFilter("TALKER");
        } else {
            showDialog();
        }

        loginHandler = new LoginHandler(registerActivity);

        code.setOnKeyListener((v, keyCode, event) -> {
            if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                progressBar.setVisibility(View.VISIBLE);
                loginHandler.login(code.getText().toString());
                return true;
            }
            return false;
        });

        phone.setOnKeyListener((v, keyCode, event) -> {
            if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                if (phone.getText().length() > 0 && phone.getText().toString().toCharArray()[0] == '+') {
                    progressBar.setVisibility(View.VISIBLE);
                    Log.e("user_number", phone.getText().toString() + "");
                    PreferencesManager.setUserNumber(this, phone.getText().toString());
                    progressBar.setVisibility(View.GONE);
                    if (smsVerifyCatcher != null)
                    smsVerifyCatcher.onStart();
                    changeVisibilityOfViews();
                    isSmsSent = true;
                } else {
                    Toast.makeText(this, "Wrong phone number", Toast.LENGTH_SHORT).show();
                }
                return true;
            }
            return false;
        });

    }

    private void showDialog() {
        new AlertDialog.Builder(this)
                .setPositiveButton("Далее", (dialog, which) -> {
                    smsVerifyCatcher = new SmsVerifyCatcher(this, message -> {
                        String code = parseSms(message);
                        this.code.setText(code);
                    });
                    smsVerifyCatcher.setPhoneNumberFilter("TALKER");
                    smsVerifyCatcher.onStart();
                })
                .setNegativeButton("Не сейчас", ((dialog, which) -> {
                    preferences.edit().putBoolean("SMS_PERM", true).apply();
                    dialog.dismiss();
                }))
                .setCancelable(false)
                .setTitle("")
                .setMessage("Что бы облегчить процесс подтверждения регистрации Im может автоматически обнаржуть ваш код если вы разрешите нам просматривать SMS-сообщения.")
                /*.show()*/;
    }

    private String parseSms(String message) {
        Pattern p = Pattern.compile("\\b\\d{6}\\b");
        Matcher m = p.matcher(message);
        String code = "";
        while (m.find()) {
            code = m.group(0);
        }
        return code;
    }

    /**
     * @return true if some credentials have been saved.
     */
    private boolean hasCredentials() {
        try {
            return Matrix.getInstance(this).getDefaultSession() != null;
        } catch (Exception e) {
            Log.e(LOG_TAG, "## Exception: " + e.getMessage());
        }

        Log.e(LOG_TAG, "## hasCredentials() : invalid credentials");

        this.runOnUiThread(() -> {
            try {
                // getDefaultSession could trigger an exception if the login data are corrupted
                CommonActivityUtils.logout(RegisterActivity.this);
            } catch (Exception e) {
                Log.w(LOG_TAG, "## Exception: " + e.getMessage());
            }
        });

        return false;
    }

    @Override
    public void getCodeSuccess() {
//        codeInputLayout.setVisibility(View.VISIBLE);
//        code.requestFocus();
        isSmsSent = true;
        changeVisibilityOfViews();
        login.setTextColor(context.getResources().getColor(R.color.vector_dark_grey_color));
        login.setClickable(false);
        timer.start();
        if (smsVerifyCatcher != null)
        smsVerifyCatcher.onStop();
    }

    @Override
    public void tooManyAttempts(PhoneBadRequest phoneBadRequest) {

    }

    @Override
    public void changePhoneSuccess(String newPhone) {

    }

    @Override
    public void onFailure(String msg) {
        login.setClickable(true);
        progressBar.setVisibility(View.GONE);
        Toast.makeText(this, isSmsSent ? "Wrong code" : "Too many code requests!", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void getCredentialsSuccess(MatrixCredentials matrixCredentials) {
        String homeServer = getResources().getString(R.string.default_hs_server_url);
        String userId = matrixCredentials.getUserId();
        String accessToken = matrixCredentials.getAccessToken();
        progressBar.setVisibility(View.GONE);
        // build a credential with the provided items
        Credentials credentials = new Credentials();
        credentials.userId = userId;
        credentials.homeServer = homeServer;
        credentials.accessToken = accessToken;
        HomeServerConnectionConfig hsConfig = getHsConfig();

        try {
            hsConfig.setCredentials(credentials);
        } catch (Exception e) {
            Log.d(LOG_TAG, "hsConfig setCredentials failed " + e.getLocalizedMessage());
        }

        Log.d(LOG_TAG, "Account creation succeeds");

        // let's go...
        MXSession session = Matrix.getInstance(getApplicationContext()).createSession(hsConfig);
        Matrix.getInstance(getApplicationContext()).addSession(session);
        goToSplash();
        finish();
    }

    /**
     * Some sessions have been registred, skip the login process.
     */
    private void goToSplash() {
        Log.d(LOG_TAG, "## gotoSplash(): Go to splash.");

        Intent intent = new Intent(this, SplashActivity.class);
        if (null != mUniversalLinkUri) {
            intent.putExtra(VectorUniversalLinkReceiver.EXTRA_UNIVERSAL_LINK_URI, mUniversalLinkUri);
        }

        startActivity(intent);
    }

    /**
     * @return the homeserver config. null if the url is not valid
     */
    private HomeServerConnectionConfig getHsConfig() {
        if (null == mHomeserverConnectionConfig) {
            String hsUrlString = getResources().getString(R.string.default_hs_server_url);

            if (TextUtils.isEmpty(hsUrlString) || !hsUrlString.startsWith("http") || TextUtils.equals(hsUrlString, "http://") || TextUtils.equals(hsUrlString, "https://")) {
                Toast.makeText(this, getString(R.string.login_error_must_start_http), Toast.LENGTH_SHORT).show();
                return null;
            }

            if (!hsUrlString.startsWith("http://") && !hsUrlString.startsWith("https://")) {
                hsUrlString = "https://" + hsUrlString;
            }

            String identityServerUrlString = getResources().getString(R.string.default_identity_server_url);

            if (TextUtils.isEmpty(identityServerUrlString) || !identityServerUrlString.startsWith("http") || TextUtils.equals(identityServerUrlString, "http://") || TextUtils.equals(identityServerUrlString, "https://")) {
                Toast.makeText(this, getString(R.string.login_error_must_start_http), Toast.LENGTH_SHORT).show();
                return null;
            }

            if (!identityServerUrlString.startsWith("http://") && !identityServerUrlString.startsWith("https://")) {
                identityServerUrlString = "https://" + identityServerUrlString;
            }

            try {
                mHomeserverConnectionConfig = null;
                mHomeserverConnectionConfig = new HomeServerConnectionConfig(Uri.parse(hsUrlString), Uri.parse(identityServerUrlString), null, new ArrayList<Fingerprint>(), false);
            } catch (Exception e) {
                Log.e(LOG_TAG, "getHsConfig fails " + e.getLocalizedMessage());
            }
        }

        RegistrationManager.getInstance().setHsConfig(mHomeserverConnectionConfig);
        return mHomeserverConnectionConfig;
    }

    private void changeVisibilityOfViews() {
        if (!isSmsSent) {
            code.setVisibility(View.GONE);
            phone.setVisibility(View.VISIBLE);
            login.setText("");
        } else {
            phone.setVisibility(View.GONE);
            code.setVisibility(View.VISIBLE);
            code.requestFocus();
            login.setText("Send Again");
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (smsVerifyCatcher != null)
        smsVerifyCatcher.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (smsVerifyCatcher != null)
        smsVerifyCatcher.onStart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        timer.cancel();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        try {
            if (requestCode == 12){
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED){
                    if (smsVerifyCatcher != null)
                        smsVerifyCatcher.onRequestPermissionsResult(requestCode, permissions, grantResults);
                } else {
                    smsVerifyCatcher = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateTime(long time) {
        timeLeft.setText("Resend in " + TimeUnit.MILLISECONDS.toSeconds(time) + " seconds...");
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        isSmsSent = false;
        progressBar.setVisibility(View.VISIBLE);
        if (phone.getText().length() > 0 && phone.getText().toString().toCharArray()[0] == '+') {
            login.setClickable(false);
            loginHandler.requestCode(phone.getText().toString());
            PreferencesManager.setUserNumber(this, phone.getText().toString());
            if (smsVerifyCatcher != null)
                smsVerifyCatcher.onStart();
            progressBar.setVisibility(View.GONE);
        } else {
            progressBar.setVisibility(View.GONE);
            Toast.makeText(this, "Wrong phone number", Toast.LENGTH_SHORT).show();
        }
        return false;
    }
}
