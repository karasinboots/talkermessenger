package com.talkermessenger.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.iskar.talkersdk.LoginHandler;
import com.iskar.talkersdk.callback.MatrixLoginInterface;
import com.iskar.talkersdk.model.MatrixCredentials;
import com.iskar.talkersdk.model.PhoneBadRequest;
import com.talkermessenger.Matrix;
import com.talkermessenger.R;
import com.talkermessenger.RegistrationManager;
import com.talkermessenger.activity.settings.EnterPassCodeActivity;
import com.talkermessenger.receiver.VectorUniversalLinkReceiver;
import com.talkermessenger.util.PreImEditText;
import com.talkermessenger.util.PreferencesManager;

import org.matrix.androidsdk.HomeServerConnectionConfig;
import org.matrix.androidsdk.MXSession;
import org.matrix.androidsdk.rest.model.login.Credentials;
import org.matrix.androidsdk.ssl.Fingerprint;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.os.Build.VERSION_CODES.M;
import static org.matrix.androidsdk.rest.model.MediaMessage.LOG_TAG;

public class EnterNumberActivity extends AppCompatActivity implements MatrixLoginInterface, View.OnKeyListener, TextView.OnEditorActionListener, View.OnClickListener {


    final int REQUEST_CODE_PERMISSION_SMS = 9999;

    @BindView(R.id.tvConfirm)
    TextView tvConfirm;
    @BindView(R.id.etNumber)
    PreImEditText etNumber;
    @BindView(R.id.llProgressBar)
    LinearLayout llProgressBar;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;


    private Parcelable mUniversalLinkUri;

    private String number = "";
    LoginHandler loginHandler;

    SharedPreferences preferences;

    private void showDialog() {
        llProgressBar.setVisibility(View.VISIBLE);
        new AlertDialog.Builder(this)
                .setPositiveButton(getResources().getString(R.string.resume), (dialog, which) -> {
                    llProgressBar.setVisibility(View.GONE);
                    if (android.os.Build.VERSION.SDK_INT >= M) {
                        ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.READ_SMS},
                                REQUEST_CODE_PERMISSION_SMS);
                    } else {
                        android.app.AlertDialog.Builder permissionsInfoDialog = new android.app.AlertDialog.Builder(this);
                        permissionsInfoDialog.setIcon(android.R.drawable.ic_dialog_info);
                        Resources resource = getResources();
                        if (null != resource) {
                            permissionsInfoDialog.setTitle(resource.getString(R.string.get_permission_for_sms));//(resource.getString(R.string.permissions_rationale_popup_title));
                        }
                        permissionsInfoDialog.setMessage(resource.getString(R.string.sms_read));//(resource.getString(R.string.permissions_msg_contacts_warning_other_androids));
                        // gives the contacts book access
                        permissionsInfoDialog.setPositiveButton(this.getString(R.string.yes), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // ContactsManager.getInstance().setIsContactBookAccessAllowed(true);
                                ActivityCompat.requestPermissions(EnterNumberActivity.this,
                                        new String[]{android.Manifest.permission.READ_SMS}, REQUEST_CODE_PERMISSION_SMS);
                            }
                        });
                        permissionsInfoDialog.show();
                    }
                })
                .setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        etNumber.requestFocus();
                        showKeyboard();
                    }
                })
                .setNegativeButton(getResources().getString(R.string.not_now), ((dialog, which) -> {
                    preferences.edit().putBoolean("SMS_PERM", false).apply();
                    dialog.dismiss();
                    llProgressBar.setVisibility(View.GONE);
                }))
                .setCancelable(false)
                .setTitle("")
                .setMessage(getResources().getString(R.string.desciption_dialog_sms))
                .show();
    }


    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {
        //Log.d("---","Keycode = "+keyCode+" | KeYEvent = "+event.getCharacters());
        if (event.getAction() != KeyEvent.ACTION_UP) return false;
        if (keyCode == KeyEvent.KEYCODE_DEL) {
            if (etNumber.getText().toString().length() <= 1) {
                etNumber.setText("+");
                etNumber.setSelection(etNumber.getText().length());
                return false;
            }
        }
        return false;

    }

    @Override
    public void onRequestPermissionsResult(int aRequestCode, @NonNull String[] aPermissions, @NonNull int[] aGrantResults) {
        org.matrix.androidsdk.util.Log.e(LOG_TAG, "permission result");
        if (aRequestCode == REQUEST_CODE_PERMISSION_SMS)
            if (aGrantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Log.d("---", "SMS perm True");
                preferences.edit().putBoolean("SMS_PERM", true).apply();
            } else {
                Log.d("---", "SMS perm False");
                preferences.edit().putBoolean("SMS_PERM", false).apply();
            }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_number);
        //Initialize views
        ButterKnife.bind(this);
        preferences = getSharedPreferences("this", MODE_PRIVATE);
        if (hasCredentials()) {
            // detect if the application has already been started
            org.matrix.androidsdk.util.Log.d(LOG_TAG, "## onCreate(): goToSplash with credentials but there is no event stream service.");
            if (TextUtils.isEmpty(PreferencesManager.getPassword(this))) {
                goToSplash();
                finish();
            } else {
                startActivity(new Intent(this, EnterPassCodeActivity.class));
                finish();
            }
        }
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_SMS)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            showDialog();
            //Log.d("---","onCreate  check SMS perm" );
        } else
            preferences.edit().putBoolean("SMS_PERM", true).apply();
        //  else
        //Log.d("---","onCreate  check SMS perm  GRANTED" );


        //Get & Set Number From Shareds
        number = PreferencesManager.getUserNumber(this);

        if (number.equals(getResources().getString(R.string.no_user_phone_number))) {
            number = "+";
        }
        etNumber.setText(number);
        etNumber.setSelection(etNumber.getText().length());


        //Set Listeners

        tvConfirm.setOnClickListener(this);
        //ivDel.setOnClickListener(this);

        PreImEditText.OnKeyPreImeListener onKeyPreImeListener = new PreImEditText.OnKeyPreImeListener() {
            @Override
            public void onBackPressed() {
                EnterNumberActivity.super.onBackPressed();
            }
        };
        etNumber.setOnKeyPreImeListener(onKeyPreImeListener);
        etNumber.setOnKeyListener(this);
        etNumber.setOnEditorActionListener(this);
    }

    private void sendNumber() {
        //  String code = "" + et1.getText() + et2.getText() + et3.getText() + et4.getText() + et5.getText() + et6.getText();
        if (validateNumber()) {
            llProgressBar.setVisibility(View.VISIBLE);
            loginHandler = new LoginHandler(EnterNumberActivity.this);
            loginHandler.requestCode(number);
        } else {
            showKeyboard();
        }

    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        showKeyboard();
    }

    @Override
    protected void onResume() {
        super.onResume();
        showKeyboard();
    }

    private boolean validateNumber() {
        number = etNumber.getText().toString();
        if (number.matches("^\\+[0-9-]{10,25}"))
            return true;
        else
            Toast.makeText(this, getResources().getString(R.string.incorrect_number), Toast.LENGTH_SHORT).show();
        return false;
    }

    private boolean hasCredentials() {
        try {
            return Matrix.getInstance(this).getDefaultSession() != null;
        } catch (Exception e) {
            org.matrix.androidsdk.util.Log.e(LOG_TAG, "## Exception: " + e.getMessage());
        }

        org.matrix.androidsdk.util.Log.e(LOG_TAG, "## hasCredentials() : invalid credentials");

        this.runOnUiThread(() -> {
            try {
                CommonActivityUtils.logout(EnterNumberActivity.this);
            } catch (Exception e) {
                org.matrix.androidsdk.util.Log.w(LOG_TAG, "## Exception: " + e.getMessage());
            }
        });
        return false;
    }

    private void goToSplash() {
        org.matrix.androidsdk.util.Log.d(LOG_TAG, "## gotoSplash(): Go to splash.");

        Intent intent = new Intent(this, SplashActivity.class);
        if (null != mUniversalLinkUri) {
            intent.putExtra(VectorUniversalLinkReceiver.EXTRA_UNIVERSAL_LINK_URI, mUniversalLinkUri);
        }
        startActivity(intent);
    }


    @Override
    public void getCodeSuccess() {
        llProgressBar.setVisibility(View.GONE);
        Intent intent = new Intent(EnterNumberActivity.this, EnterCodeActivity.class);
        PreferencesManager.setUserNumber(EnterNumberActivity.this, etNumber.getText().toString());
        startActivity(intent);
    }

    @Override
    public void tooManyAttempts(PhoneBadRequest phoneBadRequest) {
        llProgressBar.setVisibility(View.GONE);
        int seconds = 0;
        if (phoneBadRequest != null) {
            seconds = phoneBadRequest.getTimeRemaining();
            // Log.d("---", "SECOND " + seconds);
            if (seconds != 0) {
                int min = ((int) seconds / 60);
                Toast.makeText(EnterNumberActivity.this,
                        getResources().getString(R.string.try_after) + " "
                                + min + " " + this.getResources().getQuantityString(R.plurals.minutes, min), Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void changePhoneSuccess(String newPhone) {

    }

    public void onFailure(String msg) {
        llProgressBar.setVisibility(View.GONE);
        Log.d("---", "onFailure: " + msg);
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo == null) {
            Toast.makeText(this, getResources().getString(R.string.no_connection), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void getCredentialsSuccess(MatrixCredentials matrixCredentials) {
        String homeServer = getResources().getString(R.string.default_hs_server_url);
        String userId = matrixCredentials.getUserId();
        String accessToken = matrixCredentials.getAccessToken();
        llProgressBar.setVisibility(View.GONE);
        // build a credential with the provided items
        Credentials credentials = new Credentials();
        credentials.userId = userId;
        credentials.homeServer = homeServer;
        credentials.accessToken = accessToken;
        HomeServerConnectionConfig hsConfig = getHsConfig();
        try {
            hsConfig.setCredentials(credentials);
        } catch (Exception e) {
            org.matrix.androidsdk.util.Log.d(LOG_TAG, "hsConfig setCredentials failed " + e.getLocalizedMessage());
        }
        org.matrix.androidsdk.util.Log.d(LOG_TAG, "Account creation succeeds");
        // let's go...
        MXSession session = Matrix.getInstance(getApplicationContext()).createSession(hsConfig);
        Matrix.getInstance(getApplicationContext()).addSession(session);
        goToSplash();
        finish();
    }


    // save the config because trust a certificate is asynchronous.
    private HomeServerConnectionConfig mHomeserverConnectionConfig;

    /**
     * @return the homeserver config. null if the url is not valid
     */
    private HomeServerConnectionConfig getHsConfig() {
        if (null == mHomeserverConnectionConfig) {
            String hsUrlString = getResources().getString(R.string.default_hs_server_url);

            if (TextUtils.isEmpty(hsUrlString) || !hsUrlString.startsWith("http") || TextUtils.equals(hsUrlString, "http://") || TextUtils.equals(hsUrlString, "https://")) {
                Toast.makeText(this, getString(R.string.login_error_must_start_http), Toast.LENGTH_SHORT).show();
                return null;
            }

            if (!hsUrlString.startsWith("http://") && !hsUrlString.startsWith("https://")) {
                hsUrlString = "https://" + hsUrlString;
            }

            String identityServerUrlString = getResources().getString(R.string.default_identity_server_url);

            if (TextUtils.isEmpty(identityServerUrlString) || !identityServerUrlString.startsWith("http") || TextUtils.equals(identityServerUrlString, "http://") || TextUtils.equals(identityServerUrlString, "https://")) {
                Toast.makeText(this, getString(R.string.login_error_must_start_http), Toast.LENGTH_SHORT).show();
                return null;
            }

            if (!identityServerUrlString.startsWith("http://") && !identityServerUrlString.startsWith("https://")) {
                identityServerUrlString = "https://" + identityServerUrlString;
            }

            try {
                mHomeserverConnectionConfig = null;
                mHomeserverConnectionConfig = new HomeServerConnectionConfig(Uri.parse(hsUrlString), Uri.parse(identityServerUrlString), null, new ArrayList<Fingerprint>(), false);
            } catch (Exception e) {
                org.matrix.androidsdk.util.Log.e(LOG_TAG, "getHsConfig fails " + e.getLocalizedMessage());
            }
        }

        RegistrationManager.getInstance().setHsConfig(mHomeserverConnectionConfig);
        return mHomeserverConnectionConfig;
    }

    //Implements method View.OnClickListener
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvConfirm:
                sendNumber();
                break;
        }
    }

    private void showKeyboard() {
        etNumber.requestFocus();
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(view, 0);
        }
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        sendNumber();
        return true;
    }
}
