package com.talkermessenger.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;

import com.iskar.talkersdk.TranslationHistoryItemHandler;
import com.iskar.talkersdk.model.TranslationHistoryItem;
import com.talkermessenger.R;
import com.talkermessenger.adapters.TranslationHistoryAdapter;
import com.talkermessenger.listeners.TranslationHistoryClickListener;

import java.util.List;

public class TranslationHistoryActivity extends AppCompatActivity implements TranslationHistoryClickListener {
    private TranslationHistoryItemHandler handler;
    private TranslationHistoryAdapter adapter;
    List<TranslationHistoryItem> allTranslationHistoryItems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_translation_history);

        setSupportActionBar(findViewById(R.id.account_toolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        RecyclerView recyclerView = findViewById(R.id.translation_history_list);
        handler = new TranslationHistoryItemHandler(this);

        allTranslationHistoryItems = handler.getAllTranslationHistoryItems();
        adapter = new TranslationHistoryAdapter(allTranslationHistoryItems, this);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_translation_history, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.ic_action_clear:
                handler.deleteAllItems();
                adapter.clearData();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onLongClick(TranslationHistoryItem item, int position) {
        handler.deleteItem(item);
        allTranslationHistoryItems = handler.getAllTranslationHistoryItems();
        adapter.setItems(allTranslationHistoryItems);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(TranslationHistoryItem item, int position) {
        Intent intent = new Intent();
        intent.putExtra("sourceLang", item.getOriginalLanguageCode());
        intent.putExtra("sourceLangName", item.getOriginalLangugeName());
        intent.putExtra("sourceText", item.getOriginalText());
        intent.putExtra("translatedLang", item.getTranslatedLanguageCode());
        intent.putExtra("translatedLangName", item.getTranslatedLanguageName());
        intent.putExtra("translatedText", item.getTranslatedText());
        setResult(RESULT_OK, intent);
        finish();
    }
}
