package com.talkermessenger.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.talkermessenger.Matrix;
import com.talkermessenger.R;
import com.talkermessenger.util.VectorUtils;

import org.matrix.androidsdk.MXSession;
import org.matrix.androidsdk.call.IMXCall;
import org.matrix.androidsdk.data.Room;
import org.matrix.androidsdk.data.RoomState;
import org.matrix.androidsdk.rest.callback.ApiCallback;
import org.matrix.androidsdk.rest.callback.SimpleApiCallback;
import org.matrix.androidsdk.rest.model.MatrixError;
import org.matrix.androidsdk.util.Log;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class CallDialogActivity extends AppCompatActivity {
    @BindView(R.id.startVoiceCall)
    LinearLayout startVoiceCall;
    @BindView(R.id.startVideoCall)
    LinearLayout startVideoCall;
    @BindView(R.id.contact_avatar_layout)
    CircleImageView contactAvatar;
    @BindView(R.id.contact_name)
    TextView contactName;
    @BindView(R.id.presence)
    TextView presence;
    private static final String LOG_TAG = "CallDialogActivity";
    private MXSession mSession;
    private Room mRoom;
    private AppCompatActivity mActivity = this;
    private String mUserId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call_dialog);
        ButterKnife.bind(this);
        mSession = Matrix.getInstance(this).getDefaultSession();
        Intent intent = getIntent();
        mRoom = mSession.getDataHandler().getRoom(intent.getStringExtra("roomId"));
        mUserId = intent.getStringExtra("userId");
        VectorUtils.loadUserAvatar(this, mSession, contactAvatar, mSession.getDataHandler().getUser(mUserId));
        contactName.setText(getUserDisplayName(mUserId, mRoom.getState()));
        String presenceText = VectorUtils.getUserOnlineStatus(this, mSession, mUserId, new SimpleApiCallback<Void>() {
            @Override
            public void onSuccess(Void info) {
            }
        });
        presence.setText(presenceText);
        startVoiceCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startCheckCallPermissions(false);
                finish();
            }
        });
        startVideoCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startCheckCallPermissions(true);
                finish();
            }
        });
    }

    public static String getUserDisplayName(String userId, RoomState roomState) {
        if (null != roomState) {
            return roomState.getMemberName(userId);
        } else {
            return userId;
        }
    }

    private void startCheckCallPermissions(boolean aIsVideoCall) {
        int requestCode = CommonActivityUtils.REQUEST_CODE_PERMISSION_AUDIO_IP_CALL;

        if (aIsVideoCall) {
            requestCode = CommonActivityUtils.REQUEST_CODE_PERMISSION_VIDEO_IP_CALL;
        }

        if (CommonActivityUtils.checkPermissions(requestCode, this)) {
            startCall(aIsVideoCall);
        }
    }

    /**
     * Start a call in a dedicated room
     *
     * @param isVideo true if the call is a video call
     */
    private void startCall(final boolean isVideo) {
        if (!mSession.isAlive()) {
            Log.e(LOG_TAG, "startCall : the session is not anymore valid");
            return;
        }

        // create the call object
        mSession.mCallsManager.createCallInRoom(mRoom.getRoomId(), isVideo, new ApiCallback<IMXCall>() {
            @Override
            public void onSuccess(final IMXCall call) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        final Intent intent = new Intent(getApplicationContext(), VectorCallViewActivity.class);

                        intent.putExtra(VectorCallViewActivity.EXTRA_MATRIX_ID, mSession.getCredentials().userId);
                        intent.putExtra(VectorCallViewActivity.EXTRA_CALL_ID, call.getCallId());

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                startActivity(intent);
                            }
                        });
                    }
                });
            }

            @Override
            public void onNetworkError(Exception e) {
                CommonActivityUtils.displayToast(getApplicationContext(), e.getLocalizedMessage());
                Log.e(LOG_TAG, "## startCall() failed " + e.getMessage());
            }

            @Override
            public void onMatrixError(MatrixError e) {
                CommonActivityUtils.displayToast(getApplicationContext(), e.getLocalizedMessage());
                Log.e(LOG_TAG, "## startCall() failed " + e.getMessage());
            }

            @Override
            public void onUnexpectedError(Exception e) {
                CommonActivityUtils.displayToast(getApplicationContext(), e.getLocalizedMessage());
                Log.e(LOG_TAG, "## startCall() failed " + e.getMessage());
            }
        });
    }
}
