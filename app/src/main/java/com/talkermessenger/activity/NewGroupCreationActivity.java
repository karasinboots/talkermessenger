package com.talkermessenger.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.iskar.talkersdk.ui.DividerItemDecoration;
import com.talkermessenger.Matrix;
import com.talkermessenger.R;
import com.talkermessenger.adapters.NewGroupCreationMemberAdapter;
import com.talkermessenger.adapters.ParticipantAdapterItem;
import com.talkermessenger.util.ThemeUtils;
import com.talkermessenger.util.VectorUtils;

import org.matrix.androidsdk.MXSession;
import org.matrix.androidsdk.data.Room;
import org.matrix.androidsdk.data.RoomSummary;
import org.matrix.androidsdk.data.RoomTag;
import org.matrix.androidsdk.listeners.MXMediaUploadListener;
import org.matrix.androidsdk.rest.callback.ApiCallback;
import org.matrix.androidsdk.rest.callback.SimpleApiCallback;
import org.matrix.androidsdk.rest.model.CreateRoomParams;
import org.matrix.androidsdk.rest.model.MatrixError;
import org.matrix.androidsdk.util.Log;
import org.matrix.androidsdk.util.ResourceUtils;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

import static com.talkermessenger.activity.VectorRoomActivity.EXTRA_ROOM_AVATAR_URL;
import static com.talkermessenger.activity.VectorRoomActivity.EXTRA_ROOM_ID;

public class NewGroupCreationActivity extends AppCompatActivity {

    private static final String LOG_TAG = NewGroupCreationActivity.class.getSimpleName();
    public static final String EXTRA_MATRIX_ID = "KEY_EXTRA_MATRIX_ID";
    private static final String PARTICIPANTS_LIST = "PARTICIPANTS_LIST";
    public static final String EXTRA_IM_GROUP = "KEY_EXTRA_IM_GROUP";
    private String avatarUri;
    private MXSession mSession;
    private static final int REQ_CODE_UPDATE_ROOM_AVATAR = 0x10;
    private static final int INVITE_USER_REQUEST_CODE = 456;
    private ArrayList<ParticipantAdapterItem> mParticipants = new ArrayList<>();
    private NewGroupCreationMemberAdapter mAdapter;

    @BindView(R.id.change_image)
    TextView changeImageText;

    @BindView(R.id.name_section_icon_main_empty_image)
    RelativeLayout emptyImageFrame;

    @BindView(R.id.name_section_edit_text)
    EditText editName;
//
//    @BindView(R.id.name_section_edit_name_done)
//    ImageView editNameDone;

    @BindView(R.id.add_member_section_add)
    TextView addMember;

    @BindView(R.id.members_list)
    RecyclerView memberList;

    @BindView(R.id.circular_image_new_group)
    CircleImageView emptyImage;

    @BindView(R.id.group_image)
    ImageView groupImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_group_creation);
        ButterKnife.bind(this);

        setSupportActionBar(findViewById(R.id.account_toolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        if (getIntent().getExtras().getString(EXTRA_IM_GROUP) != null) {
            getSupportActionBar().setTitle(getString(R.string.create_im_group));
        } else
            getSupportActionBar().setTitle(R.string.vector_invite_members_new_group);

        emptyImage.setImageDrawable(getResources().getDrawable(R.drawable.ic_circle));

        String matrixId = getIntent().getStringExtra(EXTRA_MATRIX_ID);
        if (matrixId != null) {
            mSession = Matrix.getInstance(this).getSession(matrixId);
        }
        changeImageText.setOnClickListener(v -> onUpdateAvatarClick());
        groupImage.setOnClickListener(v -> onUpdateAvatarClick());
        emptyImage.setOnClickListener(v -> onUpdateAvatarClick());

        editName.getBackground().setColorFilter(getResources().getColor(android.R.color.transparent), PorterDuff.Mode.SRC_IN);

        editName.setOnEditorActionListener((v, actionId, event) -> {
            if ((actionId == EditorInfo.IME_ACTION_DONE)) {
                String youEditTextValue = editName.getText().toString();
                if (youEditTextValue.length() > 0) {
                    editName.clearFocus();
                    findViewById(R.id.focus_layout).requestFocus();
                }
                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
                return true;
            }
            return false;
        });

        if ((null != savedInstanceState) && savedInstanceState.containsKey(PARTICIPANTS_LIST)) {
            mParticipants.clear();
            mParticipants = new ArrayList<>((List<ParticipantAdapterItem>) savedInstanceState.getSerializable(PARTICIPANTS_LIST));
        }

        mAdapter = new NewGroupCreationMemberAdapter(mParticipants);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        memberList.setLayoutManager(mLayoutManager);
        memberList.setAdapter(mAdapter);
        memberList.addItemDecoration(new DividerItemDecoration(this, com.iskar.talkersdk.R.drawable.divider, 0));
        addMember.setOnClickListener(v -> launchSearchActivity());

    }

    /***
     * Launch the people search activity
     */
    private void launchSearchActivity() {
        Intent intent = new Intent(NewGroupCreationActivity.this, VectorRoomInviteGroupMembersActivity.class);
        intent.putExtra(VectorRoomInviteGroupMembersActivity.EXTRA_MATRIX_ID, mSession.getMyUserId());
        intent.putExtra(VectorRoomInviteGroupMembersActivity.EXTRA_HIDDEN_PARTICIPANT_ITEMS, mParticipants);
        NewGroupCreationActivity.this.startActivityForResult(intent, INVITE_USER_REQUEST_CODE);
    }

    private void onUpdateAvatarClick() {
        runOnUiThread(() -> {
            if (CommonActivityUtils.checkPermissions(CommonActivityUtils.REQUEST_CODE_PERMISSION_TAKE_PHOTO,
                    NewGroupCreationActivity.this)) {
                Intent intent = new Intent(NewGroupCreationActivity.this,
                        VectorMediasPickerActivity.class);
                intent.putExtra(VectorMediasPickerActivity.EXTRA_AVATAR_MODE, true);
                startActivityForResult(intent, VectorUtils.TAKE_IMAGE);
            }
        });
    }

    /**
     * Create a room with a list of participants.
     *
     * @param participants the list of participant
     */
    private void createRoom(final List<ParticipantAdapterItem> participants) {

        CreateRoomParams params = new CreateRoomParams();

        List<String> ids = new ArrayList<>();
        for (ParticipantAdapterItem item : participants) {
            if (null != item.mUserId) {
                ids.add(item.mUserId);
            }
        }

        params.addParticipantIds(mSession.getHomeServerConfig(), ids);

        if (!TextUtils.isEmpty(editName.getText().toString())) {
            params.name = editName.getText().toString();
        }

        mSession.createRoom(params, new SimpleApiCallback<String>(this) {
            @Override
            public void onSuccess(final String roomId) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Room room = mSession.getDataHandler().getRoom(roomId);
                        if (getIntent().getExtras().getString(EXTRA_IM_GROUP) != null) {
                            Log.e(LOG_TAG, "im room extras" + getIntent().getExtras().getString(EXTRA_IM_GROUP));
                            room.updateTopic(RoomTag.ROOM_TAG_IM, new ApiCallback<Void>() {
                                @Override
                                public void onSuccess(Void info) {
                                    Log.e(LOG_TAG, "im room created" + info);
                                    openRoom(room, participants);
                                }

                                @Override
                                public void onNetworkError(Exception e) {
                                    // onDone(e.getLocalizedMessage());
                                }

                                @Override
                                public void onMatrixError(MatrixError e) {
                                    //    onDone(e.getLocalizedMessage());
                                }

                                @Override
                                public void onUnexpectedError(Exception e) {
                                    //  onDone(e.getLocalizedMessage());
                                }
                            });
                        } else openRoom(room, participants);
                    }
                });
            }

            private void onError(final String message) {

                if (null != message) {
                    Toast.makeText(NewGroupCreationActivity.this, message, Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onNetworkError(Exception e) {
                onError(e.getLocalizedMessage());
            }

            @Override
            public void onMatrixError(final MatrixError e) {
                onError(e.getLocalizedMessage());
            }

            @Override
            public void onUnexpectedError(final Exception e) {
                onError(e.getLocalizedMessage());
            }
        });
    }

    void openRoom(final Room room, List<ParticipantAdapterItem> userIds) {
        // sanity checks
        // reported by GA
        if ((null == mSession.getDataHandler()) || (null == mSession.getDataHandler().getStore())) {
            return;
        }

        final String roomId;
        // cannot join a leaving room
        if (room == null || room.isLeaving()) {
            roomId = null;
        } else {
            roomId = room.getRoomId();
        }

        if (roomId != null) {
            final RoomSummary roomSummary = mSession.getDataHandler().getStore().getSummary(roomId);

            if (null != roomSummary) {
                room.sendReadReceipt();
            }

            // Update badge unread count in case device is offline
            CommonActivityUtils.specificUpdateBadgeUnreadCount(mSession, this);
            if (getIntent().getBooleanExtra("isCall", false)) {
                if (userIds.size() == 1) {
                    Intent intent = new Intent(this, CallDialogActivity.class);
                    intent.putExtra("roomId", roomId);
                    intent.putExtra("userId", userIds.get(0).mUserId);
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(this, VectorRoomActivity.class);
                    intent.setFlags(android.content.Intent.FLAG_ACTIVITY_CLEAR_TOP | android.content.Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    intent.putExtra(EXTRA_ROOM_ID, roomId);
                    if (!TextUtils.isEmpty(avatarUri))
                        intent.putExtra(EXTRA_ROOM_AVATAR_URL, avatarUri);
                    startActivity(intent);
                }
            } else if (getIntent().getExtras().getString(EXTRA_IM_GROUP) != null) {
                Log.e("topic", EXTRA_IM_GROUP);
                Intent intent = new Intent(this, VectorRoomActivity.class);
                intent.setFlags(android.content.Intent.FLAG_ACTIVITY_CLEAR_TOP | android.content.Intent.FLAG_ACTIVITY_SINGLE_TOP);
                intent.putExtra(EXTRA_ROOM_ID, roomId);
                intent.putExtra(EXTRA_IM_GROUP, EXTRA_IM_GROUP);
                if (!TextUtils.isEmpty(avatarUri))
                    intent.putExtra(EXTRA_ROOM_AVATAR_URL, avatarUri);
                startActivity(intent);
            } else {
                Intent intent = new Intent(this, VectorRoomActivity.class);
                intent.setFlags(android.content.Intent.FLAG_ACTIVITY_CLEAR_TOP | android.content.Intent.FLAG_ACTIVITY_SINGLE_TOP);
                intent.putExtra(EXTRA_ROOM_ID, roomId);
                if (!TextUtils.isEmpty(avatarUri))
                    intent.putExtra(EXTRA_ROOM_AVATAR_URL, avatarUri);
                startActivity(intent);
            }
            finish();
        }
    }

    /**
     * Update the avatar from the data provided the medias picker.
     *
     * @param aResultCode the result code.
     * @param aData       the provided data.
     */
    private void onActivityResultRoomAvatarUpdate(int aResultCode, final Intent aData) {
        // sanity check
        if (null == mSession) {
            return;
        }

        if (aResultCode == Activity.RESULT_OK) {
            Uri thumbnailUri = VectorUtils.getThumbnailUriFromIntent(NewGroupCreationActivity.this, aData, mSession.getMediasCache());

            if (null != thumbnailUri) {
//                avatarUri = thumbnailUri.toString();
                groupImage.setImageURI(thumbnailUri);
                emptyImageFrame.setVisibility(View.GONE);
                groupImage.setVisibility(View.VISIBLE);
                // save the bitmap URL on the server
                ResourceUtils.Resource resource = ResourceUtils.openResource(NewGroupCreationActivity.this, thumbnailUri, null);
                if (null != resource) {
                    mSession.getMediasCache().uploadContent(resource.mContentStream, null, resource.mMimeType, null, new MXMediaUploadListener() {

                        @Override
                        public void onUploadError(String uploadId, int serverResponseCode, String serverErrorMessage) {
                            NewGroupCreationActivity.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Log.e(LOG_TAG, "Fail to upload the avatar");
                                }
                            });
                        }

                        @Override
                        public void onUploadComplete(final String uploadId, final String contentUri) {
                            NewGroupCreationActivity.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Log.d(LOG_TAG, "The avatar has been uploaded, update the room avatar");
                                    avatarUri = contentUri;
                                }
                            });
                        }
                    });
                }
            }
        }
    }

    // Comparator to order members alphabetically
    // the self item is always kept at top
    private final Comparator<ParticipantAdapterItem> mAlphaComparator = new Comparator<ParticipantAdapterItem>() {
        @Override
        public int compare(ParticipantAdapterItem part1, ParticipantAdapterItem part2) {
            // keep the self user id at top
            if (TextUtils.equals(part1.mUserId, mSession.getMyUserId())) {
                return -1;
            }

            if (TextUtils.equals(part2.mUserId, mSession.getMyUserId())) {
                return +1;
            }

            String lhs = part1.getComparisonDisplayName();
            String rhs = part2.getComparisonDisplayName();

            if (lhs == null) {
                return -1;
            } else if (rhs == null) {
                return 1;
            }

            return String.CASE_INSENSITIVE_ORDER.compare(lhs, rhs);
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == VectorUtils.TAKE_IMAGE) {
            onActivityResultRoomAvatarUpdate(resultCode, data);
        } else if (requestCode == INVITE_USER_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                List<ParticipantAdapterItem> items = (List<ParticipantAdapterItem>) data.getSerializableExtra(VectorRoomInviteMembersActivity.EXTRA_OUT_SELECTED_PARTICIPANT_ITEMS);
                for (ParticipantAdapterItem newItem : items) {
                    for (ParticipantAdapterItem item : mParticipants) {
                        if (newItem.mUserId.equals(item.mUserId))
                            return;
                    }
                }
                mParticipants.addAll(items);
                mAdapter.sort(mAlphaComparator);
                mAdapter.notifyDataSetChanged();
            } else if (1 == mParticipants.size()) {
                // the user cancels the first user selection so assume he wants to cancel the room creation.
                this.finish();
            }
        }
    }

    //================================================================================
    // Menu management
    //================================================================================

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // the application is in a weird state
        // GA : mSession is null
        if (CommonActivityUtils.shouldRestartApp(this) || (null == mSession)) {
            return false;
        }

        getMenuInflater().inflate(R.menu.vector_room_creation, menu);

        CommonActivityUtils.tintMenuIcons(menu, ThemeUtils.getColor(this, R.attr.actionbar_text_color));
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_create_room:
                if (mParticipants.size() > 0) {
                    if (editName.getText().toString().equals(""))
                        Toast.makeText(this, R.string.new_group_creation_create_name_for_group, Toast.LENGTH_LONG).show();
                    else
                        createRoom(mParticipants);
                } else
                    Toast.makeText(this, R.string.new_group_creation_add_more_members_for_group_creation, Toast.LENGTH_LONG).show();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        editName.clearFocus();
        findViewById(R.id.focus_layout).requestFocus();
    }

    @Override
    protected void onStart() {
        super.onStart();
        editName.clearFocus();
        findViewById(R.id.focus_layout).requestFocus();
    }
}
