package com.talkermessenger.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.talkermessenger.Matrix;
import com.talkermessenger.R;
import com.talkermessenger.adapters.AdapterUtils;
import com.talkermessenger.util.DaySection;
import com.talkermessenger.util.RoomUtils;
import com.talkermessenger.util.VectorUtils;

import org.matrix.androidsdk.MXSession;
import org.matrix.androidsdk.call.IMXCall;
import org.matrix.androidsdk.data.Room;
import org.matrix.androidsdk.data.RoomSummary;
import org.matrix.androidsdk.rest.callback.ApiCallback;
import org.matrix.androidsdk.rest.callback.SimpleApiCallback;
import org.matrix.androidsdk.rest.model.Event;
import org.matrix.androidsdk.rest.model.MatrixError;
import org.matrix.androidsdk.rest.model.RoomMember;
import org.matrix.androidsdk.util.Log;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.github.luizgrp.sectionedrecyclerviewadapter.SectionedRecyclerViewAdapter;

import static org.matrix.androidsdk.rest.model.MediaMessage.LOG_TAG;

public class CallHistoryActivity extends AppCompatActivity {

    public static final String EXTRA_ROOM_ID = "EXTRA_ROOM_ID";

    private String mUserId;
    private MXSession mSession;
    private SectionedRecyclerViewAdapter sectionAdapter;

    private String roomId;

    @BindView(R.id.call_history_list)
    RecyclerView callHistoryList;

    @BindView(R.id.call_icon)
    ImageView  mCallImageView;

    @BindView(R.id.member_details_name)
    TextView mMemberNameTextView ;
    @BindView(R.id.member_details_presence)
    TextView mPresenceTextView ;

    private boolean isCall;

    private final ApiCallback<Void> mRoomActionsListener = new SimpleApiCallback<Void>(this) {
        @Override
        public void onMatrixError(MatrixError e) {
            Toast.makeText(CallHistoryActivity.this, e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
         //   updateUi();
        }

        @Override
        public void onSuccess(Void info) {

            //updateUi();
        }

        @Override
        public void onNetworkError(Exception e) {
            Toast.makeText(CallHistoryActivity.this, e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
           // updateUi();
        }

        @Override
        public void onUnexpectedError(Exception e) {
            Toast.makeText(CallHistoryActivity.this, e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
           // updateUi();
        }
    };


    private final ApiCallback<String> mCreateDirectMessageCallBack = new ApiCallback<String>() {
        @Override
        public void onSuccess(String roomId) {
            Log.d(LOG_TAG, "## mCreateDirectMessageCallBack: onSuccess - start goToRoomPage " + roomId);
            openRoomById(roomId, isCall);
        }

        @Override
        public void onMatrixError(MatrixError e) {
            Log.d(LOG_TAG, "## mCreateDirectMessageCallBack: onMatrixError Msg=" + e.getLocalizedMessage());
            mRoomActionsListener.onMatrixError(e);
        }

        @Override
        public void onNetworkError(Exception e) {
            Log.d(LOG_TAG, "## mCreateDirectMessageCallBack: onNetworkError Msg=" + e.getLocalizedMessage());
            mRoomActionsListener.onNetworkError(e);
        }

        @Override
        public void onUnexpectedError(Exception e) {
            Log.d(LOG_TAG, "## mCreateDirectMessageCallBack: onUnexpectedError Msg=" + e.getLocalizedMessage());
            mRoomActionsListener.onUnexpectedError(e);
        }
    };


    private void startCheckCallPermissions(boolean aIsVideoCall) {
        int requestCode = CommonActivityUtils.REQUEST_CODE_PERMISSION_AUDIO_IP_CALL;

        if (aIsVideoCall) {
            requestCode = CommonActivityUtils.REQUEST_CODE_PERMISSION_VIDEO_IP_CALL;
        }

        if (CommonActivityUtils.checkPermissions(requestCode, this)) {
            startCall(aIsVideoCall);
        }
    }

    private void startCall(final boolean isVideo) {
        if (!mSession.isAlive()) {
            Log.e(LOG_TAG, "startCall : the session is not anymore valid");
            return;
        }

        // create the call object
        mSession.mCallsManager.createCallInRoom(roomId, isVideo, new ApiCallback<IMXCall>() {
            @Override
            public void onSuccess(final IMXCall call) {
                Log.d(LOG_TAG, "## startIpCall(): onSuccess");
             CallHistoryActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        final Intent intent = new Intent(CallHistoryActivity.this, VectorCallViewActivity.class);

                        intent.putExtra(VectorCallViewActivity.EXTRA_MATRIX_ID, mSession.getCredentials().userId);
                        intent.putExtra(VectorCallViewActivity.EXTRA_CALL_ID, call.getCallId());

                        CallHistoryActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                CallHistoryActivity.this.startActivity(intent);
                            }
                        });
                    }
                });
            }

            private void onError(final String errorMessage) {
               CallHistoryActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Activity activity = CallHistoryActivity.this;
                        CommonActivityUtils.displayToastOnUiThread(activity, activity.getString(R.string.cannot_start_call) + " (user offline)");
                    }
                });
            }

            @Override
            public void onNetworkError(Exception e) {
                Log.e(LOG_TAG, "## startIpCall(): onNetworkError Msg=" + e.getMessage());
                onError(e.getLocalizedMessage());
            }

            @Override
            public void onMatrixError(MatrixError e) {
                Log.e(LOG_TAG, "## startIpCall(): onMatrixError Msg=" + e.getLocalizedMessage());
                onError(e.getLocalizedMessage());
            }

            @Override
            public void onUnexpectedError(Exception e) {
                Log.e(LOG_TAG, "## startIpCall(): onUnexpectedError Msg=" + e.getLocalizedMessage());
                onError(e.getLocalizedMessage());
            }
        });
    }

    private void openRoomById(String roomId, boolean isCall) {
        Room room = mSession.getDataHandler().getRoom(roomId);
        this.roomId = roomId;
        final RoomSummary roomSummary = mSession.getDataHandler().getStore().getSummary(roomId);
        if (null != roomSummary) {
            room.sendReadReceipt();
        }
        // Update badge unread count in case device is offline
        CommonActivityUtils.specificUpdateBadgeUnreadCount(mSession, this);
        if (isCall) {
            startCheckCallPermissions(false);
        } else {
            Intent intent = new Intent(this, VectorRoomActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            intent.putExtra(EXTRA_ROOM_ID, roomId);
            startActivity(intent);
        }
        finish();
    }

    void openRoom(final Room room, boolean isCall) {
        // sanity checks
        // reported by GA
        if ((null == mSession.getDataHandler()) || (null == mSession.getDataHandler().getStore())) {
            return;
        }

        final String roomId;
        // cannot join a leaving room
        if (room == null || room.isLeaving()) {
            roomId = null;
        } else {
            roomId = room.getRoomId();
        }

        if (roomId != null) {
            openRoomById(roomId, isCall);
        }
    }


    private void startRoom(boolean isCall) {
        this.isCall = isCall;
        String existingRoomId;
//        enableProgressBarView(CommonActivityUtils.UTILS_DISPLAY_PROGRESS_BAR);
        if (null != (existingRoomId = RoomUtils.isDirectChatRoomAlreadyExist(mUserId, mSession))) {
            openRoom(mSession.getDataHandler().getRoom(existingRoomId), isCall);
        } else {
            // direct message flow
            mSession.createDirectMessageRoom(mUserId, mCreateDirectMessageCallBack);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call_history);
        ButterKnife.bind(this);

        setSupportActionBar(findViewById(R.id.account_toolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);






        Intent intent = getIntent();
        if (intent != null) {
            mUserId = getIntent().getStringExtra("userId");
            if (mUserId != null) {
                mSession = Matrix.getInstance(getApplicationContext()).getDefaultSession();
                getSupportActionBar().setTitle(VectorUtils.getUserDisplayName(mUserId, mSession, this));
                sectionAdapter = new SectionedRecyclerViewAdapter();
                String directChatRoomId = RoomUtils.isDirectChatRoomAlreadyExist(mUserId, mSession);
                List<Event> callEvents = fillCallList(directChatRoomId);
                callHistoryList.setLayoutManager(new LinearLayoutManager(this));
                callHistoryList.setAdapter(sectionAdapter);
                setAdapterData(callEvents, sectionAdapter);
            }
        }

        mPresenceTextView.setText(VectorUtils.getUserOnlineStatus(this, mSession, mUserId, new SimpleApiCallback<Void>() {
            @Override
            public void onSuccess(Void info) {
                mPresenceTextView.setText(VectorUtils.getUserOnlineStatus(CallHistoryActivity.this, mSession, mUserId, null));
            }
        }));


        mMemberNameTextView.setText(VectorUtils.getUserDisplayName(mUserId, mSession, this));
        mCallImageView = findViewById(R.id.call_icon);
        mCallImageView.setOnClickListener(view -> {

                    Log.d(LOG_TAG, "## performItemAction(): Start new room - start chat");
//                                VectorMemberDetailsActivity.this.runOnUiThread(new Runnable() {
//                                    @Override
//                                    public void run() {
                    startRoom(true);
//                                    }
//                                });
                }
        );
    }

    private void setAdapterData(List<Event> events, SectionedRecyclerViewAdapter sectionAdapter) {
        sectionAdapter.removeAllSections();
        String date = null;
        ArrayList<Event> eventsForMonthYear = new ArrayList<>();
        Calendar today = Calendar.getInstance();
        today = clearTimes(today);
        Calendar yesterday = Calendar.getInstance();
        yesterday.add(Calendar.DAY_OF_YEAR, -1);
        yesterday = clearTimes(yesterday);
        Calendar lastFiveDays = Calendar.getInstance();
        lastFiveDays.add(Calendar.DAY_OF_YEAR, -4);
        lastFiveDays = clearTimes(lastFiveDays);
        if (events != null)
            for (Event event : events) {
                String eventDate = AdapterUtils.convertDateToString(event.getOriginServerTs(), "dd.MM.yyyy", this);
                if (!eventDate.equals(date)) {
                    if (date != null) {
                        if (event.getOriginServerTs() > today.getTimeInMillis())
                            sectionAdapter.addSection(new DaySection(getString(R.string.call_history_today), (List<Event>) eventsForMonthYear.clone(), this));
                        else if (event.getOriginServerTs() > yesterday.getTimeInMillis())
                            sectionAdapter.addSection(new DaySection(getString(R.string.call_history_yesterday), (List<Event>) eventsForMonthYear.clone(), this));
                        else if (event.getOriginServerTs() > lastFiveDays.getTimeInMillis())
                            sectionAdapter.addSection(new DaySection(new SimpleDateFormat("EEEE", Locale.getDefault()).format(event.getOriginServerTs()), (List<Event>) eventsForMonthYear.clone(), this));
                        else
                            sectionAdapter.addSection(new DaySection(eventDate, (List<Event>) eventsForMonthYear.clone(), this));
                        eventsForMonthYear.clear();
                    }
                    date = eventDate;
                    eventsForMonthYear.add(event);
                } else {
                    eventsForMonthYear.add(event);
                }
            }
        if (eventsForMonthYear.size() > 0) {
            if (events.get(events.size() - 1).getOriginServerTs() > today.getTimeInMillis())
                sectionAdapter.addSection(new DaySection(getString(R.string.call_history_today), (List<Event>) eventsForMonthYear.clone(), this));
            else if (events.get(events.size() - 1).getOriginServerTs() > yesterday.getTimeInMillis())
                sectionAdapter.addSection(new DaySection(getString(R.string.call_history_yesterday), (List<Event>) eventsForMonthYear.clone(), this));
            else if (events.get(events.size() - 1).getOriginServerTs() > lastFiveDays.getTimeInMillis())
                sectionAdapter.addSection(new DaySection(new SimpleDateFormat("EEEE", Locale.getDefault()).format(events.get(events.size() - 1).getOriginServerTs()), (List<Event>) eventsForMonthYear.clone(), this));
            else
                sectionAdapter.addSection(new DaySection(date, (List<Event>) eventsForMonthYear.clone(), this));
        }
        sectionAdapter.notifyDataSetChanged();
    }

    private static Calendar clearTimes(Calendar c) {
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        return c;
    }

    public List<Event> fillCallList(String roomId) {
        List<Event> messages = new ArrayList<>();
        Room room = mSession.getDataHandler().getRoom(roomId);
        if (room.getAllRoomMessages() == null) return null;
        ArrayList<Event> events = new ArrayList<>(room.getAllRoomMessages());
        for (int i = 0, eventsSize = events.size(); i < eventsSize; i++) {
            Event event = events.get(i);
            if (event.isCallEvent() && !event.isHidden()) {
                if (event.getSender().equals(mSession.getMyUserId())) {
                    for (RoomMember roomMember : mSession.getDataHandler().getRoom(event.roomId).getMembers()) {
                        String userId = roomMember.getUserId();
                        if (!userId.equals(mSession.getMyUserId())) {
                            event.setCustomSender(userId);
                            event.setReceiver(userId);
                        }
                    }
                    event.setIncoming(false);
                } else {
                    event.setReceiver("");
                    event.setIncoming(true);
                }
                try {
                    if ((event.getType().equals(Event.EVENT_TYPE_CALL_HANGUP) && event.isIncoming())
                            && !events.get(i - 1).getType().equals(Event.EVENT_TYPE_CALL_ANSWER)) {
                        event.setMissed(true);
                    } else event.setMissed(false);
                } catch (Exception e) {
                    e.printStackTrace();
                    event.setMissed(false);
                }
                if (mSession.getDataHandler().getRoom(event.roomId).getMembers().size() > 2)
                    event.setReceiver(Event.EVENT_RECEIVER_CONFERENCE);
                if (event.getType().equals(Event.EVENT_TYPE_CALL_HANGUP))
                    messages.add(event);
            }
        }
        try {
            Collections.sort(messages, (m1, m2) -> {
                long diff = m1.getOriginServerTs() - m2.getOriginServerTs();
                return (diff < 0) ? +1 : (diff > 0 ? -1 : 0);
            });
        } catch (Exception e) {
            //   Log.e(LOG_TAG, "## notifyDataSetChanged () : failed to sort events " + e.getMessage());
        }
        return messages;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
