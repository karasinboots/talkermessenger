/*
 * Copyright 2017 Vector Creations Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.talkermessenger.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.CharacterStyle;
import android.text.style.ClickableSpan;
import android.text.style.ImageSpan;
import android.text.style.StyleSpan;
import android.text.style.URLSpan;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.JsonNull;
import com.google.gson.JsonObject;
import com.talkermessenger.R;
import com.talkermessenger.listeners.IMessagesAdapterActionsListener;
import com.talkermessenger.util.MatrixLinkMovementMethod;
import com.talkermessenger.util.MatrixURLSpan;
import com.talkermessenger.util.RiotEventDisplay;
import com.talkermessenger.util.ThemeUtils;
import com.talkermessenger.util.VectorUtils;
import com.talkermessenger.view.PillView;
import com.talkermessenger.widgets.WidgetsManager;

import org.matrix.androidsdk.MXSession;
import org.matrix.androidsdk.adapters.MessageRow;
import org.matrix.androidsdk.data.RoomState;
import org.matrix.androidsdk.rest.model.Event;
import org.matrix.androidsdk.rest.model.Message;
import org.matrix.androidsdk.rest.model.RoomMember;
import org.matrix.androidsdk.util.EventDisplay;
import org.matrix.androidsdk.util.JsonUtils;
import org.matrix.androidsdk.util.Log;
import org.matrix.androidsdk.view.HtmlTagHandler;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

/**
 * An helper to display message information
 */
public class VectorMessagesAdapterHelper {
    private static final String LOG_TAG = VectorMessagesAdapterHelper.class.getSimpleName();
    // JSON keys
    private static final String AVATAR_URL_KEY = "avatar_url";
    private static final String MEMBERSHIP_KEY = "membership";
    private static final String DISPLAYNAME_KEY = "displayname";
    private static final List<String> mAllowedHTMLTags = Arrays.asList(
            "font", // custom to matrix for IRC-style font coloring
            "del", // for markdown
            // deliberately no h1/h2 to stop people shouting.
            "h3", "h4", "h5", "h6", "blockquote", "p", "a", "ul", "ol",
            "nl", "li", "b", "i", "u", "strong", "em", "strike", "code", "hr", "br", "div",
            "table", "thead", "caption", "tbody", "tr", "th", "td", "pre");
    private static final Pattern mHtmlPatter = Pattern.compile("<(\\w+)[^>]*>", Pattern.CASE_INSENSITIVE);
    private final MXSession mSession;
    private final Context mContext;
    private final HashMap<String, String> mHtmlMap = new HashMap<>();
    // cache the pills to avoid compute them again
    Map<String, Drawable> mPillsCache = new HashMap<>();
    private IMessagesAdapterActionsListener mEventsListener;
    private MatrixLinkMovementMethod mLinkMovementMethod;


    VectorMessagesAdapterHelper(Context context, MXSession session) {
        mContext = context;
        mSession = session;
    }

    /**
     * Returns an user display name for an user Id.
     *
     * @param userId the user id.
     * @return teh user display name.
     */
    public static String getUserDisplayName(String userId, MXSession mxSession, Context context) {
        return VectorUtils.getUserDisplayName(userId, mxSession, context);
    }

    /**
     * init the timeStamp value
     *
     * @param convertView the base view
     * @param value       the new value
     * @return the dedicated textView
     */
    static TextView setTimestampValue(View convertView, String value) {
        TextView tsTextView = convertView.findViewById(R.id.messagesAdapter_timestamp);

        if (null != tsTextView) {
            if (TextUtils.isEmpty(value)) {
                tsTextView.setVisibility(GONE);
            } else {
                tsTextView.setVisibility(VISIBLE);
                tsTextView.setText(value);
            }
        }

        return tsTextView;
    }

    /**
     * Align the avatar and the message body according to the mergeView flag
     *
     * @param subView          the message body
     * @param bodyLayoutView   the body layout
     * @param avatarLayoutView the avatar layout
     * @param isMergedView     true if the view is merged
     */
    static void alignSubviewToAvatarView(View subView, View bodyLayoutView, View avatarLayoutView, boolean isMergedView) {
        //avatarLayoutView.
       /* ViewGroup.MarginLayoutParams bodyLayout = (ViewGroup.MarginLayoutParams) bodyLayoutView.getLayoutParams();
        FrameLayout.LayoutParams subViewLinearLayout = (FrameLayout.LayoutParams) subView.getLayoutParams();

        ViewGroup.LayoutParams avatarLayout = avatarLayoutView.getLayoutParams();

        subViewLinearLayout.gravity = Gravity.LEFT | Gravity.CENTER_VERTICAL;

        if (isMergedView) {
            bodyLayout.setMargins(avatarLayout.width, bodyLayout.topMargin, 4, bodyLayout.bottomMargin);
        } else {
            bodyLayout.setMargins(4, bodyLayout.topMargin, 4, bodyLayout.bottomMargin);
        }
        subView.setLayoutParams(bodyLayout);

        bodyLayoutView.setLayoutParams(bodyLayout);
        subView.setLayoutParams(subViewLinearLayout);*/
    }

    /**
     * Update the header text.
     *
     * @param convertView the convert view
     * @param newValue    the new value
     * @param position    the item position
     */
    static void setHeader(View convertView, String newValue, int position) {
        // display the day separator
        View headerLayout = convertView.findViewById(R.id.messagesAdapter_message_header);

        if (null != headerLayout) {
            if (null != newValue) {
                TextView headerText = convertView.findViewById(R.id.messagesAdapter_message_header_text);
                headerText.setText(newValue);
                headerLayout.setVisibility(VISIBLE);

                View topHeaderMargin = headerLayout.findViewById(R.id.messagesAdapter_message_header_top_margin);
                topHeaderMargin.setVisibility((0 == position) ? VISIBLE : GONE);
            } else {
                headerLayout.setVisibility(GONE);
            }
        }
    }

    /**
     * Refresh the media progress layouts
     *
     * @param convertView    the convert view
     * @param bodyLayoutView the body layout
     */

    static void setMediaProgressLayout(View convertView, View bodyLayoutView) {
        ViewGroup.MarginLayoutParams bodyLayoutParams = (ViewGroup.MarginLayoutParams) bodyLayoutView.getLayoutParams();
        int marginLeft = bodyLayoutParams.leftMargin;

        View downloadProgressLayout = convertView.findViewById(R.id.content_download_progress_layout);

        if (null != downloadProgressLayout) {
            ViewGroup.MarginLayoutParams downloadProgressLayoutParams = (ViewGroup.MarginLayoutParams) downloadProgressLayout.getLayoutParams();
            downloadProgressLayoutParams.setMargins(marginLeft, downloadProgressLayoutParams.topMargin, downloadProgressLayoutParams.rightMargin, downloadProgressLayoutParams.bottomMargin);
            downloadProgressLayout.setLayoutParams(downloadProgressLayoutParams);
        }

        View uploadProgressLayout = convertView.findViewById(R.id.content_upload_progress_layout);

        if (null != uploadProgressLayout) {
            ViewGroup.MarginLayoutParams uploadProgressLayoutParams = (ViewGroup.MarginLayoutParams) uploadProgressLayout.getLayoutParams();
            uploadProgressLayoutParams.setMargins(marginLeft, uploadProgressLayoutParams.topMargin, uploadProgressLayoutParams.rightMargin, uploadProgressLayoutParams.bottomMargin);
            uploadProgressLayout.setLayoutParams(uploadProgressLayoutParams);
        }
    }

    /**
     * Check if an event is displayable
     *
     * @param context the context
     * @param row     the row
     * @return true if the event is managed.
     */
    static boolean isDisplayableEvent(Context context, MessageRow row) {
        if (null == row) {
            return false;
        }

        RoomState roomState = row.getRoomState();
        Event event = row.getEvent();

        if ((null == roomState) || (null == event)) {
            return false;
        }

        String eventType = event.getType();

        if (Event.EVENT_TYPE_MESSAGE.equals(eventType)) {
            // A message is displayable as long as it has a body
            Message message = JsonUtils.toMessage(event.getContent());
            return (message.body != null) && (!message.body.equals(""));
        } else if (Event.EVENT_TYPE_STATE_ROOM_TOPIC.equals(eventType)
                || Event.EVENT_TYPE_STATE_ROOM_NAME.equals(eventType)) {
            EventDisplay display = new RiotEventDisplay(context, event, roomState);
            return false;
            // return display.getTextualDisplay() != null;
        } else if (event.isCallEvent()) {
            return false;
          /*  return Event.EVENT_TYPE_CALL_INVITE.equals(eventType) ||
                    Event.EVENT_TYPE_CALL_ANSWER.equals(eventType) ||
                    Event.EVENT_TYPE_CALL_HANGUP.equals(eventType);*/
        } else if (Event.EVENT_TYPE_STATE_ROOM_MEMBER.equals(eventType) || Event.EVENT_TYPE_STATE_ROOM_THIRD_PARTY_INVITE.equals(eventType)) {
            // if we can display text for it, it's valid.
            EventDisplay display = new RiotEventDisplay(context, event, roomState);
            return false;
            //return display.getTextualDisplay() != null;
        } else if (Event.EVENT_TYPE_STATE_HISTORY_VISIBILITY.equals(eventType)) {
            return false;
        } else if (Event.EVENT_TYPE_MESSAGE_ENCRYPTED.equals(eventType) || Event.EVENT_TYPE_MESSAGE_ENCRYPTION.equals(eventType)) {
            // if we can display text for it, it's valid.
            EventDisplay display = new RiotEventDisplay(context, event, roomState);
            //return event.hasContentFields() && (display.getTextualDisplay() != null);
        } else if (TextUtils.equals(WidgetsManager.WIDGET_EVENT_TYPE, event.getType())) {
            return false;
        }
        return false;
    }

    /**
     * Sanitise the HTML.
     * The matrix format does not allow the use some HTML tags.
     *
     * @param htmlString the html string
     * @return the sanitised string.
     */
    private static String sanitiseHTML(final String htmlString) {
        String html = htmlString;
        Matcher matcher = mHtmlPatter.matcher(htmlString);

        ArrayList<String> tagsToRemove = new ArrayList<>();

        while (matcher.find()) {

            try {
                String tag = htmlString.substring(matcher.start(1), matcher.end(1));

                // test if the tag is not allowed
                if (mAllowedHTMLTags.indexOf(tag) < 0) {
                    // add it once
                    if (tagsToRemove.indexOf(tag) < 0) {
                        tagsToRemove.add(tag);
                    }
                }
            } catch (Exception e) {
                Log.e(LOG_TAG, "sanitiseHTML failed " + e.getLocalizedMessage());
            }
        }

        // some tags to remove ?
        if (tagsToRemove.size() > 0) {
            // append the tags to remove
            String tagsToRemoveString = tagsToRemove.get(0);

            for (int i = 1; i < tagsToRemove.size(); i++) {
                tagsToRemoveString += "|" + tagsToRemove.get(i);
            }

            html = html.replaceAll("<\\/?(" + tagsToRemoveString + ")[^>]*>", "");
        }

        return html;
    }

    /**
     * Define the events listener
     *
     * @param listener the events listener
     */
    void setVectorMessagesAdapterActionsListener(IMessagesAdapterActionsListener listener) {
        mEventsListener = listener;
    }

    /**
     * Define the links movement method
     *
     * @param method the links movement method
     */
    void setLinkMovementMethod(MatrixLinkMovementMethod method) {
        mLinkMovementMethod = method;
    }

    /**
     * init the sender value
     *
     * @param convertView  the base view
     * @param row          the message row
     * @param isMergedView true if the cell is merged
     */
    public void setSenderValue(View convertView, MessageRow row, boolean isMergedView, boolean inGroup) {
        // manage sender text
        TextView senderTextView = convertView.findViewById(R.id.messagesAdapter_sender);

        if (null != senderTextView) {
            Event event = row.getEvent();

            boolean myMessage = row.getEvent().getSender().equals(mSession.getMyUserId());

            if (isMergedView || !inGroup || myMessage) {
                senderTextView.setVisibility(GONE);
            } else {
                senderTextView.setVisibility(VISIBLE);
                senderTextView.setText(getUserDisplayName(event.getSender(), mSession, mContext));

                final String fSenderId = event.getSender();
                final String fDisplayName = (null == senderTextView.getText()) ? "" : senderTextView.getText().toString();

                senderTextView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (null != mEventsListener) {
                            mEventsListener.onSenderNameClick(fSenderId, fDisplayName);
                        }
                    }
                });
                //}
            }
        }
    }

    /**
     * Load the avatar image in the avatar view
     *
     * @param avatarView the avatar view
     * @param row        the message row
     */
    void loadMemberAvatar(CircleImageView avatarView, MessageRow row) {
        RoomState roomState = row.getRoomState();
        Event event = row.getEvent();

        RoomMember roomMember = null;

        if (null != roomState) {
            roomMember = roomState.getMember(event.getSender());
        }

        String url = null;
        String displayName = null;

        // Check whether this avatar url is updated by the current event (This happens in case of new joined member)
        JsonObject msgContent = event.getContentAsJsonObject();

        if (msgContent.has(AVATAR_URL_KEY)) {
            url = msgContent.get(AVATAR_URL_KEY) == JsonNull.INSTANCE ? null : msgContent.get(AVATAR_URL_KEY).getAsString();
        }

        if (msgContent.has(MEMBERSHIP_KEY)) {
            String memberShip = msgContent.get(MEMBERSHIP_KEY) == JsonNull.INSTANCE ? null : msgContent.get(MEMBERSHIP_KEY).getAsString();

            // the avatar url is the invited one not the inviter one.
            if (TextUtils.equals(memberShip, RoomMember.MEMBERSHIP_INVITE)) {
                url = null;

                if (null != roomMember) {
                    url = roomMember.getAvatarUrl();
                }
            }

            if (TextUtils.equals(memberShip, RoomMember.MEMBERSHIP_JOIN)) {
                // in some cases, the displayname cannot be retrieved because the user member joined the room with this event
                // without being invited (a public room for example)
                if (msgContent.has(DISPLAYNAME_KEY)) {
                    displayName = msgContent.get(DISPLAYNAME_KEY) == JsonNull.INSTANCE ? null : msgContent.get(DISPLAYNAME_KEY).getAsString();
                }
            }
        }

        final String userId = event.getSender();

        if (!mSession.isAlive()) {
            return;
        }

        // if there is no preferred display name, use the member one
        if (TextUtils.isEmpty(displayName) && (null != roomMember)) {
            displayName = roomMember.displayname;
        }

        if ((roomMember != null) && (null == url)) {
            url = roomMember.getAvatarUrl();
        }

        if (null != roomMember) {
            VectorUtils.loadUserAvatar(mContext, mSession, avatarView, url, roomMember.getUserId());
        } else {
            VectorUtils.loadUserAvatar(mContext, mSession, avatarView, url, userId);
        }
    }

    /**
     * init the sender avatar
     *
     * @param convertView  the base view
     * @param row          the message row
     * @param isMergedView true if the cell is merged
     * @return the avatar layout
     */
    View setSenderAvatar(View convertView, MessageRow row, boolean isMergedView) {
        Event event = row.getEvent();
        View avatarLayoutView = convertView.findViewById(R.id.messagesAdapter_roundAvatar);

        if (null != avatarLayoutView) {
            final String userId = event.getSender();

            avatarLayoutView.setClickable(true);
            avatarLayoutView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    return (null != mEventsListener) && mEventsListener.onAvatarLongClick(userId);
                }
            });

            // click on the avatar opens the details page
            avatarLayoutView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (null != mEventsListener) {
                        mEventsListener.onAvatarClick(userId);
                        mEventsListener.onCallsAvatarClick(userId, row.getRoomId());
                    }
                }
            });
            try {
              /*  ImageView callDirection = convertView.findViewById(R.id.callDirection);
                if (event.isIncoming()) {
                    callDirection.setImageDrawable(convertView.getContext().getResources().getDrawable(R.drawable.incoming));
                } else
                    callDirection.setImageDrawable(convertView.getContext().getResources().getDrawable(R.drawable.outgoing));*/
                ImageView makeCall = convertView.findViewById(R.id.makeCall);
                if (userId.equals(mSession.getMyUserId()))
                    makeCall.setVisibility(GONE);
                else makeCall.setVisibility(VISIBLE);
                makeCall.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (null != mEventsListener) {
                            //   mEventsListener.onAvatarClick(userId);
                            mEventsListener.onCallsAvatarClick(userId, row.getRoomId());
                        }
                    }
                });
            } catch (NullPointerException e) {
            }

        }


        if (null != avatarLayoutView) {
            CircleImageView avatarImageView = avatarLayoutView.findViewById(R.id.avatar_img);

            if (isMergedView) {
                avatarLayoutView.setVisibility(GONE);
            } else {
                avatarLayoutView.setVisibility(VISIBLE);
                avatarImageView.setTag(null);

                loadMemberAvatar(avatarImageView, row);
            }
        }

        return avatarLayoutView;
    }

    //================================================================================
    // HTML management
    //================================================================================

    /**
     * Hide the read receipts view
     *
     * @param convertView base view
     */
    void hideReadReceipts(View convertView) {
        // View avatarsListView = convertView.findViewById(R.id.messagesAdapter_avatars_list);
        View receipt1 = convertView.findViewById(R.id.receipt1);
        View receipt2 = convertView.findViewById(R.id.receipt);
        if (null != receipt1) {
            receipt1.setVisibility(GONE);
            receipt2.setVisibility(GONE);
        }
    }


    /**
     * Trap the clicked URL.
     *
     * @param strBuilder    the input string
     * @param span          the URL
     * @param isHighlighted true if the message is highlighted
     */
    private void makeLinkClickable(SpannableStringBuilder strBuilder, final URLSpan span,
                                   final boolean isHighlighted) {
        int start = strBuilder.getSpanStart(span);
        int end = strBuilder.getSpanEnd(span);

        if (start >= 0 && end >= 0) {
            int flags = strBuilder.getSpanFlags(span);

            if (PillView.isPillable(span.getURL())) {
                String key = span.getURL() + " " + isHighlighted;
                Drawable drawable = mPillsCache.get(key);

                if (null == drawable) {
                    PillView aView = new PillView(mContext);
                    aView.setText(strBuilder.subSequence(start, end), span.getURL());
                    aView.setHighlighted(isHighlighted);
                    drawable = aView.getDrawable();
                }
                if (null != drawable) {
                    mPillsCache.put(key, drawable);
                    ImageSpan imageSpan = new ImageSpan(drawable);
                    drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
                    strBuilder.setSpan(imageSpan, start, end, flags);
                }
            }

            ClickableSpan clickable = new ClickableSpan() {
                public void onClick(View view) {
                    if (null != mEventsListener) {
                        mEventsListener.onURLClick(Uri.parse(span.getURL()));
                    }
                }
            };

            strBuilder.setSpan(clickable, start, end, flags);
            strBuilder.removeSpan(span);
        }
    }

    /**
     * Highlight the pattern in the text.
     *
     * @param textView           the textview
     * @param text               the text to display
     * @param htmlFormattedText  the html formatted text
     * @param pattern            the  pattern
     * @param highLightTextStyle the highlight text style
     * @param isHighlighted      true when the message is highlighted
     */
    void highlightPattern(TextView textView, Spannable text, String htmlFormattedText, String
            pattern, CharacterStyle highLightTextStyle, boolean isHighlighted) {
        // sanity check
        if (null == textView) {
            return;
        }

        if (!TextUtils.isEmpty(pattern) && !TextUtils.isEmpty(text) && (text.length() >= pattern.length())) {

            String lowerText = text.toString().toLowerCase();
            String lowerPattern = pattern.toLowerCase();

            int start = 0;
            int pos = lowerText.indexOf(lowerPattern, start);

            while (pos >= 0) {
                start = pos + lowerPattern.length();
                text.setSpan(highLightTextStyle, pos, start, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                text.setSpan(new StyleSpan(android.graphics.Typeface.BOLD), pos, start, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                pos = lowerText.indexOf(lowerPattern, start);
            }
        }

        final HtmlTagHandler htmlTagHandler = new HtmlTagHandler();
        htmlTagHandler.mContext = mContext;
        htmlTagHandler.setCodeBlockBackgroundColor(ThemeUtils.getColor(mContext, R.attr.markdown_block_background_color));

        CharSequence sequence;

        // an html format has been released
        if (null != htmlFormattedText) {
            boolean isCustomizable = !htmlFormattedText.contains("<a href=") && !htmlFormattedText.contains("<table>");

            // the links are not yet supported by ConsoleHtmlTagHandler
            // the markdown tables are not properly supported
            sequence = Html.fromHtml(htmlFormattedText, null, isCustomizable ? htmlTagHandler : null);

            // sanity check
            if (!TextUtils.isEmpty(sequence)) {
                // remove trailing \n to avoid having empty lines..
                int markStart = 0;
                int markEnd = sequence.length() - 1;

                // search first non \n character
                for (; (markStart < sequence.length() - 1) && ('\n' == sequence.charAt(markStart)); markStart++)
                    ;

                // search latest non \n character
                for (; (markEnd >= 0) && ('\n' == sequence.charAt(markEnd)); markEnd--) ;

                // empty string ?
                if (markEnd < markStart) {
                    sequence = sequence.subSequence(0, 0);
                } else {
                    sequence = sequence.subSequence(markStart, markEnd + 1);
                }
            }
        } else {
            sequence = text;
        }

        SpannableStringBuilder strBuilder = new SpannableStringBuilder(sequence);
        URLSpan[] urls = strBuilder.getSpans(0, text.length(), URLSpan.class);

        if ((null != urls) && (urls.length > 0)) {
            for (URLSpan span : urls) {
                makeLinkClickable(strBuilder, span, isHighlighted);
            }
        }

        MatrixURLSpan.refreshMatrixSpans(strBuilder, mEventsListener);
        textView.setText(strBuilder);

        if (null != mLinkMovementMethod) {
            textView.setMovementMethod(mLinkMovementMethod);
        }
    }

    /**
     * Retrieves the sanitised html.
     * !!!!!! WARNING !!!!!!
     * IT IS NOT REMOTELY A COMPREHENSIVE SANITIZER AND SHOULD NOT BE TRUSTED FOR SECURITY PURPOSES.
     * WE ARE EFFECTIVELY RELYING ON THE LIMITED CAPABILITIES OF THE HTML RENDERER UI TO AVOID SECURITY ISSUES LEAKING UP.
     *
     * @param html the html to sanitize
     * @return the sanitised HTML
     */
    String getSanitisedHtml(final String html) {
        // sanity checks
        if (TextUtils.isEmpty(html)) {
            return null;
        }

        String res = mHtmlMap.get(html);

        if (null == res) {
            res = sanitiseHTML(html);
            mHtmlMap.put(html, res);
        }

        return res;
    }
}
