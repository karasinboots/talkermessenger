package com.talkermessenger.adapters;


import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import com.iskar.talkersdk.model.TranslationHistoryItem;
import com.talkermessenger.R;
import com.talkermessenger.listeners.TranslationHistoryClickListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TranslationHistoryAdapter extends RecyclerView.Adapter<TranslationHistoryAdapter.ViewHolder> {
    private List<TranslationHistoryItem> items = new ArrayList<>();
    private TranslationHistoryClickListener translationHistoryClickListener = null;

    public TranslationHistoryAdapter(List<TranslationHistoryItem> items, TranslationHistoryClickListener translationHistoryClickListener) {
        this.items = items;
        this.translationHistoryClickListener = translationHistoryClickListener;
    }

    public void setItems(List<TranslationHistoryItem> items) {
        this.items = items;
    }

    @Override
    public TranslationHistoryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new TranslationHistoryAdapter.ViewHolder(LayoutInflater.from(parent.getContext()), parent);
    }

    @Override
    public void onBindViewHolder(TranslationHistoryAdapter.ViewHolder holder, int position) {
        holder.originalLanguageCode.setText(items.get(position).getOriginalLanguageCode());
        holder.translatedLanguageCode.setText(items.get(position).getTranslatedLanguageCode());
        holder.translatedText.setText(items.get(position).getOriginalText());
        holder.historyItem.setOnLongClickListener(v -> {
            translationHistoryClickListener.onLongClick(items.get(position), position);
            return true;
        });
        holder.historyItem.setOnClickListener(v -> {
            translationHistoryClickListener.onClick(items.get(position), position);
        });
    }

    @Override
    public int getItemCount() {
        return items == null ? 0 : items.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.original_text_language_code)
        TextView originalLanguageCode;

        @BindView(R.id.history_item)
        CardView historyItem;

        @BindView(R.id.translated_text_language_code)
        TextView translatedLanguageCode;

        @BindView(R.id.translated_text)
        TextView translatedText;

        public ViewHolder(LayoutInflater inflater, ViewGroup parent) {
            super(inflater.inflate(R.layout.item_history_translation, parent, false));
            ButterKnife.bind(this, itemView);
        }
    }


    public void clearData() {
        items.clear();
        notifyDataSetChanged();
    }
}
