package com.talkermessenger.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.iskar.talkersdk.ContactHandler;
import com.iskar.talkersdk.model.TalkerContact;
import com.talkermessenger.R;
import com.talkermessenger.activity.CallHistoryActivity;
import com.talkermessenger.activity.CommonActivityUtils;
import com.talkermessenger.activity.VectorCallViewActivity;
import com.talkermessenger.activity.VectorMemberDetailsActivity;
import com.talkermessenger.util.RoomUtils;
import com.talkermessenger.util.ThemeUtils;
import com.talkermessenger.util.VectorUtils;

import org.matrix.androidsdk.MXSession;
import org.matrix.androidsdk.adapters.MessageRow;
import org.matrix.androidsdk.call.IMXCall;
import org.matrix.androidsdk.rest.callback.ApiCallback;
import org.matrix.androidsdk.rest.model.Event;
import org.matrix.androidsdk.rest.model.MatrixError;
import org.matrix.androidsdk.util.Log;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by karasinboots on 22.01.2018.
 */

public class CallLogAdapter extends RecyclerView.Adapter<CallLogAdapter.CallLogViewHolder> {
    List<MessageRow> calls;
    private Context mContext;
    private MXSession mSession;

    public CallLogAdapter(Context context, List<MessageRow> calls, MXSession mxSession) {
        this.mContext = context;
        this.calls = calls;
        this.mSession = mxSession;
    }

    public List<MessageRow> getCalls() {
        return calls;
    }

    public void setCalls(List<MessageRow> calls) {
        this.calls = calls;
    }

    @NonNull
    @Override
    public CallLogViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new CallLogViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_item_vector_message_call, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull CallLogViewHolder holder, int position) {
        MessageRow row = calls.get(position);
        Event event = row.getEvent();
        final String userId;
        if (event.getCustomSender() != null && !event.getReceiver().equals(Event.EVENT_RECEIVER_CONFERENCE))
            userId = event.getCustomSender();
        else userId = event.getSender();
        VectorUtils.loadUserAvatar(mContext, mSession, holder.avatar, row.getRoomState().getMember(userId).avatarUrl, userId);
        holder.avatar.setOnClickListener(v -> {
            try {
                Intent roomDetailsIntent = new Intent(mContext, VectorMemberDetailsActivity.class);
                roomDetailsIntent.putExtra(VectorMemberDetailsActivity.EXTRA_ROOM_ID, row.getRoomId());
                roomDetailsIntent.putExtra(VectorMemberDetailsActivity.EXTRA_MEMBER_ID, userId);
                roomDetailsIntent.putExtra(VectorMemberDetailsActivity.EXTRA_MATRIX_ID, mSession.getCredentials().userId);
                mContext.startActivity(roomDetailsIntent);
            } catch (Exception e) {
                //  Log.e(LOG_TAG, "## onAvatarClick() failed " + e.getMessage());
            }
        });
        setSenderValue(holder.sender, row);
        setTimestampValue(holder.timestamp, RoomUtils.getRoomTimestamp(mContext, event));
        holder.sender.setOnClickListener(v -> {
            startCall(false, row.getRoomId());
        });
        holder.info.setOnClickListener(v -> {
            Intent callHistoryIntent = new Intent(mContext, CallHistoryActivity.class);
            callHistoryIntent.putExtra("userId", userId);
            mContext.startActivity(callHistoryIntent);
        });
    }

    /**
     * Start a call in a dedicated room
     *
     * @param isVideo true if the call is a video call
     */
    private void startCall(final boolean isVideo, String roomId) {
        if (!mSession.isAlive()) {
            // Log.e(LOG_TAG, "startCall : the session is not anymore valid");
            return;
        }

        // create the call object
        mSession.mCallsManager.createCallInRoom(roomId, isVideo, new ApiCallback<IMXCall>() {
            @Override
            public void onSuccess(final IMXCall call) {
                final Intent intent = new Intent(mContext, VectorCallViewActivity.class);
                intent.putExtra(VectorCallViewActivity.EXTRA_MATRIX_ID, mSession.getCredentials().userId);
                intent.putExtra(VectorCallViewActivity.EXTRA_CALL_ID, call.getCallId());
                mContext.startActivity(intent);
            }

            @Override
            public void onNetworkError(Exception e) {
                CommonActivityUtils.displayToast(mContext, e.getLocalizedMessage());
                // Log.e(LOG_TAG, "## startCall() failed " + e.getMessage());
            }

            @Override
            public void onMatrixError(MatrixError e) {
                CommonActivityUtils.displayToast(mContext, e.getLocalizedMessage());
                // Log.e(LOG_TAG, "## startCall() failed " + e.getMessage());
            }

            @Override
            public void onUnexpectedError(Exception e) {
                CommonActivityUtils.displayToast(mContext, e.getLocalizedMessage());
                //  Log.e(LOG_TAG, "## startCall() failed " + e.getMessage());
            }
        });
    }

    private void setSenderValue(TextView senderTextView, MessageRow row) {
        // manage sender text
        if (null != senderTextView) {
            Event event = row.getEvent();
            final String fSenderId;
            if (event.getCustomSender() != null && !event.getReceiver().equals(Event.EVENT_RECEIVER_CONFERENCE))
                fSenderId = event.getCustomSender();
            else fSenderId = event.getSender();
            boolean hangup = false;
            if (event.getContentAsJsonObject().get("reason") != null
                    && event.getContentAsJsonObject().get("reason").getAsString().equals("user hangup"))
                hangup = true;
            senderTextView.setVisibility(View.VISIBLE);
            senderTextView.setText(getUserDisplayName(fSenderId, mSession));
            if (event.isMissed() && hangup && event.userId == null) {
                senderTextView.setTextColor(mContext.getResources().getColor(R.color.catalyst_redbox_background));
            } else {
                senderTextView.setTextColor(ThemeUtils.getColor(mContext, R.attr.riot_primary_text_color));
            }
        }
    }

    /**
     * init the timeStamp value
     *
     * @param value the new value
     * @return the dedicated textView
     */
    private TextView setTimestampValue(TextView tsTextView, String value) {
        if (null != tsTextView) {
            if (TextUtils.isEmpty(value)) {
                tsTextView.setVisibility(View.GONE);
            } else {
                tsTextView.setVisibility(View.VISIBLE);
                tsTextView.setText(value);
            }
        }
        return tsTextView;
    }

    /**
     * Returns an user display name for an user Id.
     *
     * @param userId the user id.
     * @return teh user display name.
     */
    private String getUserDisplayName(String userId, MXSession mxSession) {
        TalkerContact talkerContact = new ContactHandler().getContactFromTalkerContactSnapshot(userId, mContext);
        if (talkerContact != null && talkerContact.getDisplayName() != null)
            return talkerContact.getDisplayName();
        else if (mxSession.getDataHandler().getUser(userId).displayname != null)
            return mxSession.getDataHandler().getUser(userId).displayname;
        else return userId;
    }

    @Override
    public int getItemCount() {
        return calls.size();
    }

    class CallLogViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.avatar_img)
        CircleImageView avatar;

        @BindView(R.id.messagesAdapter_sender)
        TextView sender;

        @BindView(R.id.makeCall)
        ImageView info;

        @BindView(R.id.messagesAdapter_timestamp)
        TextView timestamp;

        private CallLogViewHolder(final View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}