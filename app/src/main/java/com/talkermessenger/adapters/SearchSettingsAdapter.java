package com.talkermessenger.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import com.talkermessenger.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Serhii on 13.02.2018.
 */

public class SearchSettingsAdapter extends RecyclerView.Adapter<SearchSettingsAdapter.ViewHolder> {

    private List<String> results = null;
    private List<String> originalList = null;
    private OnResultClickListener resultClickListener;

    public SearchSettingsAdapter(List<String> results, OnResultClickListener resultClickListener) {
        this.results = results;
        this.originalList = new ArrayList<>(results);
        this.resultClickListener = resultClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()), parent);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.textView.setText(results.get(position));
    }

    @Override
    public int getItemCount() {
        return results == null ? 0 : results.size();
    }

    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        results.clear();
        if (charText.length() == 0) {
            results.addAll(originalList);
        } else {
            for (String result : originalList) {
                if (result.toLowerCase(Locale.getDefault()).contains(charText)) {
                    results.add(result);
                }
            }
        }
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.search_name)
        TextView textView;

        public ViewHolder(LayoutInflater inflater, ViewGroup parent) {
            super(inflater.inflate(R.layout.adapter_item_search_settings, parent, false));

            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(view ->
                    resultClickListener.onResultClick(results.get(getAdapterPosition())));
        }
    }

    public interface OnResultClickListener {
        void onResultClick(String target);
    }
}
