package com.talkermessenger.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.talkermessenger.Matrix;
import com.talkermessenger.R;
import com.talkermessenger.util.VectorUtils;

import org.matrix.androidsdk.MXSession;
import org.matrix.androidsdk.util.Log;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class NewGroupCreationMemberAdapter extends RecyclerView.Adapter<NewGroupCreationMemberAdapter.ViewHolder> {
    private ArrayList<ParticipantAdapterItem> mDataset;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView mTextView;
        public CircleImageView avatar;
        public View mView;

        public ViewHolder(View v) {
            super(v);
            mTextView = v.findViewById(R.id.new_group_member_name);
            avatar = v.findViewById(R.id.planet);
            mView = v;
        }
    }

    public NewGroupCreationMemberAdapter(ArrayList<ParticipantAdapterItem> myDataset) {
        mDataset = myDataset;
    }

    @Override
    public NewGroupCreationMemberAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                                       int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_new_group_member, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.mTextView.setText(mDataset.get(position).mDisplayName);
        MXSession mxSession = Matrix.getInstance(holder.avatar.getContext()).getDefaultSession();
        VectorUtils.loadUserAvatar(holder.avatar.getContext(), mxSession
                , holder.avatar, mxSession.getDataHandler().getUser(mDataset.get(position).mUserId).avatar_url, mDataset.get(position).mUserId);
        holder.mView.setOnLongClickListener(v -> {
            mDataset.remove(position);
            notifyDataSetChanged();
            return false;
        });
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public void addAll(List<ParticipantAdapterItem> newMembers) {
        mDataset.addAll(newMembers);
        notifyDataSetChanged();
    }

    public void remove(ParticipantAdapterItem adapterItem) {
        mDataset.remove(adapterItem);
        notifyDataSetChanged();
    }

    public void sort(Comparator<? super ParticipantAdapterItem> comparator) {
        Collections.sort(mDataset, comparator);
    }

}