/*
 * Copyright 2017 Vector Creations Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.talkermessenger.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.talkermessenger.R;
import com.talkermessenger.contacts.Contact;
import com.talkermessenger.contacts.ContactsManager;
import com.talkermessenger.util.RoomUtils;
import com.talkermessenger.util.VectorUtils;

import org.matrix.androidsdk.data.MyUser;
import org.matrix.androidsdk.data.Room;
import org.matrix.androidsdk.data.RoomState;
import org.matrix.androidsdk.listeners.IMXEventListener;
import org.matrix.androidsdk.rest.callback.SimpleApiCallback;
import org.matrix.androidsdk.rest.model.Event;
import org.matrix.androidsdk.rest.model.RoomMember;
import org.matrix.androidsdk.rest.model.User;
import org.matrix.androidsdk.rest.model.bingrules.BingRule;
import org.matrix.androidsdk.util.Log;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class PeopleAdapter extends AbsAdapter {

    private static final String LOG_TAG = PeopleAdapter.class.getSimpleName();

    private static final int TYPE_CONTACT = 1;

    private final AdapterSection<Room> mDirectChatsSection;
    private final AdapterSection<ParticipantAdapterItem> mLocalContactsSection;
    private final KnownContactsAdapterSection mKnownContactsSection;

    private final OnSelectItemListener mListener;

    private final String mNoContactAccessPlaceholder;
    private final String mNoResultPlaceholder;

    private List<Character> firstLetters;
    /*
     * *********************************************************************************************
     * Constructor
     * *********************************************************************************************
     */

    public PeopleAdapter(final Context context, String fragmentType, final OnSelectItemListener listener, final InvitationListener invitationListener, final MoreRoomActionListener moreActionListener) {
        super(context, invitationListener, moreActionListener);
        mListener = listener;

        // ButterKnife.bind(this); cannot be applied here
        mNoContactAccessPlaceholder = context.getString(R.string.no_contact_access_placeholder);
        mNoResultPlaceholder = context.getString(R.string.no_result_placeholder);

        mDirectChatsSection = new AdapterSection<>(context.getString(R.string.direct_chats_header), -1,
                R.layout.adapter_item_room_view, TYPE_HEADER_DEFAULT, TYPE_ROOM, new ArrayList<Room>(), RoomUtils.getRoomsDateComparator(mSession, false));
        mDirectChatsSection.setEmptyViewPlaceholder(context.getString(R.string.no_conversation_placeholder), context.getString(R.string.no_result_placeholder));

        mLocalContactsSection = new AdapterSection<>(context.getString(R.string.local_address_book_header),
                -1, R.layout.adapter_item_contact_view, TYPE_HEADER_DEFAULT, TYPE_CONTACT, new ArrayList<ParticipantAdapterItem>(), ParticipantAdapterItem.alphaComparator);
        mLocalContactsSection.setEmptyViewPlaceholder(!ContactsManager.getInstance().isContactBookAccessAllowed() ? mNoContactAccessPlaceholder : mNoResultPlaceholder);

        mKnownContactsSection = new KnownContactsAdapterSection(context.getString(R.string.user_directory_header), -1,
                R.layout.adapter_item_contact_view_talker, TYPE_HEADER_DEFAULT, TYPE_CONTACT, new ArrayList<ParticipantAdapterItem>(), null);
        mKnownContactsSection.setEmptyViewPlaceholder(null, context.getString(R.string.no_result_placeholder));
        mKnownContactsSection.setIsHiddenWhenNoFilter(true);
        mLocalContactsSection.setIsHiddenWhenNoFilter(true);

        if (fragmentType.equals("home")) {
            addSection(mDirectChatsSection);
            //   addSection(mLocalContactsSection);
            //   addSection(mKnownContactsSection);
        }
        if (fragmentType.equals("peoples")) {
            //   addSection(mDirectChatsSection);
            addSection(mLocalContactsSection);
            //addSection(mKnownContactsSection);
        }
        firstLetters = new ArrayList<>();
    }

    /*
     * *********************************************************************************************
     * Abstract methods implementation
     * *********************************************************************************************
     */

    @Override
    protected RecyclerView.ViewHolder createSubViewHolder(ViewGroup viewGroup, int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());

        View itemView;
        switch (viewType) {
            case TYPE_ROOM:
                itemView = inflater.inflate(R.layout.adapter_item_room_view, viewGroup, false);
                return new RoomViewHolder(itemView);
            case TYPE_CONTACT:
                itemView = inflater.inflate(R.layout.adapter_item_contact_view_talker, viewGroup, false);
                return new ContactViewHolder(itemView);
        }
        return null;
    }


    private int counter = 0;

    @Override
    protected void populateViewHolder(int viewType, RecyclerView.ViewHolder viewHolder, int position) {
        switch (viewType) {
            case TYPE_ROOM:
                final RoomViewHolder roomViewHolder = (RoomViewHolder) viewHolder;
                final Room room = (Room) getItemForPosition(position);
                roomViewHolder.populateViews(mContext, mSession, room, true, false, mMoreActionListener);
                roomViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mListener.onSelectItem(room, -1);
                    }
                });
                for (RoomMember member : room.getMembers()) {
                    if (!member.getUserId().equals(mSession.getMyUserId())) {
                        loadContactPresence(roomViewHolder.itemView.findViewById(R.id.availability), member.getUserId(), position);
                    }
                }
                room.addEventListener(new IMXEventListener() {

                    @Override
                    public void onStoreReady() {

                    }

                    @Override
                    public void onPresenceUpdate(Event event, User user) {

                    }

                    @Override
                    public void onAccountInfoUpdate(MyUser myUser) {

                    }

                    @Override
                    public void onIgnoredUsersListUpdate() {

                    }

                    @Override
                    public void onDirectMessageChatRoomsListUpdate() {

                    }

                    @Override
                    public void onLiveEvent(Event event, RoomState roomState) {
                        String typingEvent;
                        Room room = mSession.getDataHandler().getRoom(roomState.roomId);
                        List<String> typingUsers = room.getTypingUsers();

                        if ((null != typingUsers) && (typingUsers.size() > 0)) {
                            String myUserId = mSession.getMyUserId();

                            // get the room member names
                            ArrayList<String> names = new ArrayList<>();

                            for (int i = 0; i < typingUsers.size(); i++) {
                                RoomMember member = room.getMember(typingUsers.get(i));

                                // check if the user is known and not oneself
                                if ((null != member) && !TextUtils.equals(myUserId, member.getUserId()) && (null != member.displayname)) {
                                    names.add(member.displayname);
                                }
                            }
                            if (!names.get(0).equals(mSession.getMyUser().displayname)) {
                                typingEvent = mContext.getResources().getString(R.string.room_one_user_is_typing);
                                ((RoomViewHolder) viewHolder).vRoomLastMessage.setText(typingEvent);
                            }
                        } else
                            ((RoomViewHolder) viewHolder).vRoomLastMessage.setText(
                                    RoomUtils.getRoomMessageToDisplay(mContext, mSession,
                                            mSession.getDataHandler().getStore(room.getRoomId())
                                                    .getSummary(room.getRoomId())));
                    }

                    class UserId {
                        List<String> user_ids;
                    }

                    @Override
                    public void onLiveEventsChunkProcessed(String fromToken, String toToken) {

                    }

                    @Override
                    public void onBingEvent(Event event, RoomState roomState, BingRule bingRule) {

                    }

                    @Override
                    public void onEventSentStateUpdated(Event event) {

                    }

                    @Override
                    public void onEventSent(Event event, String prevEventId) {

                    }

                    @Override
                    public void onEventDecrypted(Event event) {

                    }

                    @Override
                    public void onBingRulesUpdate() {

                    }

                    @Override
                    public void onInitialSyncComplete(String toToken) {

                    }

                    @Override
                    public void onCryptoSyncComplete() {

                    }

                    @Override
                    public void onNewRoom(String roomId) {

                    }

                    @Override
                    public void onJoinRoom(String roomId) {

                    }

                    @Override
                    public void onRoomFlush(String roomId) {

                    }

                    @Override
                    public void onRoomInitialSyncComplete(String roomId) {

                    }

                    @Override
                    public void onRoomInternalUpdate(String roomId) {

                    }

                    @Override
                    public void onNotificationCountUpdate(String roomId) {

                    }

                    @Override
                    public void onLeaveRoom(String roomId) {

                    }

                    @Override
                    public void onReceiptEvent(String roomId, List<String> senderIds) {

                    }

                    @Override
                    public void onRoomTagEvent(String roomId) {

                    }

                    @Override
                    public void onReadMarkerEvent(String roomId) {

                    }

                    @Override
                    public void onToDeviceEvent(Event event) {

                    }
                });
                break;
            case TYPE_CONTACT:
                final ContactViewHolder contactViewHolder = (ContactViewHolder) viewHolder;
                final ParticipantAdapterItem item = (ParticipantAdapterItem) getItemForPosition(position);
                contactViewHolder.populateViews(item, position);
                break;
        }
    }

    @Override
    protected int applyFilter(String pattern) {
        int nbResults = 0;
        nbResults += filterRoomSection(mDirectChatsSection, pattern);
        nbResults += filterLocalContacts(pattern);

        // if there is no pattern, use the local search
        if (TextUtils.isEmpty(pattern)) {
            nbResults += filterKnownContacts(pattern);
        }
        return nbResults;
    }

    /*
     * *********************************************************************************************
     * Public methods
     * *********************************************************************************************
     */

    public void setRooms(final List<Room> rooms) {
        mDirectChatsSection.setItems(rooms, mCurrentFilterPattern);
        if (!TextUtils.isEmpty(mCurrentFilterPattern)) {
            filterRoomSection(mDirectChatsSection, String.valueOf(mCurrentFilterPattern));
        }
        updateSections();
    }

    public void setLocalContacts(final List<ParticipantAdapterItem> localContacts) {
        // updates the placeholder according to the local contacts permissions
        mLocalContactsSection.setEmptyViewPlaceholder(!ContactsManager.getInstance().isContactBookAccessAllowed() ? mNoContactAccessPlaceholder : mNoResultPlaceholder);
        mLocalContactsSection.setItems(localContacts, mCurrentFilterPattern);
        if (!TextUtils.isEmpty(mCurrentFilterPattern)) {
            filterLocalContacts(String.valueOf(mCurrentFilterPattern));
        }
        updateSections();
        updateFirstLetters();
    }

    public void setKnownContacts(final List<ParticipantAdapterItem> knownContacts) {
        mKnownContactsSection.setItems(knownContacts, mCurrentFilterPattern);
        if (!TextUtils.isEmpty(mCurrentFilterPattern)) {
            filterKnownContacts(String.valueOf(mCurrentFilterPattern));
        } else {
            filterKnownContacts(null);
        }
        updateSections();
        updateFirstLetters();
    }

    public void setFilteredKnownContacts(List<ParticipantAdapterItem> filteredKnownContacts, String pattern) {
        Collections.sort(filteredKnownContacts, ParticipantAdapterItem.getComparator(mSession));
        mKnownContactsSection.setFilteredItems(filteredKnownContacts, pattern);
        updateSections();
        updateFirstLetters();
    }

    public void setKnownContactsLimited(boolean isLimited) {
        mKnownContactsSection.setIsLimited(isLimited);
    }

    public void setKnownContactsExtraTitle(String extraTitle) {
        mKnownContactsSection.setCustomHeaderExtra(extraTitle);
    }

    public void updateContactPresenceDot(Contact contact) {
        for (int i = 0; i < mLocalContactsSection.getItems().size(); i++) {
            if (mLocalContactsSection.getItems().get(i).mContact.getContactId().equals(contact.getContactId())) {
                notifyItemChanged(i);
                Log.e("contact updated", "ok");
            }
        }
    }

    public void updateRoomsPresenceDot(Contact contact) {
        for (int i = 0; i < mDirectChatsSection.getItems().size(); i++) {
            for (RoomMember member : mDirectChatsSection.getItems().get(i).getMembers())
                if (member.getUserId().equals(contact.getContactId())) {
                    notifyItemChanged(i);
                    Log.e("contact updated", "ok");
                }
        }
    }

    /**
     * Update the known contact corresponding to the given user id
     *
     * @param user
     */
    public void updateKnownContact(final User user) {
        int headerPos = getSectionHeaderPosition(mKnownContactsSection) + 1;
        List<ParticipantAdapterItem> knownContacts = mKnownContactsSection.getFilteredItems();
        for (int i = 0; i < knownContacts.size(); i++) {
            ParticipantAdapterItem item = knownContacts.get(i);
            if (TextUtils.equals(user.user_id, item.mUserId)) {
                notifyItemChanged(headerPos + i);
            }
        }
        updateFirstLetters();
    }

    /*
     * *********************************************************************************************
     * Private methods
     * *********************************************************************************************
     */

    /**
     * Filter the local contacts with the given pattern
     *
     * @param pattern
     * @return nb of items matching the filter
     */
    private int filterLocalContacts(final String pattern) {
        if (!TextUtils.isEmpty(pattern)) {
            List<ParticipantAdapterItem> filteredLocalContacts = new ArrayList<>();
            final String formattedPattern = pattern.toLowerCase();
            List<ParticipantAdapterItem> sectionItems = new ArrayList<>(mLocalContactsSection.getItems());
            for (ParticipantAdapterItem item : sectionItems) {
                if (item.mDisplayName.toLowerCase().contains(formattedPattern)
                        || (item.mContact != null && item.mContact.getDisplayName().contains(formattedPattern)))
                    filteredLocalContacts.add(item);
                else {
                    if (item.mContact != null)
                        for (Contact.PhoneNumber number : item.mContact.getPhonenumbers())
                            if (number.mE164PhoneNumber.contains(formattedPattern)
                                    && !filteredLocalContacts.contains(item))
                                filteredLocalContacts.add(item);
                }
            }
           // Collections.sort(filteredLocalContacts, ParticipantAdapterItem.getComparator(mSession));
            mLocalContactsSection.setFilteredItems(filteredLocalContacts, pattern);
        } else {
            mLocalContactsSection.resetFilter();
        }
        return mLocalContactsSection.getFilteredItems().size();
    }

    /**
     * Filter the known contacts known by this account.
     *
     * @param pattern the pattern to search
     */
    public void filterAccountKnownContacts(final String pattern) {
        filterKnownContacts(pattern);
        updateSections();
        updateFirstLetters();
    }

    /**
     * Filter the known contacts with the given pattern
     *
     * @param pattern
     * @return nb of items matching the filter
     */
    private int filterKnownContacts(final String pattern) {
        List<ParticipantAdapterItem> filteredKnownContacts = new ArrayList<>();
        if (!TextUtils.isEmpty(pattern)) {
            final String formattedPattern = pattern.toLowerCase();
            List<ParticipantAdapterItem> sectionItems = new ArrayList<>(mKnownContactsSection.getItems());
            for (ParticipantAdapterItem item : sectionItems) {
                if (item.mDisplayName.toLowerCase().contains(formattedPattern)
                        || (item.mContact != null && item.mContact.getDisplayName().contains(formattedPattern)))
                    filteredKnownContacts.add(item);
                else {
                    if (item.mContact != null)
                        for (Contact.PhoneNumber number : item.mContact.getPhonenumbers())
                            if (number.mE164PhoneNumber.contains(formattedPattern)
                                    && !filteredKnownContacts.contains(item))
                                filteredKnownContacts.add(item);
                }
            }
        }

        // The sort is done in the adapter to save loading time
        // see PeopleFragment.initKnownContacts
          Collections.sort(filteredKnownContacts, ParticipantAdapterItem.getComparator(mSession));
          mKnownContactsSection.setFilteredItems(filteredKnownContacts, pattern);

        setKnownContactsLimited(false);
        setKnownContactsExtraTitle(null);

        return filteredKnownContacts.size();
    }

    /**
     * Remove the room of the given id from the adapter
     *
     * @param roomId
     */
    public void removeDirectChat(final String roomId) {
        Room room = mSession.getDataHandler().getRoom(roomId);
        if (mDirectChatsSection.removeItem(room)) {
            updateSections();
            //updateFirstLetters();
        }
    }

    /**
     * Generate list of big letters
     */
    private void updateFirstLetters() {
        List<Character> characters = new LinkedList<>();
        char lastFirstLetter = ' ';
        for (int i = 0; i < getItemCount(); i++) {
            Object abstractObject = getItemForPosition(i);
            if (!(abstractObject instanceof ParticipantAdapterItem)) {
                characters.add(' ');
                continue;
            }
            ParticipantAdapterItem item = (ParticipantAdapterItem) abstractObject;
            String currentName = item != null ? item.getUniqueDisplayName(null) : "";
            if (currentName.length() > 0 && lastFirstLetter != Character.toUpperCase(currentName.charAt(0))) {
                lastFirstLetter = Character.toUpperCase(currentName.charAt(0));
                if (lastFirstLetter == '@' && currentName.length() > 1)
                    lastFirstLetter = Character.toUpperCase(currentName.charAt(1));
                characters.add(!characters.contains(lastFirstLetter) && lastFirstLetter != '@' ? lastFirstLetter : ' ');
            } else characters.add(' ');
        }
        firstLetters.clear();
        firstLetters.addAll(characters);
    }



    /*
     * *********************************************************************************************
     * View holder
     * *********************************************************************************************
     */

    /**
     * Get the presence for the given contact
     *
     * @param availabilityDot
     * @param item
     * @param position
     */
    private void loadContactPresence(final ImageView availabilityDot, final ParticipantAdapterItem item,
                                     final int position) {
        availabilityDot.setVisibility(View.GONE);
        if (item.mUserId != null && !item.mUserId.isEmpty())
            if (item.mUserId.charAt(0) == '@') {
                final String presence = VectorUtils.getUserPresence(mContext, mSession, item.mUserId, new SimpleApiCallback<Void>() {
                    @Override
                    public void onSuccess(Void info) {
                        notifyItemChanged(position);
                    }
                });
                if (presence != null) {
                    if (presence.equals(User.PRESENCE_ONLINE)) {
                        availabilityDot.setImageDrawable(mContext.getResources().getDrawable(R.drawable.circle_available));
                        availabilityDot.setVisibility(View.VISIBLE);
                    } else if (presence.equals(User.PRESENCE_UNAVAILABLE)) {
                        availabilityDot.setImageDrawable(mContext.getResources().getDrawable(R.drawable.circle_do_not_distirb));
                    }
                }
            }
    }

    /**
     * Get the presence for the given contact
     *
     * @param availabilityDot
     * @param userId
     * @param position
     */
    private void loadContactPresence(final ImageView availabilityDot, String userId,
                                     final int position) {
        availabilityDot.setVisibility(View.GONE);
        if (userId != null)
            if (userId.charAt(0) == '@') {
                final String presence = VectorUtils.getUserPresence(mContext, mSession, userId, new SimpleApiCallback<Void>() {
                    @Override
                    public void onSuccess(Void info) {
                        notifyItemChanged(position);
                    }
                });
                if (presence != null) {
                    if (presence.equals(User.PRESENCE_ONLINE)) {
                        availabilityDot.setImageDrawable(mContext.getResources().getDrawable(R.drawable.circle_available));
                        availabilityDot.setVisibility(View.VISIBLE);
                    } else if (presence.equals(User.PRESENCE_UNAVAILABLE)) {
                        availabilityDot.setImageDrawable(mContext.getResources().getDrawable(R.drawable.circle_do_not_distirb));
                    }
                }
            }
    }

    public interface OnSelectItemListener {
        void onSelectItem(Room item, int position);

        void onSelectItem(ParticipantAdapterItem item, int position);
        void onSelectAvatar(ParticipantAdapterItem item, View view);

    }
    /*
     * *********************************************************************************************
     * Inner classes
     * *********************************************************************************************
     */

    class ContactViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.contact_avatar)
        CircleImageView vContactAvatar;

        @BindView(R.id.contact_flag_layout)
        FrameLayout frameFlagLayout;

        @BindView(R.id.contact_name)
        TextView vContactName;

        @BindView(R.id.first_big_letter)
        TextView vFirstBigLetter;

        @BindView(R.id.availability)
        ImageView vAvailability;

        private ContactViewHolder(final View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        private void populateViews(final ParticipantAdapterItem participant, final int position) {
            if (null == participant) {
                Log.e(LOG_TAG, "## populateViews() : null participant");
                return;
            }

            if (position >= getItemCount()) {
                Log.e(LOG_TAG, "## populateViews() : position out of bound " + position + " / " + getItemCount());
                return;
            }
            if (position >= firstLetters.size() || firstLetters.size() != getItemCount()) {
                updateFirstLetters();
            }
            VectorUtils.loadUserAvatar(mContext, mSession, vContactAvatar, mSession.getDataHandler().getUser(participant.mUserId));
            vContactAvatar.setOnClickListener(v -> {
                mListener.onSelectAvatar(participant, v);
            });
            vContactName.setText(participant.mDisplayName);
            if (firstLetters != null)
                vFirstBigLetter.setText(String.valueOf(firstLetters.get(position)));
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onSelectItem(participant, -1);
                }
            });
            loadContactPresence(vAvailability, participant, position);
        }
    }
}
