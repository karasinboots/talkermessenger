package com.talkermessenger.adapters.settings.viewHolder;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.iskar.talkersdk.model.TalkerContact;
import com.talkermessenger.Matrix;
import com.talkermessenger.R;
import com.talkermessenger.activity.VectorMemberDetailsActivity;
import com.talkermessenger.adapters.settings.callback.DeleteCallback;
import com.talkermessenger.util.VectorUtils;

import org.matrix.androidsdk.MXSession;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Nikita on 18.04.2018.
 */

public class BlockedViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.contact_avatar)
    CircleImageView avatar;
    @BindView(R.id.phone_number)
    TextView name;
    @BindView(R.id.root)
    LinearLayout root;

    public BlockedViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void setupContact(TalkerContact contact, Context context){
        MXSession session = Matrix.getInstance(context).getDefaultSession();
        VectorUtils.loadUserAvatar(
                context,
                session,
                avatar,
                session
                        .getDataHandler()
                        .getUser(contact.getMatrixId()));

        name.setText(contact.getDisplayName());
    }

    public void setupClickListener(DeleteCallback callback, TalkerContact contact, Context context){
        root.setOnClickListener(v -> {
            Intent startRoomInfoIntent = new Intent(context, VectorMemberDetailsActivity.class);
            startRoomInfoIntent.putExtra(VectorMemberDetailsActivity.EXTRA_LOOKUP_URI, contact.getLookupUri());
            startRoomInfoIntent.putExtra(VectorMemberDetailsActivity.EXTRA_MEMBER_DISPLAY_NAME, contact.getDisplayName());
            startRoomInfoIntent.putExtra(VectorMemberDetailsActivity.EXTRA_MEMBER_ID, contact.getMatrixId());
            startRoomInfoIntent.putExtra(VectorMemberDetailsActivity.EXTRA_MATRIX_ID, Matrix.getInstance(context).getDefaultSession().getCredentials().userId);
            context.startActivity(startRoomInfoIntent);
        });

        root.setOnLongClickListener(v -> {
            callback.onDeleted(contact);
            return true;
        });
    }

}
