package com.talkermessenger.adapters.settings.viewHolder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.talkermessenger.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PaymentsHistoryViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.date)
    public TextView date;
    @BindView(R.id.amount)
    public TextView amount;
    @BindView(R.id.method)
    public TextView method;
    @BindView(R.id.id)
    public TextView id;
    @BindView(R.id.at_chars)
    public TextView atChars;

    public PaymentsHistoryViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
