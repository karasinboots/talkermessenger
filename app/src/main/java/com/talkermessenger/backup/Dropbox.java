package com.talkermessenger.backup;


import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

import com.dropbox.core.DbxException;
import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.v2.DbxClientV2;
import com.dropbox.core.v2.files.FileMetadata;
import com.dropbox.core.v2.files.WriteMode;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;


public class Dropbox extends AsyncTask<String, Void, String> {
    private static DbxClientV2 client;
    private static final String ACCESS_TOKEN = "AgdDrXL1B_QAAAAAAABXYRBsG7LWSVgDDE18c_SeYygsr5iB_YXFBjWzOxQdRMPk";
    public DropboxAsyncResponse delegate = null;

    public Dropbox() {
    }


    public void init() throws DbxException {

        // Create Dropbox client
        DbxRequestConfig config = new DbxRequestConfig("talkerBackup", "en_US");
        if (client == null)
            client = new DbxClientV2(config, ACCESS_TOKEN);
        //Get files and folder metadata from Dropbox root directory
    }

    private String getFileName(String path) {
        if (null != path && path.length() > 0) {
            int endIndex = path.lastIndexOf("/");
            if (endIndex != -1) {
                return path.substring(endIndex + 1); // not forgot to put check if(endIndex != -1)
            }
        }
        return null;
    }


    @Override
    protected String doInBackground(String... strings) {
        if (strings[0] != null) return putToDropbox(strings[0]);
        else if (strings[1] != null) return getFromDropbox(strings[1]);
        else return null;
    }

    private String putToDropbox(String string) {
        try {
            InputStream in = new FileInputStream(new File(string));
            String path = "backup/" + getFileName(string);
            {
                FileMetadata metadata = client.files().uploadBuilder("/backup/" + getFileName(string))
                        .withMode(WriteMode.OVERWRITE)
                        .uploadAndFinish(in);
                Log.e("metadata", metadata.getName());
                return path;
            }
        } catch (DbxException | IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private String getFromDropbox(String userId) {
        Log.e("getfrom dropbpx part", userId);
        try {
            String filename = userId + ".txt";
            String dropPath = "/backup/" + filename;
            String path = Environment.getExternalStorageDirectory() + "/talkerBackup/";
            InputStream is = client.files().download(dropPath).getInputStream();
            File file = new File(path);
            file.mkdirs();
            String fullPath = file.getPath() + "/" + filename;
            OutputStream output = new FileOutputStream(fullPath);
            byte[] buffer = new byte[4 * 1024];
            int read;
            while ((read = is.read(buffer)) != -1) {
                output.write(buffer, 0, read);
            }
            output.flush();
            output.close();
            return path;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    protected void onPostExecute(String path) {
        delegate.processFinish(path);
    }
}

